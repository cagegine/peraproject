/**
 * 
 */
package test.model.exporters;

import static org.junit.Assert.fail;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.exporters.ExporterPDF;

import java.io.IOException;
import java.net.MalformedURLException;

import junit.framework.Assert;

import org.junit.Test;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;

/**
 * @author Davide Cabbia
 *
 */
public class ExporterPDFTest {

	@Test
	public void test() {
		try {
			ExporterPDF exp= new ExporterPDF("file_esempio\\asd.pdf","test","test","test","test","test",new String[5]);
			ExporterPDF exp1= new ExporterPDF("file_esempio\\asd.pdf","test","test","test","","",new String[5]);
			ExporterPDF exp2= new ExporterPDF("file_esempio\\asd.pdf","test","test","","","",new String[5]);
			ExporterPDF exp3= new ExporterPDF("file_esempio\\asd.pdf","test","","","","",new String[5]);
			ExporterPDF exp4= new ExporterPDF("file_esempio\\asd.pdf","","","","","",new String[5]);
		    Image img=null;
			try {
				img = Image.getInstance("img/q.png");
			} catch (BadElementException e) {
				throw new ErrorExc("BadElementException", e.getMessage());
			} catch (MalformedURLException e) {
				throw new ErrorExc("MalformedURLException", e.getMessage());
			} catch (IOException e) {
				throw new ErrorExc("IOException", e.getMessage());
			}
		    exp.addImage(img);
		    exp.addImage(img);
		    exp.addImage(img);
			exp.saveFile();
		    exp1.addImage(img);
		    exp1.addImage(img);
		    exp1.addImage(img);
		    exp1.addImage(img);
			exp1.saveFile();
			
		} catch (ErrorExc e) {e.printStackTrace();}
		//fail("Not yet implemented");
	}
	
	@Test
	public void test_noFilePath() {
		try {
			ExporterPDF exp= new ExporterPDF("","","","","","",new String[5]);
			fail("WTF");
			} catch (ErrorExc e) {
				Assert.assertEquals("Impossible to save with this path", e.getKey());
			}
		}
}
