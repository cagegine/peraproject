/**
//////////////////////////////////
SakeVoxelObjTest. java
This is the test class for SakeVoxelObj
test.model.manage.objects
GPLv3
Francesco Rosso
TetraTron Group
2012.03.08
Coverage: 94,3%
//////////////////////////////////
 */
package test.model.objects;

import static org.junit.Assert.fail;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.algorithms.ImageIterativeFiltering;
import it.tetratron.pera.model.algorithms.ImageRankShaping;
import it.tetratron.pera.model.algorithms.ImageStandard;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.ObjInterface;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.SakeVoxelObj;
import it.tetratron.pera.model.objects.VoxelObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import it.tetratron.pera.model.params.RSSAParam;
import it.tetratron.pera.model.params.SAIFParam;
import it.tetratron.pera.model.params.StdSAParam;

import java.io.File;
import java.util.Vector;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class SakeVoxelObjTest {
    private static Vector<ObjInterface> testArray=new Vector<ObjInterface>();
    
    @AfterClass
    public static void afterExecution(){
	
	File toDelete = new File("testFiles\\resultVoxelObj_IStd_execution.sake");
	toDelete.delete();
	toDelete = new File("testFiles\\resultVoxelObj_RS_notValid.sake");
	toDelete.delete();
	toDelete = new File("testFiles\\resultVoxelObj_RS_execution.sake");
	toDelete.delete();
	
    }
    
    @BeforeClass
    public static void beforeExecution() throws ErrorExc{
	ArterialObj ref_art=new ArterialObj();
	ref_art.load("casoVeloce\\RolipramArteriale.txt");
	RegionObj ref_reg=new RegionObj();
	ref_reg.load("casoVeloce\\RolipramPETscan.txt");
	VoxelObj ref_vox=new VoxelObjAdapter("casoVeloce\\RolipramTest_slice10","casoVeloce\\RolipramTest_maschera_slice10");

	testArray.add(ref_art);
	testArray.add(ref_vox);
	testArray.add(ref_reg);	
    }

    @Test
    public void enums(){
	SakeVoxelObj.VoxelObjs.values();
	SakeVoxelObj.VoxelObjs.valueOf("mapVt");
	SakeVoxelObj.VoxelObjs.valueOf("mapKi");
	SakeVoxelObj.VoxelObjs.valueOf("mapVb");
    }
    
    /*test leggo file non vuoto, non valido*/
    @Test
    public void test_load_not_correct(){
	SakeVoxelObj x=new SakeVoxelObj();
	try {
	    x.load("file_esempio/file_casual");
	} catch (ErrorExc e) {
	    Assert.assertTrue(true);
	}
    }

    /*test leggo file vuoto*/
    @Test
    public void test_load_void(){
	SakeVoxelObj x=new SakeVoxelObj();
	try {
	    x.load("file_esempio/file_void");
	} catch (ErrorExc e) {
	    Assert.assertTrue(true);
	}
    }
    
    @Test
    public void testGetSet() throws ErrorExc {
	SakeVoxelObj sake = new SakeVoxelObj();
	try {
	    sake.load("testFiles\\resultVoxelObj_RS");
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	//mapKi
	sake.setCurrentObj(SakeVoxelObj.VoxelObjs.mapKi);
	sake.getCurrentObj();
	sake.getCurrentDynPet();
	sake.getMaxT();
	sake.getMaxX();
	sake.getMaxY();
	sake.getMaxZ();
	//mapVt
	sake.setCurrentObj(SakeVoxelObj.VoxelObjs.mapVt);
	sake.getCurrentObj();
	sake.getCurrentDynPet();
	//mapVb
	sake.setCurrentObj(SakeVoxelObj.VoxelObjs.mapVb);
	sake.getCurrentObj();
	sake.getCurrentDynPet();

	//setters
	sake.setFit(null);
	try{
	    sake.getFit();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModFitNotSet", e.getKey());
	}

	sake.setMapKi(null);
	try{
	    sake.getMapKi();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapKiNotSet", e.getKey());
	}

	sake.setMapVt(null);
	try{
	    sake.getMapVt();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapVtNotSet", e.getKey());
	}

	sake.setMapVb(null);
	try{
	    sake.getMapVb();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapVbNotSet", e.getKey());
	}

	sake.setMapOrder(null);
	try{
	    sake.getMapOrder();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapOrderNotSet", e.getKey());
	}

	sake.load();
	sake.setRawVoxel(null);
	try{
	    sake.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModRawVoxelNotSet", e.getKey());
	}
	
	sake.load();
	sake.setFit(null);
	try{
	    sake.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModFitNotSet", e.getKey());
	}

	sake.load();
	sake.setMapOrder(null);
	try{
	    sake.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapOrderNotSet", e.getKey());
	}

	sake.load();
	sake.setMapVb(null);
	try{
	    sake.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapVbNotSet", e.getKey());
	}

	sake.load();
	sake.setMapKi(null);
	try{
	    sake.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapKiNotSet", e.getKey());
	}

	sake.load();
	sake.setMapVt(null);
	try{
	    sake.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModMapVtNotSet", e.getKey());
	}
    }

    @Test
    public void testLoad() {
	SakeVoxelObj sake = new SakeVoxelObj();
	try {
	    sake.load("testFiles\\resultVoxelObj_RS_notValid");
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModSakeNotValid", e.getKey());
	}
	sake.setFilename("testFiles\\resultVoxelObj_RS_noFit");
	try {
	    sake.load();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFileNotPresent", e.getKey());
	}
	sake.setFilename("testFiles\\resultVoxelObj_RS");
	try {
	    sake.load();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void testLoadLogs(){
	SakeVoxelObj sake = new SakeVoxelObj();
	try {
	    sake.load("testFiles\\resultVoxelObj_RS");
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	
	try {
	    sake.getRegionPathFromLog();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
	try {
	    sake.getArterialPathFromLog();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
	try {
	    sake.getRawVoxelPathFromLog();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
	try {
	    sake.getRawVoxelMaskPathFromLog();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
    }

    @Test
    public void testSaveIIF(){
	ImageIterativeFiltering IIFTest;
	try {
	    IIFTest=new ImageIterativeFiltering();	

	    SakeVoxelObj aux = (SakeVoxelObj) IIFTest.run(new SAIFParam("C:\\temp", (double)0.12,(double)12,100), testArray);

	    aux.save("testFiles\\resultVoxelObj_IIF_execution");
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
    }

    @Test
    public void testSaveIRSTest() throws ErrorExc {
	ImageRankShaping IRSTest = null;
	IRSTest=new ImageRankShaping();
	RSSAParam params = new RSSAParam("C:\\temp", (double)0.001, (double)1.0,100);


	SakeVoxelObj aux = (SakeVoxelObj) IRSTest.run(params, testArray);

	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}

	try{
	    aux.save("testFiles\\resultVoxelObj_RS_execution");
	}catch(ErrorExc e){
	    fail("WTF "+e.getKey());
	}

	File f = new File(aux.getFilename());
	f.deleteOnExit();


	//check execution params
	aux.setArterialObj(null);
	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModExecutionParametersNotSet", e.getKey());
	}
	aux.setRegionObj(null);
	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModExecutionParametersNotSet", e.getKey());
	}
	aux.setParamInterface(null);
	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModExecutionParametersNotSet", e.getKey());
	}
	//end check execution params

	/*
		File toDelete = new File("testFiles\\resultVoxelObj_RS_execution.sake");
		toDelete.delete();
		toDelete = new File("testFiles\\resultVoxelObj_IIF_execution.sake");
		toDelete.delete();
		toDelete = new File("testFiles\\resultVoxelObj_IStd_execution.sake");
		toDelete.delete();
	 */
    }
    @Test
    public void testSaveIStdTest(){
	ImageStandard IStdTest = null;
	try {
	    IStdTest = new ImageStandard();

	    StdSAParam par=new StdSAParam(("C:\\temp"), (double)0.12,(double)12,1,100);

	    par.setNComp(100);
	    SakeVoxelObj aux = (SakeVoxelObj) IStdTest.run(par, testArray);
	    aux.save("testFiles\\resultVoxelObj_IStd_execution");
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
    }
    
    @Test
    public void testSetGetFilename(){
	SakeVoxelObj sake = new SakeVoxelObj();
	sake.setFilename("testFiles\\resultVoxelObj_RS_notValid");
	sake.getFilename();
    }

}
