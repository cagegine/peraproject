/**
//////////////////////////////////
SakeRegionObjTest. java
This is the test class for SakeRegionObj
test.model.manage.objects
GPLv3
Francesco Rosso
TetraTron Group
2012.03.09
Coverage: 92.2%
//////////////////////////////////
 */
package test.model.objects;

import static org.junit.Assert.fail;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.algorithms.RegionIterativeFiltering;
import it.tetratron.pera.model.algorithms.RegionRankShaping;
import it.tetratron.pera.model.algorithms.RegionStandard;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.ObjInterface;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.SakeRegionObj;
import it.tetratron.pera.model.objects.VoxelObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import it.tetratron.pera.model.params.RSSAParam;
import it.tetratron.pera.model.params.SAIFParam;
import it.tetratron.pera.model.params.StdSAParam;

import java.io.File;
import java.util.Vector;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class SakeRegionObjTest {
    private static Vector<ObjInterface> testArray=new Vector<ObjInterface>();

    @AfterClass
    public static void afterExecution(){
	File toDelete = new File("testFiles\\resultRegionObj_RIIF_execution.sake");
	toDelete.delete();
	toDelete = new File("testFiles\\resultRegionObj_RStd_execution.sake");
	toDelete.delete();
	toDelete = new File("testFiles\\resultRegionObj_RS_execution.sake");
	toDelete.delete();
    }

    @BeforeClass
    public static void beforeExecution() throws ErrorExc{
	ArterialObj ref_art=new ArterialObj();
	ref_art.load("casoVeloce\\RolipramArteriale.txt");
	RegionObj ref_reg=new RegionObj();
	ref_reg.load("casoVeloce\\RolipramPETscan.txt");
	VoxelObj ref_vox=new VoxelObjAdapter("casoVeloce\\RolipramTest_slice10","casoVeloce\\RolipramTest_maschera_slice10");

	testArray.add(ref_art);
	testArray.add(ref_vox);
	testArray.add(ref_reg);	
    }

    /*test leggo file non vuoto, non valido*/
    @Test
    public void test_load_not_correct(){
	SakeRegionObj x=new SakeRegionObj();
	try {
	    x.load("file_esempio/file_casual");
	} catch (ErrorExc e) {
	    Assert.assertTrue(true);
	}
    }

    /*test leggo file vuoto*/
    @Test
    public void test_load_void(){
	SakeRegionObj x=new SakeRegionObj();
	try {
	    x.load("file_esempio/file_void");
	} catch (ErrorExc e) {
	    Assert.assertTrue(true);
	}
    }

    @Test
    public void testGetFilename() {
	SakeRegionObj aux = new SakeRegionObj();

	try {
	    aux.getFilename();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}

	aux.setFilename("testFiles\\resultRegionObj_RS");

	try {
	    aux.getFilename();
	} catch (ErrorExc e) {
	    fail("WTF");
	}

    }

    @Test
    public void testGetSet() {
	SakeRegionObj sake = new SakeRegionObj();
	try {
	    sake.load("testFiles\\resultRegionObj_RS");
	} catch (ErrorExc e) {
	    fail("WTF");
	}

	try{
	    sake.getAlfa();
	    sake.getBeta();
	    sake.getRegionObj();
	    sake.getArterialObj();
	    sake.getFit();
	    sake.getParameters();
	} catch (ErrorExc e) {
	    fail("WTF");
	}

	try {
	    sake.getParametersContent();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	try {
	    sake.getFitContent();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	sake.setParameters(null);
	try{
	    sake.getParameters();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModParametersNotSet", e.getKey());
	}

	sake.setAlfa(null);
	try{
	    sake.getAlfa();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModAlfaNotSet", e.getKey());
	}

	sake.setBeta(null);
	try{
	    sake.getBeta();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModBetaNotSet", e.getKey());
	}

	sake.setFit(null);
	try{
	    sake.getFit();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModFitNotSet", e.getKey());
	}

	sake.setRegionObj(null);
	try{
	    sake.getRegionObj();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModRegionObjNotSet", e.getKey());
	}

	sake.setArterialObj(null);
	try{
	    sake.getArterialObj();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModArterialObjNotSet", e.getKey());
	}
    }
    
    @Test
    public void testLoadLogs(){
	SakeRegionObj sake = new SakeRegionObj();
	try {
	    sake.load("testFiles\\resultRegionObj_RS");
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	
	try {
	    sake.getRegionPathFromLog();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
	try {
	    sake.getArterialPathFromLog();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
	
    }
    
    @Test
    public void testLoadString() {
	SakeRegionObj sake = new SakeRegionObj();
	try {
	    sake.load("testFiles\\resultRegionObj_RS_notValid.sake");
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModSakeNotValid", e.getKey());
	}
	try {
	    sake.load("testFiles\\resultRegionObj_RS_noFit");
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFileNotPresent", e.getKey());
	}
	try {
	    sake.load("testFiles\\resultRegionObj_RS");
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }
    
    @Test
    public void testSaveIF() throws ErrorExc{
	RegionIterativeFiltering RIIFTest = new RegionIterativeFiltering();
	SakeRegionObj aux = (SakeRegionObj) RIIFTest.run(new SAIFParam("C:\\temp", (double)0.12,(double)12,100), testArray);
	try{
	    aux.save("testFiles\\resultRegionObj_RIIF_execution");
	}catch(ErrorExc e){
	    fail("WTF "+e.getKey());
	}
    }

    @Test
    public void testSaveRRS() throws ErrorExc{
	RegionRankShaping RRSTest = new RegionRankShaping();
	SakeRegionObj aux = (SakeRegionObj) RRSTest.run(new RSSAParam("C:\\temp", (double)0.001,(double)1.2,100), testArray);

	try {
	    aux.getParametersContent();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	try {
	    aux.getFitContent();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}

	try{
	    aux.save("testFiles\\resultRegionObj_RS_execution");
	}catch(ErrorExc e){
	    fail("WTF "+e.getKey());
	}

	aux.setArterialObj(null);
	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModArterialObjNotSet", e.getKey());
	}

	aux.setRegionObj(null);
	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModRegionObjNotSet", e.getKey());
	}

	aux.setParamInterface(null);
	try{
	    aux.save();
	    fail("WTF");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModExecutionParametersNotSet", e.getKey());
	}

    }
    
    @Test
    public void testSaveRStd() throws ErrorExc{
	RegionStandard RSTest = new RegionStandard();
	SakeRegionObj aux = (SakeRegionObj) RSTest.run(new StdSAParam("C:\\temp", (double)0.12,(double)12,1,100), testArray);
	try{
	    aux.save("testFiles\\resultRegionObj_RStd_execution");
	}catch(ErrorExc e){
	    fail("WTF "+e.getKey());
	}

    }
    
}
