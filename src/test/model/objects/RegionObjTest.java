package test.model.objects;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.Memento;
import it.tetratron.pera.model.objects.RegionObj;

import java.util.List;
import java.util.Vector;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class RegionObjTest {
	RegionObj x;
	
	@Before
	@Test
	public void test_init() throws ErrorExc{
		x= new RegionObj();
	}
	
	@Test (expected=ErrorExc.class)
	public void test_fail() throws ErrorExc{
		x.load("file_esempio/fileProvaRegion_error");
	}

	@Test
	public void test_memento() throws ErrorExc {
		x.load("file_esempio/LeucinaPETScan.txt");
		List<Double> lol=x.getNsd();
		Memento y= x.createMemento();
		RegionObj ro= new RegionObj();
		ro.load("file_esempio/WayPETscan.txt");
		String[] roi_labels= ro.getRoiLabel();
		Vector<Double> ro_tp= (Vector<Double>) ro.getTime_pet();
		Vector<Double> ro_de= (Vector<Double>) ro.getDelta();
		Vector<Double> ro_nsd= (Vector<Double>) ro.getNsd();
		List<List<Double>> roroi= ro.getRoi();
		Double[][] roi= ro.getRoiArray();
		double[][] newroi= new double[roroi.get(0).size()][roroi.size()];
		x.setTime_pet(ro_tp);
		x.setDelta(ro_de);
		x.setNsd(ro_nsd);
		x.setRoi(newroi);
    	y.restoreState();
    	List<Double> lol2= x.getNsd();
    	Assert.assertEquals(lol==lol2 && roroi.size()==roi[0].length && roi_labels.length == roi[0].length, true);
	}
	
	@Test
	public void save() throws ErrorExc{
		test_memento();
		x.save("file_esempio/testRegion.txt");
	}
	
	@Test
	public void test_labels() throws ErrorExc{
		x.load("file_esempio/WayPETscan.txt");
		String[] labels=new String[4];
		labels[0]= "pet_time";
		labels[1]= "atled";
		labels[2]= "dsn";
		labels[3]= "roio";
		x.setLabels(labels);
		String name= x.getFilename();
		name+="Labels.txt";
		x.save(name);
	}
	
	@Test
	public void test_default_labels(){
		String[] allabels= x.getLabels();
		String timepl= x.getTimeLabel();
		String deltal=x.getDeltaLabel();
		String nsdl= x.getNSDLabel();
		Assert.assertEquals(allabels.length==3&&timepl=="time_pet"&&deltal=="delta"&&nsdl=="nsd", true);
	}
	
	@Test (expected=ErrorExc.class)
	public void test_label_roi_null() throws ErrorExc{
		x.getRoiLabel();
	}
	
	/*NUOVI TEST*/
	/*test leggo file vuoto*/
	@Test
	public void test_load_void(){
		try {
			x.load("file_esempio/file_void.txt");
		} catch (ErrorExc e) {
			Assert.assertTrue(true);
		}
	}
	/*test leggo file non vuoto, non valido*/
	@Test
	public void test_load_not_correct(){
		try {
			x.load("file_esempio/file_casual.txt");
		} catch (ErrorExc e) {
			Assert.assertTrue(true);
		}
	}
	/*test leggo e poi salvo*/
	@Test
	public void test_load_save(){
		try {
			x.load("file_esempio/RolipramPETscan.txt");
		} catch (ErrorExc e) {
			Assert.fail(e.getKey());
		}
		try {
			x.save("file_esempio/from_RegionObj_test_load_save");
		} catch (ErrorExc e) {
			Assert.fail(e.getKey());
		}
	}
}
