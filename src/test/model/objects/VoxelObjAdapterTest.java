package test.model.objects;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.Memento;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.SlashVector;
import it.tetratron.pera.model.objects.VoxelObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import Sake_java.quantification;


public class VoxelObjAdapterTest {
	VoxelObj x;
	
	@Before
	@Test
	public void test_init(){
		x= new VoxelObjAdapter();
	}

	@Test
	public void test_save() throws Exception {
		x.load("file_esempio/TestWay", "file_esempio/TestWay_maschera");
		double[][][][] test= x.getDynPet();
		double lol= test[test.length/2][test[0].length/2][test[0][0].length/2][test[0][0][0].length/2];
		x.save("file_esempio/copia");
		double[][][][] test2= x.getDynPet();
		double xlol= test2[test2.length/2][test2[0].length/2][test2[0][0].length/2][test2[0][0][0].length/2];
		Assert.assertEquals(lol, xlol);
		double[][][] test3= x.getDynPet((short)0);
		Assert.assertEquals(test3.length==x.getX(), true);
	}
	
	@Test ()
	public void test_memento() throws ErrorExc, Exception{
		x.load("file_esempio/RolipramTest_slice10","file_esempio/RolipramTest_maschera_slice10");
		//x.load("file_esempio/TestLeucina");
		double[][][][] test= x.getDynPet();
		double lol= test[test.length/2][test[0].length/2][test[0][0].length/2][test[0][0][0].length/2];
		Memento stm= ((VoxelObjAdapter)x).createMemento();
		double[][][][] fail= new double[128][128][95][23];
		x.setDynPet(fail);
		double[][][] fake= new double[128][128][128];
		x.setDynPet(fake);
		double[][] fake2= new double[128][128];
		x.getAdaptee().ZDIM=0;
		x.setDynPet(fake2);	//questo dovrebbe dare errore*/
		x.save("file_esempio/sballiato");
		stm.restoreState();
		double[][][][] test2= x.getDynPet();
		double lol2= test2[test2.length/2][test2[0].length/2][test2[0][0].length/2][test2[0][0][0].length/2];
		x.save("file_esempio/dopoMeme");
		Assert.assertEquals(lol, lol2);
	}
	
	@Test (expected=ErrorExc.class)
	public void test_fail() throws ErrorExc{
		x.load("file_esempio/testTumor");
	}
	
	@Test
	public void test_mask_save() throws ErrorExc{
		x.load("file_esempio/RolipramTest_slice10","file_esempio/RolipramTest_maschera_slice10");
		x.save("file_esempio/Slicepost","file_esempio/Slicepost_maschera");
	}
	
	@Test
	public void test_dim() throws ErrorExc{
		x.load("file_esempio/RolipramTest_slice10","file_esempio/RolipramTest_maschera_slice10");
		int ics= x.getX();
		int y= x.getY();
		int z= x.getZ();
		int t= x.getT();
		int zzz= (int) x.getValue(0, 0, 0, (short)0);
		boolean mega= (ics==128 && y==128 && z==1 && t==0 && zzz==0);
		Assert.assertEquals(mega, false);
		double[] voxdims= ((VoxelObjAdapter)x).getVoxDims();
		Assert.assertEquals(voxdims!=null, true);
	}
	
	@Test (expected=ErrorExc.class)
	public void test_inutile() throws ErrorExc{
		x= new VoxelObjAdapter("file_esempio/TestWay","file_esempio/TestWay_maschera");
		VoxelObjAdapter y= new VoxelObjAdapter("file_esempio/TestWay");
		boolean test= x.getDynPet()==y.getDynPet();
		boolean test_mask= x.getDynMask()==y.getDynMask(); //y non ha maschera, dovrebbe dare errore!
	}
	@Test
	public void copyMH() throws ErrorExc{
		x.load("file_esempio/RolipramTest_slice10","file_esempio/RolipramTest_maschera_slice10");
		int ics=x.getX();
		int y=x.getY();
		int z= x.getZ();
		x.copyMaskHeader(x);
		Assert.assertEquals(ics==x.getX()&&y==x.getY()&&z==x.getZ(), true);
	}
	
	/*NUOVI TEST*/
	/*test leggo file vuoto*/
	@Test
	public void test_load_void(){
		try {
			x.load("file_esempio/file_void");
		} catch (ErrorExc e) {
			Assert.assertTrue(true);
		}
	}
	/*test leggo file non vuoto, non valido*/
	@Test
	public void test_load_not_correct(){
		try {
			x.load("file_esempio/file_casual");
		} catch (ErrorExc e) {
			Assert.assertTrue(true);
		}
	}
	/*test leggo e poi salvo*/
	@Test
	public void test_load_save(){
		try {
			x.load("file_esempio/RolipramTest_slice10","file_esempio/RolipramTest_maschera_slice10");
		} catch (ErrorExc e) {
			Assert.fail(e.getKey());
		}
		try {
			x.save("file_esempio/from_VoxelObj_test_load_save");
		} catch (ErrorExc e) {
			Assert.fail(e.getKey());
		}
	}
}
