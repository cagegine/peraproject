package test.model.objects;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.Memento;

import java.util.List;
import java.util.Vector;

import junit.framework.Assert;

import org.junit.Test;

public class ArterialObjTest {
	ArterialObj x=new ArterialObj();
	
	//@Before
	@Test
	public void test_init() throws ErrorExc{
		x= new ArterialObj();
	}
	
	@Test (expected=ErrorExc.class)
	public void test_fail() throws ErrorExc{
		x.load("file_esempio/fileProva_error");
	}

	@Test
	public void test_memento() throws ErrorExc {
		//x.load("file_esempio/fileProva.txt");
		x.load("file_esempio/nuovoArteriale.txt");
		ArterialObj ra= new ArterialObj();
		ra.load("file_esempio/RolipramArteriale.txt");
		List<Double> lol=x.getBlood();
		Memento y= x.createMemento();
		Vector<Double> ra_tp= (Vector<Double>) ra.getTime_plasma();
		Vector<Double> ra_pl= (Vector<Double>) ra.getPlasma();
		Vector<Double> ra_bl= (Vector<Double>) ra.getBlood();
    	x.setTime_plasma(ra_tp);
    	x.setPlasma(ra_pl);
    	x.setBlood(ra_bl);
    	double[][] tp= new double[ra.getTime_plasma().size()][1];
    	double[][] p= new double[ra.getPlasma().size()][1];
    	double[][] b= new double[ra.getBlood().size()][1];
    	x.setTime_plasma(tp);
    	x.setPlasma(p);
    	x.setBlood(b);
    	y.restoreState();
    	List<Double> lol2= x.getBlood();
    	Assert.assertEquals(lol, lol2);
	}
	
	@Test
	public void save() throws ErrorExc{
		test_memento();
		x.save("file_esempio/testArt.txt");
	}
	
	@Test
	public void test_labels() throws ErrorExc{
		x.load("file_esempio/testArt.txt");
		String[] labels=new String[3];
		labels[0]= "plasma_time";
		labels[1]= "amsalp";
		labels[2]= "doolb";
		x.setLabels(labels);
		String name= x.getFilename();
		name+="Labels.txt";
		x.save(name);
	}

	@Test
	public void test_default_labels(){
		String[] allabels= x.getLabels();
		String timel= x.getTimeLabel();
		String plasmal=x.getPlasmaLabel();
		String bloodl= x.getBloodLabel();
		Assert.assertEquals(allabels.length==3&&timel=="time_plasma"&&plasmal=="plasma"&&bloodl=="blood", true);
	}

	/*NUOVI TEST*/
	/*test leggo file vuoto*/
	@Test
	public void test_load_void(){
		try {
			x.load("file_esempio/file_void.txt");
		} catch (ErrorExc e) {
			Assert.assertTrue(true);
		}
	}
	/*test leggo file non vuoto, non valido*/
	@Test
	public void test_load_not_correct(){
		try {
			x.load("file_esempio/file_casual.txt");
		} catch (ErrorExc e) {
			Assert.assertTrue(true);
		}
	}
	/*test leggo e poi salvo*/
	@Test
	public void test_load_save(){
		try {
			x.load("file_esempio/RolipramArteriale.txt");
		} catch (ErrorExc e) {
			Assert.fail(e.getKey());
		}
		try {
			x.save("file_esempio/from_ArterialObj_test_load_save");
		} catch (ErrorExc e) {
			Assert.fail(e.getKey());
		}
	}
}
