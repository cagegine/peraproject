package test.model.objects;

import static org.junit.Assert.*;
import it.tetratron.pera.model.objects.Sake;

import org.junit.Test;

public class SakeTest {

    @Test   
    public void testGetTempDir() {   
	//Assert.assertEquals(System.getProperty("java.io.tmpdir"), Sake.getTempDir());
	String x=Sake.getTempDir();
	assertTrue((x.endsWith("/") || x.endsWith("\\") ));  
    }

}