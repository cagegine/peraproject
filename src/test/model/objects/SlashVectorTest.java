package test.model.objects;

import junit.framework.Assert;
import it.tetratron.pera.model.objects.SlashVector;
import org.junit.Test;

public class SlashVectorTest {
	
	@Test
	public void test_horizontalA(){
		double[][] a= new double[3][1];
		a[0][0]= 0.0;
		a[1][0]= 1.0;
		a[2][0]= 0.0;
		SlashVector asv= new SlashVector();
		asv= SlashVector.toHorizontal(a);
		Assert.assertEquals(asv.size(), 3);
	}
	
	@Test
	public void test_horizontalB(){
		double[][] b= new double[1][3];
		b[0][0]= 0.0;
		b[0][1]= 1.0;
		b[0][2]= 0.0;
		SlashVector bsv= new SlashVector(3);
		bsv= SlashVector.toHorizontal(b);
		Assert.assertEquals(bsv.size(), 3);
	}

}
