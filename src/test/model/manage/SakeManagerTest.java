/**
//////////////////////////////////
SakeManagerTest. java
This is the test class for SakeManager
test.model.manage
GPLv3
Francesco Rosso
TetraTron Group
2012.03.05
Coverage: 92.4%
//////////////////////////////////
 */
package test.model.manage;

import static org.junit.Assert.fail;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.SakeManager;
import it.tetratron.pera.model.objects.ArterialObj;

import java.io.File;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SakeManagerTest {

    @Test
    public void typeTest(){
	File fileSR = new File("testFiles\\test.sake");
	SakeManager smR = new SakeManager();
	smR.setFile(fileSR.getPath());

	smR = new SakeManager(fileSR.getPath());

	File fileSV = new File("testFiles\\test.sake");
	SakeManager smV = new SakeManager();
	smV.setFile(fileSV.getPath());

	//set
	try {
	    smR.setType(SakeManager.Type.Region);
	    smV.setType(SakeManager.Type.Voxel);
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	//get
	try {
	    smR.getType();
	    smV.getType();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	//getTypeNotSet
	fileSR = new File("testFiles\\fileNotExistent.sake");
	smR = new SakeManager();
	smR.setFile(fileSR.getPath());
	try {
	    smR.getType();
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModTypeNotDefined", e.getKey());
	}
	
	try {
	    SakeManager.getType("testFiles\\test.sake");
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void addNonExixstent(){
	File fileS = new File("testFiles\\test.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    sm.addFile(new File("testFiles\\nonExistent.txt"));
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModImpossibleToStoreFile", e.getKey());
	}

    }

    @Before
    @Test
    public void addFile(){
	File fileS = new File("testFiles\\test.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    sm.addFile(new File("testFiles\\Existent.txt"));
	} catch (ErrorExc e) {
	    fail("WTF");
	}

	//without overwrite
	try {
	    sm.addFile(new File("testFiles\\Existent.txt"), false);
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModImpossibleToStoreFile", e.getKey());
	}

    }

    @Test
    public void addFileOverwrite() throws ErrorExc{
	File fileS = new File("testFiles\\test.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	sm.addFile(new File("testFiles\\Existent.txt"), true);
    }

    @Test
    public void getFile(){
	File fileS = new File("testFiles\\test.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	File deleteMe;
	try {
	    deleteMe = sm.getFile("Existent.txt");
	    deleteMe.delete();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void getNonExistentFile(){
	File fileS = new File("testFiles\\test.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    sm.getFile("nonExistent.txt");
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFileNotPresent", e.getKey());
	}
    }

    @After
    @Test
    public void removeFile(){
	File fileS = new File("testFiles\\test.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    sm.addFile(new File("testFiles\\Existent.txt"), true);
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
	try {
	    sm.removeFile("Existent.txt");
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void removeNonExistentFile(){
	File fileS = new File("testFiles\\test.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    sm.removeFile("nonExistent.txt");
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModImpossibleToRemoveFile", e.getKey());
	}
    }

    @Test
    public void getFileameTest(){
	File fileS = new File("testFiles\\fileNotExistent.sake");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    sm.getFilename();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFileNotPresent", e.getKey());
	}
	fileS = new File("testFiles\\test.sake");
	sm.setFile(fileS.getPath());
	try {
	    sm.getFilename();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void withoutExtension(){
	File fileS = new File("testFiles\\test");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
    }

    @Test
    public void testIsPresent(){
	File fileS = new File("testFiles\\test");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    if(sm.isFilePresent("nonExistent.asd")){
		fail("WTF");
	    }
	} catch (ErrorExc e) {
	    fail("WTF");
	}

	File fileS2 = new File("testFiles\\emptyFile.sake");
	sm.setFile(fileS2.getPath());
	try{
	    sm.isFilePresent("Cicciopanino.lololololol");
	}catch(ErrorExc e){
	    Assert.assertEquals("EModFileSakeNotPresent", e.getKey());
	}

    }
    
    /*
    @Test
    public void objects(){
	File fileS = new File("testFiles\\test");
	SakeManager sm = new SakeManager();
	sm.setFile(fileS.getPath());
	try {
	    ArterialObj ob = (ArterialObj) sm.getObj("arteriale");
	    sm.addObj(ob, "arteriale", true);
	    sm.removeObj("arteriale");
	    sm.addObj(ob, "arteriale");
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}
	
	try{
	    sm.addObj(null, null);
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModInvalidArgument", e.getKey());
	}
	
	try{
	    sm.addObj(null, "", true);
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModInvalidArgument", e.getKey());
	}
	
    }
    */
    
    @Test
    public void enums(){
	SakeManager.Type.values();
	SakeManager.Type.valueOf("Voxel");
	SakeManager.Type.valueOf("Region");
    }
}
