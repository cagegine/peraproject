package test.model.manage;

import static org.junit.Assert.assertEquals;
import junit.framework.Assert;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.ObjectManager;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.SakeRegionObj;
import it.tetratron.pera.model.objects.SakeVoxelObj;
import it.tetratron.pera.model.objects.VoxelObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;

import it.tetratron.pera.model.params.RSSAParam;
import it.tetratron.pera.model.params.StdSAParam;
import org.junit.Test;

public class ObjectManagerTest {

	static ObjectManager ref=new ObjectManager();
		
	@Test
	public void test_load() throws ErrorExc {
		ref.loadArterial("casoVeloce\\RolipramArteriale.txt");
		
		
		ref.loadRegion("casoVeloce\\RolipramPETscan.txt");
		
		ref.loadVoxel("casoVeloce\\RolipramTest_slice10","casoVeloce\\RolipramTest_maschera_slice10");
		
		ref.loadSakeVoxel("file_esempio/lollaiduro");
		
		ref.loadSakeRegion("file_esempio/lollaiduro_reg");
		
	}
	
	@Test
	public void test_save() throws ErrorExc {
		test_load();
		ref.saveArterial("file_esempio/prova_objMan_arterial.txt");
		
		ref.saveRegion("file_esempio/prova_objMan_regione.txt");
		
		ref.saveVoxel("file_esempio/prova_objMan_voxel");
		
		double[][] alfa= ref.getSakeRegion().getAlfaArray();
		double[][] beta	= ref.getSakeRegion().getBetaArray();
		double[][] fit	= ref.getSakeRegion().getFitArray();
		double[][] parameters= ref.getSakeRegion().getParametersArray();
		VoxelObj mapVt= ref.getSakeVoxel().getMapVt();
		VoxelObj mapKi= ref.getSakeVoxel().getMapKi();
		VoxelObj mapVb= ref.getSakeVoxel().getMapVb();
		VoxelObj mapOrder= ref.getSakeVoxel().getMapOrder();
		VoxelObj fit_vo= ref.getSakeVoxel().getFit();
		
		ref.setSakeRegion(null);
		ref.setSakeVoxel(null);
		
		SakeRegionObj sro= new SakeRegionObj();
		sro.setParamInterface(new StdSAParam("file_esempio",0.001,1.0,2,100));
		sro.setArterialObj(ref.getArterial());
		sro.setRegionObj(ref.getRegion());
		sro.setAlfa(alfa);
		sro.setBeta(beta);
		sro.setFit(fit);
		sro.setParameters(parameters);
		ref.setSakeRegion(sro);
		ref.saveSakeRegion("file_esempio/prova_objMan_sakeregion");
		
		SakeVoxelObj svo= new SakeVoxelObj();

		svo.setParamInterface(new RSSAParam("file_esempio",0.001,1.0,100));

		svo.setArterialObj(ref.getArterial());
		svo.setRegionObj(ref.getRegion());
		svo.setMapVt(mapVt);
		svo.setMapVb(mapVb);
		svo.setMapKi(mapKi);
		svo.setMapOrder(mapOrder);
		svo.setFit(fit_vo);
		ref.setSakeVoxel(svo);
		ref.saveSakeVoxel("file_esempio/prova_objMan_sakevoxel");
	}
	
	@Test
	public void test_all() throws ErrorExc{
		ref.loadArterial("casoVeloce\\RolipramArteriale.txt");
		
		ref.saveArterial("file_esempio/prova_objMan_arterial");
		ref.saveRegion("file_esempio/prova_objMan_regione");
		ref.saveVoxel("file_esempio/prova_objMan_voxel");
		
		ref.setArterial(ref.getArterial());
		ref.setRegion(ref.getRegion());
		ref.setVoxel(ref.getVoxel());
		
		ref.loadRegion("casoVeloce\\RolipramPETscan.txt");
		ref.loadVoxel("casoVeloce\\RolipramTest_slice10","casoVeloce\\RolipramTest_maschera_slice10");
		
		ref.saveArterial("file_esempio/prova_objMan_arterial");	
		ref.saveRegion("file_esempio/prova_objMan_regione");
		ref.saveVoxel("file_esempio/prova_objMan_voxel");

		assertEquals("Must have 5 element inside",5,ref.getItems().size());
		ref.clear();
		assertEquals("Must be empty",0,ref.getItems().size());
		test_load();
		assertEquals("Must have 5 element inside",5,ref.getItems().size());
	}
	
	@Test
	public void test_memento() throws ErrorExc{
		test_load();
		ArterialObj t=ref.getArterial();
		
		ref.createMemento(ref.getArterial());
		ref.createMemento(ref.getRegion());
		ref.createMemento(ref.getVoxel());
		
		ref.setArterial(new ArterialObj());
		ref.setRegion(new RegionObj());
		ref.setVoxel(new VoxelObjAdapter());
		
		ref.restoreFromMemento(ref.getArterial());
		ref.restoreFromMemento(ref.getRegion());
		ref.restoreFromMemento(ref.getVoxel());
		Assert.assertEquals(t,ref.getArterial());
	}
}
