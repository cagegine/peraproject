/**
//////////////////////////////////
LocaleManagerTest. java
This is the test class for LocaleManager - VM Arguments: -Duser.language=it -Duser.country=IT
test.model.manage
GPLv3
Francesco Rosso
TetraTron Group
2012.03.13
Coverage: 76.5%
//////////////////////////////////
 */
package test.model.manage;

import it.tetratron.pera.model.manage.LocaleManager;

import org.junit.Assert;
import org.junit.Test;

public class LocaleManagerTest {

	@Test
	public void testGetString() {
		String res;
		res = LocaleManager.getString("key_set");
		
		res = LocaleManager.getString("key_not_set");
		Assert.assertEquals("key_not_set", res);
		
		res = LocaleManager.getString("key_it_set");
		Assert.assertEquals("test purpose", res);
		
		res = LocaleManager.getString("key_en_set");
		Assert.assertEquals("key_en_set", res);
	}

}
