/**
//////////////////////////////////
ErrorExcTest. java
This is the test class for ErrorExc
test.model.manage
GPLv3
Francesco Rosso
TetraTron Group
2012.03.08
Coverage: 100%
//////////////////////////////////
 */
package test.model.manage;

import it.tetratron.pera.model.manage.ErrorExc;
import junit.framework.Assert;

import org.junit.Test;

public class ErrorExcTest {

	@Test
	public void testErrorExc() {
		ErrorExc eKeySet = new ErrorExc("key_set");
		Assert.assertEquals("key_set", eKeySet.getKey());
		Assert.assertEquals("test purpose", eKeySet.getMessage());
		ErrorExc eKeyNotSet = new ErrorExc("key_not_set");
		Assert.assertEquals("key_not_set", eKeyNotSet.getKey());
		Assert.assertEquals("key_not_set", eKeyNotSet.getMessage());
	}

}
