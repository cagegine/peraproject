/**
//////////////////////////////////
TxtManagerTest. java
This is the test class for TxtManager
test.model.manage
GPLv3
Francesco Rosso
TetraTron Group
2012.03.01
Coverage: 79.6%
//////////////////////////////////
 */
package test.model.manage;

import static org.junit.Assert.fail;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.TxtManager;
import junit.framework.Assert;

import org.junit.Test;

public class TxtManagerTest {

    @Test
    public void testGetFilename() {
	TxtManager txt = new TxtManager();
	try {
	    txt.getFilename();
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}
	txt.setFile("casoVeloce\\RolipramArteriale.txt", true);
	try {
	    txt.getFilename();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void testGetLabels(){
	TxtManager txt = new TxtManager("testFiles\\file_arteriale.txt");
	try {
	    txt.getLabels();
	    fail("WTF-labels not set");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModLabelsNotSet", e.getKey());
	}
    }

    @Test
    public void testSetLabels() {
	//in teoria dovrebbe avere le labels
	TxtManager txt = new TxtManager("testFiles\\file_arteriale.txt");
	try {
	    txt.readFile();
	} catch (ErrorExc e) {
	    fail("WTF: "+e.getKey());
	}
	String[] labels = null;
	try {
	    labels = txt.getLabels();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	try {
	    txt.setLabels(labels);
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	String[] newLabelsNotConsistent = {"lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab", "lab"};
	try {
	    txt.setLabels(newLabelsNotConsistent);
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModLabelsNotConsistent", e.getKey());
	}
    }

    @Test
    public void testReadFileDouble() {
	TxtManager txt = new TxtManager();
	try {
	    txt.readFileDouble();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}
	txt.setFile("fileNonExistent");
	try {
	    txt.readFileDouble();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFileNotFound", e.getKey());
	}
	//con labels
	txt.setFile("testFiles\\file_arteriale.txt");
	try {
	    txt.readFileDouble();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	//senza labels
	txt.setFile("casoNormale\\RolipramArteriale.txt", true);
	try {
	    txt.readFileDouble();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void testGetFileContent(){
	TxtManager txt = new TxtManager();
	//con labels
	txt.setFile("testFiles\\file_arteriale.txt");
	try {
	    txt.getFileContent();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	//senza labels
	txt.setFile("casoNormale\\RolipramArteriale.txt", true);
	try {
	    txt.getFileContent();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void testReadFile() {
	TxtManager txt = new TxtManager();
	try {
	    txt.readFile();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}
	txt.setFile("fileNonExistent");
	try {
	    txt.readFile();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFileNotFound", e.getKey());
	}
	//con labels
	txt.setFile("testFiles\\file_arteriale.txt");
	try {
	    txt.readFile();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
	//senza labels
	txt.setFile("casoNormale\\RolipramArteriale.txt", true);
	try {
	    txt.readFile();
	} catch (ErrorExc e) {
	    fail("WTF");
	}
    }

    @Test
    public void testSaveFileDoubleArrayArray() {
	TxtManager txt = new TxtManager();
	double[][] content = null;

	try {
	    txt.saveFile(content);
	    fail("WTF-filename null");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}

	txt.setFile("casoNormale\\RolipramArteriale.txt", true);

	try {
	    txt.saveFile(content);
	    fail("WTF-content null");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModContentNotSet", e.getKey());
	}

	try {
	    content = txt.readFileDouble();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	String[] labelsNotOk = {"lab", "lab", "lab", "lab", "lab", "lab", "lab"};
	try {
	    txt.setLabels(labelsNotOk);
	    txt.saveFile(content);
	    fail("WTF-labels not ok");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModContentNotConsistentWithLabels", e.getKey());
	}

	txt.setFile("casoNormale\\RolipramArteriale.txt", true);

	try {
	    txt.saveFile(content);
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	double[][] notConsistentContent = new double[1][6];
	notConsistentContent[0][0] = 0.1;
	notConsistentContent[0][1] = 0.1;
	notConsistentContent[0][2] = 0.1;
	notConsistentContent[0][3] = 0.1;
	notConsistentContent[0][4] = 0.1;
	notConsistentContent[0][5] = 0.1;
	try {
	    txt.setLabels(labelsNotOk);
	    txt.saveFile(notConsistentContent);
	    fail("WTF-content not consistent");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModContentNotConsistentWithLabels", e.getKey());
	}
    }

    @Test
    public void testSaveFileStringArrayArray() {
	TxtManager txt = new TxtManager();
	String[][] content = null;

	try {
	    txt.saveFile(content);
	    fail("WTF-filename null");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModFilenameNotSet", e.getKey());
	}

	txt.setFile("casoNormale\\RolipramArteriale.txt", true);

	try {
	    txt.saveFile(content);
	    fail("WTF-content null");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModContentNotSet", e.getKey());
	}

	try {
	    content = txt.readFile();
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	String[] labelsNotOk = {"lab", "lab", "lab", "lab", "lab", "lab", "lab"};
	try {
	    txt.setLabels(labelsNotOk);
	    txt.saveFile(content);
	    fail("WTF-labels not ok");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModContentNotConsistentWithLabels", e.getKey());
	}

	txt.setFile("casoNormale\\RolipramArteriale.txt", true);

	try {
	    txt.saveFile(content);
	} catch (ErrorExc e) {
	    fail("WTF "+e.getKey());
	}

	String[][] notConsistentContent = new String[1][6];
	notConsistentContent[0][0] = "0.1";
	notConsistentContent[0][1] = "0.1";
	notConsistentContent[0][2] = "0.1";
	notConsistentContent[0][3] = "0.1";
	notConsistentContent[0][4] = "0.1";
	notConsistentContent[0][5] = "0.1";
	try {
	    txt.setLabels(labelsNotOk);
	    txt.saveFile(notConsistentContent);
	    fail("WTF-content not consistent");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModContentNotConsistentWithLabels", e.getKey());
	}
    }

}
