package test.model.manage;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import junit.framework.Assert;
import it.tetratron.pera.model.manage.AlgorithmManager;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.algorithms.ImageStandard;
import it.tetratron.pera.model.algorithms.ModArterial;
import it.tetratron.pera.model.objects.ObjInterface;
import it.tetratron.pera.model.params.StdSAParam;

import org.junit.BeforeClass;
import org.junit.Test;

public class AlgorithmManagerTest {

    static AlgorithmManager ref=null;

    @BeforeClass
    //@Test

    static public void test_instanziate(){
	ref=new AlgorithmManager();
    }

    @Test
    public void test_no_run(){
	try{
	    ref.runAlgorithm();
	}
	catch(ErrorExc e){
	    assertTrue(e.getKey(), true);
	}

	ref.setAlgorithm(new ModArterial());
	try{
	    ref.runAlgorithm();
	}
	catch(ErrorExc e){
	    assertTrue(e.getKey(), true);
	}

	ref.getParams();

	try {
	    ref.setAlgorithm(new ModArterial());
	    ref.setParams(null);
	    ref.runAlgorithm();
	    fail("WTF");
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModParamNotCorrect", e.getKey());
	}

	try{ 
	    ref.setAlgorithm(null);
	    ref.runAlgorithm();
	} catch (ErrorExc e) {
	    Assert.assertEquals("EModAlgorithmNotSelected", e.getKey());
	}
	
    }

    @Test
    public void test_run() {
	/*Vector<Double> par_in=new Vector<Double>();
		for(int i=0;i<6;i++)
			par_in.add(1.0);*/
	try {
	    //ref.setAlgorithm(new DecayCorrectionArterial());
	    ref.setAlgorithm(new ImageStandard());
	    ref.loadArterial("casoVeloce\\RolipramArteriale.txt");
	    ref.loadRegion("casoVeloce\\RolipramPETscan.txt");
	    ref.loadVoxel("casoVeloce\\RolipramTest_slice10.hdr","casoVeloce\\RolipramTest_maschera_slice10.hdr");
	    ref.setParams(new StdSAParam("c:\\temp",1.0,100.0,1,100));
	    //ref.setParams(new TypeOfCorrection(1));
	} catch (ErrorExc e) {
	    fail(e.getKey());
	}

	try{
	    ref.runAlgorithm();
	}
	catch(ErrorExc e){
	    fail(e.getKey());
	}
	ObjInterface result=ref.getResult();
	/*if((result instanceof ArterialObj)==false){
			fail("risultato errato!");
		}*/
	try {
	    result.save("file_esempio/lollaiduro");
	} catch (ErrorExc e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}
    }
}
