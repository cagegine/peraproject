package test.model.algorithms;

import static org.junit.Assert.*;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.algorithms.MatlabSingleton;

import org.junit.Test;

import Sake_java.*;

public class MatlabSingletonTest {

	@Test
	public void test_notNullReturn() throws ErrorExc{
		preprocessing p=MatlabSingleton.getInstance().getPreprocessing();
		quantification q=MatlabSingleton.getInstance().getQuantification();
		java.io.OutputStream out=MatlabSingleton.getOutputStream();
		if(p==null || q==null || out==null)
			fail("some null pointer returned" + "\np" + p + "\nq" + q + "\nout" + out);
	}
	
	@Test
	public void test_sameIstance() throws ErrorExc{
		preprocessing p=MatlabSingleton.getInstance().getPreprocessing();
		quantification q=MatlabSingleton.getInstance().getQuantification();
		java.io.OutputStream out=MatlabSingleton.getOutputStream();
		
		assertEquals(p, MatlabSingleton.getInstance().getPreprocessing());
		assertEquals(q, MatlabSingleton.getInstance().getQuantification());
		assertEquals(out, MatlabSingleton.getOutputStream());
		
		try {
			MatlabSingleton.setOutputStream(System.out);
		} catch (ErrorExc e) {
			fail(e.getMessage());
		}
		p=MatlabSingleton.getInstance().getPreprocessing();
		q=MatlabSingleton.getInstance().getQuantification();
		out=MatlabSingleton.getOutputStream();
		assertEquals(p, MatlabSingleton.getInstance().getPreprocessing());
		assertEquals(q, MatlabSingleton.getInstance().getQuantification());
		assertEquals(out, MatlabSingleton.getOutputStream());		
	}
}
