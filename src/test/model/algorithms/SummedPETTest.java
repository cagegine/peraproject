package test.model.algorithms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.algorithms.SummedPET;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.ObjInterface;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.VoxelObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import it.tetratron.pera.model.params.ModArtParam;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

public class SummedPETTest {
	
	private SummedPET test;
	
	@Before
	//@Test
	public void test_istanziate(){
		
			test=new SummedPET();
		
	}
	
	@Test
	public void test_returnNotRegion(){
		Vector<ObjInterface> testArray=new Vector<ObjInterface>();
		ArterialObj ref_art=new ArterialObj();
		VoxelObj ref_vox=new VoxelObjAdapter();
	
		testArray.add(ref_art);
		testArray.add(ref_vox);
		testArray.add(ref_art);
		try {
			test.run(null, testArray);
		} catch (ErrorExc e) {
			assertEquals("The same Error","EModRegObjNotFound", e.getKey());
		}
	}

	@Test
	public void test_returnNotVoxel(){
		Vector<ObjInterface> testArray=new Vector<ObjInterface>();
		
		ArterialObj ref_art=new ArterialObj();
		RegionObj ref_reg=new RegionObj();
	
		testArray.add(ref_art);
		testArray.add(ref_reg);
		testArray.add(ref_art);
		try {
			test.run(new ModArtParam(new Vector<Double>(), false), testArray);
		} catch (ErrorExc e) {
			assertEquals("The same Error","EModVoxObjNotFound", e.getKey());
		}
	}

	@Test
	public void test_run() {
		Vector<ObjInterface> testArray=new Vector<ObjInterface>();
		
		try {
		ArterialObj ref_art=new ArterialObj();
		ref_art.load("casoVeloce\\RolipramArteriale.txt");
		RegionObj ref_reg=new RegionObj();
		ref_reg.load("casoVeloce\\RolipramPETscan.txt");
		VoxelObj ref_vox=new VoxelObjAdapter("casoVeloce\\RolipramTest_slice10","casoVeloce\\RolipramTest_maschera_slice10");
	
		testArray.add(ref_art);
		testArray.add(ref_vox);
		testArray.add(ref_reg);
		
			test.run(null, testArray);
		} catch (ErrorExc e) {
			fail(e.getKey());
		}
	}

	/*
	@Test
	public void test_returnNoParam(){
		ModArterial test=new ModArterial();
		Vector<ObjInterface> testArray=new Vector<ObjInterface>();
		
		RegionObj ref_reg=new RegionObj();
		VoxelObj ref_vox=new VoxelObjAdapter();
	
		testArray.add(ref_reg);
		testArray.add(ref_vox);
		testArray.add(ref_reg);
		try {
			test.run(new Delay((double)12), testArray);
		} catch (ErrorExc e) {
			assertEquals("The same Error","EModParamNotCorrect", e.getKey());
		}
	}
	*/
}
