
package test.model.algorithms;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.algorithms.ModArterial;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.ObjInterface;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.VoxelObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import it.tetratron.pera.model.params.Delay;
import it.tetratron.pera.model.params.ModArtParam;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

public class ModArterialTest {
	
	private ModArterial test;
	
	@Before
	@Test
	public void test_istanziate(){
		
			test=new ModArterial();
		
	}
	
	@Test
	public void test_returnNotArterial(){
		
		Vector<ObjInterface> testArray=new Vector<ObjInterface>();
		
		RegionObj ref_reg=new RegionObj();
		VoxelObj ref_vox=new VoxelObjAdapter();
	
		testArray.add(ref_reg);
		testArray.add(ref_vox);
		testArray.add(ref_reg);
		try {
			test.run(new ModArtParam(new Vector<Double>(), false), testArray);
		} catch (ErrorExc e) {
			assertEquals("The same Error","EModArtObjNotFound", e.getKey());
		}
	}
	
	@Test
	public void test_returnNoParam(){
		
		Vector<ObjInterface> testArray=new Vector<ObjInterface>();
		
		RegionObj ref_reg=new RegionObj();
		VoxelObj ref_vox=new VoxelObjAdapter();
	
		testArray.add(ref_reg);
		testArray.add(ref_vox);
		testArray.add(ref_reg);
		try {
			test.run(new Delay((double) 12), testArray);
		} catch (ErrorExc e) {
			assertEquals("The same Error","EModParamNotCorrect", e.getKey());
		}
	}
	
	@Test
	public void test_run() {
		Vector<ObjInterface> testArray=new Vector<ObjInterface>();
		
		try {
		ArterialObj ref_art=new ArterialObj();
		ref_art.load("casoVeloce\\RolipramArteriale.txt");
		RegionObj ref_reg=new RegionObj();
		ref_reg.load("casoVeloce\\RolipramPETscan.txt");
		VoxelObj ref_vox=new VoxelObjAdapter("casoVeloce\\RolipramTest_slice10","casoVeloce\\RolipramTest_maschera_slice10");
	
		testArray.add(ref_art);
		testArray.add(ref_vox);
		testArray.add(ref_reg);
		Vector<Double> par_in=new Vector<Double>();
		for(int i=0;i<6;i++)
			par_in.add(1.0);
		ModArtParam par=new ModArtParam(par_in,false);
		test.run(par, testArray);
		} catch (ErrorExc e) {
			fail(e.getKey());
		}
	}

}
