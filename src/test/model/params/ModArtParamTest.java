package test.model.params;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.params.ModArtParam;

import java.util.Vector;

import junit.framework.Assert;

import org.junit.Test;

public class ModArtParamTest {

	@Test
	public void testSetPar_InVectorOfDouble() {
		Vector<Double> v=new Vector<Double>();
		v.add(0.001);
		v.add(0.0001);
		try{
			ModArtParam m=new ModArtParam(v,true);
			Assert.assertEquals(m.check(), true);
			for(int i=0;i<m.getPar_In().size();i++)
				System.out.println(m.getPar_In().get(i));
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}

	
	@Test
	public void testModArtParam_Int(){
		Vector<Double> v=new Vector<Double>();
		v.add(0.001);
		v.add(0.0001);
		try{
			ModArtParam m=new ModArtParam(v,1);
			for(int i=0;i<m.getPar_In().size();i++)
				System.out.println(m.getPar_In().get(i));
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
	
	@Test
	public void testSetPar_InDoubleArray() {
		double[][] d=new double[2][1];
		d[0][0]=0.001;
		d[1][0]=1000000.0;
		try{
			ModArtParam m=new ModArtParam(d,true);
			for(int i=0;i<m.getPar_In().size();i++)
				System.out.println(m.getPar_In().get(i));
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
	
	@Test
	public void testModArtParam(){
		ModArtParam m=new ModArtParam();
		m.setTypeOfAnalysis(0);
		for(int i=0;i<m.getPar_In().size();i++)
			System.out.println(m.getPar_In().get(i));
	}
}
