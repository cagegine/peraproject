package test.model.params;

import junit.framework.Assert;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.params.Delay;

import org.junit.Test;

public class DelayTest {

	@Test
	public void testDelay(){
		try{
			Delay a=new Delay(-10.0);
			System.out.println(a.getDelay());
			Assert.assertEquals(a.check(), true);
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
	
	@Test
	public void testSetDelay(){
		try{
			Delay a=new Delay(1.0);
			a.setDelay(99.9);
			System.out.println(a.getDelay());
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}

	@Test
	public void testDelay2(){
		Delay d=new Delay();
		try{
			d.setDelay(0.1);
			System.out.println(d.check());
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
		
	}
	
	@Test
	public void testDelay3(){
		try{
			new Delay(200.0);
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
}
