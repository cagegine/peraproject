package test.model.params;

import junit.framework.Assert;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.params.RSSAParam;

import org.junit.Test;

public class RSSAParamTest {

	@Test
	public void testRSSAParam() {
		try{
			RSSAParam r=new RSSAParam("/",1.0,1.3,100);
			System.out.println(r.getWorkspace_Dir());
			System.out.println(r.getNComp());
			System.out.println(r.getBeta1());
			System.out.println(r.getBeta2());
			System.out.println(r.getSNR().get(0));
			System.out.println(r.getSNR().get(1));
			Assert.assertEquals(r.check(), true);
			r.setSNR(0.01, 11.01);
			System.out.println(r.getSNR().get(0));
			System.out.println(r.getSNR().get(1));
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}

}
