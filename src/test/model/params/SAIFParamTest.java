package test.model.params;

import junit.framework.Assert;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.params.SAIFParam;

import org.junit.Test;

public class SAIFParamTest {

	@Test
	public void testSAIFParam() {
		try{
			SAIFParam s=new SAIFParam("/",0.01,0.001,100);
			System.out.println(s.getWorkspace_Dir());
			System.out.println(s.getNComp());
			System.out.println(s.getBeta1());
			System.out.println(s.getBeta2());
			System.out.println(s.getPassband().get(0));
			System.out.println(s.getPassband().get(1));
			s.setPassband(0.1, 15.1);
			System.out.println(s.getPassband().get(0));
			System.out.println(s.getPassband().get(1));
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
	
	@Test
	public void testSAIFParam2(){
		try{
			SAIFParam s=new SAIFParam("/",0.01,0.001,55);
			System.out.println(s.getWorkspace_Dir());
			System.out.println(s.getNComp());
			System.out.println(s.getBeta1());
			System.out.println(s.getBeta2());
			System.out.println(s.getPassband().get(0));
			System.out.println(s.getPassband().get(1));
			Assert.assertEquals(s.check(), true);
			s.setPassband(10.0, 10.0);
			System.out.println(s.getPassband().get(0));
			System.out.println(s.getPassband().get(1));
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}

}
