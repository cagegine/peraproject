package test.model.params;

import junit.framework.Assert;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.params.StdSAParam;

import org.junit.Test;

public class StdSAParamTest {

	@Test
	public void testStdSAParam() {
		try{
			StdSAParam s=new StdSAParam("/",0.0001,0.01,2,100);
			System.out.println(s.getWorkspace_Dir());
			System.out.println(s.getNComp());
			System.out.println(s.getBeta1());
			System.out.println(s.getBeta2());
			Assert.assertEquals(s.check(), true);
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
	
	@Test
	public void testSetType(){
		try{
			StdSAParam s=new StdSAParam("/",0.001,0.1,2,100);
			s.setWorkspace_Dir("/");
			s.setBeta1(0.01);
			s.setBeta2(1.01);
			s.setType(3);
			System.out.println(s.getWorkspace_Dir());
			System.out.println(s.getNComp());
			System.out.println(s.getBeta1());
			System.out.println(s.getBeta2());
			System.out.println(s.getType());
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
	
	@Test
	public void testGetType(){
		try{
			StdSAParam s=new StdSAParam("c:\\temp",0.001,0.1,2,100);
			s.getType();
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}

	@Test
	public void testStdSAParam2(){
		try{
			new StdSAParam("c:\\temp",0.001,0.1,4,100);
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
}
