package test.model.params;

import junit.framework.Assert;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.params.TypeOfCorrection;

import org.junit.Test;

public class TypeOfCorrectionTest {

	@Test
	public void testTypeOfCorrection() {
		try{
			TypeOfCorrection t=new TypeOfCorrection(1);
			System.out.println(t.getTypeOfCorrection());
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
	
	@Test
	public void testSetTypeOfCorrection(){
		try{
			TypeOfCorrection t=new TypeOfCorrection(1);
			t.setTypeOfCorrection(2);
			Assert.assertEquals(t.check(), true);
			System.out.println(t.getTypeOfCorrection());
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
		
	}
	
	@Test
	public void testTypeOfCorrection2(){
		try{
			new TypeOfCorrection(4);
		}catch(ErrorExc e){
			System.out.println(e.getKey());
		}
	}
}
