package test.integrazione;

import static org.junit.Assert.fail;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import junit.framework.Assert;

import it.tetratron.pera.controller.Controller;
import it.tetratron.pera.model.*;
import it.tetratron.pera.model.manage.*;
import it.tetratron.pera.model.objects.*;
import it.tetratron.pera.model.params.*;
import it.tetratron.pera.view.MainWindow;
import it.tetratron.pera.view.Menu;
import it.tetratron.pera.view.Params;

import org.junit.Before;
import org.junit.Test;

public class ControllerViewTest extends Controller{
	
	static MainWindow mainW;
	
	static{
		mainW= new MainWindow();
		mainW.setMinimumSize(new Dimension(1024,600));
	}
	public ControllerViewTest(){
		super(mainW);
	}
	
	/*@Before
	public void inizialization(){
		mainW= new MainWindow();
		mainW.setMinimumSize(new Dimension(1024,600));
		super.mainW=mainW;
		
	}
	*/
	
	@Test
	public void test_ControllerFromView(){
		
		objManager.clear();
		
		ActionEvent ae= new ActionEvent(this,0,"mainMenu");
		actionPerformed(ae);
		Assert.assertEquals(state.mainMenu, currentState);
		
		ae= new ActionEvent(this,0,"ViewPP");
		actionPerformed(ae);
		Assert.assertEquals(state.pp, currentState);
		
		ae= new ActionEvent(this,0,"ViewSA");
		actionPerformed(ae);
		Assert.assertEquals(state.sa, currentState);
		
		ae= new ActionEvent(this,0,"ViewRA");
		actionPerformed(ae);
		Assert.assertTrue((state.ra==currentState|| state.raRegion==currentState||state.raVoxel==currentState));

		ae= new ActionEvent(this,0,"exit");
		actionPerformed(ae);
		Assert.assertEquals(state.mainMenu, currentState);
		
		/*
		if(state.raRegion==currentState){
			ae= new ActionEvent(this,0,"exportCSV");
			actionPerformed(ae);
		}
		else if(state.raVoxel==currentState){
			ae= new ActionEvent(this,0,"exportPDF");
			actionPerformed(ae);
		}
		*/	
		
		/*PP BLOCK*/
		ae= new ActionEvent(this,0,"ViewPP");
		actionPerformed(ae);
		Assert.assertEquals(state.pp, currentState);

		/*PP Art*/
		ae= new ActionEvent(this,0,"ViewPPArt");
		actionPerformed(ae);
		Assert.assertEquals(state.ppArtMod, currentState);
		
		ae= new ActionEvent(this,0,"ViewPPArtMod");
		actionPerformed(ae);
		Assert.assertEquals(state.ppArtMod, currentState);
		
		//ae= new ActionEvent(this,0,"new");
		//actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPPArtDecay");
		actionPerformed(ae);
		Assert.assertEquals(state.ppArtDecay, currentState);
		
		ae= new ActionEvent(this,0,"ViewPPArtDelay");
		actionPerformed(ae);
		Assert.assertEquals(state.ppArtDelay, currentState);

		
		/*PP Reg*/
		ae= new ActionEvent(this,0,"ViewPPReg");
		actionPerformed(ae);
		Assert.assertEquals(state.ppRegDecay, currentState);
		
		ae= new ActionEvent(this,0,"ViewPPRegDecay");
		actionPerformed(ae);
		Assert.assertEquals(state.ppRegDecay, currentState);
		
		/*PP Vox*/
		ae= new ActionEvent(this,0,"ViewPPVox");
		actionPerformed(ae);
		Assert.assertEquals(state.ppVoxSum, currentState);
		
		ae= new ActionEvent(this,0,"ViewPPVoxSum");
		actionPerformed(ae);
		Assert.assertEquals(state.ppVoxSum, currentState);
		
		ae= new ActionEvent(this,0,"ViewPPVoxDecay");
		actionPerformed(ae);
		Assert.assertEquals(state.ppVoxDecay, currentState);

		ae= new ActionEvent(this,0,"ViewPPVoxVisualization");
		actionPerformed(ae);
		Assert.assertEquals(state.ppVoxVisualization, currentState);
		
		ae= new ActionEvent(this,0,"exit");
		actionPerformed(ae);
		Assert.assertEquals(state.mainMenu, currentState);

		/*SA BLOCK*/
		ae= new ActionEvent(this,0,"ViewSA");
		actionPerformed(ae);
		Assert.assertEquals(state.sa, currentState);

		/*SA Reg*/
		ae= new ActionEvent(this,0,"ViewSAReg");
		actionPerformed(ae);
		Assert.assertEquals(state.saRegStd, currentState);
		
		ae= new ActionEvent(this,0,"ViewSARegStd");
		actionPerformed(ae);
		Assert.assertEquals(state.saRegStd, currentState);
		
		ae= new ActionEvent(this,0,"ViewSARegRS");
		actionPerformed(ae);
		Assert.assertEquals(state.saRegRSSA, currentState);
		
		ae= new ActionEvent(this,0,"ViewSARegIF");
		actionPerformed(ae);
		Assert.assertEquals(state.saRegSAIF, currentState);
		
		/*SA Vox*/
		ae= new ActionEvent(this,0,"ViewSAVox");
		actionPerformed(ae);
		Assert.assertEquals(state.saVoxStd, currentState);
		
		ae= new ActionEvent(this,0,"ViewSAVoxStd");
		actionPerformed(ae);
		Assert.assertEquals(state.saVoxStd, currentState);
		
		ae= new ActionEvent(this,0,"ViewSAVoxRS");
		actionPerformed(ae);
		Assert.assertEquals(state.saVoxRSSA, currentState);
		
		ae= new ActionEvent(this,0,"ViewSAVoxIF");
		actionPerformed(ae);
		Assert.assertEquals(state.saVoxSAIF, currentState);
		
		ae= new ActionEvent(this,0,"exit");
		actionPerformed(ae);
		Assert.assertEquals(state.mainMenu, currentState);

		/*
		ae= new ActionEvent(this,0,"ViewRAReg");
		actionPerformed(ae);
		Assert.assertEquals(state.ra, currentState);
		
		
		ae= new ActionEvent(this,0,"ViewRAVox");
		actionPerformed(ae);
		Assert.assertEquals(state.ra, currentState);
		
		*/
		
		
	}
	
	@Test
	public void test_ControllerAlgorithms(){
		//Assert.assertTrue(true);
		objManager.clear();
		try{
			objManager.loadArterial("file_esempio/RolipramArteriale");
			objManager.loadRegion("file_esempio/RolipramPETscan");
			objManager.loadVoxel("file_esempio/RolipramTest_slice10", "file_esempio/RolipramTest_maschera_slice10");
		}catch(Exception e){
			fail(e.getMessage());
		}
		
		
		/*ActionEvent ae= new ActionEvent(this,0,"ViewPP");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPPArt");
		actionPerformed(ae);
		*/
		ActionEvent ae= new ActionEvent(this,0,"ViewPPArtMod");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"start");
		actionPerformed(ae);
		
		Assert.assertTrue(((AlgorithmManager)objManager).getResult() instanceof ArterialObj);
		
		ae= new ActionEvent(this,0,"reject");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPP");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPPReg");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPPRegDecay");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"start");
		actionPerformed(ae);
		
		Assert.assertTrue(((AlgorithmManager)objManager).getResult() instanceof RegionObj);
		
		ae= new ActionEvent(this,0,"reject");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPP");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPPVox");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewPPVoxSum");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"start");
		actionPerformed(ae);
		
		Assert.assertTrue(((AlgorithmManager)objManager).getResult() instanceof VoxelObj);
		
		ae= new ActionEvent(this,0,"selectTemp");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"reject");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewSA");
		actionPerformed(ae);
		
		Params.setPath("file_esempio/");
		
		ae= new ActionEvent(this,0,"ViewSAReg");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewSARegStd");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"start");
		actionPerformed(ae);
		
		Assert.assertTrue(((AlgorithmManager)objManager).getResult() instanceof SakeRegionObj);
		
		ae= new ActionEvent(this,0,"ViewSA");
		actionPerformed(ae);
		
		Params.setPath("file_esempio/");
		
		ae= new ActionEvent(this,0,"ViewSAVox");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"ViewSAVoxStd");
		actionPerformed(ae);
		
		ae= new ActionEvent(this,0,"start");
		actionPerformed(ae);
		
		Assert.assertTrue(((AlgorithmManager)objManager).getResult() instanceof SakeVoxelObj);
	
	}
}
