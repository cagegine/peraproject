package test.integrazione;

import static org.junit.Assert.*;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.SlashVector;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import junit.framework.Assert;

import org.junit.Test;

import Sake_java.preprocessing;

public class PreProcessingAlgo {

	//PreProcessing algorithm + Arterial & Region & Voxel
	@Test
	public void test_algo() throws Exception{
		preprocessing prep=new preprocessing();
		ArterialObj obj=new ArterialObj();
		obj.load("file_esempio/RolipramArteriale.txt");
		RegionObj obj2=new RegionObj();
		obj2.load("file_esempio/RolipramPETscan.txt");
		VoxelObjAdapter x=new VoxelObjAdapter();
		x.load("file_esempio/RolipramTest_slice10", "file_esempio/RolipramTest_maschera_slice10");
		Object[] obijei=null;
		//obijei=quant.stdSA_image(5,((SlashVector)(obj.getTime_plasma())).rotate(),((SlashVector)(obj.getPlasma())).rotate(),((SlashVector)(obj.getBlood())).rotate(),((SlashVector)(obj2.getTime_pet())).rotate(),((SlashVector)(obj2.getNsd())).rotate(),x.getDynPet(),x.getDynMask(),"C:\\provaSake",1,0.1,0.2,1 );
		obijei= prep.summedPET(1, ((SlashVector)(obj2.getTime_pet())).rotate(),((SlashVector)(obj2.getDelta())).rotate(),x.getDynPet(),x.getDynMask());
		Assert.assertEquals(obijei.length, 1);
		
		int errors=0;
		try{
			ArterialObj obj_error=new ArterialObj();
			obj_error.load("file_esempio/file_void.txt");
		}catch(ErrorExc e){
			errors++;
		}
		try{
			RegionObj obj2_error=new RegionObj();
			obj2_error.load("file_esempio/file_casual.txt");
		}catch(ErrorExc e){
			errors++;
		}
		VoxelObjAdapter x_error=new VoxelObjAdapter();
		x_error.load("file_esempio/RolipramTest_slice10", "file_esempio/RolipramTest_maschera_slice10");
		Object[] obijei_no=null;
		//obijei=quant.stdSA_image(5,((SlashVector)(obj.getTime_plasma())).rotate(),((SlashVector)(obj.getPlasma())).rotate(),((SlashVector)(obj.getBlood())).rotate(),((SlashVector)(obj2.getTime_pet())).rotate(),((SlashVector)(obj2.getNsd())).rotate(),x.getDynPet(),x.getDynMask(),"C:\\provaSake",1,0.1,0.2,1 );
		obijei_no= prep.summedPET(1, ((SlashVector)(obj2.getTime_pet())).rotate(),((SlashVector)(obj2.getDelta())).rotate(),x.getDynPet(),x.getDynMask());
		Assert.assertEquals(errors, 2);
	}
}
