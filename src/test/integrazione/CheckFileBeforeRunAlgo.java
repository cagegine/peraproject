package test.integrazione;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import junit.framework.Assert;

import org.junit.Test;

import com.mathworks.toolbox.javabuilder.MWException;

import Sake_java.preprocessing;

public class CheckFileBeforeRunAlgo {
	
	/**
	 * PPFR-4.2, PPFR-6.4
	 */
	@Test
	public void test_algo_pp(){
		try {
			preprocessing prep=new preprocessing();
		} catch (MWException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		int errors=0;
		ArterialObj obj=new ArterialObj();
		try {
			obj.load("file_esempio/RolipramPETscan.txt");
		} catch (ErrorExc e) {
			// TODO Auto-generated catch block
			errors++;
		}
		RegionObj obj2=new RegionObj();
		try {
			obj2.load("file_esempio/RolipramArteriale.txt");
		} catch (ErrorExc e) {
			// TODO Auto-generated catch block
			errors++;
		}
		VoxelObjAdapter x=new VoxelObjAdapter();
		try {
			x.load("file_esempio/RolipramTest_error", "file_esempio/RolipramTest_maschera_error");
		} catch (ErrorExc e) {
			// TODO Auto-generated catch block
			errors++;
		}
		
		Assert.assertEquals(errors, 3);
		//Object[] obijei=null;
		//obijei=quant.stdSA_image(5,((SlashVector)(obj.getTime_plasma())).rotate(),((SlashVector)(obj.getPlasma())).rotate(),((SlashVector)(obj.getBlood())).rotate(),((SlashVector)(obj2.getTime_pet())).rotate(),((SlashVector)(obj2.getNsd())).rotate(),x.getDynPet(),x.getDynMask(),"C:\\provaSake",1,0.1,0.2,1 );
	}
}
