/*
 /////////////////////////
 File name Controller.java;
 Description the controller class, represent the controller of the MVC design pattern ;
 Package it.tetratron.pera.controller;
 License GLP 3;
 Author ( s ) Alessandro Ferlin;
 Owner TetraTron Group;
 Date 13/2/2012;
 /////////////////////////
 */
package it.tetratron.pera.controller;

import it.tetratron.pera.model.algorithms.DecayCorrectionArterial;
import it.tetratron.pera.model.algorithms.DecayCorrectionImage;
import it.tetratron.pera.model.algorithms.DecayCorrectionROI;
import it.tetratron.pera.model.algorithms.DelayCorrectionArterial;
import it.tetratron.pera.model.algorithms.ImageIterativeFiltering;
import it.tetratron.pera.model.algorithms.ImageRankShaping;
import it.tetratron.pera.model.algorithms.ImageStandard;
import it.tetratron.pera.model.algorithms.MatlabSingleton;
import it.tetratron.pera.model.algorithms.ModArterial;
import it.tetratron.pera.model.algorithms.RegionIterativeFiltering;
import it.tetratron.pera.model.algorithms.RegionRankShaping;
import it.tetratron.pera.model.algorithms.RegionStandard;
import it.tetratron.pera.model.algorithms.SummedPET;
import it.tetratron.pera.model.exporters.ExporterCSV;
import it.tetratron.pera.model.exporters.ExporterPDF;
import it.tetratron.pera.model.manage.AlgorithmManager;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.LocaleManager;
import it.tetratron.pera.model.manage.ObjectManager;
import it.tetratron.pera.model.manage.SakeManager;
import it.tetratron.pera.model.objects.ArterialObj;
import it.tetratron.pera.model.objects.ObjInterface;
import it.tetratron.pera.model.objects.RegionObj;
import it.tetratron.pera.model.objects.SakeRegionObj;
import it.tetratron.pera.model.objects.SakeVoxelObj;
import it.tetratron.pera.model.objects.VoxelObj;
import it.tetratron.pera.model.objects.VoxelObjAdapter;
import it.tetratron.pera.model.params.ModArtParam;
import it.tetratron.pera.model.params.RSSAParam;
import it.tetratron.pera.model.params.SAIFParam;
import it.tetratron.pera.model.params.SAParam;
import it.tetratron.pera.model.params.StdSAParam;
import it.tetratron.pera.model.params.TypeOfCorrection;
import it.tetratron.pera.view.AcceptReject;
import it.tetratron.pera.view.ArtMod;
import it.tetratron.pera.view.Chart;
import it.tetratron.pera.view.ComboSA;
import it.tetratron.pera.view.Decay;
import it.tetratron.pera.view.Delay;
import it.tetratron.pera.view.ImageViewerWithChart;
import it.tetratron.pera.view.MainWindow;
import it.tetratron.pera.view.MatlabWindow;
import it.tetratron.pera.view.MatlabWindow.TextAreaOutputStream;
import it.tetratron.pera.view.Menu;
import it.tetratron.pera.view.NsdDeltaRoi;
import it.tetratron.pera.view.Params;
import it.tetratron.pera.view.RegionResult;
import it.tetratron.pera.view.SplittableChart;
import it.tetratron.pera.view.Sum;
import it.tetratron.pera.view.Table;
import it.tetratron.pera.view.TwoFieldsSA;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.itextpdf.text.BadElementException;

/**
 * Represent the controller of the MVC design pattern
 * It decides witch view show, and it instance the correctly model object.  
 */
public class Controller implements ActionListener, Observer, DocumentListener,
		MouseListener , MouseMotionListener, ChangeListener{
	/**
	 * the main window
	 */
	protected MainWindow mainW;
	protected MatlabWindow console;
	protected ImageViewerWithChart image;
	/**
	 * Instance of model to manage object
	 */
	protected ObjectManager objManager;

	protected enum state {
		mainMenu, // first window

		/* Block state */
		pp, // state pre-processing
		sa, // state quantification
		ra, // state result analysis

		/* Algorithm state */
		/* PP Arterial */
		ppArtMod, ppArtDecay, ppArtDelay,
		/* PP Region */
		ppRegDecay,
		/* PP Voxel */
		ppVoxSum, ppVoxDecay, ppVoxVisualization,
		/* SA Region */
		saRegStd, saRegRSSA, saRegSAIF,
		/* SA Voxel */
		saVoxStd, saVoxRSSA, saVoxSAIF,

		raRegion, // state result analysis region level
		raVoxel, // state result analysis voxel level
	}

	/**
	 * Current state of Controller
	 */
	protected state currentState;

	/**
	 * If MCR is installed
	 */
	private boolean MCR = true;

	/**
	 * If is the first iteration
	 */
	private boolean firstElaboration = true;

	/**
	 * If undo is accepted
	 */

	private boolean activeUndo = false;

	/**
	 * Constructor create An AlgorithmManager instance, and add this as
	 * observer.
	 * 
	 * @param m the MainWindow where draw views.
	 */

	public Controller(MainWindow m) {
		mainW = m;
		/* Create model instance */
		objManager = new AlgorithmManager();
		objManager.addObserver(this);

		/* Set OutputStream of Matlab */
		try {
			MatlabSingleton.setOutputStream(/*
											 * MatlabWindow.
											 * createTextAreaOutputStream()
											 */new TextAreaOutputStream());
			console = new MatlabWindow(
					(TextAreaOutputStream) MatlabSingleton.getOutputStream());
		} catch (ErrorExc e) {
			MCR = false;
		}
		// console=new
		// MatlabWindow((TextAreaOutputStream)MatlabSingleton.getOutputStream());

		/* Set state */
		setCurrentState("mainMenu");
		showCorrectView("mainMenu");

	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		final ActionEvent ae2 = ae;
	//	new Thread(){
	//		public void run(){
		// mainW.startWaiting();
		String action = ae2.getActionCommand();
		// System.out.println(ae.toString());
		state oldCurrentState = currentState;
		setCurrentState(action);
		if (currentState != oldCurrentState)
			showCorrectView(action);
		// setObjects();
		setParams(action);
		setAlgorithm();
		doSomething(action); // forse per salvataggio undo ecc

		// mainW.stopWaiting();
	//		}
	//	}.start();
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		try {
			if (currentState == state.ppArtMod
					|| currentState == state.ppArtDelay
					|| currentState == state.ppArtDecay) {
				new PrintStream(MatlabSingleton.getOutputStream())
						.println("Saving current state.");
				objManager.createMemento(objManager.getArterial());
			}
			if (currentState == state.ppRegDecay) {
				new PrintStream(MatlabSingleton.getOutputStream())
						.println("Saving current state.");
				objManager.createMemento(objManager.getRegion());
			}
			if (currentState == state.ppVoxDecay
					|| currentState == state.ppVoxSum) {
				new PrintStream(MatlabSingleton.getOutputStream())
						.println("Saving current state.");
				objManager.createMemento(objManager.getVoxel());

			}

			new PrintStream(MatlabSingleton.getOutputStream())
					.println("Operation Complete!");
		} catch (ErrorExc e) {
			mainW.setMainNotification(e.getMessage());
			Params.disableParamsButton("undo");
		}
		mainW.stopWaiting();
		mainW.setEnabled(true);
		mainW.setMainNotification("");
		if (console != null)
			console.stop();
		ObjInterface res = ((AlgorithmManager) objManager).getResult();
		String[] accept_reject = new String[2];
		accept_reject[0] = LocaleManager.getString("ViewAccept");
		accept_reject[1] = LocaleManager.getString("ViewReject");
		String[] accept_reject_action = new String[2];
		accept_reject_action[0] = "accept";
		accept_reject_action[1] = "reject";

		// if is an ArterialObj, draw the arterial view
		if (res instanceof ArterialObj) {
			ArterialObj result = (it.tetratron.pera.model.objects.ArterialObj) res;
			SplittableChart chart = new SplittableChart();
			chart.setNumber(1);
			chart.showSave(false);
			try {
				chart.addData(result.getPlasmaLabel(), 
						// data
						result.getTime_plasma(),
						result.getPlasma(), 
						objManager.getArterial().getTime_plasma(), 
						objManager.getArterial().getPlasma(),  
						// axis name
						result.getTimeLabel(), 
						result.getPlasmaLabel(),0);
			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				chart.addData(result.getBloodLabel(), 
						// data
						result.getTime_plasma(),
						result.getBlood(), 
						objManager.getArterial().getTime_plasma(), 
						objManager.getArterial().getBlood(), 
						// axis name
						result.getTimeLabel(), 
						result.getBloodLabel(), 0);
			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			chart.addListner(this);
			mainW.setNewWorkArea(new AcceptReject(chart, accept_reject,
					accept_reject_action, this));
			mainW.setMainNotification(LocaleManager
					.getString("ViewTipArterialView"));
		}

		// if is an RegionObj, draw the Region view
		else if (res instanceof RegionObj) {
			RegionObj result = (it.tetratron.pera.model.objects.RegionObj) res;
			SplittableChart chart = new SplittableChart();
			chart.setNumber(1);
			chart.showSave(false);
			Chart chartNSD = new Chart();
			Chart chartDelta = new Chart();

			try {
				chartNSD.setTitle(result.getNSDLabel());
				chartNSD.setAxis(result.getTimeLabel(), result.getNSDLabel());
				chartNSD.addCollection(result.getNSDLabel(), 
						result.getTime_pet(), 
						result.getNsd()  /*, 
						objManager.getRegion().getTime_pet(), 
						objManager.getRegion().getNsd()*/
						);
				chartNSD.updateChart();
			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				chartDelta.setTitle(result.getDeltaLabel());
				chartDelta.setAxis(result.getTimeLabel(),
						result.getDeltaLabel());
				chartDelta.addCollection(result.getDeltaLabel(), 
						result.getTime_pet(), 
						result.getDelta()/* ,
						objManager.getRegion().getTime_pet(), 
						objManager.getRegion().getDelta()*/
						);
				chartDelta.updateChart();
			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// String label[] = result.getRoiLabel();
			try {
				for (int i = 0; i < result.getRoi().size(); i++) {
					chart.addData((result.getRoiLabel())[i],
							result.getTime_pet(), 
							result.getRoi().get(i),
							objManager.getRegion().getTime_pet(), 
							objManager.getRegion().getRoi().get(i),
							result.getTimeLabel(),
							(result.getRoiLabel())[i], 
							 0);
				}
			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			chart.addListner(this);
			mainW.setNewWorkArea(new AcceptReject(
					new NsdDeltaRoi(chartNSD, chartDelta, chart),
					accept_reject, accept_reject_action, this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipRegionView"));
		}

		// if is an VoxelObj, draw the Voxel view
		else if (res instanceof VoxelObj) {
			VoxelObj result = (it.tetratron.pera.model.objects.VoxelObj) res;
			image = new ImageViewerWithChart(result.getT(), mainW);
			image.addAllListener(this, null, this, null);
			double[] dim = ((VoxelObjAdapter) result).getVoxDims();
			image.setVoxelDim(dim[0], dim[1], dim[2]);

			SplittableChart chart = new SplittableChart();
			chart.setNumber(1);
			chart.showSave(false);
			try {
				image.setVoxel(result.getDynPet((short) 0));

				chart.addData(objManager.getRegion().getNSDLabel(), 
						objManager.getRegion().getTime_pet(), 
						objManager.getRegion().getNsd(), 
						objManager.getRegion().getTimeLabel(), 
						objManager.getRegion().getNSDLabel(),
						0);

				chart.addData(objManager.getRegion().getDeltaLabel(),
						objManager.getRegion().getTime_pet(), 
						objManager.getRegion().getDelta(), 
						objManager.getRegion().getTimeLabel(), 
						objManager.getRegion().getDeltaLabel(), 
						0);

			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			chart.addListner(this);
			image.setCharts(chart);
			// image.set
			mainW.setNewWorkArea(new AcceptReject(image, accept_reject,
					accept_reject_action, this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipVoxelView"));
		} else if (res instanceof SakeRegionObj) {
			mainW.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			mainW.startWaiting();

			objManager.setSakeRegion((SakeRegionObj) res);
			try {
				objManager.saveSakeRegion(objManager.getSakeRegion()
						.getParamInterface().getWorkspace_Dir()
						+ Menu.getNameForOutput() + "_Region_" + objManager.getSakeRegion().getAlgorithm());
				mainW.setMainNotification(LocaleManager
						.getString("ViewTipSARegionSave"));
			} catch (ErrorExc e) {
				mainW.setMainNotification(e.getMessage());
			} finally {
				mainW.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				mainW.stopWaiting();
			}
		} else if (res instanceof SakeVoxelObj) {
			mainW.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			mainW.startWaiting();
			objManager.setSakeVoxel((SakeVoxelObj) res);
			try {
				objManager.saveSakeVoxel(objManager.getSakeVoxel()
						.getParamInterface().getWorkspace_Dir()
						+ Menu.getNameForOutput() + "_Voxel_"  + objManager.getSakeVoxel().getAlgorithm());
				mainW.setMainNotification(LocaleManager
						.getString("ViewTipSAVoxelSave"));
			} catch (ErrorExc e) {
				mainW.setMainNotification(e.getMessage());
			} finally {
				mainW.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				mainW.stopWaiting();
			}
		}
	}

	/* Private method */

	/**
	 * Set the correct currentState according to the action
	 * 
	 * @param action
	 *            to execute.
	 */
	private void setCurrentState(String action) {
		if (action == "mainMenu")
			currentState = state.mainMenu;

		/* Block */
		if (action == "ViewPP") {
			currentState = state.pp;
		}
		if (action == "ViewSA") {
			currentState = state.sa;
		}
		if (action == "ViewRA") {
			currentState = state.ra;
		}

		/* SubBlock */
		if (action == "ViewPPArt") {
			currentState = state.ppArtMod;
		}
		if (action == "ViewPPReg") {
			currentState = state.ppRegDecay;
		}
		if (action == "ViewPPVox") {
			currentState = state.ppVoxSum;
		}
		if (action == "ViewSAReg") {
			currentState = state.saRegStd;
		}
		if (action == "ViewSAVox") {
			currentState = state.saVoxStd;
		}
		if (action == "ViewRAReg") {
			currentState = state.ra;
		}
		if (action == "ViewRAVox") {
			currentState = state.ra;
		}

		/* Algorithms */

		/* PP Arterial */
		if (action == "ViewPPArtMod") {
			currentState = state.ppArtMod;
		}
		if (action == "ViewPPArtDecay") {
			currentState = state.ppArtDecay;
		}
		if (action == "ViewPPArtDelay") {
			currentState = state.ppArtDelay;
		}

		/* PP Region */
		if (action == "ViewPPRegDecay") {
			currentState = state.ppRegDecay;
		}

		/* PP Voxel */
		if (action == "ViewPPVoxSum") {
			currentState = state.ppVoxSum;
		}
		if (action == "ViewPPVoxDecay") {
			currentState = state.ppVoxDecay;
		}
		if (action == "ViewPPVoxVisualization") {
			currentState = state.ppVoxVisualization;
		}
		/* SA Region */
		if (action == "ViewSARegStd") {
			currentState = state.saRegStd;
		}
		if (action == "ViewSARegRS") {
			currentState = state.saRegRSSA;
		}
		if (action == "ViewSARegIF") {
			currentState = state.saRegSAIF;
		}

		/* SA Voxel */
		if (action == "ViewSAVoxStd") {
			currentState = state.saVoxStd;
		}
		if (action == "ViewSAVoxRS") {
			currentState = state.saVoxRSSA;
		}
		if (action == "ViewSAVoxIF") {
			currentState = state.saVoxSAIF;
		}

		/* RA */
		if (action == "ViewRAReg") {
			currentState = state.ra;
		}
		if (action == "ViewRAVox") {
			currentState = state.ra;
		}

		/* Buttons */
		if (action == "exit") {
			currentState = state.mainMenu;
		}

	}

	/**
	 * Show the correct view according to the action and currentState
	 * 
	 * @param action
	 *            to execute.
	 */

	private void showCorrectView(String action) {
		if (currentState == state.mainMenu) {
			String[][] label = new String[3][2];
			label[0][1] = "ViewPP";
			label[1][1] = "ViewSA";
			label[2][1] = "ViewRA";
			label[0][0] = LocaleManager.getString("ViewPP");
			label[1][0] = LocaleManager.getString("ViewSA");
			label[2][0] = LocaleManager.getString("ViewRA");
			mainW.setNewWorkArea(Menu.draw(true, false, label, this, null));

			// clean objManager
			objManager.clear();
			firstElaboration = true;

			if (MCR == false) {
				 Menu.disableButton("ViewPP");
				 Menu.disableButton("ViewSA");
			}
			mainW.setMainNotification("WELCOME TO PERA");

		}
		if (currentState == state.pp) {
			String[][] label = new String[3][2];
			label[0][1] = "ViewPPArt";
			label[1][1] = "ViewPPReg";
			label[2][1] = "ViewPPVox";
			label[0][0] = LocaleManager.getString("ViewPPArt");
			label[1][0] = LocaleManager.getString("ViewPPReg");
			label[2][0] = LocaleManager.getString("ViewPPVox");
			mainW.setNewWorkArea(Menu.draw(false, false, label, this, null));
			mainW.setMainNotification(LocaleManager.getString("ViewTipPP"));
		}
		if (currentState == state.sa) {
			String[][] label = new String[2][2];
			label[0][1] = "ViewSAReg";
			label[1][1] = "ViewSAVox";
			label[0][0] = LocaleManager.getString("ViewSAReg");
			label[1][0] = LocaleManager.getString("ViewSAVox");
			String s = Menu.getDir();
			mainW.setNewWorkArea(Menu.draw(false, true, label, this, this));
			Menu.setDir(s);
			if (s.isEmpty())
				Menu.enableButtons(false);
			mainW.setMainNotification(LocaleManager.getString("ViewTipSA"));

			// Menu.setDir(Menu.getDir(mainW), mainW);
		}
		/*
		 * if(currentState==state.ra){ String
		 * path=Params.openFileChooser(Params.getSakeFilter());
		 * 
		 * try{ if(SakeManager.getType(path)==SakeManager.Type.Region){
		 * objManager.loadSakeRegion(path); currentState=state.raRegion; } else{
		 * objManager.loadSakeVoxel(path); currentState=state.raVoxel; } }
		 * catch(ErrorExc e){
		 * JOptionPane.showMessageDialog(mainW,e.getMessage(),
		 * "Error",JOptionPane.ERROR_MESSAGE,null); } }
		 */

		/* PPArterial */
		if (currentState == state.ppArtMod) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewPPArtMod";
			label[1][1] = "ViewPPArtDecay";
			label[2][1] = "ViewPPArtDelay";
			label[0][0] = LocaleManager.getString("ViewPPArtMod");
			label[1][0] = LocaleManager.getString("ViewPPArtDecay");
			label[2][0] = LocaleManager.getString("ViewPPArtDelay");
			/* Set file chooser */
			String[][] fileChooser = new String[1][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[0][2] = "Art";
			/* Set TypeOfAnalysis */
			String[] items = new String[2];
			items[0] = LocaleManager.getString("ViewSolve");
			items[1] = LocaleManager.getString("ViewFit");

			mainW.setNewWorkArea(Params.draw(label, true, true, true, ArtMod
					.draw(fileChooser,
							LocaleManager.getString("ViewTypeOfAnalysis"),
							items, this, this), this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipPPArtMod"));
			Params.setSelectedButton(0);
			if (firstElaboration) {
				ArtMod.setParIn(ModArtParam.getDefaultParIn());
				Params.disableParamsButton("save");
				Params.disableParamsButton("undo");
				Params.disableParamsButton("new");
			}
			else{
				ArtMod.setParIn(((ModArtParam)((AlgorithmManager) objManager).getParams()).getPar_In());
			}
			if (activeUndo == false)
				Params.disableParamsButton("undo");

			if (action == "accept" || action == "undo"
					|| firstElaboration == false) {
				ArtMod.alreadyLoadedFile(this);
				try {
					addArtChart();
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (objManager.getArterial() != null) {
				try {
					ArtMod.setFile(objManager.getArterial().getFilename());
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (currentState == state.ppArtDecay) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewPPArtMod";
			label[1][1] = "ViewPPArtDecay";
			label[2][1] = "ViewPPArtDelay";
			label[0][0] = LocaleManager.getString("ViewPPArtMod");
			label[1][0] = LocaleManager.getString("ViewPPArtDecay");
			label[2][0] = LocaleManager.getString("ViewPPArtDelay");
			/* Set file chooser */
			String[][] fileChooser = new String[1][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[0][2] = "Art";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewIsotope11c");
			items[1] = LocaleManager.getString("ViewIsotope18f");
			items[2] = LocaleManager.getString("ViewIsotope15o");

			mainW.setNewWorkArea(Params.draw(label, true, true, true, Decay
					.draw(fileChooser,
							LocaleManager.getString("ViewDecayCorrection"),
							items, this, this), this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipPPArtDecay"));
			Params.setSelectedButton(1);

			if (firstElaboration) {
				Params.disableParamsButton("save");
				Params.disableParamsButton("undo");
				Params.disableParamsButton("new");
			}

			if (activeUndo == false)
				Params.disableParamsButton("undo");

			if (action == "accept" || action == "undo"
					|| firstElaboration == false) {
				Decay.alreadyLoadedFiles(this);
				try {
					addArtChart();
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (objManager.getArterial() != null) {
				try {
					Decay.setFile(objManager.getArterial().getFilename(), 0);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		if (currentState == state.ppArtDelay) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewPPArtMod";
			label[1][1] = "ViewPPArtDecay";
			label[2][1] = "ViewPPArtDelay";
			label[0][0] = LocaleManager.getString("ViewPPArtMod");
			label[1][0] = LocaleManager.getString("ViewPPArtDecay");
			label[2][0] = LocaleManager.getString("ViewPPArtDelay");
			/* Set file chooser */
			String[][] fileChooser = new String[1][3];
			fileChooser[0][1] = "FileChooserArt";
			;
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[0][2] = "Art";
			mainW.setNewWorkArea(Params.draw(
					label,
					true,
					true,
					true,
					Delay.draw(fileChooser,
							LocaleManager.getString("ViewDelay"), this, this),
					this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipPPArtDelay"));
			Params.setSelectedButton(2);
			Delay.setDelay(it.tetratron.pera.model.params.Delay
					.getDefaultDelay().toString());
			if (firstElaboration) {
				Params.disableParamsButton("save");
				Params.disableParamsButton("undo");
				Params.disableParamsButton("new");
			}

			if (activeUndo == false)
				Params.disableParamsButton("undo");

			if (action == "accept" || action == "undo"
					|| firstElaboration == false) {
				Delay.alreadyLoadedFile(this);
				try {
					addArtChart();
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (objManager.getArterial() != null) {
				try {
					Delay.setFile(objManager.getArterial().getFilename());
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		/* PP Region */
		if (currentState == state.ppRegDecay) {
			/* Set name algorithm */
			String[][] label = new String[1][2];
			label[0][1] = "ViewPPRegDecay";
			label[0][0] = LocaleManager.getString("ViewPPRegDecay");
			/* Set file chooser */
			String[][] fileChooser = new String[1][3];
			fileChooser[0][1] = "FileChooserReg";
			;
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[0][2] = "Reg";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewIsotope11c");
			items[1] = LocaleManager.getString("ViewIsotope18f");
			items[2] = LocaleManager.getString("ViewIsotope15o");

			mainW.setNewWorkArea(Params.draw(label, true, true, true, Decay
					.draw(fileChooser,
							LocaleManager.getString("ViewDecayCorrection"),
							items, this, this), this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipPPRegDecay"));

			Params.setSelectedButton(0);

			if (firstElaboration) {
				Params.disableParamsButton("save");
				Params.disableParamsButton("undo");
				Params.disableParamsButton("new");
			}

			if (activeUndo == false)
				Params.disableParamsButton("undo");

			if (action == "accept" || action=="reject" || action == "undo"
					|| firstElaboration == false) {
				Decay.alreadyLoadedFiles(this);
				try {
					addRegChart();
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		/* PP Voxel */
		if (currentState == state.ppVoxSum) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewPPVoxSum";
			label[1][1] = "ViewPPVoxDecay";
			label[2][1] = "ViewPPVoxVisualization";
			label[0][0] = LocaleManager.getString("ViewPPVoxSum");
			label[1][0] = LocaleManager.getString("ViewPPVoxDecay");
			label[2][0] = LocaleManager.getString("ViewPPVoxVisualization");
			/* Set file chooser */
			String[][] fileChooser = new String[3][3];
			fileChooser[0][1] = "FileChooserReg";
			fileChooser[1][1] = "FileChooserVoxDynPET";
			fileChooser[2][1] = "FileChooserVoxMask";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[1][0] = LocaleManager
					.getString("ViewFileChooserVoxDyn");
			fileChooser[2][0] = LocaleManager
					.getString("ViewFileChooserVoxMask");
			fileChooser[0][2] = "Reg";
			fileChooser[1][2] = "Vox";
			fileChooser[2][2] = "Mask";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewIsotope11c");
			items[1] = LocaleManager.getString("ViewIsotope18f");
			items[2] = LocaleManager.getString("ViewIsotope15o");

			mainW.setNewWorkArea(Params.draw(label, true, true, true,
					Sum.draw(fileChooser, this, this), this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipPPVoxSum"));
			Params.setSelectedButton(0);

			if (firstElaboration) {
				Params.disableParamsButton("save");
				Params.disableParamsButton("undo");
				Params.disableParamsButton("new");
			}

			if (activeUndo == false)
				Params.disableParamsButton("undo");

			if (action == "accept" || action == "undo"
					|| firstElaboration == false) {
				Sum.alreadyLoadedFiles(this);
			} else {
				if (objManager.getRegion() != null) {
					try {
						Sum.setFile(objManager.getRegion().getFilename(), 0);
					} catch (ErrorExc e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (objManager.getVoxel() != null) {
					Sum.setFile(objManager.getVoxel().getHeaderFilename(), 1);
					Sum.setFile(objManager.getVoxel().getHeaderMaskFilename(),
							2);
				}
			}
		}
		if (currentState == state.ppVoxVisualization) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewPPVoxSum";
			label[1][1] = "ViewPPVoxDecay";
			label[2][1] = "ViewPPVoxVisualization";
			label[0][0] = LocaleManager.getString("ViewPPVoxSum");
			label[1][0] = LocaleManager.getString("ViewPPVoxDecay");
			label[2][0] = LocaleManager.getString("ViewPPVoxVisualization");
			/* Set file chooser */
			String[][] fileChooser = new String[3][3];
			fileChooser[0][1] = "FileChooserReg";
			fileChooser[1][1] = "FileChooserVoxDynPET";
			fileChooser[2][1] = "FileChooserVoxMask";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[1][0] = LocaleManager
					.getString("ViewFileChooserVoxDyn");
			fileChooser[2][0] = LocaleManager
					.getString("ViewFileChooserVoxMask");
			fileChooser[0][2] = "Reg";
			fileChooser[1][2] = "Vox";
			fileChooser[2][2] = "Mask";

			mainW.setNewWorkArea(Params.draw(label, true, true, true,
					Sum.draw(fileChooser, this, this), this));
			mainW.setMainNotification(LocaleManager
					.getString("ViewTipPPVoxVisualization"));
			Params.setSelectedButton(2);

			if (firstElaboration) {
				Params.disableParamsButton("save");
				Params.disableParamsButton("undo");
				Params.disableParamsButton("new");
			}

			if (activeUndo == false)
				Params.disableParamsButton("undo");

			if (action == "accept" || action == "undo"
					|| firstElaboration == false) {
				Sum.alreadyLoadedFiles(this);
			} else {
				if (objManager.getRegion() != null) {
					try {
						Sum.setFile(objManager.getRegion().getFilename(), 0);
					} catch (ErrorExc e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (objManager.getVoxel() != null) {
					Sum.setFile(objManager.getVoxel().getHeaderFilename(), 1);
					Sum.setFile(objManager.getVoxel().getHeaderMaskFilename(),
							2);
				}
			}
		}
		if(currentState==state.ppVoxDecay){
			/*Set name algorithm*/
			String[][] label= new String[3][2];
			label[0][1]="ViewPPVoxSum";
			label[1][1]="ViewPPVoxDecay";
			label[2][1]="ViewPPVoxVisualization";
			label[0][0]=LocaleManager.getString("ViewPPVoxSum");
			label[1][0]=LocaleManager.getString("ViewPPVoxDecay");
			label[2][0]=LocaleManager.getString("ViewPPVoxVisualization");
			/*Set file chooser*/
			String[][] fileChooser= new String[3][3];
			fileChooser[0][1]="FileChooserReg";
			fileChooser[1][1]="FileChooserVoxDynPET";
			fileChooser[2][1]="FileChooserVoxMask";
			fileChooser[0][0]=LocaleManager.getString("ViewFileChooserReg");
			fileChooser[1][0]=LocaleManager.getString("ViewFileChooserVoxDyn");
			fileChooser[2][0]=LocaleManager.getString("ViewFileChooserVoxMask");
			fileChooser[0][2]="Reg";
			fileChooser[1][2]="Vox";
			fileChooser[2][2]="Mask";
			/*Set item of type of correction*/
			String[] items= new String[3];
			items[0]=LocaleManager.getString("ViewIsotope11c");
			items[1]=LocaleManager.getString("ViewIsotope18f");
			items[2]=LocaleManager.getString("ViewIsotope15o");
			
			mainW.setNewWorkArea(Params.draw(label, true, true, true, Decay
					.draw(fileChooser,
							LocaleManager.getString("ViewDecayCorrection"),
							items, this, this), this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipPPVoxDecay"));
			Params.setSelectedButton(1);

			if (firstElaboration) {
				Params.disableParamsButton("save");
				Params.disableParamsButton("undo");
				Params.disableParamsButton("new");
			}

			if (activeUndo == false)
				Params.disableParamsButton("undo");

			if (action == "accept" || action == "undo"
					|| firstElaboration == false) {
				Decay.alreadyLoadedFiles(this);
			} else {
				if (objManager.getRegion() != null) {
					try {
						Decay.setFile(objManager.getRegion().getFilename(), 0);
					} catch (ErrorExc e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if (objManager.getVoxel() != null) {
					Decay.setFile(objManager.getVoxel().getHeaderFilename(), 1);
					Decay.setFile(
							objManager.getVoxel().getHeaderMaskFilename(), 2);
				}
			}
		}
		/* SA Region */
		if (currentState == state.saRegStd) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewSARegStd";
			label[1][1] = "ViewSARegRS";
			label[2][1] = "ViewSARegIF";
			label[0][0] = LocaleManager.getString("ViewSARegStd");
			label[1][0] = LocaleManager.getString("ViewSARegRS");
			label[2][0] = LocaleManager.getString("ViewSARegIF");
			/* Set file chooser */
			String[][] fileChooser = new String[2][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[1][1] = "FileChooserReg";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[1][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[0][2] = "Art";
			fileChooser[1][2] = "Reg";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewReversible");
			items[1] = LocaleManager.getString("ViewIrreversible");
			items[2] = LocaleManager.getString("ViewUnknow");

			mainW.setNewWorkArea(Params.draw(label, true, true, true, ComboSA
					.draw(fileChooser, LocaleManager.getString("ViewNcomp"),
							LocaleManager.getString("ViewBeta1"),
							LocaleManager.getString("ViewBeta2"),
							LocaleManager.getString("ViewType"), items, this,
							this), this));

			mainW.setMainNotification(LocaleManager.getString("ViewTipSARegStd"));

			Params.setSelectedButton(0);
			Params.hideParamsButton("save");
			Params.hideParamsButton("undo");
			Params.hideParamsButton("new");
			ComboSA.setNComp(SAParam.getDefaultNComp().toString());
			ComboSA.setBeta1(SAParam.getDefaultBeta1().toString());
			ComboSA.setBeta2(SAParam.getDefaultBeta2().toString());
			if (objManager.getArterial() != null) {
				try {
					ComboSA.setFile(objManager.getArterial().getFilename(), 0);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getRegion() != null) {
				try {
					ComboSA.setFile(objManager.getRegion().getFilename(), 1);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		if (currentState == state.saRegRSSA) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewSARegStd";
			label[1][1] = "ViewSARegRS";
			label[2][1] = "ViewSARegIF";
			label[0][0] = LocaleManager.getString("ViewSARegStd");
			label[1][0] = LocaleManager.getString("ViewSARegRS");
			label[2][0] = LocaleManager.getString("ViewSARegIF");
			/* Set file chooser */
			String[][] fileChooser = new String[2][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[1][1] = "FileChooserReg";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[1][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[0][2] = "Art";
			fileChooser[1][2] = "Reg";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewReversible");
			items[1] = LocaleManager.getString("ViewIrreversible");
			items[2] = LocaleManager.getString("ViewUnknow");

			mainW.setNewWorkArea(Params.draw(label, true, true, true,
					TwoFieldsSA.draw(fileChooser,
							LocaleManager.getString("ViewNcomp"),
							LocaleManager.getString("ViewBeta1"),
							LocaleManager.getString("ViewBeta2"),
							LocaleManager.getString("ViewSnr"), 0, mainW, this,
							this), this));

			mainW.setMainNotification(LocaleManager.getString("ViewTipSARegRS"));

			Params.setSelectedButton(1);
			Params.hideParamsButton("save");
			Params.hideParamsButton("undo");
			Params.hideParamsButton("new");
			TwoFieldsSA.setNComp(SAParam.getDefaultNComp().toString());
			TwoFieldsSA.setBeta1(SAParam.getDefaultBeta1().toString());
			TwoFieldsSA.setBeta2(SAParam.getDefaultBeta2().toString());
			if (objManager.getArterial() != null) {
				try {
					TwoFieldsSA.setFile(objManager.getArterial().getFilename(),
							0);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getRegion() != null) {
				try {
					TwoFieldsSA
							.setFile(objManager.getRegion().getFilename(), 1);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

		if (currentState == state.saRegSAIF) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewSARegStd";
			label[1][1] = "ViewSARegRS";
			label[2][1] = "ViewSARegIF";
			label[0][0] = LocaleManager.getString("ViewSARegStd");
			label[1][0] = LocaleManager.getString("ViewSARegRS");
			label[2][0] = LocaleManager.getString("ViewSARegIF");
			/* Set file chooser */
			String[][] fileChooser = new String[2][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[1][1] = "FileChooserReg";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[1][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[0][2] = "Art";
			fileChooser[1][2] = "Reg";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewReversible");
			items[1] = LocaleManager.getString("ViewIrreversible");
			items[2] = LocaleManager.getString("ViewUnknow");

			mainW.setNewWorkArea(Params.draw(label, true, true, true,
					TwoFieldsSA.draw(fileChooser,
							LocaleManager.getString("ViewNcomp"),
							LocaleManager.getString("ViewBeta1"),
							LocaleManager.getString("ViewBeta2"),
							LocaleManager.getString("ViewPassband"), 1, mainW,
							this, this), this));

			mainW.setMainNotification(LocaleManager.getString("ViewTipSARegIF"));

			Params.setSelectedButton(2);
			Params.hideParamsButton("save");
			Params.hideParamsButton("undo");
			Params.hideParamsButton("new");
			TwoFieldsSA.setNComp(SAParam.getDefaultNComp().toString());
			TwoFieldsSA.setBeta1(SAParam.getDefaultBeta1().toString());
			TwoFieldsSA.setBeta2(SAParam.getDefaultBeta2().toString());
			if (objManager.getArterial() != null) {
				try {
					TwoFieldsSA.setFile(objManager.getArterial().getFilename(),
							0);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getRegion() != null) {
				try {
					TwoFieldsSA
							.setFile(objManager.getRegion().getFilename(), 1);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		/* SA Voxel */
		if (currentState == state.saVoxStd) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewSAVoxStd";
			label[1][1] = "ViewSAVoxRS";
			label[2][1] = "ViewSAVoxIF";
			label[0][0] = LocaleManager.getString("ViewSAVoxStd");
			label[1][0] = LocaleManager.getString("ViewSAVoxRS");
			label[2][0] = LocaleManager.getString("ViewSAVoxIF");
			/* Set file chooser */
			String[][] fileChooser = new String[4][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[1][1] = "FileChooserReg";
			fileChooser[2][1] = "FileChooserVoxDynPET";
			fileChooser[3][1] = "FileChooserVoxMask";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[1][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[2][0] = LocaleManager
					.getString("ViewFileChooserVoxDyn");
			fileChooser[3][0] = LocaleManager
					.getString("ViewFileChooserVoxMask");
			fileChooser[0][2] = "Art";
			fileChooser[1][2] = "Reg";
			fileChooser[2][2] = "Vox";
			fileChooser[3][2] = "Mask";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewReversible");
			items[1] = LocaleManager.getString("ViewIrreversible");
			items[2] = LocaleManager.getString("ViewUnknow");

			mainW.setNewWorkArea(Params.draw(label, true, true, true, ComboSA
					.draw(fileChooser, LocaleManager.getString("ViewNcomp"),
							LocaleManager.getString("ViewBeta1"),
							LocaleManager.getString("ViewBeta2"),
							LocaleManager.getString("ViewType"), items, this,
							this), this));
			mainW.setMainNotification(LocaleManager.getString("ViewTipSAVoxStd"));

			Params.setSelectedButton(0);
			Params.hideParamsButton("save");
			Params.hideParamsButton("undo");
			Params.hideParamsButton("new");
			ComboSA.setNComp(SAParam.getDefaultNComp().toString());
			ComboSA.setBeta1(SAParam.getDefaultBeta1().toString());
			ComboSA.setBeta2(SAParam.getDefaultBeta2().toString());

			if (objManager.getArterial() != null) {
				try {
					ComboSA.setFile(objManager.getArterial().getFilename(), 0);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getRegion() != null) {
				try {
					ComboSA.setFile(objManager.getRegion().getFilename(), 1);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getVoxel() != null) {
				ComboSA.setFile(objManager.getVoxel().getHeaderFilename(), 2);
				ComboSA.setFile(objManager.getVoxel().getHeaderMaskFilename(),
						3);
			}

		}
		if (currentState == state.saVoxRSSA) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewSAVoxStd";
			label[1][1] = "ViewSAVoxRS";
			label[2][1] = "ViewSAVoxIF";
			label[0][0] = LocaleManager.getString("ViewSAVoxStd");
			label[1][0] = LocaleManager.getString("ViewSAVoxRS");
			label[2][0] = LocaleManager.getString("ViewSAVoxIF");
			/* Set file chooser */
			String[][] fileChooser = new String[4][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[1][1] = "FileChooserReg";
			fileChooser[2][1] = "FileChooserVoxDynPET";
			fileChooser[3][1] = "FileChooserVoxMask";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[1][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[2][0] = LocaleManager
					.getString("ViewFileChooserVoxDyn");
			fileChooser[3][0] = LocaleManager
					.getString("ViewFileChooserVoxMask");
			fileChooser[0][2] = "Art";
			fileChooser[1][2] = "Reg";
			fileChooser[2][2] = "Vox";
			fileChooser[3][2] = "Mask";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewReversible");
			items[1] = LocaleManager.getString("ViewIrreversible");
			items[2] = LocaleManager.getString("ViewUnknow");

			mainW.setNewWorkArea(Params.draw(label, true, true, true,
					TwoFieldsSA.draw(fileChooser,
							LocaleManager.getString("ViewNcomp"),
							LocaleManager.getString("ViewBeta1"),
							LocaleManager.getString("ViewBeta2"),
							LocaleManager.getString("ViewSnr"), 0, mainW, this,
							this), this));

			mainW.setMainNotification(LocaleManager.getString("ViewTipSAVoxRS"));

			Params.setSelectedButton(1);
			Params.hideParamsButton("save");
			Params.hideParamsButton("undo");
			Params.hideParamsButton("new");
			TwoFieldsSA.setNComp(SAParam.getDefaultNComp().toString());
			TwoFieldsSA.setBeta1(SAParam.getDefaultBeta1().toString());
			TwoFieldsSA.setBeta2(SAParam.getDefaultBeta2().toString());

			if (objManager.getArterial() != null) {
				try {
					TwoFieldsSA.setFile(objManager.getArterial().getFilename(),
							0);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getRegion() != null) {
				try {
					TwoFieldsSA
							.setFile(objManager.getRegion().getFilename(), 1);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getVoxel() != null) {
				TwoFieldsSA.setFile(objManager.getVoxel().getHeaderFilename(),
						2);
				TwoFieldsSA.setFile(objManager.getVoxel()
						.getHeaderMaskFilename(), 3);
			}

		}
		if (currentState == state.saVoxSAIF) {
			/* Set name algorithm */
			String[][] label = new String[3][2];
			label[0][1] = "ViewSAVoxStd";
			label[1][1] = "ViewSAVoxRS";
			label[2][1] = "ViewSAVoxIF";
			label[0][0] = LocaleManager.getString("ViewSAVoxStd");
			label[1][0] = LocaleManager.getString("ViewSAVoxRS");
			label[2][0] = LocaleManager.getString("ViewSAVoxIF");
			/* Set file chooser */
			String[][] fileChooser = new String[4][3];
			fileChooser[0][1] = "FileChooserArt";
			fileChooser[1][1] = "FileChooserReg";
			fileChooser[2][1] = "FileChooserVoxDynPET";
			fileChooser[3][1] = "FileChooserVoxMask";
			fileChooser[0][0] = LocaleManager.getString("ViewFileChooserArt");
			fileChooser[1][0] = LocaleManager.getString("ViewFileChooserReg");
			fileChooser[2][0] = LocaleManager
					.getString("ViewFileChooserVoxDyn");
			fileChooser[3][0] = LocaleManager
					.getString("ViewFileChooserVoxMask");
			fileChooser[0][2] = "Art";
			fileChooser[1][2] = "Reg";
			fileChooser[2][2] = "Vox";
			fileChooser[3][2] = "Mask";
			/* Set item of type of correction */
			String[] items = new String[3];
			items[0] = LocaleManager.getString("ViewReversible");
			items[1] = LocaleManager.getString("ViewIrreversible");
			items[2] = LocaleManager.getString("ViewUnknow");

			mainW.setNewWorkArea(Params.draw(label, true, true, true,
					TwoFieldsSA.draw(fileChooser,
							LocaleManager.getString("ViewNcomp"),
							LocaleManager.getString("ViewBeta1"),
							LocaleManager.getString("ViewBeta2"),
							LocaleManager.getString("ViewPassband"), 1, mainW,
							this, this), this));

			mainW.setMainNotification(LocaleManager.getString("ViewTipSAVoxIF"));

			Params.setSelectedButton(2);

			Params.hideParamsButton("save");
			Params.hideParamsButton("undo");
			Params.hideParamsButton("new");
			TwoFieldsSA.setNComp(SAParam.getDefaultNComp().toString());
			TwoFieldsSA.setBeta1(SAParam.getDefaultBeta1().toString());
			TwoFieldsSA.setBeta2(SAParam.getDefaultBeta2().toString());

			if (objManager.getArterial() != null) {
				try {
					TwoFieldsSA.setFile(objManager.getArterial().getFilename(),
							0);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getRegion() != null) {
				try {
					TwoFieldsSA
							.setFile(objManager.getRegion().getFilename(), 1);
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (objManager.getVoxel() != null) {
				TwoFieldsSA.setFile(objManager.getVoxel().getHeaderFilename(),
						2);
				TwoFieldsSA.setFile(objManager.getVoxel()
						.getHeaderMaskFilename(), 3);
			}

		}

		if (currentState == state.raRegion) {
			mainW.startWaiting();
			SplittableChart chartRoi = new SplittableChart();
			boolean b[] = {false,true};
			boolean domainInt[] = {true,false};
			chartRoi.setNumber(b, domainInt);
			chartRoi.showExit(true);
			chartRoi.addListner(this);
			try {
				
				for (int i = 0; i < objManager.getSakeRegion().getRegionObj()
						.getRoi().size(); i++) {
					chartRoi.addData((objManager.getSakeRegion().getRegionObj().getRoiLabel())[i], 
							objManager.getSakeRegion().getRegionObj().getTime_pet(),
							objManager.getSakeRegion().getRegionObj().getRoi().get(i),
							objManager.getSakeRegion().getRegionObj().getTime_pet(), 
							objManager.getSakeRegion().getFit().get(i),
							// axis name
							objManager.getSakeRegion().getRegionObj().getTimeLabel(), 
							(objManager.getSakeRegion().getRegionObj().getRoiLabel())[i], 
							0);

					chartRoi.addData((objManager.getSakeRegion().getRegionObj().getRoiLabel())[i], 
							objManager.getSakeRegion().getBeta().get(i), 
							objManager.getSakeRegion()
							.getAlfa().get(i),
							"Beta",
							"Alfa", 
							 1);
				}
			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mainW.stopWaiting();
			}

			try {
				mainW.setNewWorkArea(new RegionResult(chartRoi, new Table(
						objManager.getSakeRegion().getParametersContent(),
						Color.orange, Color.LIGHT_GRAY, Color.WHITE)));
				mainW.setMainNotification(LocaleManager
						.getString("ViewTipRARegion"));

			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				mainW.stopWaiting();
			}

			mainW.stopWaiting();

		}
		if (currentState == state.raVoxel) {
			mainW.startWaiting();
			try {
				image = new ImageViewerWithChart(objManager.getSakeVoxel()
						.getMapVb().getT(), mainW);
				image.addAllListener(this, this, this,this);
				double[] dim = ((VoxelObjAdapter) objManager.getSakeVoxel()
						.getMapVb()).getVoxDims();
				image.setVoxelDim(dim[0], dim[1], dim[2]);

				image.setVoxel(objManager.getSakeVoxel().getMapVb()
						.getDynPet((short) 0));

				image.setVoxel(
						objManager.getSakeVoxel().getMapKi()
								.getDynPet((short) 0),
						objManager.getSakeVoxel().getMapVt()
								.getDynPet((short) 0), objManager
								.getSakeVoxel().getMapVb().getDynPet((short) 0));
				 fixImageViewerChart();

			} catch (ErrorExc e) {
				// TODO Auto-generated catch block
				mainW.stopWaiting();
				e.printStackTrace();
			}
			mainW.stopWaiting();
			mainW.setNewWorkArea(image);
			mainW.setMainNotification(LocaleManager.getString("ViewTipRAVoxel"));
		}

	}

	/**
	 * This method save, make undo, starts algorithm, and manage all button action provide from view
	 */

	private void doSomething(String action) {

		if (action.equals("credits")){
			mainW.showCredits();
		}
		
		/* Manage PDF Exporter */
		if (action.equals("exportPDF")) {
			String pathToSaveFile = Params.openSaveChooser();
			pathToSaveFile = Params.checkExtension(pathToSaveFile, ".pdf");
			if (pathToSaveFile != "") {
				try {
					ExporterPDF exp = new ExporterPDF(pathToSaveFile,
							objManager.getSakeVoxel().getArterialPathFromLog(),
							objManager.getSakeVoxel().getRegionPathFromLog(),
							objManager.getSakeVoxel().getRawVoxelPathFromLog(),
							objManager.getSakeVoxel().getRawVoxelMaskPathFromLog(), 
							objManager.getSakeVoxel().getFilename(),
							objManager.getSakeVoxel().getInputs());

					Image[] imgs = image.getImages();
					for (int i = 0; i < imgs.length; i++)
						try {
							if(imgs[i]!=null)
								exp.addImage(com.itextpdf.text.Image.getInstance(
									imgs[i], null));
						} catch (BadElementException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							mainW.stopWaiting();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							mainW.stopWaiting();
						}
					exp.addGradient(image.getGradient().getName());
					exp.addMin(image.getMin());
					exp.addMax(image.getMax());
					exp.addValue(image.getValue());
					exp.addMap(image.getMap());
					exp.addEstimates(image.getEstimates());
					exp.addCoord(image.getXCoord(), image.getYCoord(), image.getZCoord(),image.getTemp());
					exp.saveFile();
				} catch (ErrorExc e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					mainW.stopWaiting();
				}
			}

		}

		/* Manage CSV exporter */
		if (action.equals("exportCSV")) {
			String pathToSaveFile = Params.openSaveChooser();
			if (pathToSaveFile != "") {
				try {

					double[][] alfa_ = objManager.getSakeRegion()
							.getAlfaArray();
					double[][] beta_ = objManager.getSakeRegion()
							.getBetaArray();
					Double[][] alfa = new Double[alfa_.length][alfa_[0].length];
					Double[][] beta = new Double[beta_.length][beta_[0].length];

					for (int i = 0; i < alfa.length; i++)
						for (int j = 0; j < alfa[0].length; j++)
							alfa[i][j] = alfa_[i][j];

					for (int i = 0; i < beta.length; i++)
						for (int j = 0; j < beta[0].length; j++)
							beta[i][j] = beta_[i][j];

					@SuppressWarnings("unused")
					ExporterCSV exp = new ExporterCSV(pathToSaveFile,
							objManager.getSakeRegion().getFitContent(), alfa,
							beta, objManager.getSakeRegion()
									.getParametersContent());
					
					//objManager.getSakeRegion().getSakeFile().getFileCSV("logfile.txt", pathToSaveFile);
					File f= objManager.getSakeRegion().getLog();
					InputStream in= null;
					OutputStream out = null;
					try {
						in = new FileInputStream(f);
						out = new FileOutputStream(new File(pathToSaveFile+"_log.txt"));
						byte[] buf =new byte[1024];
						int len;
						while((len=in.read(buf)) > 0){
							out.write(buf,0,len);
						}
						in.close();
						out.close();
					} catch (FileNotFoundException e){
						throw new ErrorExc("EModGeneralIOError");
					}catch(IOException io){
						throw new ErrorExc("EModGeneralIOError");						
					}
					
				} catch (ErrorExc e) {
					e.printStackTrace();
					mainW.stopWaiting();
				}
			}

		}

		/* Manage RA View */
		if (action == "ViewRA") {
			mainW.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			mainW.startWaiting();
			String path = Params.openFileChooser(Params.getSakeFilter());
			if (path != "") {
				try {
					if (SakeManager.getType(path) == SakeManager.Type.Region) {
						objManager.loadSakeRegion(path);
						currentState = state.raRegion;
					} else {
						objManager.loadSakeVoxel(path);
						currentState = state.raVoxel;
					}
					showCorrectView(action);
				} catch (ErrorExc e) {
					JOptionPane.showMessageDialog(mainW, e.getMessage() + "\n"
							+ e.getDetails(), "Error",
							JOptionPane.ERROR_MESSAGE, null);
				}
			}
			mainW.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			mainW.stopWaiting();
		}

		if (action == "selectTemp") {
			short t = (short) image.getTemp();
			try {
				if (currentState == state.ppVoxDecay
						|| currentState == state.ppVoxSum) {
					ObjInterface res = ((AlgorithmManager) objManager)
							.getResult();
					if (res instanceof VoxelObj)
						image.setVoxel(((VoxelObj) res).getDynPet(t));
				}

				if (currentState == state.ppVoxVisualization) {
					image.setVoxel(objManager.getVoxel().getDynPet(t));
				}

				if (currentState == state.raVoxel) {
					image.setVoxel(objManager.getSakeVoxel().getMapKi()
							.getDynPet(t), objManager.getSakeVoxel().getMapVt()
							.getDynPet(t), objManager.getSakeVoxel().getMapVb()
							.getDynPet(t));
					// ImageViewrWithChart.sert;
				}
			} catch (ErrorExc e) {
				mainW.setMainNotification(e.getMessage());
			}
		}
		/* Manage new Elaboration */
		if (action == "new") {
			objManager.clear();
			firstElaboration = true;
			showCorrectView("");
		}
		/* Manage Save and Undo */
		if (action == "save") {
			try {
				if (currentState == state.ppArtMod
						|| currentState == state.ppArtDecay
						|| currentState == state.ppArtDelay) {
					mainW.setCursor(Cursor
							.getPredefinedCursor(Cursor.WAIT_CURSOR));
					mainW.startWaiting();
					objManager.saveArterial(Params.openSaveChooser());
					mainW.setMainNotification(LocaleManager
							.getString("ViewTipSaveArterial"));
				}
				if (currentState == state.ppRegDecay) {
					mainW.setCursor(Cursor
							.getPredefinedCursor(Cursor.WAIT_CURSOR));
					mainW.startWaiting();
					objManager.saveRegion(Params.openSaveChooser());
					mainW.setMainNotification(LocaleManager
							.getString("ViewTipSaveRegion"));
				}
				if (currentState == state.ppVoxSum
						|| currentState == state.ppVoxDecay
						|| currentState == state.ppVoxVisualization) {
					mainW.setCursor(Cursor
							.getPredefinedCursor(Cursor.WAIT_CURSOR));
					mainW.startWaiting();
					objManager.saveVoxel(Params.openSaveChooser());
					mainW.setMainNotification(LocaleManager
							.getString("ViewTipSaveVoxel"));
				}
			} catch (ErrorExc e) {
				mainW.setMainNotification(e.getMessage());
			} finally {
				mainW.setCursor(Cursor
						.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				mainW.stopWaiting();
			}
		}
		if (action == "undo") {
			activeUndo = false;
			if (currentState == state.ppArtMod
					|| currentState == state.ppArtDecay
					|| currentState == state.ppArtDelay) {
				objManager.restoreFromMemento(objManager.getArterial());
				// showCorrectView("undo");
			}
			if (currentState == state.ppRegDecay) {
				objManager.restoreFromMemento(objManager.getRegion());
				// (showCorrectView("undo");
			}
			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxDecay) {
				objManager.restoreFromMemento(objManager.getVoxel());
				// showCorrectView("undo");
			}
			// Params.disableParamsButton("undo");
			showCorrectView("undo");
			mainW.setMainNotification(LocaleManager.getString("ViewTipUndoCorrect"));

		}

		/* Manage accept-reject */
		if (action == "accept") {
			if (currentState == state.ppArtMod
					|| currentState == state.ppArtDecay
					|| currentState == state.ppArtDelay)
				objManager
						.setArterial((ArterialObj) ((AlgorithmManager) objManager)
								.getResult());
			if (currentState == state.ppRegDecay)
				objManager
						.setRegion((RegionObj) ((AlgorithmManager) objManager)
								.getResult());
			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxDecay)
				objManager.setVoxel((VoxelObj) ((AlgorithmManager) objManager)
						.getResult());
			firstElaboration = false;
			showCorrectView("accept");

		}

		if (action == "reject") {
			showCorrectView("reject");
		}

		/* Manage start of algorithms */
		else if (action == "start") {
			activeUndo = true;
			mainW.startWaiting();

			if (currentState != state.ppVoxVisualization) {
				new Thread() {
			public void run() {
						try {
							mainW.setEnabled(false);
							// mainW.startWaiting();
							if (console != null)
								console.setVisible(true);

							// Run Algorithm
							((AlgorithmManager) objManager).runAlgorithm();
							// firstElaboration=false;
						} catch (ErrorExc e) {
							mainW.stopWaiting();
							mainW.setEnabled(true);
							if (console != null) {
								if (e.getKey() == "EModMatlabError")
									console.stop();
								else
									console.setVisible(false);
							}
							if (objManager.getArterial() == null)
								errors("Art");
							if (objManager.getRegion() == null)
								errors("Reg");
							if (objManager.getVoxel() == null) {
								errors("Vox");
								errors("Mask");
							}
							// mainW.stopWaiting();
							mainW.setMainNotification(e.getMessage());
						}
				}
				}.start();
			}// end if currentState!=state.ppVoxVisualization
			else {
				if (objManager.getVoxel() != null
						&& objManager.getRegion() != null) {
					
					// mainW.startWaiting();
					firstElaboration = false;
					VoxelObj result = objManager.getVoxel();
					image = new ImageViewerWithChart(result.getT(), mainW);
					image.addAllListener(this, null, this, null);
					double[] dim = ((VoxelObjAdapter) result).getVoxDims();
					image.setVoxelDim(dim[0], dim[1], dim[2]);

					SplittableChart chart = new SplittableChart();
					chart.setNumber(1);
					chart.showSave(false);
					try {
						image.setVoxel(result.getDynPet((short) 0));

						chart.addData(objManager.getRegion().getNSDLabel(),
								objManager.getRegion().getTime_pet(),
								objManager.getRegion().getNsd(), 
								objManager.getRegion().getTimeLabel(),
								objManager.getRegion().getNSDLabel(), 
								0);

						chart.addData(objManager.getRegion().getDeltaLabel(),
								objManager.getRegion().getTime_pet(),
								objManager.getRegion().getDelta(), 
								objManager.getRegion().getTimeLabel(),
								objManager.getRegion().getDeltaLabel(),
								0);

					} catch (ErrorExc e) {
						mainW.stopWaiting();
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					chart.addListner(this);
					image.setCharts(chart);
					mainW.setNewWorkArea(new AcceptReject(image,
							new String[] { "OK" }, new String[] { "accept" },
							this));
					// mainW.stopWaiting();
					mainW.setMainNotification(LocaleManager
							.getString("ViewTipVoxelRawView"));
					mainW.stopWaiting();
				}
				mainW.stopWaiting();
			}// end else

		}

		/* Manage FileChooser */
		/* Arterial */
		if (action == "FileChooserArt") {
			mainW.startWaiting();
			if (currentState == state.ppArtMod) {
				ArtMod.setFile(Params.openFileChooser(Params.getTxtFilter()));
				ArtMod.setFileError(false);
				try {
					objManager.loadArterial(ArtMod.getFilePath());
					addArtChart();
				} catch (ErrorExc e) {
					ArtMod.setFileError(true);
					Params.removeChart();
				}
			}

			if (currentState == state.ppArtDecay) {
				Decay.setFile(Params.openFileChooser(Params.getTxtFilter()), 0);

				Decay.setFileError(false, 0);
				try {
					objManager.loadArterial(Decay.getFilePath(0));
					addArtChart();
				} catch (ErrorExc e) {
					Decay.setFileError(true, 0);
					Params.removeChart();
				}
			}

			if (currentState == state.ppArtDelay) {
				Delay.setFile(Params.openFileChooser(Params.getTxtFilter()));

				Delay.setFileError(false);
				try {
					objManager.loadArterial(Delay.getFilePath());
					addArtChart();
				} catch (ErrorExc e) {
					Delay.setFileError(true);
					Params.removeChart();
				}
			}

			if (currentState == state.saRegStd
					|| currentState == state.saVoxStd) {
				ComboSA.setFile(Params.openFileChooser(Params.getTxtFilter()),
						0);

				ComboSA.setFileError(false, 0);
				try {
					objManager.loadArterial(ComboSA.getFilePath(0));
				} catch (ErrorExc e) {
					ComboSA.setFileError(true, 0);
				}
			}

			if (currentState == state.saRegRSSA
					|| currentState == state.saVoxRSSA
					|| currentState == state.saRegSAIF
					|| currentState == state.saVoxSAIF) {

				TwoFieldsSA.setFile(
						Params.openFileChooser(Params.getTxtFilter()), 0);

				TwoFieldsSA.setFileError(false, 0);
				try {
					objManager.loadArterial(TwoFieldsSA.getFilePath(0));
				} catch (ErrorExc e) {
					TwoFieldsSA.setFileError(true, 0);
				}
			}
			mainW.stopWaiting();
		}

		/* Region */
		if (action == "FileChooserReg") {
			mainW.startWaiting();
			if (currentState == state.ppVoxDecay) {
				Decay.setFile(Params.openFileChooser(Params.getTxtFilter()), 0);

				Decay.setFileError(false, 0);
				try {
					objManager.loadRegion(Decay.getFilePath(0));
				} catch (ErrorExc e) {
					Decay.setFileError(true, 0);
				}
			}

			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxVisualization) {
				Sum.setFile(Params.openFileChooser(Params.getTxtFilter()), 0);

				Sum.setFileError(false, 0);
				try {
					objManager.loadRegion(Sum.getFilePath(0));
				} catch (ErrorExc e) {
					Sum.setFileError(true, 0);
				}
			}

			if (currentState == state.ppRegDecay) {
				Decay.setFile(Params.openFileChooser(Params.getTxtFilter()), 0);

				Decay.setFileError(false, 0);
				try {
					objManager.loadRegion(Decay.getFilePath(0));
					addRegChart();
				} catch (ErrorExc e) {
					Decay.setFileError(true, 0);
				}
			}

			if (currentState == state.saRegStd
					|| currentState == state.saVoxStd) {
				ComboSA.setFile(Params.openFileChooser(Params.getTxtFilter()),
						1);

				ComboSA.setFileError(false, 1);
				try {
					objManager.loadRegion(ComboSA.getFilePath(1));
				} catch (ErrorExc e) {
					ComboSA.setFileError(true, 1);
				}
			}

			if (currentState == state.saRegRSSA
					|| currentState == state.saVoxRSSA
					|| currentState == state.saRegSAIF
					|| currentState == state.saVoxSAIF) {
				TwoFieldsSA.setFile(
						Params.openFileChooser(Params.getTxtFilter()), 1);

				TwoFieldsSA.setFileError(false, 1);
				try {
					objManager.loadRegion(TwoFieldsSA.getFilePath(1));
				} catch (ErrorExc e) {
					TwoFieldsSA.setFileError(true, 1);
				}
			}
			mainW.stopWaiting();
		}

		/* Voxel */
		if (action == "FileChooserVoxDynPET") {
			mainW.startWaiting();
			if (currentState == state.ppVoxDecay) {

				Decay.setFile(Params.openFileChooser(Params.getImgFilter()), 1);
				if (Decay.checkMask()) {
					Decay.setFileError(false, 1);
					Decay.setFileError(false, 2);
					try {
						objManager.loadVoxel(Decay.getFilePath(1),
								Decay.getFilePath(2));
					} catch (ErrorExc e) {
						Decay.setFileError(true, 1);
						Decay.setFileError(true, 2);
					}
				}
			}

			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxVisualization) {
				Sum.setFile(Params.openFileChooser(Params.getImgFilter()), 1);
				if (Sum.checkMask()) {
					Sum.setFileError(false, 1);
					Sum.setFileError(false, 2);
					try {
						objManager.loadVoxel(Sum.getFilePath(1),
								Sum.getFilePath(2));
					} catch (ErrorExc e) {
						Sum.setFileError(true, 1);
						Sum.setFileError(true, 2);
					}
				}
			}

			if (currentState == state.saVoxStd) {
				ComboSA.setFile(Params.openFileChooser(Params.getImgFilter()),
						2);
				if (ComboSA.checkMask()) {
					ComboSA.setFileError(false, 2);
					ComboSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(ComboSA.getFilePath(2),
								ComboSA.getFilePath(3));
					} catch (ErrorExc e) {
						ComboSA.setFileError(true, 2);
						ComboSA.setFileError(true, 3);
					}
				}
			}

			if (currentState == state.saVoxRSSA
					|| currentState == state.saVoxSAIF) {
				TwoFieldsSA.setFile(
						Params.openFileChooser(Params.getImgFilter()), 2);
				if (TwoFieldsSA.checkMask()) {
					TwoFieldsSA.setFileError(false, 2);
					TwoFieldsSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(TwoFieldsSA.getFilePath(2),
								TwoFieldsSA.getFilePath(3));
					} catch (ErrorExc e) {
						TwoFieldsSA.setFileError(true, 2);
						TwoFieldsSA.setFileError(true, 3);
					}
				}
			}
			mainW.stopWaiting();
		}

		if (action == "FileChooserVoxMask") {
			mainW.startWaiting();
			if (currentState == state.ppVoxDecay) {
				Decay.setFile(Params.openFileChooser(Params.getImgFilter()), 2);
				if (Decay.checkMask()) {
					Decay.setFileError(false, 1);
					Decay.setFileError(false, 2);
					try {
						objManager.loadVoxel(Decay.getFilePath(1),
								Decay.getFilePath(2));
					} catch (ErrorExc e) {
						Decay.setFileError(true, 1);
						Decay.setFileError(true, 2);
					}
				}
			}
			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxVisualization) {
				Sum.setFile(Params.openFileChooser(Params.getImgFilter()), 2);
				if (Sum.checkMask()) {
					Sum.setFileError(false, 1);
					Sum.setFileError(false, 2);
					try {
						objManager.loadVoxel(Sum.getFilePath(1),
								Sum.getFilePath(2));
					} catch (ErrorExc e) {
						Sum.setFileError(true, 1);
						Sum.setFileError(true, 2);
					}
				}
			}

			if (currentState == state.saVoxStd) {
				ComboSA.setFile(Params.openFileChooser(Params.getImgFilter()),
						3);
				if (ComboSA.checkMask()) {
					ComboSA.setFileError(false, 2);
					ComboSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(ComboSA.getFilePath(2),
								ComboSA.getFilePath(3));
					} catch (ErrorExc e) {
						ComboSA.setFileError(true, 2);
						ComboSA.setFileError(true, 3);
					}
				}
			}

			if (currentState == state.saVoxRSSA
					|| currentState == state.saVoxSAIF) {
				TwoFieldsSA.setFile(
						Params.openFileChooser(Params.getImgFilter()), 3);
				if (TwoFieldsSA.checkMask()) {
					TwoFieldsSA.setFileError(false, 2);
					TwoFieldsSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(TwoFieldsSA.getFilePath(2),
								TwoFieldsSA.getFilePath(3));
					} catch (ErrorExc e) {
						TwoFieldsSA.setFileError(true, 2);
						TwoFieldsSA.setFileError(true, 3);
					}
				}
			}
			mainW.stopWaiting();
		} // end FileChooserVoxMask

		if (action == "openDirChooser") {
			mainW.startWaiting();
			String buff = Menu.openDirChooser();
			Menu.setDir(buff);
			Params.setPath(buff);
			if (SAParam.checkWorkspace(buff) == false)
				Menu.enableButtons(false);
			else
				Menu.enableButtons(true);
			mainW.stopWaiting();
		}
	}

	/**
	 * Set the correct Algorithm according to the currentState
	 */
	private void setAlgorithm() {
		switch (currentState) {
		case ppArtMod:
			((AlgorithmManager) objManager).setAlgorithm(new ModArterial());
			break;
		case ppArtDecay:
			((AlgorithmManager) objManager)
					.setAlgorithm(new DecayCorrectionArterial());
			break;
		case ppArtDelay:
			((AlgorithmManager) objManager)
					.setAlgorithm(new DelayCorrectionArterial());
			break;
		case ppRegDecay:
			((AlgorithmManager) objManager)
					.setAlgorithm(new DecayCorrectionROI());
			break;
		case ppVoxDecay:
			((AlgorithmManager) objManager)
					.setAlgorithm(new DecayCorrectionImage());
			break;
		case ppVoxSum:
			((AlgorithmManager) objManager).setAlgorithm(new SummedPET());
			break;
		case saRegStd:
			((AlgorithmManager) objManager).setAlgorithm(new RegionStandard());
			break;
		case saRegRSSA:
			((AlgorithmManager) objManager)
					.setAlgorithm(new RegionRankShaping());
			break;
		case saRegSAIF:
			((AlgorithmManager) objManager)
					.setAlgorithm(new RegionIterativeFiltering());
			break;
		case saVoxStd:
			((AlgorithmManager) objManager).setAlgorithm(new ImageStandard());
			break;
		case saVoxRSSA:
			((AlgorithmManager) objManager)
					.setAlgorithm(new ImageRankShaping());
			break;
		case saVoxSAIF:
			((AlgorithmManager) objManager)
					.setAlgorithm(new ImageIterativeFiltering());
			break;
		}
	}

	/**
	 * Set the correct Parameters according to the currentState
	 */

	public void setParams(String action) {
		if (action.equals("start")) {
			if (currentState == state.ppArtMod) {
				ModArtParam param = new ModArtParam();
				try {
					ArtMod.setParInError(false);
					param.setPar_In(ArtMod.getParIn());
				} catch (ErrorExc e) {
					ArtMod.setParInError(true);
					// mainW.setMainNotification(e.getMessage());
				}
				// try {
				param.setTypeOfAnalysis(ArtMod.getType());
				// }
				/*
				 * catch(ErrorExc e){ mainW.setMainNotification(e.getMessage()); }
				 */
				((AlgorithmManager) objManager).setParams(param);
			}

			if (currentState == state.ppArtDelay) {
				it.tetratron.pera.model.params.Delay param = new it.tetratron.pera.model.params.Delay();
				try {
					Delay.setDelayError(false);
					param.setDelay(Delay.getDelay());
				} catch (ErrorExc e) {
					Delay.setDelayError(true);
					// mainW.setMainNotification(e.getMessage());
				}
				((AlgorithmManager) objManager).setParams(param);
			}
			if (currentState == state.ppArtDecay
					|| currentState == state.ppRegDecay
					|| currentState == state.ppVoxDecay) {
				TypeOfCorrection param = new TypeOfCorrection();
				try {
					param.setTypeOfCorrection(Decay.getTypeOfCorrection());
				} catch (ErrorExc e) {
					mainW.setMainNotification(e.getMessage());
				}
				((AlgorithmManager) objManager).setParams(param);
			}

			if (currentState == state.saRegStd
					|| currentState == state.saVoxStd) {
				StdSAParam param = new StdSAParam();
				try {
					ComboSA.setNCompError(false);
					param.setNComp(ComboSA.getNComp());
				} catch (ErrorExc e) {
					ComboSA.setNCompError(true);
					System.out.println(e.getMessage());
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					ComboSA.setBeta1Error(false);
					param.setBeta1(ComboSA.getBeta1());
				} catch (ErrorExc e) {
					ComboSA.setBeta1Error(true);
					System.out.println(e.getMessage());
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					ComboSA.setBeta2Error(false);
					param.setBeta2(ComboSA.getBeta2());
				} catch (ErrorExc e) {
					ComboSA.setBeta2Error(true);
					System.out.println(e.getMessage());
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					param.setType(ComboSA.getType());
				} catch (ErrorExc e) {
					mainW.setMainNotification(e.getMessage());
				}
				try {
					param.setWorkspace_Dir(Params.getPath());
				} catch (ErrorExc e) {
					// System.out.println(e.getMessage());
					mainW.setMainNotification(e.getMessage());
				}
				((AlgorithmManager) objManager).setParams(param);

			}
			if (currentState == state.saRegRSSA
					|| currentState == state.saVoxRSSA) {
				RSSAParam param = new RSSAParam();
				try {
					TwoFieldsSA.setNCompError(false);
					param.setNComp(TwoFieldsSA.getNComp());
				} catch (ErrorExc e) {
					TwoFieldsSA.setNCompError(true);
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					TwoFieldsSA.setBeta1Error(false);
					param.setBeta1(TwoFieldsSA.getBeta1());
				} catch (ErrorExc e) {
					TwoFieldsSA.setBeta1Error(true);
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					TwoFieldsSA.setBeta2Error(false);
					param.setBeta2(TwoFieldsSA.getBeta2());
				} catch (ErrorExc e) {
					TwoFieldsSA.setBeta2Error(true);
					// mainW.setMainNotification(e.getMessage());
				}
				double[] buff = TwoFieldsSA.getTwoFields();
				try {
					TwoFieldsSA.setTwoFieldsError(false);
					param.setSNR(buff[0], buff[1]);
				} catch (ErrorExc e) {
					TwoFieldsSA.setTwoFieldsError(true);
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					param.setWorkspace_Dir(Params.getPath());
				} catch (ErrorExc e) {
					mainW.setMainNotification(e.getMessage());
				}
				((AlgorithmManager) objManager).setParams(param);

			}
			if (currentState == state.saRegSAIF
					|| currentState == state.saVoxSAIF) {
				SAIFParam param = new SAIFParam();
				try {
					TwoFieldsSA.setNCompError(false);
					param.setNComp(TwoFieldsSA.getNComp());
				} catch (ErrorExc e) {
					TwoFieldsSA.setNCompError(true);
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					TwoFieldsSA.setBeta1Error(false);
					param.setBeta1(TwoFieldsSA.getBeta1());
				} catch (ErrorExc e) {
					TwoFieldsSA.setBeta1Error(true);
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					TwoFieldsSA.setBeta2Error(false);
					param.setBeta2(TwoFieldsSA.getBeta2());
				} catch (ErrorExc e) {
					TwoFieldsSA.setBeta2Error(true);
					// mainW.setMainNotification(e.getMessage());
				}
				double[] buff = TwoFieldsSA.getTwoFields();
				try {
					TwoFieldsSA.setTwoFieldsError(false);
					param.setPassband(buff[0], buff[1]);
				} catch (ErrorExc e) {
					TwoFieldsSA.setTwoFieldsError(true);
					// mainW.setMainNotification(e.getMessage());
				}
				try {
					param.setWorkspace_Dir(Params.getPath());
				} catch (ErrorExc e) {
					mainW.setMainNotification(e.getMessage());
				}
				((AlgorithmManager) objManager).setParams(param);
			}
		}
	}

	/**
	 * Add an Arterial Chart for preview.
	 * @throws ErrorExc
	 */
	
	private void addArtChart() throws ErrorExc {
		Params.removeChart();
		SplittableChart chart = new SplittableChart();
		chart.setNumber(1);
		chart.showSave(false);
		
		chart.addData(objManager.getArterial().getPlasmaLabel(), 
				objManager.getArterial().getTime_plasma(),
				objManager.getArterial().getPlasma(), 
				objManager.getArterial().getTimeLabel(), 
				objManager.getArterial().getPlasmaLabel(),
				0);
		
		chart.addData(objManager.getArterial().getBloodLabel(), 
				objManager.getArterial().getTime_plasma(), 
				objManager.getArterial().getBlood(), 
				objManager.getArterial().getTimeLabel(), 
				objManager.getArterial().getBloodLabel(),
				0);
		chart.addListner(this);
		Params.setChart(chart);
	}

	/**
	 * Add an Region Chart for preview.
	 * @throws ErrorExc
	 */
	
	private void addRegChart() throws ErrorExc {

		Params.removeChart();
		Chart c = new Chart();
		c.setTitle(objManager.getRegion().getNSDLabel());
		c.setAxis(objManager.getRegion().getTimeLabel(), objManager.getRegion()
				.getNSDLabel());
		c.addCollection(objManager.getRegion().getNSDLabel(), objManager
				.getRegion().getTime_pet(), objManager.getRegion().getNsd());
		c.updateChart();
		Params.addChart(c);
		c = new Chart();
		c.setTitle(objManager.getRegion().getDeltaLabel());
		c.setAxis(objManager.getRegion().getTimeLabel(), objManager.getRegion()
				.getDeltaLabel());
		c.addCollection(objManager.getRegion().getDeltaLabel(), objManager
				.getRegion().getTime_pet(), objManager.getRegion().getDelta());
		c.updateChart();
		Params.addChart(c);

		SplittableChart chart = new SplittableChart();
		chart.setNumber(1);
		chart.showSave(false);
		int x = 0;
		for (Iterator<List<Double>> i = objManager.getRegion().getRoi()
				.iterator(); i.hasNext();) {
			String s = objManager.getRegion().getRoiLabel()[x];
			chart.addData(s, 
					objManager.getRegion().getTime_pet(), 
					i.next(), 
					objManager.getRegion().getTimeLabel(), 
					s,
					0);
			x++;
		}
		chart.addListner(this);
		Params.addChart(chart);
	}

	// Document listener
	@Override
	public void changedUpdate(DocumentEvent e) {
		if (e.getDocument().getProperty("parent") instanceof JTextField) {
			JTextField textField = (JTextField) e.getDocument().getProperty(
					"parent");
			errors(textField.getName());
		}
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		if (e.getDocument().getProperty("parent") instanceof JTextField) {
			JTextField textField = (JTextField) e.getDocument().getProperty(
					"parent");
			errors(textField.getName());
		}
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		if (e.getDocument().getProperty("parent") instanceof JTextField) {
			JTextField textField = (JTextField) e.getDocument().getProperty(
					"parent");
			errors(textField.getName());
		}
	}

	/**
	 * Sets the error to be displayed in the view
	 * @throws ErrorExc
	 */ 
	
	private void errors(String name) {

		if (name.equals("Dir")) {
			String buff = Menu.getDir();
			Params.setPath(buff);
			if (SAParam.checkWorkspace(buff) == false)
				Menu.enableButtons(false);
			else
				Menu.enableButtons(true);
		}

		if (name.equals("Art")) {
			if (currentState == state.ppArtMod) {
				ArtMod.setFileError(false);
				try {
					objManager.loadArterial(ArtMod.getFilePath());
					addArtChart();
				} catch (ErrorExc err) {
					ArtMod.setFileError(true);
					Params.removeChart();
				}
			}
			if (currentState == state.ppArtDecay) {
				Decay.setFileError(false, 0);
				try {
					objManager.loadArterial(Decay.getFilePath(0));
					addArtChart();
				} catch (ErrorExc err) {
					Decay.setFileError(true, 0);
					Params.removeChart();
				}
			}
			if (currentState == state.ppArtDelay) {
				Delay.setFileError(false);
				try {
					objManager.loadArterial(Delay.getFilePath());
					addArtChart();
				} catch (ErrorExc err) {
					Delay.setFileError(true);
					Params.removeChart();
				}
			}
			if (currentState == state.saRegStd
					|| currentState == state.saVoxStd) {
				ComboSA.setFileError(false, 0);
				try {
					objManager.loadArterial(ComboSA.getFilePath(0));
				} catch (ErrorExc err) {
					ComboSA.setFileError(true, 0);
				}
			}
			if (currentState == state.saRegRSSA
					|| currentState == state.saVoxRSSA
					|| currentState == state.saRegSAIF
					|| currentState == state.saVoxSAIF) {
				TwoFieldsSA.setFileError(false, 0);
				try {
					objManager.loadArterial(TwoFieldsSA.getFilePath(0));
				} catch (ErrorExc err) {
					TwoFieldsSA.setFileError(true, 0);
				}
			}
		}

		else if (name.equals("Reg")) {
			if (currentState == state.ppVoxDecay) {
				Decay.setFileError(false, 0);
				try {
					objManager.loadRegion(Decay.getFilePath(0));
				} catch (ErrorExc err) {
					Decay.setFileError(true, 0);
				}
			}
			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxVisualization) {
				Sum.setFileError(false, 0);
				try {
					objManager.loadRegion(Sum.getFilePath(0));
				} catch (ErrorExc err) {
					Sum.setFileError(true, 0);
				}
			}
			if (currentState == state.ppRegDecay) {
				Decay.setFileError(false, 0);
				try {
					objManager.loadRegion(Decay.getFilePath(0));
					addRegChart();
				} catch (ErrorExc err) {
					Decay.setFileError(true, 0);
					Params.removeChart();
				}
			}
			if (currentState == state.saRegStd
					|| currentState == state.saVoxStd) {
				ComboSA.setFileError(false, 1);
				try {
					objManager.loadRegion(ComboSA.getFilePath(1));
				} catch (ErrorExc err) {
					ComboSA.setFileError(true, 1);
				}
			}
			if (currentState == state.saRegRSSA
					|| currentState == state.saVoxRSSA
					|| currentState == state.saRegSAIF
					|| currentState == state.saVoxSAIF) {
				TwoFieldsSA.setFileError(false, 1);
				try {
					objManager.loadRegion(TwoFieldsSA.getFilePath(1));
				} catch (ErrorExc err) {
					TwoFieldsSA.setFileError(true, 1);
				}
			}
		}

		else if (name.equals("Vox")) {
			if (currentState == state.ppVoxDecay) {
				if (Decay.checkMask()) {
					Decay.setFileError(false, 1);
					Decay.setFileError(false, 2);
					try {
						objManager.loadVoxel(Decay.getFilePath(1),
								Decay.getFilePath(2));
					} catch (ErrorExc err) {
						Decay.setFileError(true, 1);
						Decay.setFileError(true, 2);
					}
				}
			}

			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxVisualization) {
				if (Sum.checkMask()) {
					Sum.setFileError(false, 1);
					Sum.setFileError(false, 2);
					try {
						objManager.loadVoxel(Sum.getFilePath(1),
								Sum.getFilePath(2));
					} catch (ErrorExc err) {
						Sum.setFileError(true, 1);
						Sum.setFileError(true, 2);
					}
				}
			}

			if (currentState == state.saVoxStd) {
				if (ComboSA.checkMask()) {
					ComboSA.setFileError(false, 2);
					ComboSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(ComboSA.getFilePath(2),
								ComboSA.getFilePath(3));
					} catch (ErrorExc err) {
						ComboSA.setFileError(true, 2);
						ComboSA.setFileError(true, 3);
					}
				}
			}

			if (currentState == state.saVoxRSSA
					|| currentState == state.saVoxSAIF) {
				if (TwoFieldsSA.checkMask()) {
					TwoFieldsSA.setFileError(false, 2);
					TwoFieldsSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(TwoFieldsSA.getFilePath(2),
								TwoFieldsSA.getFilePath(3));
					} catch (ErrorExc err) {
						TwoFieldsSA.setFileError(true, 2);
						TwoFieldsSA.setFileError(true, 3);
					}
				}
			}
		}

		else if (name.equals("Mask")) {
			if (currentState == state.ppVoxDecay) {
				if (Decay.checkMask()) {
					Decay.setFileError(false, 1);
					Decay.setFileError(false, 2);
					try {
						objManager.loadVoxel(Decay.getFilePath(1),
								Decay.getFilePath(2));
					} catch (ErrorExc err) {
						Decay.setFileError(true, 1);
						Decay.setFileError(true, 2);
					}
				}
			}
			if (currentState == state.ppVoxSum
					|| currentState == state.ppVoxVisualization) {
				if (Sum.checkMask()) {
					Sum.setFileError(false, 1);
					Sum.setFileError(false, 2);
					try {
						objManager.loadVoxel(Sum.getFilePath(1),
								Sum.getFilePath(2));
					} catch (ErrorExc err) {
						Sum.setFileError(true, 1);
						Sum.setFileError(true, 2);
					}
				}
			}

			if (currentState == state.saVoxStd) {
				if (ComboSA.checkMask()) {
					ComboSA.setFileError(false, 2);
					ComboSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(ComboSA.getFilePath(2),
								ComboSA.getFilePath(3));
					} catch (ErrorExc err) {
						ComboSA.setFileError(true, 2);
						ComboSA.setFileError(true, 3);
					}
				}
			}

			if (currentState == state.saVoxRSSA
					|| currentState == state.saVoxSAIF) {
				if (TwoFieldsSA.checkMask()) {
					TwoFieldsSA.setFileError(false, 2);
					TwoFieldsSA.setFileError(false, 3);
					try {
						objManager.loadVoxel(TwoFieldsSA.getFilePath(2),
								TwoFieldsSA.getFilePath(3));
					} catch (ErrorExc err) {
						TwoFieldsSA.setFileError(true, 2);
						TwoFieldsSA.setFileError(true, 3);
					}
				}
			}
		}

		if (currentState == state.saRegRSSA || currentState == state.saVoxRSSA
				|| currentState == state.saRegSAIF
				|| currentState == state.saVoxSAIF) {
			if (name.equals("beta1"))
				TwoFieldsSA.setTwoFields(0, TwoFieldsSA.getBeta1() * 10);
			if (name.equals("beta2"))
				TwoFieldsSA.setTwoFields(1, TwoFieldsSA.getBeta2() / 2);
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		fixImageViewerChart();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(e.getSource() instanceof JButton)
			mainW.setNotification(LocaleManager.getString("Tip" + ((JButton)e.getSource()).getActionCommand()));
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		mainW.restoreNotification();
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseDragged(MouseEvent e1) {
		fixImageViewerChart();
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void stateChanged(ChangeEvent e) {
		fixImageViewerChart();
	}
		
	
	private void fixImageViewerChart(){
		if(currentState==state.raVoxel)
			if(objManager.getSakeVoxel()!=null && image != null)
				try{
					int x = image.getXCoord();
					int y = image.getYCoord();
					int z = image.getZCoord();
					image.fixChart(objManager.getSakeVoxel().getFit().getTmcrs(x, y, z),
							objManager.getSakeVoxel().getRawVoxel().getTmcrs(image.getXCoord(), image.getYCoord(), image.getZCoord()),
							objManager.getSakeVoxel().getRegionObj().getTime_pet()
							);
				}catch(ErrorExc e2){mainW.setMainNotification(e2.getMessage());}}
	
}
