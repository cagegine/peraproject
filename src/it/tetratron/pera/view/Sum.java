/*
/////////////////////////
Pera.java;
a class to draw a form for the insertion of the input of the files for the Summed PET algorithm;
it.tetratron.pera.view;
GLP 3;
Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

/**
*
* Class to draw and manage the insert parameters form for summed PET algorithm
*
*/
public class Sum {
	
	public static JPanel[] inputFile;
	
	/**
	 * Draw a form for the input of the files
	 * @param input a matrix with 2 columns in witch the first will be the text of the labels for the input and the second is the action of the browse buttons
	 * @param al a listener for the actions performed
	 * @param dl a listener for changes on the text fields
	 * @return return a JPanel
	 */
	public static JPanel draw(String[][] input, ActionListener al, DocumentListener dl){
		
		JPanel aux = new RoundedPanel();
		aux.setLayout(new GridBagLayout());
		
		inputFile = new JPanel[input.length];
		
		for (int i=0; i<input.length; i++){
			inputFile[i] = Params.inputFile(input[i][0], input[i][1], input[i][2], al, dl);
			aux.add(inputFile[i],new GridBagConstraints(0,i,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0),i);
		}
		
		return aux;
	}
	
	/**
	 * This method set a string to the field of the chosen inputFile panel the given text 
	 * the index of the first inputFile panel is 0 and it goes to increase by one for each inputFile panel in the form
	 * @param s the text to set to the field
	 * @param i the index of the chosen inputFile panel
	 */
	public static void setFile(String s, int i){
		if (inputFile[i].getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputFile[i].getComponent(0);
			tf.setText(s);
		}
	}
	
	/**
	 * Disable the file chooser and set to the text field the text "Already loaded.."
	 * @param c the document listener that need to been removed from the text field 
	 */
	public static void alreadyLoadedFiles(DocumentListener c){
		for (int i=0; i<inputFile.length; i++){
			if (inputFile[i].getComponent(0) instanceof JTextField){
				JTextField tf = (JTextField) inputFile[i].getComponent(0);
				tf.getDocument().removeDocumentListener(c);
				tf.setFont(new Font(Font.SANS_SERIF,Font.BOLD,13));
				tf.setText("Already loaded..");
				tf.setEnabled(false);
				inputFile[i].getComponent(1).setEnabled(false);
			}
		}
	}
	
	/**
	 * Check if the voxel file and also the mask file has been selected
	 * @return true if both files has been selected else return false
	 */
	public static boolean checkMask(){
		if (getFilePath(1).equals("") || getFilePath(2).equals(""))
			return false;
		return true;
	}

	
	/**
	 * Set red color to the elements in the chosen inputFile panel
	 * @param i the index of the chosen inputFile panel
	 */
	public static void setFileError(boolean t, int i){
		if (inputFile[i].getComponent(0) instanceof JTextField && inputFile[i].getComponent(2) instanceof JLabel){
			JTextField tf = (JTextField) inputFile[i].getComponent(0);
			JLabel l = (JLabel) inputFile[i].getComponent(2);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}
	
	/**
	 * Return the path of the file in the chosen inputFile panel
	 * @param i the index of the chosen inputFile panel
	 * @return a String that contain the path of the selected file
	 */
	public static String getFilePath(int i){
		if (inputFile[i].getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputFile[i].getComponent(0);
			return tf.getText();
		}
		return "";
	}

}
