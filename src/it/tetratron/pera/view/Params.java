/*
/////////////////////////
Pera.java;
a class to draw a the interface to insert parameters;
it.tetratron.pera.view;
GLP 3;
Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

/**
 * Class to draw a form for the params insertions and to select what actions will be made on the loaded data  
 *
 */
public class Params {



	private static String path="";
	private static String pathFile="";
	
	private static JButton[] selectAlgs;
	private static JButton[] rightButtons;
	private static JPanel params;
	private static JPanel previewChart;
	
	/**
	 * Draw a JPanel that contain the form for the selection of the algorithm,
	 * the insertion of the parameters and the buttons to perform actions on the input data
	 * @param buttons a matrix with 2 columns in witch the first is the text that will be write on button and the second is the action of these buttons
	 * @param save if it's true the save button will be shown, otherwise it will be set not visible
	 * @param back if it's true the undo button will be shown, otherwise it will be set not visible
	 * @param params the input form for files and parameters
	 * @param al the controller
	 * @return return a JPanel
	 */
	public static JPanel draw(String[][] buttons, boolean iteration, boolean save, boolean back, JPanel p, ActionListener al){
		
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		Insets margin = new Insets(5,5,5,5);		
		
		selectAlgs = new JButton[buttons.length];
		rightButtons = new JButton[5];
		params=p;
		
		JPanel leftTopPane = new RoundedPanel();
		JButton b;
		for (int i=0; i<buttons.length; i++){
			b=getButton("orange");
			b.setHorizontalTextPosition(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setBorderPainted(false);
			b.setContentAreaFilled(false);
			b.setForeground(Color.WHITE);
			b.setText(buttons[i][0]);
			b.setActionCommand(buttons[i][1]);
			b.addActionListener(al);
			b.addMouseListener((MouseListener) al);
			selectAlgs[i]=b;
			leftTopPane.add(b,new GridBagConstraints(i,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0));
		}
		
		JPanel rightPane = new RoundedPanel();
		rightPane.setLayout(new GridBagLayout());
		
		b=new JButton();
		b=getButton("red");
		b.setHorizontalTextPosition(SwingConstants.CENTER);
		b.setOpaque(false);
		b.setBorderPainted(false);
		b.setContentAreaFilled(false);
		b.setForeground(Color.WHITE);
		b.setActionCommand("exit");
		b.setText("Exit to Main Menu");
		b.addActionListener(al);
		b.addMouseListener((MouseListener) al);
		rightButtons[0]=b;
		rightPane.add(b,new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0),0);
		
		if (iteration){
			b=new JButton();
			b=getButton("green_small");
			b.setHorizontalTextPosition(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setBorderPainted(false);
			b.setContentAreaFilled(false);
			b.setForeground(Color.WHITE);
			b.setActionCommand("new");
			b.setText("New Elaboration");
			b.addActionListener(al);
			b.addMouseListener((MouseListener) al);
			rightButtons[1]=b;
			rightPane.add(b,new GridBagConstraints(0,1,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0),1);
		}
		
		if (save){
			b=new JButton();
			b=getButton("save");
			b.setHorizontalTextPosition(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setBorderPainted(false);
			b.setContentAreaFilled(false);
			b.setForeground(Color.WHITE);
			b.setActionCommand("save");
			b.setText("    Save");
			b.addActionListener(al);
			b.addMouseListener((MouseListener) al);
			rightButtons[2]=b;
			rightPane.add(b,new GridBagConstraints(0,3,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0),2);
		}
		
		if (back){
			b=new JButton();
			b=getButton("undo");
			b.setHorizontalTextPosition(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setBorderPainted(false);
			b.setContentAreaFilled(false);
			b.setForeground(Color.WHITE);
			b.setActionCommand("undo");
			b.setText("    Undo");
			b.addActionListener(al);
			b.addMouseListener((MouseListener) al);
			rightButtons[3]=b;
			rightPane.add(b,new GridBagConstraints(0,4,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0),3);
		}
	
		b=new JButton();
		b=getButton("play");
		b.setHorizontalTextPosition(SwingConstants.CENTER);
		b.setOpaque(false);
		b.setBorderPainted(false);
		b.setContentAreaFilled(false);
		b.setForeground(Color.WHITE);
		b.setActionCommand("start");
		b.setText("    Start");
		b.addActionListener(al);
		b.addMouseListener((MouseListener) al);
		rightButtons[4]=b;
		rightPane.add(b,new GridBagConstraints(0,5,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0),4);
		
		rightPane.add(Box.createGlue(),new GridBagConstraints(0,2,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0));
		
		setSelectedButton(0);
		
		previewChart = new RoundedPanel();
		previewChart.setLayout(new GridBagLayout());
		previewChart.setVisible(false);
		
		aux.add(leftTopPane, new GridBagConstraints(1,0,1,1,1.0,0,GridBagConstraints.CENTER,GridBagConstraints.BOTH,margin,0,0),0);
		aux.add(rightPane, new GridBagConstraints(3,0,1,4,0,1.0,GridBagConstraints.CENTER,GridBagConstraints.VERTICAL,margin,0,0),1);
		aux.add(params, new GridBagConstraints(1,1,1,1,1.0,0,GridBagConstraints.CENTER,GridBagConstraints.BOTH,margin,0,0),2);
		//aux.add(new JSplitPane(JSplitPane.VERTICAL_SPLIT,params,previewChart), new GridBagConstraints(0,1,3,2,1.0,0,GridBagConstraints.CENTER,GridBagConstraints.BOTH,margin,0,0),2);
		aux.add(previewChart, new GridBagConstraints(0,2,3,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.BOTH,margin,10,10),3);
		
		aux.add(Box.createGlue(), new GridBagConstraints(0,0,1,3,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0));
		aux.add(Box.createGlue(), new GridBagConstraints(2,0,1,3,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0));
		return aux;

	}

	/**
	 * Disable the button selected in the right panel
	 * @param s the action of the button that will be disabled
	 */
	public static void disableParamsButton(String s){
		for (int i=0; i< rightButtons.length; i++)
			if (rightButtons[i].getActionCommand()==s)
				rightButtons[i].setEnabled(false);
	}
	
	/**
	 * Hide the button selected in the right panel
	 * @param s the action of the button that will be set as invisible
	 */
	public static void hideParamsButton(String s){
		for (int i=0; i< rightButtons.length; i++)
			if (rightButtons[i].getActionCommand()==s)
				rightButtons[i].setVisible(false);
		
	}
	
	/**
	 * Open a file chooser
	 * @param ff what extension will be accepted 
	 * @return a String with the path of the file selected
	 */
	public static String openFileChooser(FileFilter ff){
		JFileChooser fc;
		if (pathFile!="")
			fc = new JFileChooser(pathFile);
		else
			fc = new JFileChooser();
		fc.setFileFilter(ff);
		int returnVal = fc.showOpenDialog(null);		 
		if (returnVal == JFileChooser.APPROVE_OPTION) {		 
//			path=fc.getSelectedFile().getAbsolutePath();
			if (fc.getSelectedFile().getPath().lastIndexOf(System.getProperty("file.separator")) > 0)
				pathFile=fc.getSelectedFile().getPath().substring(0,fc.getSelectedFile().getPath().lastIndexOf(System.getProperty("file.separator")));
			return fc.getSelectedFile().getPath(); 	 
		}
		return "";
	}
	
	/**
	 * Open a file chooser to save the current file
	 * @return a String with the path of the saved selected
	 */
	public static String openSaveChooser(){
		JFileChooser fileChooser;
		if (pathFile!="")
			fileChooser = new JFileChooser(pathFile);
		else{
		fileChooser = new JFileChooser(System.getProperty("user.dir")){
			private static final long serialVersionUID = 1L;

			@Override
			public void approveSelection(){
			    File f = getSelectedFile();
			    if(f.exists() && getDialogType() == SAVE_DIALOG){
			        int result = JOptionPane.showConfirmDialog(this,"The file already exists, overwrite?","Existing file",JOptionPane.YES_NO_CANCEL_OPTION);
			        switch(result){
			            case JOptionPane.YES_OPTION:
			                super.approveSelection();
			                return;
			            case JOptionPane.NO_OPTION:
			                return;
			            case JOptionPane.CANCEL_OPTION:
			                return;
			        }
			    }
			    super.approveSelection();
			}};
		}
	    fileChooser.showSaveDialog(null);
	    String fileToSavePath="";
	    
	    if (fileChooser.getSelectedFile()==null)
	    	return fileToSavePath;
	    fileToSavePath=fileChooser.getSelectedFile().toString();
		return fileToSavePath;  
    }
	
	
	
	/**
	 * Draw a JPanel that contain one label, one textField and on button to open the file chooser 
	 * the text field have index 0 and the button have index 1
	 * @param txtLabel the text of the label
	 * @param action the action of the button
	 * @param al a listener for the actions performed
	 * @param dl a listener for changes on the text fields
	 * @return return a JPanel
	 */
	public static JPanel inputFile(String txtLabel, String action, String id, ActionListener al, DocumentListener dl){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		
		JLabel label = new JLabel(txtLabel);
		
		JTextField text = new JTextField();
		text.setColumns(30);
		//text.setMinimumSize(text.getSize());
		if (action!=null){
			text.getDocument().addDocumentListener(dl);
			text.getDocument().putProperty("parent", text);
			text.setName(id);
		}
		text.setDisabledTextColor(new Color(17,96,98));
		//text.setEditable(false);
		
		JButton b = new JButton("browse..");
		b.setActionCommand(action);
		b.addActionListener(al);
		
		aux.add(text,new GridBagConstraints(1,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(0,1,0,1),0,0),0);
		aux.add(b,new GridBagConstraints(2,0,1,1,0,1.0,GridBagConstraints.LINE_END,GridBagConstraints.NONE,new Insets(0,1,0,0),0,0),1);
		aux.add(label,new GridBagConstraints(0,0,1,1,0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.NONE,new Insets(0,0,0,1),0,0),2);
		return aux;
	}
	
	/**
	 * Draw a panel that contain the fields to insert the parIn parameters
	 * @return return a JPanel
	 */
	public static JPanel parIn(){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		
		String[] s = new String[] {"A1","A2","A3","B1","B2","B3"};
		
		JTextField text;
		
		for (int i=0; i<s.length; i++){
			text = new JTextField();
			text.setColumns(5);
			text.setDocument(onlyDecimals());
			aux.add(text, new GridBagConstraints(1+(i%3)*2,(int)(1+i/3),1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(3,3,3,3),0,0), i);			
		}
		
		for (int i=0; i<s.length; i++){
			aux.add(new JLabel(s[i]), new GridBagConstraints((i%3)*2,(int)(1+i/3),1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(3,3,3,3),0,0),6+i);			
		}
		
		aux.add(new JLabel("parIn"), new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(3,3,3,3),0,0),12);
		
		return aux;
	}
	
	
	/**
	 * Draw a JPanel containing a label and a text field of 5 columns
	 * the text field have index 0
	 * @param txtLabel the text of the label
	 * @param decimal if it is true decimal numbers will be accepted, else only integer numbers will be accepted
	 * @param id the name to set to the text field
	 * @param dl a listener for changes on the text fields
	 * @return return a JPanel
	 */
	public static JPanel oneField(String txtLabel, boolean decimal, String id, DocumentListener dl){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		
		JTextField text = new JTextField();
		text.setColumns(5);
		
		if (decimal)
			text.setDocument(onlyDecimals());
		else
			text.setDocument(onlyNumbers());
		
		if (dl!=null){
			text.getDocument().addDocumentListener(dl);
			text.getDocument().putProperty("parent", text);
			text.setName(id);
		}
		
		aux.add(text, new GridBagConstraints(1,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,5,0,0),0,0),0);
		aux.add(new JLabel(txtLabel), new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,0,0,5),0,0));
		aux.add(Box.createGlue(), new GridBagConstraints(2,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0),0,0));
		return aux;
	}
	
	/**
	 * Draw a JPanel containing a label and two text fields of 5 columns
	 * @param txtLabel the text of the label
	 * @return return a JPanel
	 */
	public static JPanel twoFields(String txtLabel){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		
		JTextField text = new JTextField();
		text.setColumns(5);
		text.setDocument(onlyDecimals());
		aux.add(text, new GridBagConstraints(1,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,5,0,0),0,0),0);
		text = new JTextField();
		text.setColumns(5);
		text.setDocument(onlyDecimals());
		aux.add(text, new GridBagConstraints(2,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,5,0,0),0,0),1);
		aux.add(new JLabel(txtLabel), new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,0,0,5),0,0));
		//aux.add(Box.createGlue(), new GridBagConstraints(3,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0),0,0));
		return aux;
	}

	/**
	 * Draw a JPanel containing a label and a comboBox
	 * @param txtLabel the text of the label
	 * @param item an array containing all the item that will be added to the comboBox
	 * @return return a JPanel
	 */
	public static JPanel comboBox(String txtLabel, String[] item){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		
		JComboBox combo = new JComboBox();
		for (int i=0; i<item.length; i++){
			combo.addItem(item[i]);
		}
		
		aux.add(combo, new GridBagConstraints(1,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,5,0,0),0,0),0);
		aux.add(new JLabel(txtLabel), new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,0,0,5),0,0));
		aux.add(Box.createGlue(), new GridBagConstraints(2,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0),0,0));
		
		return aux;
	}
	
	/**
	 * @return return a TxtFilter
	 */
	public static TxtFilter getTxtFilter(){
		Params p = new Params();	
		return p.new TxtFilter();
	}
	
	/**
	 * @return return a ImgFilter
	 */
	public static ImgFilter getImgFilter(){
		Params p = new Params();	
		return p.new ImgFilter();
	}
	
	/**
	 * @return return a SakeFilter
	 */
	public static SakeFilter getSakeFilter(){
		Params p = new Params();	
		return p.new SakeFilter();
	}
	
	/**
	 * A document that accept only integer
	 * @return an integerDocument
	 */
	public static integerDocument onlyNumbers(){
		Params p = new Params();	
		return p.new integerDocument();
	}
	
	/**
	 * A document that accept only double
	 * @return an doubleDocument
	 */
	public static doubleDocument onlyDecimals(){
		Params p = new Params();	
		return p.new doubleDocument();
	}
	
	/**
	 * Get the extension of the file
	 * @param f the file from witch get the extension
	 * @return return a String
	 */
	private static String getExtension(File f) {
		String ext = null;
		String s = f.getName();
		int i = s.lastIndexOf('.');

		if (i > 0 && i < s.length() - 1) {
			ext = s.substring(i + 1).toLowerCase();
		}
		return ext;
	}
	
	/**
	 * Control if the extension of the given path is conform to the given extension and return the correct path
	 * @param path the given path
	 * @param ext the given extension
	 * @return a String with the correct path
	 */
	public static String checkExtension(String path,String ext){
		
		int dot=path.lastIndexOf('.');
		if(dot>=0)
		{
			String x=path.substring(dot);
			x=x.toLowerCase();
			ext=ext.toLowerCase();
			if(!(x.equals(ext)))
				path= path + ext;
			return path;
		}
		return path+ext;
	}
	
	/**
	 * @return the form for the input of parameters
	 */
	public static JPanel getParams(){
		return params;
	}
	
	/**
	 * Return the path of the path of the last chosen directory
	 * @return
	 */
	public static String getPath(){
		return path;
	}
	
	/**
	 * Set the given path to the variable path
	 * @param p the given path
	 */
	public static void setPath(String p){
		path=p;
	}
	
	/**
	 * Return a graphic button
	 * @param s the type of button chosen
	 * @return a JButton 
	 */
	public static JButton getButton(String s){
		JButton b = new JButton();
		if (s.equals("green_very_small")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/button_green.png").getImage().getScaledInstance(100, 30, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,10));
		}
		if (s.equals("credits")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/credits.png").getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH))));
		}
		if (s.equals("green")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/button_green.png").getImage().getScaledInstance(200, 75, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,20));
		}
		if (s.equals("green_small")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/button_green.png").getImage().getScaledInstance(150, 50, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,12));
		}
		if (s.equals("orange")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/button_green.png").getImage().getScaledInstance(200, 50, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,12));	
			b.setDisabledIcon(new ImageIcon((new ImageIcon("./img/button_orange.png").getImage().getScaledInstance(200, 50, Image.SCALE_SMOOTH))));
		}
		if (s.equals("red")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/button_red.png").getImage().getScaledInstance(150, 50, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,12));	
		}
		if (s.equals("small_red")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/button_red.png").getImage().getScaledInstance(100, 30, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,10                                   ));
		}
		if (s.equals("save")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/save_button.png").getImage().getScaledInstance(150, 50, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,12));
		}
		if (s.equals("undo")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/undo_button.png").getImage().getScaledInstance(150, 50, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,12));
		}
		if (s.equals("play")){
			b.setIcon(new ImageIcon((new ImageIcon("./img/play_button.png").getImage().getScaledInstance(150, 50, Image.SCALE_SMOOTH))));
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC | Font.BOLD,12));
		}
		return b;
	}
	/**
	 * Set disabled the selected button 
	 * @param i the index of the chosen button
	 */
	public static void setSelectedButton(int i){
		for (int x=0; x<selectAlgs.length; x++)
			selectAlgs[x].setEnabled(true);
		selectAlgs[i].setEnabled(false);
	}
	
	/**
	 * Set a component into the preview area
	 * @param c the chart
	 */
	public static void setChart(Component c){
		previewChart.add(c, new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,5,10,10),0,0),0);
		previewChart.setVisible(true);
	}
	
	/**
	 * Add a component to the preview area
	 * @param c the chart
	 */
	public static void addChart(Component c){
		if (previewChart.getComponentCount()<2)
			previewChart.add(c, new GridBagConstraints(0,previewChart.getComponentCount(),1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,5,10,10),0,0),0);
		else
			previewChart.add(c, new GridBagConstraints(previewChart.getComponentCount(),0,1,2,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5,5,10,10),0,0),0);
		previewChart.setVisible(true);
	}
	
	/**
	 * Remove all the components in the preview area
	 */
	public static void removeChart(){
		previewChart.removeAll();
		previewChart.setVisible(false);
	}
	
	// inner classes
	/**
	 * Class to filter only .txt files in JFileChooser
	 */
	private class TxtFilter extends FileFilter {

		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			String extension = getExtension(f);
			if (extension != null && extension.equals("txt"))
				return true;
			return false;
		}

		@Override
		public String getDescription() {
			return "*.txt";
		}
	}
	
	/**
	 * Class to filter only .nii, .img and .hdr files in JFileChooser
	 * 
	 */
	private class ImgFilter extends FileFilter {

		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			String extension = getExtension(f);
			if (extension != null) {

				if (extension.equals("nii") || extension.equals("img") || extension.equals("hdr"))
					return true;
			}
			return false;
		}

		@Override
		public String getDescription() {
			return "*.nii, *.img, *.hdr";
		}
	}
	
	/**
	 *Class to filter only .nii, .img and .hdr files in JFileChooser
	 * 
	 */
	private class SakeFilter extends FileFilter {

		@Override
		public boolean accept(File f) {
			if (f.isDirectory()) {
				return true;
			}
			String extension = getExtension(f);
			if (extension != null) {

				if (extension.equals("sake"))
					return true;
			}
			return false;
		}

		@Override
		public String getDescription() {
			return "*.sake";
		}
	}
	
	/**
	 * A PlainDocument that accept only strings in integer format
	 *
	 */
	private class integerDocument extends PlainDocument {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
	        if(str!=null && str.matches("[-]?[\\d]*") && !str.matches("[-].*[-]"))
	            super.insertString(offs, str, a);
	    }
	}

	/**
	 * A PlainDocument that accept only strings in double format
	 *
	 */
	private class doubleDocument extends PlainDocument {
	    /**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
	    	
	    	if (super.getText(0, super.getLength()).matches("[-]?[\\d]*[\\.][\\d]*") && !str.matches("[-].*[-]")){
		        if(str!=null && str.matches("[-]?[\\d]*") && !str.matches("[-].*[-]"))
		            super.insertString(offs, str, a);
	    	}
	    	else if(str!=null && str.matches("[-]?[\\d]*[\\.]?[\\d]*") && !str.matches("[-].*[-]")){
		            super.insertString(offs, str, a);
	    	}
	    	else {
	    		if(str!=null && str.matches("[-]?[\\d]*[\\.]?[\\d]*E-[\\d]") && !str.matches("[-].*[-]"))
		            super.insertString(offs, str, a);
	    	}
	    }
	}
	
}
