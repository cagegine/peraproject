package it.tetratron.pera.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsConfiguration;
import java.awt.event.WindowAdapter;
import java.io.FileInputStream;

import javax.media.j3d.AmbientLight;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.PickInfo;
import javax.media.j3d.Texture;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.JFrame;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;

import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.pickfast.PickCanvas;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;

public class Credits extends JFrame {

	private static final long serialVersionUID = 1L;
	private Box box;
	private int height = 500;
	private int width = 800;
	private SimpleUniverse universe;
	private BranchGroup group = new BranchGroup();
	private PickCanvas pickCanvas;
	private TransformGroup boxTransformGroup;
	private Transform3D transform;
	private TransparencyAttributes ta;
	private float i = 0.0f;
	private Transform3D vpRotation;
	private TransformGroup vpGroup;
	private ViewingPlatform vp;
	private Thread soundThread;
	private javazoom.jl.player.Player mp3 = null;

	public Credits() {
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				if (mp3 != null)
					mp3.close();
			}
		});
		setVisible(true);
		setSize(width, height);
		setMinimumSize(new Dimension(width, height));
		setMaximumSize(new Dimension(width, height));
		setVisible(true);
		playSound();
		new Thread() {
			public void run() {
				startDrawing();
			}
		}.start();
	}

	private void startDrawing() {
		GraphicsConfiguration config = SimpleUniverse
				.getPreferredConfiguration();
		Canvas3D canvas = new Canvas3D(config);
		universe = new SimpleUniverse(canvas);

		vp = universe.getViewingPlatform();
		vp.setNominalViewingTransform();
		vpGroup = vp.getMultiTransformGroup().getTransformGroup(0);
		vpRotation = new Transform3D();

		// vpRotation.setRotation(new AxisAngle4d(1, 2.0, 0.0,0));
		vpRotation.rotX(-Math.PI / 6);
		vpRotation.setTranslation(new Vector3d(0, 2, -3.5));
		vpGroup.setTransform(vpRotation);

		TransformGroup tg1 = vp.getViewPlatformTransform();
		Transform3D t3d = new Transform3D();
		tg1.getTransform(t3d);

		getScene();
		universe.addBranchGraph(group);
		pickCanvas = new PickCanvas(canvas, group);
		pickCanvas.setMode(PickInfo.PICK_BOUNDS);

		add(canvas, BorderLayout.CENTER);
		validate();
	}

	public void getScene() {

		boxTransformGroup = new TransformGroup();
		boxTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		boxTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);

		ta = new TransparencyAttributes();
		ta.setTransparencyMode(TransparencyAttributes.BLENDED);
		ta.setTransparency(0.5f);

		create();
		new t().start();

	}

	private void create() {

		Color3f light1Color = new Color3f(0.7f, 0.8f, 0.8f);
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
				100.0);
		Vector3f light1Direction = new Vector3f(4.0f, -7.0f, -12.0f);
		DirectionalLight light1 = new DirectionalLight(light1Color,
				light1Direction);
		light1.setInfluencingBounds(bounds);
		group.addChild(light1);
		AmbientLight light2 = new AmbientLight(new Color3f(0.3f, 0.3f, 0.3f));
		light2.setInfluencingBounds(bounds);
		group.addChild(light2);

		TextureLoader tl = new TextureLoader("./img/star_wars_credits.png",
				new Container());
		Texture texture = tl.getTexture();
		texture.setBoundaryModeS(Texture.CLAMP);
		texture.setBoundaryModeT(Texture.CLAMP);
		Appearance ap2 = new Appearance();
		ap2.setTransparencyAttributes(ta);
		ap2.setTexture(texture);

		box = new Box(1f, 0.0f, 5f, Primitive.GENERATE_TEXTURE_COORDS, ap2);

		box.setCapability(Box.ENABLE_APPEARANCE_MODIFY);
		box.setCapability(Box.GEOMETRY_NOT_SHARED);

		final TransformGroup tg = new TransformGroup();
		transform = new Transform3D();

		Vector3f vector = new Vector3f(0f, 0f, 0f);
		transform.setTranslation(vector);
		tg.setTransform(transform);
		tg.addChild(box);
		boxTransformGroup.addChild(tg);

		group.addChild(boxTransformGroup);

	}

	class t extends Thread {

		private long lastLoopTime = System.nanoTime();
		private final long OPTIMAL_TIME = 1000000000 / 60; // 60fps is optimal

		// private long lastFpsTime;
		// private int fps;

		public void run() {

			// keep looping round til the game ends
			while (true) {
				// work out how long its been since the last update, this
				// will be used to calculate how far the entities should
				// move this loop
				long now = System.nanoTime();
				long updateLength = now - lastLoopTime;
				lastLoopTime = now;
				double delta = ((double) updateLength) * 60 / 1000000000;
				/*
				 * // update the frame counter lastFpsTime += updateLength;
				 * fps++;
				 * 
				 * // update our FPS counter if a second has passed since // we
				 * last recorded if (lastFpsTime >= 1000000000) {
				 * System.out.println("(FPS: " + fps + ")"); lastFpsTime = 0;
				 * fps = 0; }
				 */
				// update the game logic
				update(delta);

				// we want each frame to take 10 milliseconds, to do this
				// we've recorded when we started the frame. We add 10
				// milliseconds
				// to this and then factor in the current time to give
				// us our final value to wait for
				// remember this is in ms, whereas our lastLoopTime etc. vars
				// are in ns.
				try {

					long sleep = (long) ((OPTIMAL_TIME - (System.nanoTime() - lastLoopTime)) / 1000000);
					// System.out.println(sleep);
					if (sleep > 1)
						Thread.sleep(sleep);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	private void playSound() {
		soundThread = new Thread() {
			public void run() {

				try {
					FileInputStream mp3_file = new FileInputStream(
							"./img/StarWars.mp3");
					mp3 = new javazoom.jl.player.Player(mp3_file);
					mp3.play();
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		};
		soundThread.start();

	}

	private void update(double delta) {
		i = (float) (i + delta * 0.008f);
		Vector3f vector;
		if (i > 15.3) {
			i = 0;
			vector = new Vector3f(0, 0, 0);
		} else {
			vector = new Vector3f(0, 0, -i);
		}

		transform.setTranslation(vector);
		boxTransformGroup.setTransform(transform);
		// vpRotation.setTranslation(vector);
		// vpGroup.setTransform(vpRotation);
	}
}