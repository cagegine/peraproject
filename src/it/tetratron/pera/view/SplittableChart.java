/*
////////////////////////
name SplittableChart.java
show multi chart with possibility of zoom, change color, hide or show chart
it.tetratron.pera.view
GLP 3
Lorenzo Scorzato
TetraTron Group
2012.03.01
/////////////////////////
 */

package it.tetratron.pera.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

public class SplittableChart extends JPanel {

	private static final long serialVersionUID = 1L;
	private SplittableChartCore scc[] = null;
	private JFrame frame;
	private JPanel controlPanel;
	private JButton joinSplit;
	private JToolBar buttonPanel;
	private GridBagConstraints c;
	private JButton[] zoom;
	private JButton show;
	private int chartNumber;
	private JButton save;
	private JButton exit;
	private ChangeColor changeColor;
	private ChangeOriginalColor changeOriginalColor;
	private ChangeVisibility changeVisibility;
	private ChangeSplitJoin changeSplitJoin;
	private ButtonZoom buttonZoom;
	private ScrollZoom scrollZoom;


	/**
	 * Constructor
	 */
	public SplittableChart() {
		changeColor = new ChangeColor(this);
		changeOriginalColor = new ChangeOriginalColor(this);
		changeVisibility = new ChangeVisibility();
		changeSplitJoin = new ChangeSplitJoin();
		buttonZoom = new ButtonZoom();
		scrollZoom = new ScrollZoom();
		createGui();
	}

	/**
	 * Remove all chart
	 */
	public void reset() {
		for (int k = 0; scc != null && k < scc.length; k++) {
			scc[k].reset();
		}
		controlPanel.removeAll();
		chartNumber = 0;

	}

	/**
	 * Set number of chart area, chart will be line charts
	 * 
	 * @param i
	 *            chart area number
	 */
	public void setNumber(int i) {
		boolean[] bar = new boolean[i];
		boolean[] domainInt = new boolean[i];
		for (int k = 0; k < i; k++){
			bar[k] = false;
			domainInt[k]=true;
			}
		setNumber(bar,domainInt);
	}

	/**
	 * Set number of chart area, chart will be line charts or bar
	 * 
	 * @param bar
	 *            chart area with bar = true will show bar chart, otherwise line
	 *            chart
	 */
	public void setNumber(boolean[] bar, boolean[] domainInt) {
		if (scc != null)
			for (int k = 0; k < scc.length; k++) {
				scc[k].removeMouseWheelListener(scrollZoom);
				remove(scc[k]);
			}
		controlPanel.removeAll();
		scc = new SplittableChartCore[bar.length];
		scc[0] = new SplittableChartCore(bar[0], domainInt[0]);
		scc[0].chartArea.addMouseWheelListener(scrollZoom);
		JComponent first = scc[0];
		JComponent p = first;

		for (int k = 1; k < scc.length; k++) {
			scc[k] = new SplittableChartCore(bar[k], domainInt[k]);
			scc[k].chartArea.addMouseWheelListener(scrollZoom);

			p = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, first, scc[k]);
			((JSplitPane) p).setResizeWeight(0.5);
			((JSplitPane) p).setOneTouchExpandable(true);
			first = p;

		}
		add(p);
	}

	/**
	 * Show or hide save button
	 * 
	 * @param b
	 *            if true show save button, otherwise hide it
	 */
	public void showSave(boolean b) {
		save.setVisible(b);
	}

	/**
	 * Show or hide exit button
	 * 
	 * @param b
	 *            if true show exit button, otherwise hide it
	 */
	public void showExit(boolean b) {
		exit.setVisible(b);
	}

	/**
	 * Get chart image
	 * 
	 * @param width
	 *            image width
	 * @param height
	 *            image height
	 * @return the join chart image
	 */
	public Image[] getImage(int width, int height) {
		Image[] imgs = new Image[scc.length];
		for (int i = 0; i < scc.length; i++)
			imgs[i] = scc[i].getImage(width, height);
		return imgs;
	}

	/**
	 * Create gui
	 */
	private void createGui() {
		chartNumber = 0;
		frame = new JFrame();
		controlPanel = new JPanel();
		joinSplit = new JButton("Join");
		joinSplit.addActionListener(changeSplitJoin);
		this.setLayout(new BorderLayout());
		controlPanel.setLayout(new GridBagLayout());
		buttonPanel = new JToolBar();
		buttonPanel.add(joinSplit);
		add(buttonPanel, BorderLayout.NORTH);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(3, 3, 3, 3);
		c.anchor = GridBagConstraints.PAGE_START;
		c.gridy = 0;
		c.weightx = 1;
		c.weighty = 0;
		zoom = new JButton[2];
		zoom[0] = new JButton();
		zoom[1] = new JButton();
		zoom[0].setIcon(new ImageIcon("./img/zoom-in.png"));
		zoom[1].setIcon(new ImageIcon("./img/zoom-out.png"));
		zoom[0].setActionCommand("in");
		zoom[1].setActionCommand("out");
		zoom[0].addActionListener(buttonZoom);
		zoom[1].addActionListener(buttonZoom);
		buttonPanel.add(zoom[0]);
		buttonPanel.add(zoom[1]);

		JScrollPane scroll = new JScrollPane(controlPanel);
		JPanel p = new JPanel();
		p.setLayout(new BorderLayout());
		p.add(scroll, BorderLayout.CENTER);
		frame.setSize(250, 500);
		frame.add(p);

		show = new JButton("Show Legend");
		show.setActionCommand("show");
		frame.setTitle("legends");
		show.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(!frame.isVisible());
			}
		});
		buttonPanel.add(show);
		save = new JButton();
		save.setIcon(new ImageIcon("./img/csv.png"));
		save.setActionCommand("exportCSV");
		buttonPanel.add(save);

		exit = new JButton("EXIT");
		exit = Params.getButton("small_red");
		exit.setHorizontalTextPosition(SwingConstants.CENTER);
		exit.setOpaque(false);
		exit.setBorderPainted(false);
		exit.setContentAreaFilled(false);
		exit.setForeground(Color.WHITE);
		exit.setText("Main menu");
		exit.setActionCommand("exit");
		buttonPanel.add(exit);
		exit.setVisible(false);
	}

	/**
	 * Add new chart
	 * 
	 * @param title
	 *            chart's title
	 * @param x
	 *            x axis for elaborated value
	 * @param y
	 *            y axis for elaborated value
	 * @param xA
	 *            x axis name
	 * @param yA
	 *            y axis name
	 * @param ind
	 *            chart area index
	 */
	public void addData(String title, List<Double> x, List<Double> y,
			String xA, String yA, int ind, boolean line) {
		addData(title, x, y, null, null, xA, yA, ind, line);
	}

	public void addData(String title, List<Double> x, List<Double> y,
			String xA, String yA, int ind) {
		addData(title, x, y, null, null, xA, yA, ind, true);
	}

	public void addData(String title, List<Double> x, List<Double> y,
			List<Double> x2, List<Double> y2, String yA2, String xA2, int ind) {
		addData(title, x, y, x2, y2, yA2, xA2, ind, true);
	}

	/**
	 * Add new chart
	 * 
	 * @param title
	 *            chart's title
	 * @param x
	 *            x axis for elaborated value
	 * @param y
	 *            y axis for elaborated value
	 * @param x2
	 *            x axis for not elaborated value
	 * @param y2
	 *            y axis for not elaborated value
	 * @param xA2
	 *            x axis name
	 * @param yA2
	 *            y axis name
	 * @param ind
	 *            chart area index
	 */
	public void addData(String title, List<Double> x, List<Double> y,
			List<Double> x2, List<Double> y2, String yA2, String xA2, int ind,
			boolean line) {

		scc[ind].addData(title, x, y, x2, y2, yA2, xA2, line);

		if (ind == 0) {
			Color[] colors = scc[ind].getLastColor();
			String lastTitle[] = scc[ind].getLastName();

			ColorButton color = new ColorButton();
			color.setColor(colors[0]);
			color.setActionCommand("" + chartNumber);
			color.addActionListener(changeColor);

			JLabel name = new JLabel(lastTitle[0]);
			JCheckBox visibilityBox = new JCheckBox();
			c.gridx = 0;
			controlPanel.add(visibilityBox, c);
			c.gridx = 1;
			controlPanel.add(name, c);
			c.gridx = 2;
			controlPanel.add(color, c);

			if (x2 != null) {
				ColorButton color2 = new ColorButton();
				color2.setColor(colors[1]);
				color2.setActionCommand("" + chartNumber);
				color2.addActionListener(changeOriginalColor);
				c.gridx++;
				controlPanel.add(color2, c);
			}

			c.gridy++;

			visibilityBox.setSelected(true);
			visibilityBox.setActionCommand("" + chartNumber);
			visibilityBox.addActionListener(changeVisibility);
			chartNumber++;
		}
	}

	/**
	 * Class used to change color of elaborates value of a chart
	 * */
	private class ChangeColor implements ActionListener {
		private JPanel panel;

		/**
		 * constructor
		 * 
		 * @param _panel
		 *            pane that use this
		 */
		public ChangeColor(JPanel _panel) {
			panel = _panel;
		}

		@Override
		/**
		 *  Change color of elaborates value of a chart
		 */
		public void actionPerformed(ActionEvent e) {
			int i = new Integer(e.getActionCommand());
			Color newColor = JColorChooser.showDialog(panel,
					"Choose Background Color", scc[0].getColors(i)[0]);
			if (newColor != null) {
				for (int k = 0; k < scc.length; k++) {
					scc[k].changeColor(i, newColor);
				}
				ColorButton surce = (ColorButton) e.getSource();
				surce.setColor(newColor);
			}
		}

	}

	/**
	 * Class used to change color of not elaborates value of a chart
	 */
	private class ChangeOriginalColor implements ActionListener {
		private JPanel panel;

		/**
		 * Constructor
		 * 
		 * @param _panel
		 *            pane that use this
		 */
		public ChangeOriginalColor(JPanel _panel) {
			panel = _panel;
		}

		/**
		 * Change color of not elaborates value of a chart
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			int i = new Integer(e.getActionCommand());
			Color newColor = JColorChooser.showDialog(panel,
					"Choose Background Color", scc[0].getColors(i)[1]);
			if (newColor != null) {
				for (int k = 0; k < scc.length; k++) {
					scc[k].changeOriginalColor(i, newColor);
				}
				ColorButton surce = (ColorButton) e.getSource();
				surce.setColor(newColor);
			}
		}
	}

	/**
	 * Class used to show or hide a chart
	 */
	private class ChangeVisibility implements ActionListener {

		@Override
		/**
		 *  ActionPerformed, show or hide a chart
		 */
		public void actionPerformed(ActionEvent e) {
			int index = new Integer(e.getActionCommand());
			JCheckBox surce = (JCheckBox) e.getSource();
			for (int k = 0; k < scc.length; k++) {
				scc[k].changeVisibility(index, surce.isSelected());
			}
		}

	}

	/**
	 * Add listener
	 * 
	 * @param al
	 *            actionListener
	 */
	public void addListner(ActionListener al) {
		save.addActionListener(al);
		exit.addActionListener(al);
		zoom[0].addMouseListener((MouseListener) al);
		zoom[1].addMouseListener((MouseListener) al);
		save.addMouseListener((MouseListener) al);
		exit.addMouseListener((MouseListener) al);
		joinSplit.addMouseListener((MouseListener) al);
		show.addMouseListener((MouseListener) al);
	}

	/**
	 * A colored JButton
	 */
	private class ColorButton extends JButton {

		private static final long serialVersionUID = 1L;
		private Color color;

		/**
		 * Constructor
		 */
		public ColorButton() {
			super(" ");
		}

		/**
		 * Set button's color
		 * 
		 * @param color
		 *            color
		 */
		private void setColor(Color color) {
			this.color = color;
			repaint();
		}

		/**
		 * Paint color over button
		 */
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			if (this.isSelected()) {
				g.setColor(color);
			} else {
				g.setColor(color);
			}
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
		}
	};

	/**
	 * Class used to split or join chart
	 * 
	 */
	private class ChangeSplitJoin implements ActionListener {
		boolean split;

		/**
		 * Constructor
		 */
		ChangeSplitJoin() {
			split = true;
		}

		@Override
		/**
		 *  ActionPerformed join or split chart
		 */
		public void actionPerformed(ActionEvent e) {
			if (split) {
				for (int k = 0; k < scc.length; k++) {
					scc[k].joinAll(true);
				}
				joinSplit.setText("Split");
				joinSplit.setActionCommand("Split");
			} else {
				for (int k = 0; k < scc.length; k++) {
					scc[k].splitAll();
				}
				joinSplit.setText("Join");
				joinSplit.setActionCommand("Join");
			}
			split = !split;
		}
	}
	

	/**
	 * Class used to perform zoom when a zoom button is pressed
	 */
	private class ButtonZoom implements ActionListener {

		@Override
		/**
		 * ActionPerformed, perform zoom
		 */
		public void actionPerformed(ActionEvent e) {
			String z = e.getActionCommand();
			if (z.equals("in"))
				for (int k = 0; k < scc.length; k++) {
					scc[k].zoomIn(true);
				}
			else
				for (int k = 0; k < scc.length; k++) {
					scc[k].zoomOut(true);
				}

		}

	}

	/**
	 * Class used to perform zoom when mouse wheel is scrolled
	 */
	private class ScrollZoom implements MouseWheelListener {

		@Override
		/**
		 * MouseWheelMoved, perform a zoom
		 */
		public void mouseWheelMoved(MouseWheelEvent arg0) {
			int i = arg0.getWheelRotation();
			if (i < 0)
				// UP
				for (int k = 0; k < scc.length; k++) {
					scc[k].zoomIn(false);
				}
			else
				for (int k = 0; k < scc.length; k++) {
					scc[k].zoomOut(false);
				}

		}
	}

}
