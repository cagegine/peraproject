/*
/////////////////////////
Pera.java;
a class to draw a table;
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * class used to draw a table
 */
public class Table extends JPanel {

	private static final long serialVersionUID = 1L;

	/**
	 * Draw a table
	 * 
	 * @param args
	 *            String[][] with data
	 * @param top
	 *            first row's color
	 * @param first
	 *            first color
	 * @param second
	 *            second color
	 */
	public Table(String[][] args, Color top, Color first, Color second) {
		int xDim = args.length;
		int yDim = args[0].length;

		for (int y = 0; y < yDim; y++) {
			for (int x = 0; x < xDim; x++) {
				if (args[x][y].equals("-1.0"))
					args[x][y] = "---";
			}
		}

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		c.weightx = 1;
		c.insets = new Insets(1, 1, 1, 1);
		for (int y = 0; y < yDim; y++) {
			JLabel label = new JLabel();
			label.setText(args[0][y]);
			label.setHorizontalAlignment(JLabel.CENTER);
			label.setBackground(top);
			label.setOpaque(true);
			add(label, c);
			c.gridx++;
		}
		c.gridy = 0;
		Color col = second;
		for (int x = 1; x < xDim; x++) {
			c.gridy++;
			c.gridx = 0;
			if (col == second)
				col = first;
			else
				col = second;
			for (int y = 0; y < yDim; y++) {
				JLabel label = new JLabel();
				label.setText(trunk(args[x][y]));
				label.setHorizontalAlignment(JLabel.CENTER);
				label.setBackground(col);
				label.setOpaque(true);
				add(label, c);
				c.gridx++;
			}
		}
	}

	/**
	 * Trunk a string with number
	 * 
	 * @param s
	 *            the string
	 * @return trunked string
	 */
	public static  String trunk(String s) {
		for (int i = 3; i < s.length(); i++) {
			if (s.substring(0, i).matches("[0-9]*\\.[0-9]*[1-9][0-9]")
					|| s.substring(0, i).matches("[1-9][0-9]*\\.[0-9]{2}"))
				return s.substring(0, i);
		}
		return s;
	}
}
