/*
/////////////////////////
Pera.java;
a class to draw a form for the insertion of the parameters and the input of the files for Arterial Delay algortihm;
it.tetratron.pera.view;
GLP 3;
Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

/**
*
* Class to draw and manage the insert parameters form for arterial delay correction algorithm
* 
*/
public class Delay {

	private static JPanel inputArt;

	private static JPanel delay;
	
	
	/**
	 * Draw a form for the insertion of the parameters and the input of the files
	 * @param input a matrix with 2 columns in witch the first will be the text of the label for the input and the second is the action of the browse button (only the first row is conidered)
	 * @param delayLabel the text of the label attached to the delay text field
	 * @param al a listener for the actions performed
	 * @param dl a listener for changes on the text fields
	 * @return return a JPanel
	 */
	public static JPanel draw(String[][] input, String delayLabel, ActionListener al, DocumentListener dl){
		
		JPanel aux = new RoundedPanel();
		aux.setLayout(new GridBagLayout());
		
		inputArt = Params.inputFile(input[0][0],input[0][1], input[0][2], al, dl);
		delay = Params.oneField(delayLabel, true, null, null);
		
		aux.add(inputArt, new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0),0);
		aux.add(delay, new GridBagConstraints(0,1,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0),1);
		
		return aux;
	}
	
	/**
	 * This method set a string to the field of the inputFile panel the given text 
	 * @param s the text to set to the field
	 * @param m the main window
	 */
	public static void setFile(String s){
		if (inputArt.getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			tf.setText(s);
		}
	}
	
	/**
	 * Set the default value for the Delay field in the view
	 * @param s the string to be set in the field
	 */
	public static void setDelay(String s){
		if (delay.getComponent(0) instanceof JTextField){
			((JTextField)delay.getComponent(0)).setText(s);
		}
	}

	
	/**
	 * Disable the file chooser and set to the text field the text "Already loaded.."
	 * @param c the document listener that need to been removed from the text field 
	 */
	public static void alreadyLoadedFile(DocumentListener c){
		if (inputArt.getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			tf.getDocument().removeDocumentListener(c);
			tf.setFont(new Font(Font.SANS_SERIF,Font.BOLD,13));
			tf.setText("Already loaded..");
			tf.setEnabled(false);
			inputArt.getComponent(1).setEnabled(false);
		}
	}
	
	/**
	 * Set red color to the elements in the input panel
	 */
	public static void setFileError(boolean t){
		if (inputArt.getComponent(0) instanceof JTextField && inputArt.getComponent(2) instanceof JLabel){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			JLabel l = (JLabel) inputArt.getComponent(2);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}

	/**
	 * Set red/black color to the elements in the delay panel
	 * @param t if true set color as red else set it black
	 */
	public static void setDelayError(boolean t){
		if (delay.getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) delay.getComponent(0);
			JLabel l = (JLabel) delay.getComponent(1);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}
	
	/**
	 * Return the path of the file
	 * @return a String that contain the path of the selected file
	 */
	public static String getFilePath(){
		if (inputArt.getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			return tf.getText();
		}
		return "";
	}
	
	/**
	 * Return the delay
	 * @return a Double that contain the delay
	 */
	public static Double getDelay(){
		if (delay.getComponent(0) instanceof JTextField){
			try{
				return Double.parseDouble((((JTextField) delay.getComponent(0)).getText()));
			}catch(Exception e){
				return -1000.0;
			}
		}
		return -1000.0;
	}
}
