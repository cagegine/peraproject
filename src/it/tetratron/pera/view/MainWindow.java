/*
/////////////////////////
Pera.java;
the main window of the PERA software;
it.tetratron.pera.view;
GLP 3;
Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.util.Observable;
import java.util.Observer;

/**
 * 
 * create the main window of PERA software and manage the contents
 * 
 */
public class MainWindow extends JFrame implements Observer {

	private static final long serialVersionUID = 1L;

	/*
	 * the notification area
	 */
	private JPanel notificationArea;

	private JTextArea log;

	private JButton closeNotification;

	protected JPanel workArea;

	private JButton openNotification;

	private String oldMessage;

	private JLabel wait;

	private boolean showingCredits;

	/**
	 * the constructor
	 */
	public MainWindow() {
		// set close operation
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		createGUI();
		setVisible(false);
		pack();
	}

	/**
	 * create the structure of the window, create the notification area and the
	 * work area
	 */
	public void createGUI() {

		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		Insets margin = new Insets(5, 5, 5, 5);

		/* QUESTION MARK ICON */

		ImageIcon questionMarkIcon = new ImageIcon(
				(new ImageIcon("./img/q.png").getImage().getScaledInstance(24,
						24, Image.SCALE_SMOOTH)));
		openNotification = new JButton(questionMarkIcon);
		openNotification.setBorderPainted(false);
		openNotification.setContentAreaFilled(false);
		openNotification.setPreferredSize(new Dimension(questionMarkIcon
				.getIconWidth() + 10, questionMarkIcon.getIconHeight() + 10));
		openNotification.setVisible(false);

		c.gridy = 2;
		c.gridx = 2;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.anchor = GridBagConstraints.LAST_LINE_END;
		add(openNotification, new GridBagConstraints(2, 3, 1, 1, 0, 0,
				GridBagConstraints.LAST_LINE_END, GridBagConstraints.NONE,
				new Insets(0, 0, 0, 0), 0, 0));

		/* WORK AREA */

		workArea = new JPanel();
		workArea.setLayout(new GridBagLayout());
		workArea.setBackground(new Color(0, 0, 0, 0));
		add(workArea, new GridBagConstraints(0, 0, 3, 3, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, margin, 0,
				0));
		/* FRAME BORDE */
		Image icon = new ImageIcon("./img/Pera_icon.png").getImage();
		setIconImage(icon);
		setTitle("Pera PET");

		/* NOTIFICATION AREA */

		notificationArea = new RoundedPanel();
		notificationArea.setLayout(new GridBagLayout());
		notificationArea.setBackground(new Color(252, 230, 221));
		notificationArea.setVisible(true);
		notificationArea.setMinimumSize(new Dimension(notificationArea
				.getWidth(), 70));
		add(notificationArea, new GridBagConstraints(0, 3, 3, 1, 1.0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, margin, 0,
				0));

		/* "X" to close the NOTIFICATION AREA */

		ImageIcon xIcon = new ImageIcon("./img/x.png");
		closeNotification = new JButton(xIcon);
		closeNotification.setBorderPainted(false);
		closeNotification.setContentAreaFilled(false);
		closeNotification.setPreferredSize(new Dimension(xIcon.getIconWidth(),
				xIcon.getIconHeight()));
		notificationArea.add(closeNotification, new GridBagConstraints(0, 0, 1,
				1, 0, 0, GridBagConstraints.FIRST_LINE_END,
				GridBagConstraints.NONE, new Insets(5, 0, 5, 10), 0, 0));

		/* TEXT AREA and SCROLL AREA in the NOTIFICATION AREA */

		log = new JTextArea();
		log.setEditable(false);
		log.setFont(new Font(Font.SANS_SERIF, Font.BOLD | Font.ITALIC, 17));
		log.setBackground(new Color(252, 230, 221));
		log.setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
		log.setLineWrap(true);
		log.setWrapStyleWord(true);

		JScrollPane scrollArea = new JScrollPane(log);
		scrollArea.setBackground(new Color(252, 230, 221));
		scrollArea.setBorder(javax.swing.BorderFactory.createEmptyBorder());
		notificationArea.add(scrollArea, new GridBagConstraints(0, 0, 1, 1,
				0.1, 0.1, GridBagConstraints.PAGE_START,
				GridBagConstraints.HORIZONTAL, new Insets(2, 5, 5, 50), 0, 0));
		log.setMinimumSize(new Dimension(log.getHeight(), 62));

		/* END OF THE NOTIFICATION AREA */

		ImageIcon ii = new ImageIcon("./img/load.gif");
		wait = new JLabel(ii, JLabel.CENTER) {
			private static final long serialVersionUID = 1L;

			public void paint(Graphics g) {
				Graphics2D g2 = (Graphics2D) g;
				g2.setColor(new Color(255, 255, 255, 100));
				g2.fillRect(0, 0, getWidth(), getHeight());
				super.paint(g2);
			}
		};
		add(wait, new GridBagConstraints(0, 0, 4, 4, 1, 1,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						0, 0, 0, 0), 0, 0));
		wait.setVisible(false);

		/* ACTIONS */

		closeNotification.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				notificationArea.setVisible(false);
				openNotification.setVisible(true);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				setNotification("Close notification area");
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				restoreNotification();
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}
		});

		openNotification.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				notificationArea.setVisible(true);
				openNotification.setVisible(false);
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
			}
		});

		/* CREDITS */

	}

	/**
	 * show a window with software information
	 */
	public void showCredits(){
		if(showingCredits == false){
		showingCredits = true;
		Credits credits = new Credits();
		credits.addWindowListener(new WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e)  {
				showingCredits = false;
			}});
		}
		/*
		credits = new JFrame();
		JTextArea creditsArea = new JTextArea();
		creditsArea.setEditable(false);
		creditsArea.setText("PERA software");
		creditsArea.append("\n");
		creditsArea.append("\ndeveloped by:");
		creditsArea.append("\n");
		creditsArea.append("\nTetraTron Group");
		creditsArea.append("\n");
		creditsArea.append("\nCabbia Davide");
		creditsArea.append("\nFerlin Alessandro");
		creditsArea.append("\nGuizzon Luca");
		creditsArea.append("\nPalleva Enrico");
		creditsArea.append("\nRosso Francesco");
		creditsArea.append("\nScorzato Lorenzo");
		creditsArea.append("\nZambon Jacopo");
		creditsArea.append("\n");
		creditsArea.append("\nalgorithms developed by:");
		creditsArea.append("\n");
		creditsArea.append("\nRizzo Gaia");
		creditsArea.append("\nVeronese Mattia");
		creditsArea.setBackground(new Color(240,235,226));
		credits.setLayout(new GridBagLayout());
		credits.add(creditsArea, new GridBagConstraints(0,0,1,1,1,1,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));
		credits.setSize(200, 330);
		credits.setVisible(true);*/
	}

	/**
	 * give the current work area
	 * 
	 * @return a JPanel
	 */
	public JPanel getWorkArea() {
		return workArea;
	}

	/**
	 * erase the previous work area and set a new one
	 * 
	 * @param workArea
	 *            the new work area that will be painted
	 */
	public void setNewWorkArea(JPanel workArea) {
		this.workArea.removeAll();
		// this.workArea.setVisible(false);
		remove(this.workArea);
		this.workArea = workArea;
		// this.workArea.setBackground(new Color(0,0,0,0));

		add(this.workArea, new GridBagConstraints(0, 0, 3, 3, 1.0, 1.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(
						5, 5, 5, 5), 0, 0));

		// this.workArea.setVisible(true);
		this.workArea.updateUI();
	}

	/**
	 * set a new notification to the work area
	 * 
	 * @param text
	 *            a string that will be set as notification area text
	 */
	public void setNotification(String text) {
		log.setText(text);
		notificationArea.repaint();
	}

	/**
	 * set a new notification to the work area and save it as default
	 * notification
	 * 
	 * @param text
	 *            a string that will be set as notification area text
	 */
	public void setMainNotification(String text) {
		oldMessage = text;
		log.setText(text);
		notificationArea.repaint();
	}

	/**
	 * restore the default notification
	 */
	public void restoreNotification() {
		log.setText(oldMessage);
		notificationArea.repaint();
	}

	/**
	 * show the waiting panel
	 */
	public void startWaiting() {
		wait.setVisible(true);
	}

	/**
	 * hide the waiting panel
	 */
	public void stopWaiting() {
		wait.setVisible(false);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub

	}

}