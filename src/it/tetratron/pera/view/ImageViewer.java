/*
////////////////////////
name ImageViewer.java
show pet images with possibility to zoom, chose color scale and other things
it.tetratron.pera.view
GLP 3
Lorenzo Scorzato
TetraTron Group
2012.03.01
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.LinearGradientPaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.JToolBar;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Toolkit;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Vector;

/**
 * Class used to show result of image level
 */
public class ImageViewer extends JPanel {

	private static final long serialVersionUID = 1L;
	private JButton zoomIn;
	private JButton zoomOut;
	private JButton zoomOriginal;
	protected ImagePanel imagePanel[];
	protected JScrollPane scrollPane;
	private JPanel imagesPanel;
	private JSpinner spin[];
	private JLabel spinLabel[];
	private JComboBox gradientBox;
	private JComboBox temp;
	private JToggleButton showLines;
	private boolean lines = true;
	protected JPanel panel4;
	private JLabel value;
	protected JLabel estimates;
	private int xCoord;
	private int yCoord;
	private int zCoord;
	private int xDim;
	private int yDim;
	private int zDim;
	private double[][][] vox;
	private Color lineColor = Color.CYAN;
	private Image[] imgs;
	private Gradient currentGradient;
	private Gradient[] gradientArray;
	protected JButton saveToPDF;
	private JSpinner minSpin;
	private JSpinner maxSpin;
	private double realMax;
	private double realMin;
	private double selectedMax;
	private double selectedMin;
	protected double[][][] vb;
	protected double[][][] vt;
	protected double[][][] ki;
	protected JToolBar top;
	private int maxTemp;
	private double xVoxScale;
	private double yVoxScale;
	private double zVoxScale;
	private boolean first;
	private JButton button3D;
	private Component parent;

	/**
	 * Constructor
	 * 
	 * @param maxTemp
	 *            the maximum time
	 * 
	 * @param parent
	 *            component that contain this
	 */
	public ImageViewer(int _maxTemp, Component parent) {
		imgs = new Image[3];
		maxTemp = _maxTemp;
		xVoxScale = 1;
		yVoxScale = 1;
		zVoxScale = 1;
		first = true;
		createToolBar();
		createGui();
		xCoord = 0;
		yCoord = 0;
		zCoord = 0;
		this.parent = parent;
	}

	/**
	 * Create toolbar
	 */
	private void createToolBar() {
		// toolbar
		top = new JToolBar("Toolbar", JToolBar.HORIZONTAL);

		// coordinates
		spin = new JSpinner[3];
		spinLabel = new JLabel[3];
		spinLabel[0] = new JLabel("x: ");
		spinLabel[1] = new JLabel("y: ");
		spinLabel[2] = new JLabel("z: ");

		for (int i = 0; i < 3; i++) {
			spin[i] = new JSpinner();
			spin[i].setModel(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
			spin[i].setPreferredSize(new Dimension(50, 25));
			spin[i].setMaximumSize(new Dimension(50, 25));
			top.add(spinLabel[i]);
			top.add(spin[i]);
		}
		addSpinListener();

		// zoom
		zoomIn = new JButton();
		zoomIn.setIcon(new ImageIcon("./img/zoom-in.png"));
		zoomIn.setActionCommand("zoomIn");
		zoomOut = new JButton();
		zoomOut.setIcon(new ImageIcon("./img/zoom-out.png"));
		zoomOut.setActionCommand("zoomOut");
		zoomOriginal = new JButton();
		zoomOriginal.setIcon(new ImageIcon("./img/zoom.png"));
		zoomOriginal.setActionCommand("zoomOriginal");
		top.add(zoomIn);
		top.add(zoomOriginal);
		top.add(zoomOut);

		addZoomListener();
		createGradients();

		// temp chooser
		temp = new JComboBox();
		temp.setActionCommand("selectTemp");
		for (int i = 0; i < maxTemp ; i++)
			temp.addItem("" + (i+1));
		top.add(temp);
		showLines = new JToggleButton();
		showLines.setIcon(new ImageIcon("./img/mirino.png"));
		showLines.setActionCommand("lines");
		top.add(showLines);
		showLines.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				lines = !lines;
				adjustLayout();
			}
		});

		// select max and min
		minSpin = new JSpinner();
		maxSpin = new JSpinner();

		JComponent editor = new JSpinner.NumberEditor(minSpin, "#.##");
		DecimalFormatSymbols	dfs = new DecimalFormatSymbols ();
		dfs.setDecimalSeparator('.');
		((JSpinner.NumberEditor) editor) .getFormat().setDecimalFormatSymbols(dfs);
		minSpin.setEditor(editor);
		

		JComponent editor2 = new JSpinner.NumberEditor(maxSpin, "#.##");
		((JSpinner.NumberEditor) editor2) .getFormat().setDecimalFormatSymbols(dfs);
		maxSpin.setEditor(editor2);

		minSpin.setPreferredSize(new Dimension(60, 25));
		minSpin.setMaximumSize(new Dimension(60, 25));
		maxSpin.setPreferredSize(new Dimension(60, 25));
		maxSpin.setMaximumSize(new Dimension(60, 25));

		top.add(new JLabel("min max:"));
		top.add(minSpin);
		top.add(maxSpin);
		addMaxMinListener();

		// save button
		saveToPDF = new JButton();
		saveToPDF.setIcon(new ImageIcon("./img/pdf.png"));
		top.add(saveToPDF);
		saveToPDF.setActionCommand("exportPDF");

		// 3d buttom
		button3D = new JButton();
		button3D.setIcon(new ImageIcon("./img/3D.png"));
		button3D.setActionCommand("3D");
		top.add(button3D);
		button3D.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do3D();
			}
		});
	}

	/**
	 * Add listener for zoom buttons
	 */
	private void addZoomListener() {
		zoomOut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				zoomOutAll();
			}
		});

		zoomIn.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				zoomInAll();
			}
		});

		zoomOriginal.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < 3; i++)
					imagePanel[i].originalSize();
				adjustLayout();
			}
		});
	}

	/**
	 * Add listener for max and min spin selector
	 */
	private void addMaxMinListener() {

		minSpin.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				selectedMin = (Double) minSpin.getValue();
				fixImages();
			}
		});

		maxSpin.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				selectedMax = (Double) maxSpin.getValue();
				fixImages();
			}
		});

	}

	/**
	 * Add listener for coordinate spin selector
	 */
	private void addSpinListener() {

		spin[0].addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				xCoord = (Integer) spin[0].getValue();
				fixImages();
			}
		});
		spin[1].addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				yCoord = (Integer) spin[1].getValue();
				fixImages();
			}
		});
		spin[2].addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				zCoord = (Integer) spin[2].getValue();
				fixImages();
			}
		});

	}

	/**
	 * Create gui
	 */
	private void createGui() {
		// imagePanel
		imagePanel = new ImagePanel[3];
		for (int i = 0; i < 3; i++)
			imagePanel[i] = new ImagePanel(20.0, i);
		setLayout(new BorderLayout());
		imagesPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.NONE;
		c.gridx = 0;
		c.gridy = 0;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		imagesPanel.add(imagePanel[0], c);
		c.gridx = 1;
		c.gridy = 0;
		c.anchor = GridBagConstraints.NORTHWEST;
		imagesPanel.add(imagePanel[1], c);
		c.gridx = 0;
		c.gridy = 1;
		c.anchor = GridBagConstraints.NORTHWEST;
		imagesPanel.add(imagePanel[2], c);

		add(BorderLayout.NORTH, top);
		scrollPane = new JScrollPane(imagesPanel);
		scrollPane.setAutoscrolls(true);
		add(BorderLayout.CENTER, scrollPane);

		// panel4
		panel4 = new JPanel();
		panel4.setLayout(new GridBagLayout());
		value = new JLabel();
		c.gridx = 0;
		c.gridy = 0;
		panel4.add(value, c);
		estimates = new JLabel();
		c.gridx = 0;
		c.gridy = 1;
		panel4.add(estimates, c);
		c.gridx = 1;
		c.gridy = 1;
		c.fill = GridBagConstraints.BOTH;
		c.weightx = 1;
		c.weighty = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.insets = new Insets(3, 3, 3, 3);
		imagesPanel.add(panel4, c);

		imagesPanel.addMouseWheelListener(new MouseWheelListener() {

			public void mouseWheelMoved(MouseWheelEvent arg0) {
				int i = arg0.getWheelRotation();
				if (i < 0)
					// UP
					zoomInAll();
				else
					zoomOutAll();
			}
		});

	}

	/**
	 * Create gradients
	 */
	private void createGradients() {
		gradientArray = new Gradient[12];
		gradientArray[0] = new Gradient(Color.black, Color.white, "Grayscale");
		gradientArray[0].add(Color.DARK_GRAY, 0.25);
		gradientArray[0].add(Color.GRAY, 0.50);
		gradientArray[0].add(Color.LIGHT_GRAY, 0.75);
		gradientArray[1] = new Gradient(Color.black, Color.red, "Black red");
		gradientArray[2] = new Gradient(Color.black, Color.blue, "Black blue");
		gradientArray[3] = new Gradient(Color.black, Color.cyan, "Black cyan");
		gradientArray[4] = new Gradient(Color.black, Color.green, "Black green");
		gradientArray[5] = new Gradient(Color.black, Color.magenta,
				"Black magenta");
		gradientArray[6] = new Gradient(Color.black, Color.orange,
				"Black orange");
		gradientArray[7] = new Gradient(Color.black, Color.pink, "Black pink");
		gradientArray[8] = new Gradient(Color.black, Color.yellow,
				"Black yellow");
		gradientArray[9] = new Gradient(Color.blue, Color.red, "X_Rain");
		gradientArray[9].add(Color.green, 0.33);
		gradientArray[9].add(Color.yellow, 0.66);
		gradientArray[10] = new Gradient(Color.black, Color.red, "Rainbow");
		gradientArray[10].add(Color.blue, 0.25);
		gradientArray[10].add(Color.green, 0.5);
		gradientArray[10].add(Color.yellow, 0.75);
		gradientArray[11] = new Gradient(Color.black, Color.white, "Ice");
		gradientArray[11].add(Color.blue, 0.33);
		gradientArray[11].add(Color.CYAN, 0.66);

		gradientBox = new JComboBox();

		for (int i = 0; i < gradientArray.length; i++)
			gradientBox.addItem(gradientArray[i].getName());

		gradientBox.setSelectedIndex(10);
		currentGradient = gradientArray[10];

		top.add(gradientBox);

		gradientBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int i = gradientBox.getSelectedIndex();
				currentGradient = gradientArray[i];
				fixImages();
				adjustLayout();
			}
		});
	}

	public Gradient getGradient() {
		return currentGradient;
	}

	public double getMax() {
		return selectedMax;
	}

	public double getMin() {
		return selectedMin;
	}

	public double getValue() {
		return vox[xCoord][yCoord][zCoord];
	}

	/**
	 * Return current temp
	 * 
	 * @return int current temp
	 */
	public int getTemp() {
		return (Integer) temp.getSelectedIndex();
	}

	/**
	 * Return the images
	 * 
	 * @return images[3] currently displayed
	 */
	public Image[] getImages() {
		return imgs;
	}

	/**
	 * Return x coordinates
	 * 
	 * @return x coordinates
	 */
	public synchronized int getXCoord() {
		if (xCoord > xDim - 1)
			return xDim - 1;
		else if (xCoord < 0)
			return 0;
		return xCoord;
	}

	/**
	 * Return y coordinates
	 * 
	 * @return y coordinates
	 */
	public synchronized int getYCoord() {
		if (yCoord > yDim - 1)
			return yDim - 1;
		else if (yCoord < 0)
			return 0;
		return yCoord;
	}

	/**
	 * Return z coordinates
	 * 
	 * @return z coordinates
	 */
	public synchronized int getZCoord() {
		if (zCoord > zDim - 1)
			return zDim - 1;
		else if (zCoord < 0)
			return 0;
		return zCoord;
	}

	/**
	 * Perform a zoomOut
	 */
	private void zoomOutAll() {
		for (int i = 0; i < 3; i++) {
			imagePanel[i].zoomOut();
			adjustLayout();
		}
	}

	/**
	 * Perform a zoomIn
	 */
	private void zoomInAll() {
		for (int i = 0; i < 3; i++) {
			imagePanel[i].zoomIn();
			adjustLayout();
		}
	}

	/**
	 * Fix layout when needed
	 */
	private void adjustLayout() {
		for (int i = 0; i < 3; i++) {
			imagePanel[i].repaint();
			imagesPanel.doLayout();
			scrollPane.doLayout();
			scrollPane.revalidate();
		}
	}

	/**
	 * Set new voxel
	 * 
	 * @param vox
	 *            double[][][] matrix with voxel value
	 */
	public void setVoxel(double[][][] _vox) {
		// [x][y][z]
		vox = _vox;
		prepareVoxel();

		spin[0].setModel(new SpinnerNumberModel(xCoord+1, 1, xDim , 1));
		spin[1].setModel(new SpinnerNumberModel(yCoord+1, 1, yDim , 1));
		spin[2].setModel(new SpinnerNumberModel(zCoord+1, 1, zDim , 1));

		if (first) {
			first = false;
			int h1 = imgs[2].getHeight(null);
			int h2 = imgs[0].getHeight(null);
			for (int i = 0; i < 3; i++) {
				double z = (double) (parent.getHeight() - 150) / (h1 + h2);
				imagePanel[i].zoom(z);
				adjustLayout();
			}
			xCoord = xDim / 2;
			yCoord = yDim / 2;
			zCoord = zDim / 2;
			spin[0].setValue(xCoord);
			spin[1].setValue(yCoord);
			spin[2].setValue(zCoord);
		}
		fixImages();
	}

	/**
	 * Set voxel's dimension
	 * 
	 * @param x
	 *            x dimension
	 * @param y
	 *            y dimension
	 * @param z
	 *            z dimension
	 */
	public void setVoxelDim(double x, double y, double z) {
		xVoxScale = x;
		yVoxScale = y;
		zVoxScale = z;
	}

	/**
	 * Calculate max and min, set max and min spin model
	 */
	protected void prepareVoxel() {
		realMax = Double.MIN_VALUE;
		realMin = Double.MAX_VALUE;

		xDim = vox.length;
		yDim = vox[0].length;
		zDim = vox[0][0].length;
		for (int x = 0; x < xDim; x++)
			for (int y = 0; y < yDim; y++)
				for (int z = 0; z < zDim; z++) {
					if (realMax < vox[x][y][z])
						realMax = vox[x][y][z];
					else if (realMin > vox[x][y][z])
						realMin = vox[x][y][z];
				}

		selectedMax = realMax;
		selectedMin = realMin;
		double scale = (realMax - realMin) / 100;

		
		JComponent editor = new JSpinner.NumberEditor(minSpin, "#.##");
		DecimalFormatSymbols	dfs = new DecimalFormatSymbols ();
		dfs.setDecimalSeparator('.');
		((JSpinner.NumberEditor) editor) .getFormat().setDecimalFormatSymbols(dfs);
		minSpin.setEditor(editor);
		

		JComponent editor2 = new JSpinner.NumberEditor(maxSpin, "#.##");
		((JSpinner.NumberEditor) editor2) .getFormat().setDecimalFormatSymbols(dfs);
		maxSpin.setEditor(editor2);
		
		
		minSpin.setModel(new SpinnerNumberModel(0, Double.NEGATIVE_INFINITY,
				Double.MAX_VALUE, scale));
		maxSpin.setModel(new SpinnerNumberModel(0, Double.NEGATIVE_INFINITY,
				Double.MAX_VALUE, scale));
		
		
		minSpin.setValue(selectedMin);
		maxSpin.setValue(selectedMax);
	}

	/**
	 * Generate images
	 */
	protected void fixImages() {

		createBottomLeftImg();
		createTopLeftImg();
		createTopRightImg();
		double val = new BigDecimal(vox[getXCoord()][getYCoord()][getZCoord()])
				.setScale(3, BigDecimal.ROUND_UP).doubleValue();
		value.setText("value = " + Table.trunk("" + val));

		adjustLayout();
	}

	/**
	 * Generate bottom left image with x and y axis
	 */
	private void createBottomLeftImg() {
		// x - y bottom left

		double scale;
		int pix[] = new int[xDim * yDim];
		int a = 0;

		scale = selectedMax - selectedMin;
		for (int y = yDim - 1; y >= 0; y--) {
			for (int x = 0; x < xDim; x++) {
				double pointOriginal = vox[x][y][getZCoord()];
				double point = pointOriginal - selectedMin;
				point /= scale;
				int[] rgb = new int[3];

				rgb = currentGradient.getColor(point);
				pix[a++] = (255 << 24) | (rgb[0] << 16) | (rgb[1] << 8)
						| rgb[2];

			}
		}

		MemoryImageSource image = new MemoryImageSource(xDim, yDim, pix, 0,
				xDim);
		Image img = Toolkit.getDefaultToolkit().createImage(image);
		int newHeight = (int) (img.getHeight(null) * yVoxScale);
		int newWidth = (int) (img.getWidth(null) * xVoxScale);
		img = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
		imgs[2] = img;
		imagePanel[2].setImage(img);
	}

	/**
	 * Generate top left image with x and z axis
	 */
	private void createTopLeftImg() {
		double scale;
		int pix[] = new int[xDim * yDim];
		int a = 0;
		// x -z top left
		a = 0;
		scale = selectedMax - selectedMin;
		pix = new int[zDim * xDim];

		for (int z = zDim - 1; z >= 0; z--) {
			for (int x = 0; x < xDim; x++) {
				double pointOriginal = vox[x][getYCoord()][z];
				double point = pointOriginal - selectedMin;
				point /= scale;
				int[] rgb = new int[3];
				rgb = currentGradient.getColor(point);
				pix[a++] = (255 << 24) | (rgb[0] << 16) | (rgb[1] << 8)
						| rgb[2];

			}
		}
		MemoryImageSource image = new MemoryImageSource(xDim, zDim, pix, 0,
				xDim);
		Image img = Toolkit.getDefaultToolkit().createImage(image);
		int newHeight = (int) (img.getHeight(null) * zVoxScale);
		int newWidth = (int) (img.getWidth(null) * xVoxScale);
		img = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
		imgs[0] = img;
		imagePanel[0].setImage(img);
	}

	/**
	 * Generate top right image with y and z axis
	 */
	private void createTopRightImg() {
		double scale;
		int pix[] = new int[xDim * yDim];
		int a = 0;
		// y - z top right
		a = 0;

		scale = selectedMax - selectedMin;
		pix = new int[zDim * yDim];
		for (int z = zDim - 1; z >= 0; z--) {
			for (int y = 0; y < yDim; y++) {
				double pointOriginal = vox[getXCoord()][y][z];
				double point = pointOriginal - selectedMin;
				point /= scale;
				int[] rgb = new int[3];
				rgb = currentGradient.getColor(point);
				pix[a++] = (255 << 24) | (rgb[0] << 16) | (rgb[1] << 8)
						| rgb[2];
			}
		}
		MemoryImageSource image = new MemoryImageSource(yDim, zDim, pix, 0,
				yDim);
		Image img = Toolkit.getDefaultToolkit().createImage(image);
		int newHeight = (int) (img.getHeight(null) * zVoxScale);
		int newWidth = (int) (img.getWidth(null) * yVoxScale);
		img = img.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
		imgs[1] = img;
		imagePanel[1].setImage(img);
	}

	/**
	 * Private class used to show the image
	 */
	protected class ImagePanel extends JComponent {

		private static final long serialVersionUID = 1L;
		private double zoom = 1;
		private double zoomPercentage;
		private Image image = null;
		private int n;

		/**
		 * Set image
		 * 
		 * @param image
		 *            new image
		 */
		public void setImage(Image image) {
			this.image = image;
		}

		/**
		 * Perform a custom zoom
		 * 
		 * @param z
		 *            zoom value
		 */
		public void zoom(double z) {
			zoom = z;
		}

		/**
		 * Constructor
		 * 
		 * @param zoomPercentage
		 *            how zoom increase or decrease
		 * @param a
		 *            position:
		 *            <ul>
		 *            <li>0 = top left</li>
		 *            <li>1 = top right</li>
		 *            <li>2 = bottom left</li>
		 *            </ul>
		 */
		public ImagePanel(double zoomPercentage, int a) {
			this.zoomPercentage = zoomPercentage / 100;

			n = a;
			addMouseListener(new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent arg0) {
					fixCoord(arg0.getX(), arg0.getY());

				}

				@Override
				public void mouseEntered(MouseEvent arg0) {
				}

				@Override
				public void mouseExited(MouseEvent arg0) {
				}

				@Override
				public void mousePressed(MouseEvent arg0) {
				}

				@Override
				public void mouseReleased(MouseEvent arg0) {
				}
			});

			addMouseMotionListener(new MouseMotionListener() {

				@Override
				public void mouseDragged(MouseEvent arg0) {
					fixCoord(arg0.getX(), arg0.getY());
				}

				@Override
				public void mouseMoved(MouseEvent arg0) {
				}
			});

		}

		/**
		 * This method is override to draw image, scale, max and min and the
		 * crosshair if required
		 */
		public void paintComponent(Graphics grp) {
			Graphics2D g2D = (Graphics2D) grp;

			// background
			g2D.setColor(Color.black);
			g2D.fillRect(0, 0, getWidth(), getHeight());

			int width = this.getWidth();
			int height = this.getHeight();

			Color[] c = new Color[2];
			LinearGradientPaint gradient = new LinearGradientPaint(0, height,
					0, 0, currentGradient.getValues(),
					currentGradient.getColors(c));

			g2D.setPaint(gradient);
			g2D.fillRect(width - 70, 0, width - 60, height);
			g2D.setColor(Color.black);
			g2D.fillRect(width - 60, 0, width, height);
			g2D.setColor(Color.white);
			double max = new BigDecimal(selectedMax).setScale(3,
					BigDecimal.ROUND_UP).doubleValue();
			double min = new BigDecimal(selectedMin).setScale(3,
					BigDecimal.ROUND_UP).doubleValue();
			g2D.drawString("" + max, width - 60, 13);
			g2D.drawString("" + min, width - 60, height);

			// draw image
			g2D.scale(zoom, zoom);
			if (image != null) {
				grp.drawImage(image, 0, 0, this);
			}

			// draw lines if required
			if (lines) {
				g2D.setColor(lineColor);
				if (n == 0) {
					g2D.drawLine(0, (int) ((zDim - zCoord) * zVoxScale),
							image.getWidth(this),
							(int) ((zDim - zCoord) * zVoxScale));
					g2D.drawLine((int) (xCoord * xVoxScale), 0,
							(int) (xCoord * xVoxScale), image.getHeight(this));

					BufferedImage bufferedImage = new BufferedImage(
							image.getWidth(null), image.getHeight(null),
							BufferedImage.TYPE_INT_ARGB);
					Graphics gb = bufferedImage.getGraphics();
					gb.setColor(lineColor);
					gb.drawImage(image, 0, 0, null);
					gb.drawLine(0, (int) ((zDim - zCoord) * zVoxScale),
							image.getWidth(this),
							(int) ((zDim - zCoord) * zVoxScale));
					gb.drawLine((int) (xCoord * xVoxScale), 0,
							(int) (xCoord * xVoxScale), image.getHeight(this));
					gb.dispose();
					imgs[0] = bufferedImage;

				} else if (n == 1) {
					g2D.drawLine(0, (int) ((zDim - zCoord) * zVoxScale),
							image.getWidth(this),
							(int) ((zDim - zCoord) * zVoxScale));
					g2D.drawLine((int) (yCoord * yVoxScale), 0,
							(int) (yCoord * yVoxScale), image.getHeight(this));

					BufferedImage bufferedImage = new BufferedImage(
							image.getWidth(null), image.getHeight(null),
							BufferedImage.TYPE_INT_ARGB);
					Graphics gb = bufferedImage.getGraphics();
					gb.setColor(lineColor);
					gb.drawImage(image, 0, 0, null);
					gb.drawLine(0, (int) ((zDim - zCoord) * zVoxScale),
							image.getWidth(this),
							(int) ((zDim - zCoord) * zVoxScale));
					gb.drawLine((int) (yCoord * yVoxScale), 0,
							(int) (yCoord * yVoxScale), image.getHeight(this));
					gb.dispose();
					imgs[1] = bufferedImage;

				} else if (n == 2) {
					g2D.drawLine((int) (xCoord * xVoxScale), 0,
							(int) (xCoord * xVoxScale), image.getHeight(this));
					g2D.drawLine(0, (int) ((yDim - yCoord) * yVoxScale),
							image.getWidth(this),
							(int) ((yDim - yCoord) * yVoxScale));

					BufferedImage bufferedImage = new BufferedImage(
							image.getWidth(null), image.getHeight(null),
							BufferedImage.TYPE_INT_ARGB);
					Graphics gb = bufferedImage.getGraphics();
					gb.setColor(lineColor);
					gb.drawImage(image, 0, 0, null);
					gb.drawLine((int) (xCoord * xVoxScale), 0,
							(int) (xCoord * xVoxScale), image.getHeight(this));
					gb.drawLine(0, (int) ((yDim - yCoord) * yVoxScale),
							image.getWidth(this),
							(int) ((yDim - yCoord) * yVoxScale));
					gb.dispose();
					imgs[2] = bufferedImage;

				}
			} else {
				if (n == 0) {
					imgs[0] = image;
				} else if (n == 1) {
					imgs[1] = image;
				} else if (n == 2) {
					imgs[2] = image;
				}
			}
		}

		/**
		 * Update coordinates from mouse coordinate
		 * 
		 * @param x1
		 *            x mouse position
		 * @param y1
		 *            y mouse position
		 */
		private synchronized void fixCoord(int x1, int y1) {

			if (n == 0) {
				double x = x1 / zoom;
				double y = y1 / zoom;
				x /= xVoxScale;
				y /= zVoxScale;
				xCoord = (int) x;
				zCoord = (int) (zDim - y);
				if (xCoord > xDim)
					xCoord = xDim - 1;
			
				if (zCoord > zDim)
					zCoord = zDim - 1;

				if (xCoord < 0)
					xCoord = 0;
			
				if (zCoord < 0)
					zCoord = 0;
				spin[0].setValue(xCoord+1);
				spin[2].setValue(zCoord+1);
				
			} else if (n == 1) {
				double x = x1 / zoom;
				double y = y1 / zoom;
				x /= yVoxScale;
				y /= zVoxScale;
				yCoord = (int) x;
				zCoord = (int) (zDim - y);
			
				if (yCoord > yDim)
					yCoord = yDim - 1;
				if (zCoord > zDim)
					zCoord = zDim - 1;

				
				if (yCoord < 0)
					yCoord = 0;
				if (zCoord < 0)
					zCoord = 0;
			
				spin[1].setValue(yCoord+1);
				spin[2].setValue(zCoord+1);
			} else if (n == 2) {
				double x = x1 / zoom;
				double y = y1 / zoom;
				x /= xVoxScale;
				y /= yVoxScale;
				xCoord = (int) x;
				yCoord = (int) (yDim - y);
				if (xCoord > xDim)
					xCoord = xDim - 1;
				if (yCoord > yDim)
					yCoord = yDim - 1;
				

				if (xCoord < 0)
					xCoord = 0;
				if (yCoord < 0)
					yCoord = 0;
				
				spin[0].setValue(xCoord+1);
				spin[1].setValue(yCoord+1);

			}
			fixImages();
		}

		/**
		 * This method is overriden to return the preferred size which will be
		 * the width and height of the image plus the zoomed width and height,
		 * plus the gradient and max/min values size.
		 */
		public Dimension getPreferredSize() {
			if (image != null)
				return new Dimension(
						(int) (image.getWidth(this) + (image.getWidth(this) * (zoom - 1))) + 70,
						(int) (image.getHeight(this) + (image.getHeight(this) * (zoom - 1))));
			else
				return new Dimension(0, 0);
		}

		/**
		 * This method set the image to the original size by setting the zoom
		 * factor to 1. i.e. 100%
		 */
		public void originalSize() {
			zoom = 1;
		}

		/**
		 * This method increments the zoom factor with the zoom percentage, to
		 * create the zoom in effect
		 */
		public void zoomIn() {
			zoom += zoomPercentage;
		}

		/**
		 * This method decrements the zoom factor with the zoom percentage, to
		 * create the zoom out effect
		 */
		public void zoomOut() {
			zoom -= zoomPercentage;
			if (zoom < 0.3)
				zoom = 0.3;
		}
	}

	/**
	 * Add listeners. actionComand: selectTemp
	 * 
	 * @param al
	 *            actionListener
	 * @param mml
	 *            MouseMotionListener
	 * @param ml
	 *            MouseListener
	 */
	public void addAllListener(ActionListener al, MouseMotionListener mml,
			MouseListener ml, ChangeListener cl) {

		zoomIn.addMouseListener(ml);
		zoomOut.addMouseListener(ml);
		zoomOriginal.addMouseListener(ml);
		spin[0].addChangeListener(cl);
		spin[1].addChangeListener(cl);
		spin[2].addChangeListener(cl);
		saveToPDF.addActionListener(al);
		saveToPDF.addMouseListener(ml);
		button3D.addMouseListener(ml);
		temp.addActionListener(al);
		showLines.addMouseListener(ml);
	}

	/**
	 * Remove listeners
	 * 
	 * @param al
	 *            actionListener
	 * @param mml
	 *            MouseMotionListener
	 * @param ml
	 *            MouseListener
	 */
	public void removeAllListener(ActionListener al, MouseMotionListener mml,
			MouseListener ml, ChangeListener cl) {
		temp.removeActionListener(al);
		imagePanel[0].removeMouseListener(ml);
		imagePanel[0].removeMouseMotionListener(mml);
		imagePanel[1].removeMouseListener(ml);
		imagePanel[1].removeMouseMotionListener(mml);
		imagePanel[2].removeMouseListener(ml);
		imagePanel[2].removeMouseMotionListener(mml);
		spin[0].removeChangeListener(cl);
		spin[1].removeChangeListener(cl);
		spin[2].removeChangeListener(cl);
		saveToPDF.removeActionListener(al);
	}

	/**
	 * Class used to represent gradients
	 */
	public class Gradient {

		private Vector<Color> colors;
		private Vector<Double> values;
		private String name;

		/**
		 * Constructor
		 * 
		 * @param startColor
		 *            start color
		 * @param endColor
		 *            end color
		 * @param name
		 *            gradient name
		 */
		public Gradient(Color startColor, Color endColor, String name) {
			colors = new Vector<Color>();
			values = new Vector<Double>();
			values.add(0.0);
			values.add(1.0);
			colors.add(startColor);
			colors.add(endColor);
			this.name = name;
		}

		/**
		 * Return the gradient name
		 * 
		 * @return the gradient's name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Get gradient's colors
		 * 
		 * @param c
		 *            Color array
		 * @return Color array
		 */
		public Color[] getColors(Color[] c) {
			return colors.toArray(c);
		}

		/**
		 * Return the colo's position
		 * 
		 * @return float[] with color position
		 */
		public float[] getValues() {
			float[] fl = new float[values.size()];
			for (int i = 0; i < values.size(); i++)
				fl[i] = values.get(i).floatValue();
			return fl;
		}

		/**
		 * Add a color to the gradients
		 * 
		 * @param color
		 *            the new color
		 * @param value
		 *            the color position, o is start and 1 is the end
		 */
		public void add(Color color, double value) {
			boolean done = false;
			for (int k = 0; k < values.size() && !done; k++) {
				double val = values.get(k);
				if (value > val && value < values.get(k + 1)) {
					values.add(k + 1, value);
					colors.add(k + 1, color);
					done = true;
				}
			}
		}

		/**
		 * Get the color in the current point
		 * 
		 * @param point
		 *            the point
		 * @return int[3] with rgb color value
		 */
		public int[] getColor(double point) {
			int[] rgb = new int[3];
			if (point >= 1) {
				Color col = colors.lastElement();
				rgb[0] = col.getRed();
				rgb[1] = col.getGreen();
				rgb[2] = col.getBlue();
			} else if (point <= 0) {
				Color col = colors.firstElement();
				rgb[0] = col.getRed();
				rgb[1] = col.getGreen();
				rgb[2] = col.getBlue();
			}
			for (int k = 0; k < values.size() - 1; k++) {
				if (point == values.get(k)) {
					Color col = colors.get(k);
					rgb[0] = col.getRed();
					rgb[1] = col.getGreen();
					rgb[2] = col.getBlue();
				} else if (point > values.get(k) && point < values.get(k + 1)) {
					Color colStart = colors.get(k);
					Color colEnd = colors.get(k + 1);

					int diffCol = colEnd.getRed() - colStart.getRed();
					double diffVal = values.get(k + 1) - values.get(k);
					double val = point - values.get(k);

					rgb[0] = (int) ((colStart.getRed() + (diffCol / diffVal)
							* val));

					diffCol = colEnd.getGreen() - colStart.getGreen();
					rgb[1] = (int) ((colStart.getGreen() + (diffCol / diffVal)
							* val));

					diffCol = colEnd.getBlue() - colStart.getBlue();
					rgb[2] = (int) ((colStart.getBlue() + (diffCol / diffVal)
							* val));
				}
			}
			return rgb;
		}
	}

	/**
	 * Start a 3D view
	 */
	private void do3D() {
		int alphaMax = 60;
		new View3d(createImages1(alphaMax), createImages2(alphaMax),
				createImages3(alphaMax));
	}

	/**
	 * Create the horizontal image for the 3d view x and y axis
	 * 
	 * @param alphaMax
	 *            max alpha
	 * @return Image[] the horizontal image
	 */
	private Image[] createImages1(int alphaMax) {
		Image[] images1 = new Image[zDim];
		double scale = selectedMax - selectedMin;
		for (int k = 0; k < zDim; k++) {
			int pix[] = new int[xDim * yDim];
			int a = 0;
			for (int y = yDim - 1; y >= 0; y--) {
				for (int x = 0; x < xDim; x++) {
					double pointOriginal = vox[x][y][k];
					double point = pointOriginal - selectedMin;
					point /= scale;
					int[] rgb = new int[3];
					if (pointOriginal != 0) {
						rgb = currentGradient.getColor(point);
						pix[a++] = ((int) (point * alphaMax) << 24)
								| (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
					} else {
						pix[a++] = (0 << 24) | (0 << 16) | (0 << 8) | 0;
					}
				}
			}
			MemoryImageSource image = new MemoryImageSource(xDim, yDim, pix, 0,
					xDim);
			Image img = Toolkit.getDefaultToolkit().createImage(image);
			int newHeight = (int) (img.getHeight(null) * yVoxScale);
			int newWidth = (int) (img.getWidth(null) * xVoxScale);
			img = img
					.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
			images1[k] = img;
		}
		return images1;
	}

	/**
	 * Create vertical image for the 3d view, z and x axis
	 * 
	 * @param alphaMax
	 *            max alpha
	 * @return Image[] image array
	 */
	private Image[] createImages2(int alphaMax) {
		double scale = selectedMax - selectedMin;
		Image[] images2 = new Image[yDim];
		for (int k = 0; k < yDim; k++) {
			int[] pix = new int[zDim * xDim];
			int a = 0;
			for (int z = zDim - 1; z >= 0; z--) {
				for (int x = 0; x < xDim; x++) {
					double pointOriginal = vox[x][k][z];
					double point = pointOriginal - selectedMin;
					point /= scale;
					int[] rgb = new int[3];
					if (pointOriginal != 0) {
						rgb = currentGradient.getColor(point);
						pix[a++] = ((int) (point * alphaMax) << 24)
								| (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
					} else {
						pix[a++] = (0 << 24) | (0 << 16) | (0 << 8) | 0;
					}
				}
			}
			MemoryImageSource image = new MemoryImageSource(xDim, zDim, pix, 0,
					xDim);
			Image img = Toolkit.getDefaultToolkit().createImage(image);
			int newHeight = (int) (img.getHeight(null) * zVoxScale);
			int newWidth = (int) (img.getWidth(null) * xVoxScale);
			img = img
					.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
			images2[k] = img;
		}
		return images2;
	}

	/**
	 * Create vertical image for the 3d view, y and z axis
	 * 
	 * @param alphaMax
	 *            max alpha
	 * @return Image[] image array
	 */
	private Image[] createImages3(int alphaMax) {
		double scale = selectedMax - selectedMin;
		Image[] images3 = new Image[xDim];

		for (int k = 0; k < xDim; k++) {
			int[] pix = new int[zDim * yDim];
			int a = 0;
			for (int z = zDim - 1; z >= 0; z--) {
				for (int y = 0; y < yDim; y++) {
					double pointOriginal = vox[k][y][z];
					double point = pointOriginal - selectedMin;
					point /= scale;
					if (pointOriginal != 0) {
						int[] rgb = new int[3];
						rgb = currentGradient.getColor(point);
						pix[a++] = ((int) (point * alphaMax) << 24)
								| (rgb[0] << 16) | (rgb[1] << 8) | rgb[2];
					} else {
						pix[a++] = (0 << 24) | (0 << 16) | (0 << 8) | 0;
					}

				}
			}

			MemoryImageSource image = new MemoryImageSource(yDim, zDim, pix, 0,
					yDim);
			Image img = Toolkit.getDefaultToolkit().createImage(image);
			int newHeight = (int) (img.getHeight(null) * zVoxScale);
			int newWidth = (int) (img.getWidth(null) * yVoxScale);
			img = img
					.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
			images3[k] = img;
		}
		return images3;
	}

}
