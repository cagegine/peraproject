/*
/////////////////////////
Pera.java;
a class to draw the menu;
it.tetratron.pera.view;
GLP 3;
Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentListener;

/**
 * 
 * Class to draw the menu of PERA software
 *
 */
public class Menu {

	
	private static JPanel workSpace = null;
	
	private static JButton[] menuButtons = null;
	
	/**
	 * Draw all the menu of PERA software
	 * @param logo if true the PERA logo and the credits button will be displayed, else a back to main menu button will be displayed
	 * @param workspace if true a field to select a directory chooser 
	 * @param buttons a matrix with 2 columns in witch the first will be the text of the buttons and the second is the action of the buttons
	 * @param al a listener for the actions performed
	 * @param dl a listener for changes on the text fields
	 * @return a JPanel
	 */
	public static JPanel draw(boolean logo, boolean workspace, String[][] buttons, ActionListener al, DocumentListener dl){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		Insets margin=new Insets(5,5,5,5);
		
		if (workspace){
			workSpace = new RoundedPanel();
			workSpace.setLayout(new GridBagLayout());
			workSpace.add(selectDir("Select a directory", "openDirChooser", al, dl), new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_START,GridBagConstraints.NONE,margin,0,0),0);
			workSpace.add(nameField("Name for output"), new GridBagConstraints(0,1,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_START,GridBagConstraints.NONE,margin,0,0),1);
			aux.add(workSpace, new GridBagConstraints(0,1,2,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0),0);
		}
		
		menuButtons = new JButton[buttons.length];
		
		JButton b;
		for (int i=0; i<buttons.length; i++){
			b=new JButton();
			b=Params.getButton("green");
			b.setHorizontalTextPosition(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setBorderPainted(false);
			b.setContentAreaFilled(false);
			b.setForeground(Color.WHITE);
			b.setFont(new Font(Font.SANS_SERIF,Font.ITALIC,20));		
			b.setText(buttons[i][0]);
			b.setActionCommand(buttons[i][1]);
			b.addActionListener(al);
			b.addMouseListener((MouseListener) al);
			menuButtons[i]=b;
			aux.add(b,new GridBagConstraints(0,i+2,2,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.NONE,margin,0,0));
		}
		
		if (logo){
			ImageIcon l = new ImageIcon((new ImageIcon("./img/logo.png").getImage().getScaledInstance(300, 150, Image.SCALE_SMOOTH)));
			aux.add(new JLabel(l), new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_START,GridBagConstraints.NONE,margin,0,0));
			l = new ImageIcon((new ImageIcon("./img/logoProgetto.png").getImage().getScaledInstance(300, 79, Image.SCALE_SMOOTH)));
			aux.add(new JLabel(l), new GridBagConstraints(0,0,3,1,1.0,1.0,GridBagConstraints.FIRST_LINE_START,GridBagConstraints.BOTH,margin,0,0));
			b=new JButton();
			b=Params.getButton("credits");
			b.setHorizontalTextPosition(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setBorderPainted(false);
			b.setContentAreaFilled(false);
			b.setActionCommand("credits");
			b.addActionListener(al);
			b.addMouseListener((MouseListener) al);
			aux.add(b, new GridBagConstraints(1,0,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_END,GridBagConstraints.NONE,margin,0,0));
			aux.add(Box.createGlue(), new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_START,GridBagConstraints.NONE,margin,0,0));
		}
		else{
			b=new JButton();
			b=Params.getButton("red");
			b.setHorizontalTextPosition(SwingConstants.CENTER);
			b.setOpaque(false);
			b.setBorderPainted(false);
			b.setContentAreaFilled(false);
			b.setForeground(Color.WHITE);
			b.setText("Exit to Main Menu");
			b.setActionCommand("exit");
			b.addActionListener(al);
			b.addMouseListener((MouseListener) al);
			aux.add(b, new GridBagConstraints(1,0,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_END,GridBagConstraints.NONE,margin,0,0));
			aux.add(Box.createGlue(), new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_START,GridBagConstraints.NONE,margin,0,0));
		}
		
		aux.add(Box.createGlue(), new GridBagConstraints(0,buttons.length+2,1,1,1.0,1.0,GridBagConstraints.FIRST_LINE_START,GridBagConstraints.NONE,margin,0,0));
		return aux;
	}

	/**
	 * Draw a JPanel that contain one label, one textField and on button to open the directory chooser  
	 * @param txtLabel the text of the label
	 * @param action the action of the button
 	 * @param al a listener for the actions performed
	 * @param dl a listener for changes on the text fields
	 * @return return a JPanel
	 */
	public static JPanel selectDir(String txtLabel, String action, ActionListener al, DocumentListener dl){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		
		JLabel label = new JLabel(txtLabel);
		
		JTextField text = new JTextField();
		
		text.setColumns(30);
		text.setText(Params.getPath());
		text.getDocument().addDocumentListener(dl);
		text.getDocument().putProperty("parent", text);
		text.setName("Dir");
		//text.setEditable(false);
		
		JButton b = new JButton("browse..");
		b.setActionCommand(action);
		b.addActionListener(al);
		
		aux.add(text,new GridBagConstraints(1,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(0,1,0,1),0,0),0);
		aux.add(b,new GridBagConstraints(2,0,1,1,0,1.0,GridBagConstraints.LINE_END,GridBagConstraints.NONE,new Insets(0,1,0,0),0,0),1);
		aux.add(label,new GridBagConstraints(0,0,1,1,0,1.0,GridBagConstraints.LINE_START,GridBagConstraints.NONE,new Insets(0,0,0,1),0,0),2);
		return aux;
	}
	
	/**
	 * Draw a JPanel containing a label and a text field of 20 columns
	 * @return return a JPanel
	 */
	public static JPanel nameField(String txtLabel){
		JPanel aux = new JPanel();
		aux.setLayout(new GridBagLayout());
		
		JTextField text = new JTextField();
		text.setColumns(20);
		
		aux.add(text, new GridBagConstraints(1,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,5,0,0),0,0),0);
		aux.add(new JLabel(txtLabel), new GridBagConstraints(0,0,1,1,0,0,GridBagConstraints.CENTER,GridBagConstraints.NONE,new Insets(0,0,0,5),0,0));
		aux.add(Box.createGlue(), new GridBagConstraints(2,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.HORIZONTAL,new Insets(0,0,0,0),0,0));
		return aux;
	}
	
	/**
	 * Open a file chooser where is possible to select only directories
	 * @return a String with the path of the directory selected
	 */
	public static String openDirChooser(){
		JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = fc.showOpenDialog(null);		 
		if (returnVal == JFileChooser.APPROVE_OPTION) {		 
			return fc.getSelectedFile().getPath();		
		}
		return "";
	}
	
	/**
	 * This method set a string to the field of the workspace directory panel the given text 
	 * @param s the text to set to the field
	 */
	public static void setDir(String s){
		if (workSpace.getComponent(0) instanceof JPanel){
			JPanel dirChooser = (JPanel) workSpace.getComponent(0);
			if (dirChooser.getComponent(0) instanceof JTextField)
				((JTextField) dirChooser.getComponent(0)).setText(s);
		}
	}
	
	/**
	 * This method set a string to the field of the workspace directory panel the given text 
	 * @param s the text to set to the field
	 */
	public static void setNameForOutput(String s){
		if (workSpace.getComponent(1) instanceof JPanel){
			JPanel dirChooser = (JPanel) workSpace.getComponent(1);
			if (dirChooser.getComponent(0) instanceof JTextField)
				((JTextField) dirChooser.getComponent(0)).setText(s);
		}
	}
	
	/**
	 * This method get the content of the name field of the workspace directory panel
	 */
	public static String getNameForOutput(){
		if (workSpace!=null && workSpace.getComponent(1) instanceof JPanel){
			JPanel dirChooser = (JPanel) workSpace.getComponent(1);
			if (dirChooser.getComponent(0) instanceof JTextField){
				String name=((JTextField) dirChooser.getComponent(0)).getText();
				DateFormat df = new SimpleDateFormat("yyyy-MM-dd_hh.mm.ss");    
				df.setTimeZone(TimeZone.getDefault());
				name+= "_"+df.format(new Date());
				String newName = name.replaceAll("\\s", "_");
				return newName;
			}
		}
		return "";
	}
	
	/**
	 * This method get the content of the choose directory field of the workspace directory panel
	 */
	public static String getDir(){
		if (workSpace!=null && workSpace.getComponent(0) instanceof JPanel){
			JPanel dirChooser = (JPanel) workSpace.getComponent(0);
			if (dirChooser.getComponent(0) instanceof JTextField)
				return ((JTextField) dirChooser.getComponent(0)).getText();
		}
		return "";
	}
	
	/**
	 * Enable/disable all the buttons
	 * @param t if true enable all the buttons
	 * 			if false disable them all
	 */
	public static void enableButtons(boolean t){
		for (int i=0; i<menuButtons.length; i++)
			menuButtons[i].setEnabled(t);
	}
	
	/**
	 * Disable the button selected in the right panel
	 * @param s the action of the button that will be disabled
	 */
	public static void disableButton(String s){
		for (int i=0; i< menuButtons.length; i++)
			if (menuButtons[i].getActionCommand().equals(s))
				menuButtons[i].setEnabled(false);
	}
}
