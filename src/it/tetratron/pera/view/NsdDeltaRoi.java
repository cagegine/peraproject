/*
/////////////////////////
NsdDeltaRoi.java;
a class to draw three component, the first is top left, second bottom left and third right
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato;
TetraTron Group;
2012.03.12
/////////////////////////
 */

package it.tetratron.pera.view;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

/**
 * Class to draw three component, the first is top left, second bottom left
 * and third right
 */
public class NsdDeltaRoi extends JPanel {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor
	 * 
	 * @param comp1
	 *            top left
	 * @param comp2
	 *            bottom left
	 * @param comp3
	 *            right
	 */
	public NsdDeltaRoi(JComponent comp1, JComponent comp2, JComponent comp3) {
		JSplitPane p1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, comp1, comp2);
		JSplitPane p2 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, p1, comp3);
		p2.setDividerLocation(350);
		p1.setResizeWeight(0.5);
		p2.setResizeWeight(0.5);
		p1.setOneTouchExpandable(true);
		p2.setOneTouchExpandable(true);
		setLayout(new BorderLayout());
		add(p2);
	}
}
