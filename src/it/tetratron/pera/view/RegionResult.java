/*
/////////////////////////
Pera.java;
a class to draw the region results;
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.BorderLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

/**
 * Class to draw result of result analysis region level
 * 
 */
public class RegionResult extends JPanel {

	private static final long serialVersionUID = 1L;

	/**
	 * Draw a table with four components, usually used to show result of region
	 * analysis with two chart and a table
	 * 
	 * @param first
	 *            top left component
	 * @param second
	 *            top right component
	 * @return a JPanel
	 */
	public RegionResult(JComponent first, JComponent second) {
		JSplitPane p1 = new JSplitPane(JSplitPane.VERTICAL_SPLIT, first,
				new JScrollPane(second));
		p1.setResizeWeight(0.5);
		p1.setOneTouchExpandable(true);
		setLayout(new BorderLayout());
		add(p1);
	}

}
