/*
/////////////////////////
Pera.java;
a class to create a window for the output of matlab algorithms;
it.tetratron.pera.view;
GLP 3;
Alessandro Feriln, Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.Insets;
import java.io.IOException;
import java.io.OutputStream;

public class MatlabWindow extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JProgressBar bar;
	
	//private ConsoleMatlab cM; 
	
	private JTextArea console;
	
	public MatlabWindow(TextAreaOutputStream textAreaOutputStream){
		createGUI(textAreaOutputStream);
		//setVisible(true);
		setSize(400,400);
		setResizable(false);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); 	
	}
	
	public void createGUI(TextAreaOutputStream output){
		
		setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		setTitle("PERA - Processing data..");
		
		c.insets= new Insets(10,10,10,10);
		c.fill=GridBagConstraints.HORIZONTAL;
		c.weighty=0;
		c.gridy=0;
		c.gridx=0;
		c.gridwidth=2;
		bar=new JProgressBar();
		bar.setIndeterminate(true);
		bar.setString("Working..");
		bar.setStringPainted(true);
		add(bar,c);
		
		/*FRAME BORDE*/
		Image icon = new ImageIcon("./img/Pera_icon.png").getImage();
		setIconImage(icon);
		
		/*set console options*/
		console=new JTextArea();
		output.setTextArea(console);
		console.setEditable(false);
	    console.setBackground(Color.DARK_GRAY);
	    console.setForeground(Color.ORANGE);
	    console.setLineWrap(true);
	    console.setWrapStyleWord(true);
	    
	    /*create JScrollPanel*/
	    JScrollPane scrollArea=new JScrollPane(console);
	    scrollArea.setBorder(null);
	    //scrollArea.getv
	    
	    c.insets=new Insets(5,5,10,10);
	    c.fill = GridBagConstraints.BOTH;
	    c.weightx=1.0;
	    c.weighty=1.0;
	    c.gridy=1;
	    RoundedPanel r= new RoundedPanel();
	    r.setBackground(Color.DARK_GRAY);
	    r.setLayout(new GridBagLayout());
	    r.add(scrollArea,new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(5,5,10,10),5,5));
	    add(r,c);
	    
	    JScrollBar vbar = scrollArea.getVerticalScrollBar();
	    vbar.setValue(vbar.getMaximum());
	    
		/*
		cM = new ConsoleMatlab(logFile);
		cM.setVisible(true);
		cM.setBackground(Color.DARK_GRAY);
		
		c.gridx=0;
		c.gridy=2;
		c.fill=GridBagConstraints.BOTH;
		c.weighty=1.0;
		c.weightx=1.0;
		add(scrollArea,c);
		*/
	}

	public void stop(){
		bar.setIndeterminate(false);
		bar.setValue(100);
		bar.setString("Completed");
		setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	}
	
	/*public static TextAreaOutputStream createTextAreaOutputStream(){
		return new TextAreaOutputStream();
	}
	*/
	
	@Override
	public void setVisible(boolean b){
		console.setText("");
		bar.setIndeterminate(true);
		bar.setString("Working..");
		bar.setStringPainted(true);
    	setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    	super.setVisible(b);
    }
	
	public static class TextAreaOutputStream extends OutputStream {
	    private JTextArea textControl;
	    
	    /**
	     * Creates a new instance of TextAreaOutputStream.
	     * The javax.swing.JTextArea will be must set with setTextArea method.
	     *
	     */
	    public TextAreaOutputStream() {
	  
	    }
	    
	    /**
	     * Creates a new instance of TextAreaOutputStream which writes
	     * to the specified instance of javax.swing.JTextArea control.
	     *
	     * @param control   A reference to the javax.swing.JTextArea
	     *                  control to which the output must be redirected
	     *                  to.
	     */
	    public TextAreaOutputStream( JTextArea control ) {
	        textControl = control;
	    }
	    
	    public void setTextArea(JTextArea textArea){
	    	textControl=textArea;
	    }
	    
	    /**
	     * Writes the specified byte as a character to the 
	     * javax.swing.JTextArea.
	     *
	     * @param   b   The byte to be written as character to the 
	     *              JTextArea.
	     */
	    public void write( int b ) throws IOException {
	        // append the data as characters to the JTextArea control
	    	if(textControl!=null)
	    		textControl.append( String.valueOf( ( char )b ) );
	    	else
	    		System.out.print( String.valueOf( ( char )b ) );
	    }
	    
	}
	
	/*public PrintStream getPrintStream(){
	   return cM.getPrintStream();
	}
*/
}
