/*
/////////////////////////
Pera.java;
a class to draw a chart;
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.StandardTickUnitSource;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Class used to draw a single chart
 */
public class Chart extends JPanel {

	private static final long serialVersionUID = 1L;
	private String xAxis = "";
	private String yAxis = "";
	private String title = "";
	private XYSeriesCollection collection;
	private JFreeChart chart;
	private final JLabel label;
	private List<Boolean> line;

	/**
	 * Constructor
	 */
	public Chart() {
		this.setLayout(new BorderLayout());
		collection = new XYSeriesCollection();
		setLayout(new BorderLayout());
		label = new JLabel();
		label.setBackground(Color.WHITE);
		label.setOpaque(true);
		line = new LinkedList<Boolean>();
	}

	/**
	 * Get a Image of the chart
	 * 
	 * @param width
	 *            width dimension
	 * @param height
	 *            height dimension
	 * @return a BufferedImage
	 */
	public BufferedImage getImage(int width, int height) {
		return chart.createBufferedImage(width, height);
	}

	/**
	 * Set axis title
	 * 
	 * @param x
	 *            title for x
	 * @param y
	 *            title for y
	 */
	public void setAxis(String x, String y) {
		xAxis = x;
		yAxis = y;
	}

	/**
	 * Set chart title
	 * 
	 * @param title
	 *            the title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Add a collection, it will be displayed with line
	 * 
	 * @param title
	 *            collection name
	 * @param x
	 *            x axis value
	 * @param y
	 *            y axis value
	 */
	public void addCollection(String title, List<Double> x, List<Double> y) {
		addCollection(title, x, y, null, null);
	}

	/**
	 * Add a collection that will be displayed with line and the original value
	 * that will be displayed with dots
	 * 
	 * @param title
	 *            collection name
	 * @param x
	 *            x value
	 * @param y
	 *            y value
	 * @param x2
	 *            x original value
	 * @param y2
	 *            y original value
	 */
	public void addCollection(String title, String title2, List<Double> x, List<Double> y,
			List<Double> x2, List<Double> y2) {
		XYSeries series1 = new XYSeries(title);

		Iterator<Double> it1 = x.iterator();
		Iterator<Double> it2 = y.iterator();
		line.add(true);

		while (it1.hasNext())
			series1.add(it1.next(), it2.next());
		collection.addSeries(series1);

		if (x2 != null && y2 != null) {
			if(title2 == ""){
				title2 = title + " original";
			}
				
			XYSeries series2 = new XYSeries(title2);

			Iterator<Double> it3 = x2.iterator();
			Iterator<Double> it4 = y2.iterator();
			while (it3.hasNext())
				series2.add(it3.next(), it4.next());
			collection.addSeries(series2);
			line.add(false);
		}

	}
	
	public void addCollection(String title, List<Double> x, List<Double> y,
			List<Double> x2, List<Double> y2){
		this.addCollection(title, "",  x, y, x2, y2);
	}
	

	/**
	 * Generate the chart, call this method after add all collection
	 */
	@SuppressWarnings("deprecation")
	public void updateChart() {
		chart = ChartFactory.createXYLineChart(title, // chart title
				xAxis, // x axis label
				yAxis, // y axis label
				collection, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips
				false // urls
				);

		chart.setBackgroundPaint(Color.white);
		final XYPlot plot = chart.getXYPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();

		plot.setRenderer(renderer);
		renderer.setDrawSeriesLineAsPath(false);
		renderer.setToolTipGenerator(new StandardXYToolTipGenerator());

		NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
		rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

		Iterator<Boolean> it = line.iterator();
		int i = 0;
		while (it.hasNext()) {
			boolean b = it.next();
			renderer.setSeriesLinesVisible(i, b);
			renderer.setSeriesShapesVisible(i, !b);
			i++;
		}

		ChartPanel chartPanel = new ChartPanel(chart, true, true, true, false,
				true);

		chartPanel.setPreferredSize(new java.awt.Dimension(100, 100));
		chartPanel.setMinimumSize(new java.awt.Dimension(0, 0));

		chartPanel.setMinimumDrawWidth(0);
		chartPanel.setMinimumDrawHeight(0);
		chartPanel.setMaximumDrawWidth(1920);
		chartPanel.setMaximumDrawHeight(1200);

		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(
				getLocale());
		otherSymbols.setDecimalSeparator('.');
		// otherSymbols.setGroupingSeparator('.');
		DecimalFormat df = new DecimalFormat("#0.00", otherSymbols);

		Axis axis = plot.getDomainAxis();
		if (axis != null && axis instanceof NumberAxis) {
			NumberAxis na = (NumberAxis) axis;
			try {
				na.setStandardTickUnits(new StandardTickUnitSource());
				na.setNumberFormatOverride(new DecimalFormat("#0", otherSymbols));
			} catch (IllegalArgumentException iae) {
			}
		}

		axis = plot.getRangeAxis();
		if (axis != null && axis instanceof NumberAxis) {
			NumberAxis na = (NumberAxis) axis;
			try {
				na.setStandardTickUnits(new StandardTickUnitSource());
				na.setNumberFormatOverride(df);
			} catch (IllegalArgumentException iae) {
			}
		}

		add(chartPanel, BorderLayout.CENTER);

		chart.addProgressListener(new ChartProgressListener() {
			@Override
			public void chartProgress(ChartProgressEvent arg0) {
				double x = plot.getDomainCrosshairValue();
				double y = plot.getRangeCrosshairValue();
				double x2 = new BigDecimal(x).setScale(3, BigDecimal.ROUND_UP)
						.doubleValue();
				double y2 = new BigDecimal(y).setScale(3, BigDecimal.ROUND_UP)
						.doubleValue();
				label.setText("  x = " + x2 + " y = " + y2);
			}
		});
		removeAll();
		add(chartPanel, BorderLayout.CENTER);
		add(label, BorderLayout.SOUTH);
		this.setBackground(Color.black);
		revalidate();
	}
}
