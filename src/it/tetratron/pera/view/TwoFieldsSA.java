/*
/////////////////////////
Pera.java;
a class to draw a form for the insertion of the parameters and the input of the files for the SA algorithms that requires SNR or passband;
it.tetratron.pera.view;
GLP 3;
Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

/**
*
* Class to draw and manage the insert parameters form for Rank Shaping SA and SA with Iterative Filter algorithms (at region and voxel levels)
*
*/
public class TwoFieldsSA {
	
	private static JPanel[] inputFile;

	private static JPanel nComp;
	
	private static JPanel beta1;
	
	private static JPanel beta2;
	
	private static JPanel twoFields;
	
	/**
	 * Draw a form for the insertion of the parameters and the input of the files
	 * the index of the first inputFile panel is 4 and it goes to increase by one for each inputFile panel added
	 * the nComp panel index is 0
	 * the beta1 panel index is 1
	 * the beta2 panel index is 2
	 * the twoFields panel index is 3 (SNR or passband)
	 * @param input input a matrix with 2 columns in witch the first will be the text of the labels for the input and the second is the action of the browse buttons
	 * @param nComp the text of the label attached to the number of components text field
	 * @param beta1 the text of the label attached to the beta1 text field
	 * @param beta2 the text of the label attached to the beta2 text field
	 * @param twoFieldsLabel the text of the label attached to the two text fields (SNR or passband)
	 * @param al the controller
	 * @return return a JPanel
	 */
	public static JPanel draw(String[][] input, String nC, String b1, String b2, String twoFieldsLabel, int type, MainWindow m, ActionListener al, DocumentListener dl){
		
		JPanel aux = new RoundedPanel();
		aux.setLayout(new GridBagLayout());
		
		inputFile = new JPanel[input.length];
		
		for (int i=0; i<input.length; i++){
			inputFile[i] = Params.inputFile(input[i][0], input[i][1], input[i][2], al, dl);
			aux.add(inputFile[i], new GridBagConstraints(0,i,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0));
		}
		                                
		nComp = Params.oneField(nC,false, null, null);
		beta1 = Params.oneField(b1,true, "beta1", dl);
		beta2 = Params.oneField(b2,true, "beta2", dl);
		twoFields = Params.twoFields(twoFieldsLabel);
		
		
		aux.add(nComp,new GridBagConstraints(0,input.length+0,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0));
		
		aux.add(beta1,new GridBagConstraints(0,input.length+1,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0));
		
		aux.add(beta2,new GridBagConstraints(0,input.length+2,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0));		
		
		aux.add(twoFields,new GridBagConstraints(0,input.length+3,1,1,1.0,1.0,GridBagConstraints.WEST, GridBagConstraints.NONE,new Insets(10,5,10,10),0,0));
		
		return aux;
	}
	
	/**
	 * This method set a string to the field of the chosen inputFile panel the given text 
	 * @param s the text to set to the field
	 * @param i the index of the chosen inputFile panel
	 */
	public static void setFile(String s, int i){
		if (inputFile[i].getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputFile[i].getComponent(0);
			tf.setText(s);
		}
	}
	
	/**
	 * Set the default value for the NComp field in the view
	 * @param s the string to be set in the field
	 */
	public static void setNComp(String s){
		if (nComp.getComponent(0) instanceof JTextField && nComp.getComponent(1) instanceof JLabel){
			JTextField tf = (JTextField) nComp.getComponent(0);
			tf.setText(s);
		}
	}
	
	/**
	 * Set the default value for the Beta1 field in the view
	 * @param s the string to be set in the field
	 */
	public static void setBeta1(String s){
		if (beta1.getComponent(0) instanceof JTextField && beta1.getComponent(1) instanceof JLabel){
			JTextField tf = (JTextField) beta1.getComponent(0);
			tf.setText(s);
		}
	}
	
	/**
	 * Set the default value for the Beta2 field in the view
	 * @param s the string to be set in the field
	 */
	public static void setBeta2(String s){
		if (beta2.getComponent(0) instanceof JTextField && beta1.getComponent(1) instanceof JLabel){
			JTextField tf = (JTextField) beta2.getComponent(0);
			tf.setText(s);
		}
	}

	public static void alreadyLoadedFiles(DocumentListener c){
		for (int i=0; i<inputFile.length; i++){
			if (inputFile[i].getComponent(0) instanceof JTextField){
				JTextField tf = (JTextField) inputFile[i].getComponent(0);
				tf.getDocument().removeDocumentListener(c);
				tf.setFont(new Font(Font.SANS_SERIF,Font.BOLD,13));
				tf.setText("Already loaded..");
				tf.setEnabled(false);
				inputFile[i].getComponent(1).setEnabled(false);
			}
		}
	}
	
	/**
	 * Check if the voxel file and also the mask file has been selected
	 * @return true if both files has been selected else return false
	 */
	public static boolean checkMask(){
		if (getFilePath(2).equals("") || getFilePath(3).equals(""))
			return false;
		return true;
	}

	
	/**
	 * Set red color to the elements in the chosen inputFile panel
	 * the index of the first inputFile panel is 4 and it goes to increase by one for each inputFile panel in the form
	 * @param i the index of the chosen inputFile panel
	 */
	public static void setFileError(boolean t, int i){
		if (inputFile[i].getComponent(0) instanceof JTextField && inputFile[i].getComponent(2) instanceof JLabel){
			JTextField tf = (JTextField) inputFile[i].getComponent(0);
			JLabel l = (JLabel) inputFile[i].getComponent(2);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}

	/**
	 * Highlights the field of NComp if the user insert a wrong value
	 * @param t - <it>true</it> if the value is wrong and the field has to be highlighted, else <it>false</it>
	 */
	public static void setNCompError(boolean t){
		if (nComp.getComponent(0) instanceof JTextField && nComp.getComponent(1) instanceof JLabel){
			JTextField tf = (JTextField) nComp.getComponent(0);
			JLabel l = (JLabel) nComp.getComponent(1);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}
	
	/**
	 * Highlights the field of Beta1 if the user insert a wrong value
	 * @param t - <it>true</it> if the value is wrong and the field has to be highlighted, else <it>false</it>
	 */
	public static void setBeta1Error(boolean t){
		if (beta1.getComponent(0) instanceof JTextField && beta1.getComponent(1) instanceof JLabel){
			JTextField tf = (JTextField) beta1.getComponent(0);
			JLabel l = (JLabel) beta1.getComponent(1);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}
	
	/**
	 * Highlights the field of Beta2 if the user insert a wrong value
	 * @param t - <it>true</it> if the value is wrong and the field has to be highlighted, else <it>false</it>
	 */
	public static void setBeta2Error(boolean t){
		if (beta2.getComponent(0) instanceof JTextField && beta2.getComponent(1) instanceof JLabel){
			JTextField tf = (JTextField) beta2.getComponent(0);
			JLabel l = (JLabel) beta2.getComponent(1);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}
	
	/**
	 * Sets the default values to the SNR or the passband
	 * @param i to set SNR default values i must been 0 else to set the passband default values i must been 1 with other values nothing happens
	 */
	public static void setTwoFields(int  i, Double d){
		if (twoFields.getComponent(i) instanceof JTextField){
			if (d>0)
				((JTextField) twoFields.getComponent(i)).setText(d.toString());
		}
	}
	
	/**
	 * Set red color to the elements in the chosen twoFields panel
	 * @param t if true set color as red else set it black
	 * @param i the index of the chosen text fields
	 */
	public static void setTwoFieldsError(boolean t){
		if (twoFields.getComponent(2) instanceof JLabel){
			JLabel l = (JLabel) twoFields.getComponent(2);
			if (t){
				twoFields.setBorder(BorderFactory.createLineBorder(Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				twoFields.setBorder(null);
				l.setForeground(Color.BLACK);
			}
		}
	}
	/**
	 *Return the path of the file in the chosen inputFile panel
	 * @param i the index of the chosen inputFile panel
	 * @return a String that contain the path of the selected file
	 */
	public static String getFilePath(int i){
		if (inputFile[i].getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputFile[i].getComponent(0);
			return tf.getText();
		}
		return "";
	}

	/**
	 * Return the number of components
	 * @return an int that contain the number of components
	 */
	public static int getNComp(){
		if (nComp.getComponent(0) instanceof JTextField){
			try{
				return Integer.parseInt((((JTextField) nComp.getComponent(0)).getText()));
			}
			catch(Exception e){
				return -1;
			}
		}
		return -1;
	}
	
	/**
	 * Return the beta1
	 * @return a double that contain the beta1
	 */
	public static double getBeta1(){
		if (beta1.getComponent(0) instanceof JTextField){
			try{
				return Double.parseDouble((((JTextField) beta1.getComponent(0)).getText()));
			}catch(Exception e){
				return -1;
			}
		}
		return -1;
	}
	
	/**
	 * Return the beta2
	 * @return a double that contain the beta2
	 */
	public static double getBeta2(){
		if (beta2.getComponent(0) instanceof JTextField){
			try{
				return Double.parseDouble((((JTextField) beta2.getComponent(0)).getText()));
			}catch(Exception e){
				return -1;
			}
		}
		return -1;
	}
	
	/**
	 * Return the values into the two fields
	 * @return return an array of double with two elements
	 */
	public static double[] getTwoFields(){
		double[] d = {-1,-1};
		if (twoFields.getComponent(0) instanceof JTextField && twoFields.getComponent(1) instanceof JTextField){
			try{
				 d[0]=Double.parseDouble((((JTextField) twoFields.getComponent(0)).getText()));
				 d[1]=Double.parseDouble((((JTextField) twoFields.getComponent(1)).getText()));
			}catch(Exception e){
				return d;
			}
		}
		return d;
	}
}
	
