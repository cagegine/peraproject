/*
/////////////////////////
Pera.java;
a class to draw a layout with a PET image and a chart;
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * a ImageViewer with a SplittableChart in the fourth panel
 */
public class ImageViewerWithChart extends ImageViewer {

	private static final long serialVersionUID = 1L;
	private SplittableChart chart;
	private Chart chart2;
	private JComboBox map;
	private MapListener ml;
	private EstimatesListener el;
	private JButton exit;

	/**
	 * possible state
	 */
	private enum state {
		undefinited, PreProcessing, ResultAnalysis
	};

	private state currentState;

	/**
	 * constructor
	 * 
	 * @param maxTemp
	 *            the maximum time,
	 * @param parent
	 *            Component that contain this
	 */
	public ImageViewerWithChart(int maxTemp, Component parent) {
		super(maxTemp, parent);
		currentState = state.undefinited;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 1;
		c.weighty = 1;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		c.fill = GridBagConstraints.BOTH;
		createMapChooser();

		exit = new JButton("EXIT");
		exit = Params.getButton("small_red");
		exit.setHorizontalTextPosition(SwingConstants.CENTER);
		exit.setOpaque(false);
		exit.setBorderPainted(false);
		exit.setContentAreaFilled(false);
		exit.setForeground(Color.WHITE);
		exit.setText("Main menu");
		exit.setActionCommand("exit");
		top.add(exit);
		ml = new MapListener();
		el = new EstimatesListener();
	}

	/**
	 * create a comboBox used to select maps
	 */
	private void createMapChooser() {
		// map type
		String[] maps = { "mapVb", "mapVt", "mapKi" };
		map = new JComboBox(maps);
		map.setActionCommand("map");
		top.add(map);
	}

	/**
	 * return current map
	 * 
	 * @return 0 = mapVb, 1 = mapVt, or 2 = mapKi
	 */
	public int getMap() {
		return map.getSelectedIndex();
	}

	/**
	 * return the PET image plus the chart image
	 */
	@Override
	public Image[] getImages() {
		Image[] imgs = super.getImages();
		Image[] imgs2 = new Image[4];
		imgs2[0] = imgs[0];
		imgs2[1] = imgs[1];
		imgs2[2] = imgs[2];
		if (currentState == state.PreProcessing)
			imgs2[3] = chart.getImage(600, 600)[0];
		else
			imgs2[3] = chart2.getImage(600, 600);
		return imgs2;
	}

	/**
	 * change the chart with a new one
	 * 
	 * @param chart2
	 *            new chart
	 */
	public void setCharts(SplittableChart chart2) {
		setPreProcessing();
		chart = chart2;
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 2;
		c.weightx = 1;
		c.weighty = 1;
		c.fill = GridBagConstraints.BOTH;
		c.anchor = GridBagConstraints.FIRST_LINE_START;
		panel4.add(chart, c);
		panel4.revalidate();
		panel4.repaint();
	}

	/**
	 * show items needed for pre processing and hide not need items
	 */
	private void setPreProcessing() {
		if (currentState != state.PreProcessing) {
			currentState = state.PreProcessing;
			saveToPDF.setVisible(false);
			estimates.setVisible(false);
			saveToPDF.setVisible(false);
			map.setVisible(false);
			exit.setVisible(false);
			map.removeActionListener(ml);
			super.removeAllListener(null, el, el, el);
		}
	}

	/**
	 * show items needed fo result analysis and hide not need items
	 */
	private void setResultAnalysis() {
		if (currentState != state.ResultAnalysis) {
			currentState = state.ResultAnalysis;
			saveToPDF.setVisible(true);
			estimates.setVisible(true);
			exit.setVisible(true);
			chart2 = new Chart();
			if(chart != null)
				panel4.remove(chart);
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 2;
			c.fill = GridBagConstraints.BOTH;
			c.weightx = 1;
			c.weighty = 1;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			panel4.add(chart2, c);
			map.setVisible(true);
			map.addActionListener(ml);
			super.addAllListener(null, el, el, el);
		}// end if
	}

	/**
	 * set new voxel map
	 * 
	 * @param _ki
	 *            new ki map
	 * @param _vb
	 *            new vb map
	 * @param _vt
	 *            new vt map
	 */
	public void setVoxel(double[][][] _ki, double[][][] _vb, double[][][] _vt) {
		setResultAnalysis();
		ki = _ki;
		vb = _vb;
		vt = _vt;
		selectMap();
	}

	/**
	 * update chart whit custom values for fit and dynPet
	 * 
	 * @param data
	 *            fit vlalues
	 * @param data2
	 *            dynPet values
	 */
	public void fixChart(double[] data, double[] data2, List<Double> temp) {
		if (currentState == state.ResultAnalysis) {
			setResultAnalysis();
		//	chart.reset();
			// fit
			List<Double> y1 = new LinkedList<Double>();
			for (int i = 0; i < data.length; i++) {
				y1.add(data[i]);
			}

			// dynPet
			List<Double> y2 = new LinkedList<Double>();
			for (int i = 0; i < data2.length; i++) {
				y2.add(data2[i]);
			}
/*
			chart2.addData("fit", temp, y1, "time", "fit", 0, true);
			chart2.addData("dynPet", temp, y2, "time", "dynPet", 0, false);
			*/
			
			panel4.remove(chart2);
			chart2 = new Chart();
			if(chart != null)
				panel4.remove(chart);
			GridBagConstraints c = new GridBagConstraints();
			c.gridx = 0;
			c.gridy = 2;
			c.fill = GridBagConstraints.BOTH;
			c.weightx = 1;
			c.weighty = 1;
			c.anchor = GridBagConstraints.FIRST_LINE_START;
			panel4.add(chart2, c);
			
			
			chart2.setAxis("time", "");
			chart2.addCollection("fit", "dynPet",temp, y1, temp, y2);
			chart2.updateChart();
			/*
			chart.revalidate();
			chart.repaint();
*/
		}
	}

	/**
	 * change current displayed map according to the map comboBox selector
	 */
	private void selectMap() {
		int m = map.getSelectedIndex();
		switch (m) {
		case 0:
			setVoxel(vb);
			break;
		case 1:
			setVoxel(vt);
			break;
		case 2:
			setVoxel(ki);
			break;
		}
	}

	public double[] getEstimates() {
		double es[] = new double[3];
		int x = super.getXCoord();
		int y = super.getYCoord();
		int z = super.getZCoord();

		es[0] = vb[x][y][z];
		es[1] = vt[x][y][z];
		es[2] = ki[x][y][z];

		return es;
	}

	/**
	 * update the estimates label with map value
	 */
	private void setEstimates() {
		int x = super.getXCoord();
		int y = super.getYCoord();
		int z = super.getZCoord();

		double vt2 = vt[x][y][z];
		double vb2 = vb[x][y][z];
		double ki2 = ki[x][y][z];

		estimates.setText("<html>estimates:  Vt: " + Table.trunk("" + vt2)
				+ " Vb: " + Table.trunk("" + vb2) + " Ki: "
				+ Table.trunk("" + ki2) + "<html>");
	}

	/**
	 * add listener, actionComands = charts
	 */
	public void addAllListener(ActionListener al, MouseMotionListener mml,
			MouseListener ml, ChangeListener cl) {
		super.addAllListener(al, mml, ml, cl);
		exit.addActionListener(al);
		exit.addMouseListener(ml);
		for (int k = 0; k < 3; k++) {
			imagePanel[k].addMouseListener(ml);
			imagePanel[k].addMouseMotionListener(mml);
		}
	}

	/**
	 * remove listener
	 */
	public void removeAllListener(ActionListener al, MouseMotionListener mml,
			MouseListener ml, ChangeListener cl) {
		super.removeAllListener(al, mml, ml, cl);
		for (int k = 0; k < 3; k++) {
			imagePanel[k].removeMouseListener(ml);
			imagePanel[k].removeMouseMotionListener(mml);
		}
	}

	// listeners
	// ---------------------------------------------------------------------------------------
	/**
	 * listener to change map
	 */
	class MapListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			selectMap();
		}
	}

	/**
	 * listener to change estimates
	 */
	class EstimatesListener implements MouseMotionListener, MouseListener,
			ChangeListener {

		@Override
		public void mouseDragged(MouseEvent arg0) {
			setEstimates();
		}

		@Override
		public void mouseMoved(MouseEvent arg0) {

		}

		@Override
		public void stateChanged(ChangeEvent arg0) {
			setEstimates();
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			setEstimates();
		}

		@Override
		public void mouseEntered(MouseEvent e) {

		}

		@Override
		public void mouseExited(MouseEvent e) {

		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {

		}

	}
}
