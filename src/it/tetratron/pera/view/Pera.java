/*
/////////////////////////
Pera.java;
the class with the main method ;
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato, Luca Guizzon;
TetraTron Group;
2012.02.13
/////////////////////////
 */
package it.tetratron.pera.view;

import it.tetratron.pera.controller.Controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Pera software
 *  
 */
public class Pera {
	/**
	 * main, start Pera software
	 * 
	 * @param args
	 *            inline parameters; not used.
	 */
	/**
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			// Set cross-platform Java L&F (also called "Metal")
			UIManager.setLookAndFeel(UIManager
					.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			// handle exception
		} catch (ClassNotFoundException e) {
			// handle exception
		} catch (InstantiationException e) {
			// handle exception
		} catch (IllegalAccessException e) {
			// handle exception
		}

		
		final JFrame logo = new JFrame(); 
		logo.setSize(350, 200);
		logo.setLocationRelativeTo(null);
		logo.setUndecorated(true);
		ImageIcon l = new ImageIcon((new ImageIcon("./img/logo.png").getImage().getScaledInstance(300, 150, Image.SCALE_SMOOTH)));
		JPanel x = new JPanel();
		x.setLayout(new GridBagLayout());
		x.add(new JLabel(l),new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));
		x.setBackground(Color.WHITE);
		x.setBorder(BorderFactory.createLineBorder(new Color(1,50,32)));
		logo.setLayout(new GridBagLayout());
		logo.add(x,new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(0,0,0,0),0,0));
		logo.setVisible(true);
		
		MainWindow mainW = new MainWindow();
		mainW.setMinimumSize(new Dimension(1024,600));
		new Controller(mainW);
		logo.dispose();
		mainW.setVisible(true);
	}

}
