/*
////////////////////////
name SplittableChartCore.java
show chart with possibility of zoom, change color, hide or show chart
it.tetratron.pera.view
GLP 3
Lorenzo Scorzato
TetraTron Group
2012.03.01
/////////////////////////
 */

package it.tetratron.pera.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.StandardTickUnitSource;
import org.jfree.chart.event.ChartProgressEvent;
import org.jfree.chart.event.ChartProgressListener;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;

/**
 * Class used to show chart with possibility of zoom, change color, hide or show
 * chart
 */
public class SplittableChartCore extends JPanel {

	private static final long serialVersionUID = 1L;
	private int heightActual;
	private int weightActual;
	private int height;
	private int weight;
	private int weight2;
	private int height2;
	private double zoomActual;
	private double zoomPercentage;
	private double zoomPercentage2;
	private Vector<Boolean> visibility;
	private Vector<Color> colors;
	private JFreeChart joinChart;
	private Vector<JPanel> charts;
	private boolean isJoin;
	private Vector<Color> originalColors;
	private Vector<String> xA;
	private Vector<String> yA;
	private Vector<XYSeries> data;
	private Vector<XYSeries> data2;
	private Vector<String> name1;
	private int originalColorNumber;

	private ChartPanel joinPanel;
	private Color[] lastColor;
	private String[] lastName;

	JPanel chartArea;
	private JPanel myJoinPanel;
	private CardLayout card;
	private JPanel one;
	private CardListener cardListener;
	private JScrollPane scrollPane;
	private boolean bar;
	private boolean domainInt = true;

	private Vector<Boolean> line;

	public SplittableChartCore(boolean bar) {
		this(bar, true);
	}

	/**
	 * Constructor
	 * 
	 * @param bar
	 *            if true make bar chart, otherwise line chart
	 */
	public SplittableChartCore(boolean bar, boolean domainInt) {
		this.domainInt = domainInt;
		this.bar = bar;
		isJoin = false;
		xA = new Vector<String>();
		yA = new Vector<String>();
		data = new Vector<XYSeries>();
		data2 = new Vector<XYSeries>();
		charts = new Vector<JPanel>();
		visibility = new Vector<Boolean>();
		line = new Vector<Boolean>();
		name1 = new Vector<String>();
		new Vector<String>();
		colors = new Vector<Color>();
		colors.add(Color.black);
		colors.add(Color.red);
		colors.add(Color.blue);
		colors.add(Color.CYAN);
		colors.add(Color.DARK_GRAY);
		colors.add(Color.green);
		colors.add(Color.magenta);
		colors.add(Color.YELLOW);
		originalColors = new Vector<Color>();
		originalColors.add(Color.CYAN);
		originalColors.add(Color.DARK_GRAY);
		originalColors.add(Color.green);
		originalColors.add(Color.magenta);
		originalColors.add(Color.YELLOW);
		originalColors.add(Color.black);
		originalColors.add(Color.red);
		originalColors.add(Color.blue);
		originalColorNumber = colors.size();
		zoomPercentage = 0.20;
		zoomPercentage2 = 0.05;
		zoomActual = 1;
		weight = 350;
		height = 270;
		weight2 = 500;
		height2 = 350;
		weightActual = 350;
		heightActual = 270;

		lastColor = new Color[2];
		lastName = new String[2];
		card = new CardLayout();
		setLayout(card);
		chartArea = new JPanel(new WrapLayout());
		scrollPane = new JScrollPane(chartArea);
		add(scrollPane, "all");
		one = new JPanel(new BorderLayout());
		add(one, "one");
		card.show(this, "all");
		cardListener = new CardListener(this);

	}

	/**
	 * Remove all charts
	 */
	public void reset() {
		xA.removeAllElements();
		yA.removeAllElements();
		data.removeAllElements();
		data2.removeAllElements();
		charts.removeAllElements();
		visibility.removeAllElements();
		name1.removeAllElements();
		chartArea.removeAll();
	}

	/**
	 * Join all visible chart
	 * 
	 * @param display
	 *            if false don't show the chart
	 */
	public void joinAll(boolean display) {
		weightActual = (int) (weight2 * zoomActual);
		heightActual = (int) (height2 * zoomActual);
		XYSeriesCollection collection = new XYSeriesCollection();
		Iterator<XYSeries> it = data.iterator();
		Iterator<XYSeries> it2 = data2.iterator();
		int i = 0;
		Vector<Color> col = new Vector<Color>();
		List<Boolean> lineOrDot = new LinkedList<Boolean>();
		while (it.hasNext()) {
			XYSeries serie = it.next();
			XYSeries serie2 = it2.next();
			if (visibility.get(i)) {
				collection.addSeries(serie);
				lineOrDot.add(line.get(i));
				col.add(colors.get(i));
				if (serie2 != null) {
					collection.addSeries(serie2);
					lineOrDot.add(false);
					col.add(originalColors.get(i));
				}
			}
			i++;
		}

		JFreeChart chart = createChart("all", collection, col, true, null,
				null, lineOrDot, true);
		joinChart = chart;
		if (display) {
			JPanel chartPanel = createChartPanel(chart);
			chartArea.removeAll();
			chartArea.add(chartPanel);
			myJoinPanel = chartPanel;
			joinPanel = (ChartPanel) chartPanel.getComponent(0);
			charts.removeAllElements();
			isJoin = true;
			chartArea.revalidate();
			chartArea.repaint();
		}
	}

	/**
	 * split all chart and show it
	 */
	public void splitAll() {
		weightActual = (int) (weight * zoomActual);
		heightActual = (int) (height * zoomActual);
		XYSeriesCollection collection;
		XYSeries dataSet;
		XYSeries dataSet2;
		Iterator<XYSeries> it = data.iterator();
		Iterator<XYSeries> it2 = data2.iterator();
		chartArea.removeAll();
		int i = 0;
		charts.removeAllElements();
		while (it.hasNext()) {
			List<Boolean> lineOrDot = new LinkedList<Boolean>();
			Vector<Color> col = new Vector<Color>();
			collection = new XYSeriesCollection();
			dataSet = it.next();
			dataSet2 = it2.next();
			collection.addSeries(dataSet);
			lineOrDot.add(line.get(i));
			col.add(colors.get(i));
			if (dataSet2 != null) {
				collection.addSeries(dataSet2);
				lineOrDot.add(false);
				col.add(originalColors.get(i));
			}

			JFreeChart chart = createChart((String) dataSet.getKey(),
					collection, col, false, xA.get(i), yA.get(i), lineOrDot,
					false);
			JPanel chartPanel = createChartPanel(chart);
			chartArea.add(chartPanel);
			charts.add(chartPanel);
			chartPanel.setVisible(visibility.get(i));
			i++;
		}
		isJoin = false;
		chartArea.revalidate();
		chartArea.repaint();
	}

	/**
	 * Perform a zoom in
	 * 
	 * @param button
	 *            true if performed by click a button
	 */
	public void zoomIn(boolean button) {
		if (button)
			zoomActual += zoomPercentage;
		else
			zoomActual += zoomPercentage2;

		if (!isJoin) {
			weightActual = (int) (weight * zoomActual);
			heightActual = (int) (height * zoomActual);
			// splitAll();
			for (int i = 0; i < charts.size(); i++) {
				JPanel cp = charts.get(i);
				cp.setPreferredSize(new Dimension(weightActual, heightActual));
			}
		} else {
			weightActual = (int) (weight2 * zoomActual);
			heightActual = (int) (height2 * zoomActual);
			joinAll(true);
		}
		chartArea.revalidate();
		chartArea.repaint();
		scrollPane.revalidate();
	}

	/**
	 * Perform a zoom out
	 * 
	 * @param button
	 *            true if performed by click a button
	 */
	public void zoomOut(boolean button) {
		if (button)
			zoomActual -= zoomPercentage;
		else
			zoomActual -= zoomPercentage2;
		if (zoomActual < 0.5)
			zoomActual = 0.5;

		if (!isJoin) {
			weightActual = (int) (weight * zoomActual);
			heightActual = (int) (height * zoomActual);
			for (int i = 0; i < charts.size(); i++) {
				JPanel cp = charts.get(i);
				cp.setPreferredSize(new Dimension(weightActual, heightActual));
			}
		} else {
			weightActual = (int) (weight2 * zoomActual);
			heightActual = (int) (height2 * zoomActual);
			joinAll(true);
		}
		chartArea.revalidate();
		chartArea.repaint();
		scrollPane.revalidate();
	}

	/**
	 * Make a chart
	 * 
	 * @param title
	 *            chart's title
	 * @param collection
	 *            data
	 * @param col
	 *            vector with color
	 * @param join
	 *            if make a join chart
	 * @param xa3
	 *            x axis name
	 * @param ya3
	 *            y axis name
	 * @param lineOrDot
	 *            list that indicates if the data must be represented with line
	 *            (true = line, false = dot) or dot
	 * @param legend
	 *            show or hide legend
	 * @return chart
	 */
	@SuppressWarnings("deprecation")
	private JFreeChart createChart(String title,
			final XYSeriesCollection collection, Vector<Color> col,
			boolean join, String xa3, String ya3, List<Boolean> lineOrDot,
			boolean legend) {

		String xAxis;
		String yAxis;

		xAxis = xa3;
		yAxis = ya3;
		if (join) {
			xAxis = "all";
			yAxis = "all";
		}
		JFreeChart chart;
		// create the chart...
		if (!bar) {
			chart = ChartFactory.createXYLineChart(title, // chart title
					xAxis, // x axis label
					yAxis, // y axis label
					collection, // data
					PlotOrientation.VERTICAL, legend, // include legend
					true, // tooltips
					false // urls
					);
		} else {
			chart = ChartFactory.createXYBarChart(title, // chart title
					xAxis, // x axis label
					false, // date
					yAxis, // y axis label
					collection, // data
					PlotOrientation.VERTICAL, legend, // include legend
					true, // tooltips
					false // urls
					);
		}
		chart.setBackgroundPaint(Color.white);
		chart.setAntiAlias(true);
		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
		final XYPlot plot = chart.getXYPlot();

		if (!bar) {
			plot.setRenderer(renderer);
			Iterator<Boolean> it = lineOrDot.iterator();
			for (int i = 0; i < collection.getSeriesCount(); i++) {
				Boolean b = it.next();
				renderer.setSeriesLinesVisible(i, b);
				renderer.setSeriesShapesVisible(i, !b);
				renderer.setSeriesPaint(i, col.get(i));
			}
		} else {
			for (int i = 0; i < collection.getSeriesCount(); i++) {
				// XYPlot plot2 = (XYPlot) chart.getPlot();
				XYBarRenderer renderer2 = (XYBarRenderer) plot.getRenderer();
				renderer2.setMargin(0.98);
				renderer2.setSeriesPaint(i, col.get(i));

			}
		}

		renderer.setDrawSeriesLineAsPath(false);
		renderer.setToolTipGenerator(new StandardXYToolTipGenerator());

		plot.setBackgroundPaint(Color.lightGray);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);

		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(
				getLocale());
		otherSymbols.setDecimalSeparator('.');
		// otherSymbols.setGroupingSeparator('.');
		DecimalFormat df = new DecimalFormat("#0.00", otherSymbols);

		Axis axis = plot.getDomainAxis();
		if (axis != null && axis instanceof NumberAxis) {
			NumberAxis na = (NumberAxis) axis;
			try {
				na.setStandardTickUnits(new StandardTickUnitSource());
				if (domainInt)
					na.setNumberFormatOverride(new DecimalFormat("#0",
							otherSymbols));
				else
					na.setNumberFormatOverride(df);
			} catch (IllegalArgumentException iae) {
			}
		}

		axis = plot.getRangeAxis();
		if (axis != null && axis instanceof NumberAxis) {
			NumberAxis na = (NumberAxis) axis;
			try {
				na.setStandardTickUnits(new StandardTickUnitSource());
				na.setNumberFormatOverride(df);
			} catch (IllegalArgumentException iae) {
			}
		}

		return chart;
	}

	/**
	 * Make a jpanel with a chart, it add a label to show selected point value
	 * 
	 * @param chart
	 *            the chart
	 * @return jpanel with chart
	 */
	private JPanel createChartPanel(JFreeChart chart) {
		final ChartPanel chartPanel = new ChartPanel(chart, true, true, true,
				false, true);

		chartPanel.setPreferredSize(new java.awt.Dimension(weightActual,
				heightActual));
		chartPanel.setMinimumSize(new java.awt.Dimension(0, 0));
		chartPanel.setMaximumSize(new java.awt.Dimension(2000, 2000));

		JPanel cp = new JPanel();
		cp.setLayout(new BorderLayout());
		cp.add(chartPanel, BorderLayout.CENTER);
		final JLabel label = new JLabel();
		label.setBackground(Color.WHITE);
		label.setOpaque(true);
		cp.add(label, BorderLayout.SOUTH);

		final XYPlot plot = chart.getXYPlot();
		chart.addProgressListener(new ChartProgressListener() {
			@Override
			public void chartProgress(ChartProgressEvent arg0) {
				double x = plot.getDomainCrosshairValue();
				double y = plot.getRangeCrosshairValue();
				double x2 = new BigDecimal(x).setScale(3, BigDecimal.ROUND_UP)
						.doubleValue();
				double y2 = new BigDecimal(y).setScale(3, BigDecimal.ROUND_UP)
						.doubleValue();
				label.setText("  x = " + x2 + " y = " + y2);
			}
		});

		chartPanel.addMouseListener(cardListener);
		cp.addMouseListener(cardListener);

		return cp;
	}

	/**
	 * Add new chart
	 * 
	 * @param title
	 *            chart title
	 * @param x
	 *            x axis value
	 * @param y
	 *            y axis value
	 * @param xA
	 *            x axis name
	 * @param yA
	 *            y axis name
	 */
	public void addData(String title, List<Double> x, List<Double> y,
			String xA, String yA) {

		addData(title, x, y, null, null, xA, yA, true);
	}

	public void addData(String title, List<Double> x, List<Double> y,
			String xA, String yA, boolean line) {

		addData(title, x, y, null, null, xA, yA, line);
	}

	/**
	 * Add new chart
	 * 
	 * @param title
	 *            chart title
	 * @param x
	 *            x axis value for elaborated data
	 * @param y
	 *            y axis value for elaborated data
	 * @param x2
	 *            x axis value for not elaborated data
	 * @param y2
	 *            y axis value for not elaborated data
	 * @param xA2
	 *            x axis name
	 * @param yA2
	 *            y axis name
	 */
	public void addData(String title, List<Double> x, List<Double> y,
			List<Double> x2, List<Double> y2, String xA2, String yA2,
			boolean line) {

		this.line.add(line);

		xA.add(xA2);
		yA.add(yA2);
		String newTitle = title;
		String title2 = (title + " original");
		int k = 0;
		Iterator<String> it = name1.iterator();
		while (it.hasNext()) {
			if (it.next().equals(title))
				k++;
		}
		XYSeries series1;
		if (k != 0) {
			newTitle = title + " " + k;
			series1 = new XYSeries(newTitle);
			title2 = title2 + " " + k;
		} else {
			series1 = new XYSeries(title);
		}
		name1.add(title);
		Iterator<Double> it1 = x.iterator();
		Iterator<Double> it2 = y.iterator();
		while (it1.hasNext()) {
			double a = it1.next();
			double b = it2.next();
			if (bar) {
				if (a < 0)
					a = 0;
				if (b < 0)
					b = 0;
			}
			series1.add(a, b);
		}
		data.add(series1);

		if (x2 != null) {
			XYSeries series2 = new XYSeries(title2);
			Iterator<Double> it3 = x2.iterator();
			Iterator<Double> it4 = y2.iterator();
			while (it3.hasNext()) {
				double a = it3.next();
				double b = it4.next();
				if (bar) {
					if (a < 0)
						a = 0;
					if (b < 0)
						b = 0;
				}
				series2.add(a, b);
			}
			data2.add(series2);
		} else {
			data2.add(null);
		}

		final int i = data.size() - 1;
		if (i >= colors.size()) {
			colors.add(colors.get(i % originalColorNumber));
		}
		if (i >= originalColors.size()) {
			originalColors.add(originalColors.get(i % originalColorNumber));
		}

		lastColor[0] = colors.get(i);
		lastColor[1] = originalColors.get(i);
		lastName[0] = newTitle;
		lastName[1] = title2;

		visibility.add(new Boolean(true));
		joinAll(false);
		if (isJoin)
			joinAll(true);
		else
			splitAll();
		revalidate();

	}

	/**
	 * Change color of a chart's elaborate data
	 * 
	 * @param index
	 *            chart's index
	 * @param newCol
	 *            new color
	 */
	public void changeColor(int index, Color newCol) {
		colors.set(index, newCol);
		if (!isJoin) {
			splitAll();
			revalidate();
		} else {
			joinAll(true);
			revalidate();
		}
	}

	/**
	 * Change color of a chart's not elaborate data
	 * 
	 * @param index
	 *            chart's index
	 * @param newCol
	 *            new color
	 */
	public void changeOriginalColor(int index, Color newCol) {
		originalColors.set(index, newCol);
		if (!isJoin) {
			splitAll();
			revalidate();
		} else {
			joinAll(true);
			revalidate();
		}
	}

	/**
	 * Show or hide a chart
	 * 
	 * @param index
	 *            chart's index
	 * @param vis
	 *            is true show the chart, otherwise hide it
	 */
	public void changeVisibility(int index, boolean vis) {
		visibility.set(index, vis);
		if (joinPanel != null) {
			// join
			// make a new chart
			joinAll(true);

		} else {
			charts.get(index).setVisible(vis);
		}
		revalidate();
		repaint();
	}

	/**
	 * Get chart image
	 * 
	 * @param width
	 *            image's width
	 * @param height
	 *            image's height
	 * @return the image
	 */
	public BufferedImage getImage(int width, int height) {
		if (joinChart != null)
			return joinChart.createBufferedImage(width, height);
		return null;
	}

	/**
	 * Get colors of last chart inserted
	 * 
	 * @return Color[2], color of elaborated and not elaborated data
	 */
	public Color[] getLastColor() {
		return lastColor;
	}

	/**
	 * Get last inserted chart's name
	 * 
	 * @return String[2] with name of elaborated and not elaborated data
	 */
	public String[] getLastName() {
		return lastName;
	}

	/**
	 * Get color of a chart
	 * 
	 * @param i
	 *            chart's index
	 * @return Color[2] with color of elaborated and not elaborated data
	 */
	public Color[] getColors(int i) {
		Color[] col = new Color[2];
		col[0] = colors.get(i);
		col[1] = colors.get(i);
		return col;
	}

	/**
	 * Listener used to show a chart in all available space
	 */
	private class CardListener extends MouseAdapter {

		private JPanel pan;
		private boolean all;

		/**
		 * Constructor
		 * 
		 * @param pan
		 *            panel with card layout
		 */
		public CardListener(JPanel pan) {
			this.pan = pan;
			all = true;
		}

		@Override
		/**
		 * Show a chart in all available space or return to a normal view by double click
		 */
		public void mouseClicked(MouseEvent arg0) {
			if (arg0.getClickCount() == 2) {
				if (all) {
					one.removeAll();
					if (arg0.getSource() instanceof ChartPanel) {
						if (isJoin)
							one.add(myJoinPanel);
						else {
							for (int i = 0; i < charts.size(); i++)
								if (charts.get(i).getComponent(0) == arg0
										.getSource())
									one.add(charts.get(i));
						}
					} else
						one.add((Component) arg0.getSource());
					one.revalidate();
					card.show(pan, "one");
				} else {
					chartArea.removeAll();
					if (isJoin)
						chartArea.add(myJoinPanel);
					else
						for (int i = 0; i < charts.size(); i++)
							chartArea.add(charts.get(i));
					card.show(pan, "all");
				}
				all = !all;
				chartArea.revalidate();
				chartArea.repaint();
				scrollPane.revalidate();
				scrollPane.repaint();
			}
		}
	}
}
