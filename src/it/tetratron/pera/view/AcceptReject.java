/*
/////////////////////////
Pera.java;
a class to draw the accept and reject buttons under a component;
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Class used to draw accept and reject button under a component
 */
public class AcceptReject extends JPanel{

	private static final long serialVersionUID = 1L;

	/**
	 * Constructor, make a JPanel with accept and reject button under a component
	 * 
	 * @param comp
	 *            the component
	 * @param name
	 *            String[] with accept and reject button name respectively
	 * @param action
	 *            String[] with accept and reject button actionCommand
	 *            respectively
	 * @param al
	 *            actionListener
	 */
	public AcceptReject(JComponent comp, String[] name, String[] action, ActionListener al) {
		JPanel pan = new JPanel();
		pan.setLayout(new GridBagLayout());
		JButton b;
		String[] verde_rosso = {"green_very_small", "small_red"};
		for (int i=0; i<name.length; i++){
		b = Params.getButton(verde_rosso[i]);
		b.setText(name[i]);
		b.setActionCommand(action[i]);
		b.addActionListener(al);
		b.setHorizontalTextPosition(SwingConstants.CENTER);
		b.setOpaque(false);
		b.setBorderPainted(false);
		b.setContentAreaFilled(false);
		b.setForeground(Color.WHITE);
		pan.add(b, new GridBagConstraints(i, 0, 1, 1, 0, 0,
				GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(
						0, 0, 0, 0), 0, 0));
		}
		setLayout(new BorderLayout());
		add(comp, BorderLayout.CENTER);
		add(pan, BorderLayout.SOUTH);
	}
}
