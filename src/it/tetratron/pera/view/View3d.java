/*
/////////////////////////
Pera.java;
a class to show a 3d view of voxel
it.tetratron.pera.view;
GLP 3;
Lorenzo Scorzato;
TetraTron Group;
2012.03.12
/////////////////////////
 */

package it.tetratron.pera.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.behaviors.mouse.MouseTranslate;
import com.sun.j3d.utils.behaviors.mouse.MouseWheelZoom;
import com.sun.j3d.utils.geometry.Box;
import com.sun.j3d.utils.geometry.Primitive;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.pickfast.PickCanvas;
import com.sun.j3d.utils.universe.*;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.PickInfo;
import javax.media.j3d.Texture;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.media.j3d.TransparencyAttributes;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3f;

/**
 * Class to show a 3d view of voxel
 */
public class View3d extends JFrame {

	private static final long serialVersionUID = 1L;
	private Box box;
	private int height = 600;
	private int width = 600;
	private Canvas3D canvas;
	private SimpleUniverse universe;
	private BranchGroup group = new BranchGroup();
	private PickCanvas pickCanvas;
	private TransformGroup boxTransformGroup;
	private Image[] images1;
	private Image[] images2;
	private Image[] images3;
	private JLabel wait;
	private JProgressBar prog;

	private TransparencyAttributes ta;
	private float dim;

	/**
	 * Constructor
	 * 
	 * @param images1
	 *            horizontal image
	 * @param images2
	 *            vertical image
	 * @param images3
	 *            vertical image 2
	 */
	public View3d(Image[] images1, Image[] images2, Image[] images3) {

		this.images1 = images1;
		this.images2 = images2;
		this.images3 = images3;

		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		ImageIcon ii = new ImageIcon("./img/loading3D.gif");
		wait = new JLabel("", ii, JLabel.CENTER) {
			private static final long serialVersionUID = 1L;

			public void paint(Graphics g) {
				Graphics2D g2 = (Graphics2D) g;
				g2.setColor(new Color(255, 255, 255, 100));
				g2.fillRect(0, 0, getWidth(), getHeight());
				super.paint(g2);
			}
		};
		add(wait, BorderLayout.CENTER);
		prog = new JProgressBar();
		int n = images1.length + images2.length + images3.length + 15;
		prog.setMaximum(n);
		add(prog, BorderLayout.NORTH);
		setSize(width, height);
		setVisible(true);
		new Thread() {
			public void run() {
				startDrawing();
			}
		}.start();

	}

	/**
	 * Start drawing voxel
	 */
	private void startDrawing() {
		setLayout(new BorderLayout());
		GraphicsConfiguration config = SimpleUniverse
				.getPreferredConfiguration();

		//canvas = new Canvas3D(config);
		
		canvas = new Canvas3D(config)
	    {
	        private static final long serialVersionUID = 7144426579917281131L;

	        public void postRender()
	        {
	            this.getGraphics2D().setColor(Color.white);
	            this.getGraphics2D().drawString("Heads Up Display (HUD) Works!",000,000);
	            this.getGraphics2D().flush(false);
	        }
	    };

		universe = new SimpleUniverse(canvas);

		positionViewer();
		getScene();
		universe.addBranchGraph(group);
		prog.setValue(prog.getValue() + 1);
		pickCanvas = new PickCanvas(canvas, group);
		prog.setValue(prog.getValue() + 1);
		pickCanvas.setMode(PickInfo.PICK_BOUNDS);
		prog.setValue(prog.getValue() + 1);
		remove(prog);
		prog.setValue(prog.getValue() + 1);
		remove(wait);
		prog.setValue(prog.getValue() + 1);
		add(canvas, BorderLayout.CENTER);
		prog.setValue(prog.getValue() + 1);
		validate();
	}

	/**
	 * Create geometry
	 */
	private void getScene() {
		MouseRotate behavior = new MouseRotate();
		BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0),
				100.0);

		boxTransformGroup = new TransformGroup();
		boxTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
		boxTransformGroup.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
		behavior.setTransformGroup(boxTransformGroup);
		boxTransformGroup.addChild(behavior);

		behavior.setSchedulingBounds(bounds);

		ta = new TransparencyAttributes();
		ta.setTransparencyMode(TransparencyAttributes.BLEND_ONE);
		ta.setTransparency(0.9f);
		dim = 0;

		createHorizontal();
		createVertical1();
		createVertical2();

		MouseWheelZoom mouseW = new MouseWheelZoom(boxTransformGroup);
		prog.setValue(prog.getValue() + 1);
		mouseW.setSchedulingBounds(bounds);
		prog.setValue(prog.getValue() + 1);
		mouseW.setFactor(-0.02);
		prog.setValue(prog.getValue() + 1);
		group.addChild(mouseW);
		prog.setValue(prog.getValue() + 1);
		MouseTranslate mouset = new MouseTranslate(boxTransformGroup);
		prog.setValue(prog.getValue() + 1);
		mouset.setSchedulingBounds(bounds);
		prog.setValue(prog.getValue() + 1);
		mouset.setFactor(0.008);
		prog.setValue(prog.getValue() + 1);
		group.addChild(mouset);
		prog.setValue(prog.getValue() + 1);
		group.addChild(boxTransformGroup);
		prog.setValue(prog.getValue() + 1);
	}

	/**
	 * Create horizontal slice
	 */
	private void createHorizontal() {

		for (int i = 0; i < images1.length; i++) {
			TextureLoader tl = new TextureLoader(images1[i], new Container());
			Texture texture = tl.getTexture();
			texture.setBoundaryModeS(Texture.CLAMP);
			texture.setBoundaryModeT(Texture.CLAMP);
			Appearance ap2 = new Appearance();
			ap2.setTransparencyAttributes(ta);
			ap2.setTexture(texture);

			box = new Box(0.5f, 0, 0.5f, Primitive.GENERATE_TEXTURE_COORDS, ap2);

			Appearance ap3 = new Appearance();
			ap3.setTransparencyAttributes(ta);

			BufferedImage bufferedImage = new BufferedImage(
					images1[i].getWidth(null), images1[i].getHeight(null),
					BufferedImage.TYPE_INT_ARGB);

			Graphics gb = bufferedImage.getGraphics();
			gb.drawImage(images1[i], 0, 0, null);
			gb.dispose();

			AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
			tx.translate(0, -images1[i].getHeight(null));
			AffineTransformOp op = new AffineTransformOp(tx,
					AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			bufferedImage = op.filter(bufferedImage, null);

			TextureLoader tl2 = new TextureLoader(bufferedImage);
			Texture texture2 = tl2.getTexture();
			texture2.setBoundaryModeS(Texture.CLAMP);
			texture2.setBoundaryModeT(Texture.CLAMP);

			ap3.setTexture(texture2);
			box.getShape(Box.BOTTOM).setAppearance(ap3);

			box.setCapability(Box.ENABLE_APPEARANCE_MODIFY);
			box.setCapability(Box.GEOMETRY_NOT_SHARED);

			Appearance tras = new Appearance();
			TransparencyAttributes ta2 = new TransparencyAttributes();
			ta2.setTransparencyMode(TransparencyAttributes.NICEST);
			ta2.setTransparency(1);
			tras.setTransparencyAttributes(ta2);

			TransformGroup tg = new TransformGroup();
			Transform3D transform = new Transform3D();

			float pos = (box.getXdimension() / images1.length)
					* (i - (images1.length / 2));
			if (i == images1.length - 1)
				dim = pos;

			Vector3f vector = new Vector3f(0, pos, 0);
			transform.setTranslation(vector);
			tg.setTransform(transform);
			tg.addChild(box);
			prog.setValue(prog.getValue() + 1);
			boxTransformGroup.addChild(tg);

		}
	}

	/**
	 * Create vertical slice
	 */
	private void createVertical1() {

		for (int i = 0; i < images3.length; i++) {
			TextureLoader tl = new TextureLoader(images3[i], new Container());
			Texture texture = tl.getTexture();
			texture.setBoundaryModeS(Texture.CLAMP);
			texture.setBoundaryModeT(Texture.CLAMP);
			Appearance ap2 = new Appearance();
			ap2.setTransparencyAttributes(ta);
			ap2.setTexture(texture);

			box = new Box(0, dim, 0.5f, Primitive.GENERATE_TEXTURE_COORDS, null);

			box.setCapability(Box.ENABLE_APPEARANCE_MODIFY);
			box.setCapability(Box.GEOMETRY_NOT_SHARED);

			Appearance ap3 = new Appearance();
			ap3.setTransparencyAttributes(ta);

			BufferedImage bufferedImage = new BufferedImage(
					images3[i].getWidth(null), images3[i].getHeight(null),
					BufferedImage.TYPE_INT_ARGB);

			Graphics gb = bufferedImage.getGraphics();
			gb.drawImage(images3[i], 0, 0, null);
			gb.dispose();

			AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
			tx.translate(-images3[i].getWidth(null), 0);
			AffineTransformOp op = new AffineTransformOp(tx,
					AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			bufferedImage = op.filter(bufferedImage, null);

			TextureLoader tl2 = new TextureLoader(bufferedImage);
			Texture texture2 = tl2.getTexture();

			texture2.setBoundaryModeS(Texture.CLAMP);
			texture2.setBoundaryModeT(Texture.CLAMP);
			ap3.setTexture(texture2);

			box.getShape(Box.RIGHT).setAppearance(ap2);
			box.getShape(Box.LEFT).setAppearance(ap3);

			TransformGroup tg = new TransformGroup();
			Transform3D transform = new Transform3D();

			float pos = ((box.getZdimension() * 2) / images3.length)
					* (i - (images3.length / 2));

			Vector3f vector = new Vector3f(pos, 0, 0);
			transform.setTranslation(vector);
			tg.setTransform(transform);
			tg.addChild(box);
			prog.setValue(prog.getValue() + 1);
			boxTransformGroup.addChild(tg);
		}
	}

	/**
	 * Create other vertical slice
	 */
	private void createVertical2() {
		for (int i = 0; i < images2.length; i++) {

			TextureLoader tl = new TextureLoader(
					images2[images2.length - i - 1], new Container());
			Texture texture = tl.getTexture();
			texture.setBoundaryModeS(Texture.CLAMP);
			texture.setBoundaryModeT(Texture.CLAMP);
			Appearance ap2 = new Appearance();
			ap2.setTransparencyAttributes(ta);
			ap2.setTexture(texture);

			box = new Box(0.5f, dim, 0, Primitive.GENERATE_TEXTURE_COORDS, ap2);

			Appearance ap3 = new Appearance();
			ap3.setTransparencyAttributes(ta);

			BufferedImage bufferedImage = new BufferedImage(
					images2[images2.length - i - 1].getWidth(null),
					images2[images2.length - i - 1].getHeight(null),
					BufferedImage.TYPE_INT_ARGB);

			Graphics gb = bufferedImage.getGraphics();
			gb.drawImage(images2[images2.length - i - 1], 0, 0, null);
			gb.dispose();

			AffineTransform tx = AffineTransform.getScaleInstance(-1, 1);
			tx.translate(-images2[images2.length - i - 1].getWidth(null), 0);
			AffineTransformOp op = new AffineTransformOp(tx,
					AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			bufferedImage = op.filter(bufferedImage, null);

			TextureLoader tl2 = new TextureLoader(bufferedImage);
			Texture texture2 = tl2.getTexture();

			ap3.setTexture(texture2);
			box.getShape(Box.BACK).setAppearance(ap3);

			box.setCapability(Box.ENABLE_APPEARANCE_MODIFY);
			box.setCapability(Box.GEOMETRY_NOT_SHARED);

			TransformGroup tg = new TransformGroup();
			Transform3D transform = new Transform3D();
			float pos = ((box.getXdimension() * 2) / images2.length)
					* (i - (images2.length / 2));

			Vector3f vector = new Vector3f(0, 0, pos);
			transform.setTranslation(vector);
			tg.setTransform(transform);
			tg.addChild(box);
			prog.setValue(prog.getValue() + 1);
			boxTransformGroup.addChild(tg);
		}
	}

	/**
	 * Position the camera
	 */
	private void positionViewer() {
		ViewingPlatform vp = universe.getViewingPlatform();
		TransformGroup tg1 = vp.getViewPlatformTransform();
		Transform3D t3d = new Transform3D();
		tg1.getTransform(t3d);
		vp.setNominalViewingTransform();
	}
}