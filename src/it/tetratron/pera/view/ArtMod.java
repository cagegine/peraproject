/*
/////////////////////////
Pera.java;
a class to draw a form for the insertion of the parameters and the input of the files for Arterial Modelling algorithm;
it.tetratron.pera.view;
GLP 3;
Luca Guizzon;
TetraTron Group;
2012.03.12
/////////////////////////
 */
package it.tetratron.pera.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

/**
 *
 * Class to draw and manage the insert parameters form for arterial modelling algorithm
 *
 */
public class ArtMod {
	
	private static JPanel inputArt;
	
	private static JPanel parIn;
	
	private static JPanel comboBox;
	
	/**
	 * draw a form for the insertion of the parameters and the input of the files
	 * @param input a matrix with 2 columns in witch the first will be the text of the label for the input and the second is the action of the browse button (only the first row is conidered)
	 * @param comboLabel the text of the label attached to the comboBox
	 * @param items an array containing all the item that will be added to the comboBox
	 * @param al a listener for the actions performed
	 * @param dl a listener for changes on the text fields
	 * @return return a JPanel
	 */
	public static JPanel draw(String[][] input, String comboLabel, String[] items, ActionListener al, DocumentListener dl){
		
		JPanel aux = new RoundedPanel();
		aux.setLayout(new GridBagLayout());
		
		inputArt = Params.inputFile(input[0][0], input[0][1], input[0][2], al, dl); 
		parIn = Params.parIn();
		comboBox = Params.comboBox(comboLabel, items);
		
		aux.add(inputArt, new GridBagConstraints(0,0,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0),0);
		aux.add(parIn,new GridBagConstraints(0,1,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0),1);
		aux.add(comboBox,new GridBagConstraints(0,2,1,1,1.0,1.0,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,new Insets(10,5,10,10),0,0),2);
		
		return aux;
	}
	
	/**
	 * This method set a string to the field of the inputFile panel the given text 
	 * @param s the text to set to the field
	 */
	public static void setFile(String s){
		if (inputArt.getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			tf.setText(s);
		}
	}
	
	/**
	 * Disable the file chooser and set to the text field the text "Already loaded.."
	 * @param c the document listener that need to been removed from the text field 
	 */
	public static void alreadyLoadedFile(DocumentListener c){
		if (inputArt.getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			tf.getDocument().removeDocumentListener(c);
			tf.setFont(new Font(Font.SANS_SERIF,Font.BOLD,13));
			tf.setText("Already loaded..");
			tf.setEnabled(false);
			inputArt.getComponent(1).setEnabled(false);
		}
	}

	/**
	 * Set red/black color to the elements in the input panel
	 * @param t if true set color as red else set it black
	 */
	public static void setFileError(boolean t){
		if (inputArt.getComponent(0) instanceof JTextField && inputArt.getComponent(2) instanceof JLabel){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			JLabel l = (JLabel) inputArt.getComponent(2);
			if (t){
				tf.setBorder(BorderFactory.createBevelBorder(1,Color.RED,Color.RED));
				l.setForeground(Color.RED);
			}
			else{
				tf.setBorder((new JTextField()).getBorder());
				l.setForeground(Color.BLACK);
			}
		}
	}
	
	/**
	 * Sets the par in fields with the given values
	 * @param d a vector of values to set to the fields
	 */
	public static void setParIn(Vector<Double> d){
		for (int i=0; i<d.size(); i++){
			if (parIn.getComponent(i) instanceof JTextField){
				((JTextField)parIn.getComponent(i)).setText(d.get(i).toString());
			}
		}
	}
	
	/**
	 * Set red/black color to the parIn panel
	 * @param t if true set color as red else set it black
	 */
	public static void setParInError(boolean t){
		JLabel l = (JLabel) parIn.getComponent(12);
		if (t){
			parIn.setBorder(BorderFactory.createLineBorder(Color.RED));
			l.setForeground(Color.RED);
		}
		else{
			parIn.setBorder(null);
			l.setForeground(Color.BLACK);
		}
	}
	/**
	 * Give the path of the file
	 * @return a String that contain the path of the selected file
	 */
	public static String getFilePath(){
		if (inputArt.getComponent(0) instanceof JTextField){
			JTextField tf = (JTextField) inputArt.getComponent(0);
			return tf.getText();
		}
		return "";
	}
	
	/**
	 * Give the parIn values
	 * @return return an array of Double
	 */
	public static Vector<Double> getParIn(){
		Vector<Double> d=new Vector<Double>();
		for (int i=0; i<6; i++){
			if (parIn.getComponent(i) instanceof JTextField){
				try{
					d.add(Double.parseDouble(((JTextField) parIn.getComponent(i)).getText()));
				}catch(Exception e){
					d.add(new Double(-1));
				}
			}
		}
		return d;
	}
	
	/**
	 * Give the index of the selected type (Solve or Fit)
	 * @return return an int
	 */
	public static int getType(){
		if (comboBox.getComponent(0) instanceof JComboBox){
			return ((JComboBox) comboBox.getComponent(0)).getSelectedIndex();//.getSelectedItem().toString();
		}
		return 0;
	}
	
}