/*
 /////////////////////////
 File name ExporterCSV.java;
 Description class used to export in CSV format the data shown in Results Analysis
 at Region Level
 Package it.tetratron.pera.model.exporters;
 License GLP 3;
 Author ( s ) Cabbia Davide;
 Owner TetraTron Group;
 Date 14/3/2012;
 /////////////////////////
 */
package it.tetratron.pera.model.exporters;

import it.tetratron.pera.model.manage.ErrorExc;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

/**
 * Class used to export in CSV format the data shown in Results Analysis at Region Level
 * @author Cabbia Davide
 *
 */
public class ExporterCSV {

	private Object[][] fit;
	private Object[][] alfa;
	private Object[][] beta;
	private Object[][] parameters;
	private String pathToSaveFile;
	private String pathFit;
	private String pathAlfa;
	private String pathBeta;
	private String pathParameters;
	
	/**
	 * Creates 4 CSV file contaning the matrix displayed during analysis of results
	 * @param _path must be a String, contains the path (and filename) where to save all four file
	 * @param _fit must be a double dimension array of object,
	 * contains the matrix (with labels) of fit
	 * @param _alfa must be a double dimension array of object,
	 * contains the matrix (with labels) of alfa
	 * @param _beta must be a double dimension array of object,
	 * contains the matrix (with labels) of beta
	 * @param _parameters must be a double dimension array of object,
	 * contains the matrix (with labels) of parameters
	 * @throws ErrorExc
	 */
	public ExporterCSV(String _path,Object[][] _fit,Object[][] _alfa,Object[][] _beta, Object[][] _parameters) throws ErrorExc{
	fit= _fit;
	alfa=_alfa;
	beta=_beta;
	parameters=_parameters;
	pathToSaveFile=_path;
	buildFilePath();
	// write files
	writeCSV(fit,pathFit);
	writeCSV(alfa,pathAlfa);
	writeCSV(beta,pathBeta);
	writeCSV(parameters,pathParameters);
	}
	
	/**
	 * Generates name of all the four files
	 */
	private void buildFilePath(){
		if(!pathToSaveFile.endsWith(".csv")){
			pathFit=pathToSaveFile+"_fit.csv";
			pathAlfa=pathToSaveFile+"_alfa.csv";
			pathBeta=pathToSaveFile+"_beta.csv";
			pathParameters=pathToSaveFile+"_parameters.csv";
		}
		if(pathToSaveFile.endsWith(".csv")){
			pathFit=pathToSaveFile.replaceAll(".csv","_fit.csv");
			pathAlfa=pathToSaveFile.replaceAll(".csv","_alfa.csv");
			pathBeta=pathToSaveFile.replaceAll(".csv","_beta.csv");
			pathParameters=pathToSaveFile.replaceAll(".csv","_parameters.csv");
		}
	}
	/**
	 * Generates and save to disk a CSV file containing matrix to path
	 * @param matrix must be a double dimension array of object,
	 * contains a matrix (with labels)
	 * @param path must be a String, contains the path (and filename) where to save file
	 * @throws ErrorExc
	 */
	private void writeCSV(Object[][] matrix, String path) throws ErrorExc{
		ICsvMapWriter writer=null;
		try {
			int dime= matrix[0].length;
			String[] header=new String[dime];
			for(int i=0;i<dime;i++){
				header[i]=matrix[0][i].toString();
			}
			//final String[] header = new String[] { "asd", "lol", "rofl" };
			writer = new CsvMapWriter(new FileWriter(path), CsvPreference.EXCEL_PREFERENCE);
			final HashMap<String, ? super Object> data = new HashMap<String, Object>();
			writer.writeHeader(header);
	
			for(int i=1;i<matrix.length;i++){
				for(int j=0;j<matrix[0].length;j++){
					data.put(header[j], matrix[i][j]);
				}
				writer.write(data,header);
			}
		} catch (IOException e) {
			throw new ErrorExc("IOException", e.getMessage());
		}

		finally {
			try {
				writer.close();
			} catch (IOException e) {
				throw new ErrorExc("IOException", e.getMessage());
			}
			catch(NullPointerException e) {
				throw new ErrorExc("NullPointerException",e.getMessage());
			}
		}
	}
}
