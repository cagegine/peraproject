/*
 /////////////////////////
 File name ExporterPDF.java;
 Description Class used to export in PDF format the data rappresented in Results Analysis at Voxel Level
 Package it.tetratron.pera.model.exporters;
 License GLP 3;
 Author ( s ) Cabbia Davide;
 Owner TetraTron Group;
 Date 14/3/2012;
 /////////////////////////
 */
package it.tetratron.pera.model.exporters;

import it.tetratron.pera.model.manage.ErrorExc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Date;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Section;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;


/**
 * Class used to export in PDF format the data shown in Results Analysis at Voxel Level
 * @author Cabbia Davide
 *
 */
public class ExporterPDF {
	//private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
	private static Font catFont = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
	//private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	//private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
	private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD);
	private static Font small = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL);
	private static Font red= new Font(Font.FontFamily.TIMES_ROMAN,12,Font.ITALIC);
	private static Font under= new Font(Font.FontFamily.TIMES_ROMAN,12,Font.UNDERLINE);
	private static int border=new Integer(10);
	
	private Document document;
	private String fileToSavePath;
	private PdfPTable table;
	private String arteriale;
	private String roi;
	private String voxel;
	private String voxelMask;
	private String[] input;
	private short nImages;
	private String gradient;
	private int map;
	private double min;
	private double max;
	private Double value;
	private double[] estimates;
	private int xCoord;
	private int yCoord;
	private int zCoord;
	private int time;
    private String header;
    private TableHeader event;
/**
 * Creates a PDF file containing images generated from the analysis
 * of results
 * @param _path must be a String, contains the path (and filename) where to save PDF file
 * @param _arteriale must be a String, contains the path of the original arterial file
 * @param _roi must be a String, contains the path of the original roi file
 * @param _voxel must be a String, contains the path of the original image file
 * @param _voxel_mask must be a String, contains the path of the original image-mask file
 * @throws ErrorExc
 */
	public ExporterPDF(String _path,String _arteriale, String _roi, String _voxel, String _voxel_mask, 
			String reportName, String[] inputs) throws ErrorExc{
		//Path of file used for elaboration
		fileToSavePath=new String(_path);
		arteriale=new String(_arteriale);
		roi=new String(_roi);
		voxel=new String(_voxel);
		voxelMask=new String(_voxel_mask);
		input=inputs;
		//Create Document
        document = new Document(PageSize.A4, 36, 36, 54, 36);	    
	    //Open output stream using file path selected
		try {
			PdfWriter.getInstance(document, new FileOutputStream(fileToSavePath));
		} catch (FileNotFoundException e) {
			throw new ErrorExc("Impossible to save with this path", e.getMessage());
		} catch (DocumentException e) {
			throw new ErrorExc("Impossible to add new part to Document (PDF)", e.getMessage());
		}
		//Initialize table
		table = new PdfPTable(2);
		//Add Metadata to Document
		addMetaData("Export PET");
		
        PdfWriter writer=null;
		try {
			writer = PdfWriter.getInstance(document, new FileOutputStream(fileToSavePath));
		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        event = new TableHeader();
        writer.setPageEvent(event);
		document.open();
		event.setHeader("Export: Input Files");
		
        
        //add First Page to document
		addTitlePage(document, reportName);
		//add table (content)
		addContent();		
	}
	
    /**
     * Inner class to add a table as header.
     */
    
	/**
	 * Writes to disk PDF file contained in document; this has to be the last
	 * function called after editing the content of PDF file
	 * @throws ErrorExc
	 */
	public void saveFile() throws ErrorExc{
		try {
			if(nImages%2==0)
				document.add(table);
			else{
				table.addCell("");
				document.add(table);
			}
			Paragraph info= new Paragraph();
			info.add(new Paragraph("",small));
			info.add(new Paragraph("Info",red));
			String mapString="";
			if(map==0)
				mapString="Vb";
			if(map==1)
				mapString="Vt";
			if(map==2)
				mapString="Ki";
			info.add(new Paragraph("Map: "+mapString,small));
			info.add(new Paragraph("Gradient: "+gradient,small));
			info.add(new Paragraph("Min: "+min+", Max: "+max,small));
			info.add(new Paragraph("Value: "+value,small));
			info.add(new Paragraph("Estimates: "+"Vb "+estimates[0]+", Vt "+estimates[1]+", Ki "+estimates[2],small));
			info.add(new Paragraph("Coordinates: x="+xCoord+" y="+yCoord+" z="+zCoord+" slice="+time,small));
			document.add(info);
			

			
			
		} catch (DocumentException e) {
			throw new ErrorExc("Impossible to add new part to Document (PDF)", e.getMessage());
		}
		document.close();
	}
	
	/**
	 * Adds meta data to document
	 * @param Title must be a String, contains the title of the document
	 * @throws ErrorExc
	 */

	private void addMetaData(String Title) {
		document.addTitle		(Title);
		document.addSubject		("PET image exported via iText");
		document.addKeywords	("PET, SAKE, Pera");
		document.addAuthor		("Pera Project");
		document.addCreator		("Pera Project");
	}

	/**
	 * add an image to document
	 * @param img must be Image, contains Image file that must be added to document
	 * @throws ErrorExc
	 * 
	 */
	public void addImage(Image img){
		table.addCell(img);
		nImages++;
	}
	
	public void addMap(int _map){
		map= _map;
	}
	
	public void addGradient(String grad){
		gradient= grad;
	}
	
	public void addMin(double m){
		min=m;
	}
	
	public void addMax(double m){
		max=m;
	}
	
	public void addValue(Double val){
		value= val;
	}
	
	public void addEstimates(double[] est){
		estimates= est;
	}
	
	public void addCoord(int x, int y, int z, int t){
		xCoord=x;
		yCoord=y;
		zCoord=z;
		time = t+1;
	}
	
	/**
	 * Adds the first page to document accordingly to the elaboration parameters and files
	 * @param document must be Image, contains Image file that must be added to document according to
	 * params of the constructor
	 * @throws ErrorExc
	 * 
	 */
	private void addTitlePage(Document document, String reportName) throws ErrorExc {
		Paragraph preface = new Paragraph();
		// We add one empty line
		addEmptyLine(preface, 2);
		// Lets write a big header
		if(reportName==null)
			reportName="";
		preface.add(new Paragraph("Report generated from: "+reportName, catFont));

		addEmptyLine(preface, 1);
		// Will create: Report generated by: _name, _date
		preface.add(new Paragraph(
				"Report generated by: " + System.getProperty("user.name") + ", " + 
				new Date(), smallBold)); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		addEmptyLine(preface, 3);
		
		red.setColor(BaseColor.RED);
		preface.add(new Paragraph("File used for the elaboration: ",red));
		if(arteriale!=null){
			Paragraph arte= new Paragraph();
			arte.add(new Paragraph("Arterial",under));
			arte.add(new Paragraph(arteriale,small));
			preface.add(arte);
		}
		if(roi!=null){
			Paragraph regi= new Paragraph();
			regi.add(new Paragraph("ROI",under));
			regi.add(new Paragraph(roi,small));
			preface.add(regi);
		}
		if(voxel!=null){
			Paragraph vox= new Paragraph();
			vox.add(new Paragraph("Voxel Image",under));
			vox.add(new Paragraph(voxel,small));
			preface.add(vox);
		}
		if(voxelMask!=null){
			Paragraph maskimg= new Paragraph();
			maskimg.add(new Paragraph("Voxel Image Mask",under));
			maskimg.add(new Paragraph(voxelMask,small));
			preface.add(maskimg);
		}
		
		addEmptyLine(preface, 1);
		
		preface.add(new Paragraph("Input algorithm and parameters", red));
		if(input!=null){
			Paragraph in= new Paragraph();
			for(int i=0; i<input.length; i++)
				in.add(new Paragraph(input[i],small));
			preface.add(in);
		}
		try {
			document.add(preface);
		} catch (DocumentException e) {
			throw new ErrorExc("Impossible to add new part to Document (PDF)", e.getMessage());
		}
		// Start a new page
		document.newPage();
	}
	
	/**
	 * Adds the body of the document, according to the added images
	 * @throws ErrorExc
	 * 
	 */
	private void addContent() throws ErrorExc{
		event.setHeader("Export: Image and Values");
		Paragraph anchor = new Paragraph();
		addEmptyLine(anchor, 2);
		anchor.add(new Paragraph("PET Export", catFont));
		// Second parameter is the number of the chapter
		Paragraph catPart = new Paragraph(anchor);
		addEmptyLine(catPart, 2);
		
		// Add a table
		createTable(catPart);
	try {
			document.add(catPart);
		} catch (DocumentException e) {
			throw new ErrorExc("Impossible to add new part to Document (PDF)", e.getMessage());
		}

	}

	/**
	 * Adds a table to the document
	 * @param catPart must be a Section, contains the section where to create a table
	 * params of the constructor
	 * @throws ErrorExc
	 * 
	 */
	private void createTable(Paragraph catPart){

		table.setTotalWidth(document.getPageSize().getWidth()-2*border);
//		document.setMargins(30, 30, 30, 60);
		//document.setMargins(10,10,10,10);
/*  	 table.setBorderColor(BaseColor.WHITE);
		 table.setPadding(4);
		 table.setSpacing(4);
		 table.setBorderWidth(1);
*/
		/*PdfPCell c1 = new PdfPCell(new Phrase("Table Header 1"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Table Header 2"));
		c1.setHorizontalAlignment(Element.ALIGN_CENTER);
		table.addCell(c1);
*/
		table.setHeaderRows(0);
	}
	
	/**
	 * Adds empty lines inside a Paragraph
	 * @param paragraph must be a Paragraph, contains the paragraph where to create empty line
	 * @param number must be int, contains the number of empty line to create
	 * @throws ErrorExc
	 */
	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}
	
	/**
	 * Adds empty lines inside a Section
	 * @param paragraph must be a Section, contains the Section where to create empty line
	 * @param number must be int, contains the number of empty line to create
	 * @throws ErrorExc
	 */
	private static void addEmptyLine(Section paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}
	
	
	
	
    class TableHeader extends PdfPageEventHelper {
        /** The header text. */
        String header;
        /** The template with the total number of pages. */
        PdfTemplate total;
 
        /**
         * Allows us to change the content of the header.
         * @param header The new header String
         */
        public void setHeader(String header) {
            this.header = header;
        }
 
        /**
         * Creates the PdfTemplate that will hold the total number of pages.
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onOpenDocument(
         *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onOpenDocument(PdfWriter writer, Document document) {
            total = writer.getDirectContent().createTemplate(30, 16);
        }
 
        /**
         * Adds a header to every page
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onEndPage(
         *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onEndPage(PdfWriter writer, Document document) {
            PdfPTable table = new PdfPTable(3);
            try {
                table.setWidths(new int[]{24, 24, 2});
                table.setTotalWidth(527);
                //table.setLockedWidth(true);
                table.getDefaultCell().setFixedHeight(20);
                table.getDefaultCell().setBorder(Rectangle.BOTTOM);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_LEFT);
				//Image img = Image.getInstance("img/error.png");
				//table.addCell(img);
                table.addCell(header);
                
				table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(String.format("Page %d of", writer.getPageNumber()));
                PdfPCell cell = new PdfPCell(Image.getInstance(total));
                cell.setBorder(Rectangle.BOTTOM);
                table.addCell(cell);
                table.writeSelectedRows(0, -1, 34, 803, writer.getDirectContent());
            }
            catch(DocumentException de) {
                throw new ExceptionConverter(de);
            }
        }
 
        /**
         * Fills out the total number of pages before the document is closed.
         * @see com.itextpdf.text.pdf.PdfPageEventHelper#onCloseDocument(
         *      com.itextpdf.text.pdf.PdfWriter, com.itextpdf.text.Document)
         */
        public void onCloseDocument(PdfWriter writer, Document document) {
            ColumnText.showTextAligned(total, Element.ALIGN_LEFT,
                    new Phrase(String.valueOf(writer.getPageNumber() - 1)),
                    2, 2, 0);
        }
    }

}

