/*
 /////////////////////////
 File name FileManager.java;
 Description the FileManager interface, it's used as an interface for input and output files managed in the software;
 Package it.tetratron.pera.model;
 License GLP 3;
 Author ( s ) Francesco Rosso;
 Owner TetraTron Group;
 Date 14/2/2012;
 /////////////////////////
 */

package it.tetratron.pera.model.manage;

public interface FileManager {

	public void setFile(String file);

	public String getFilename() throws ErrorExc;

}
