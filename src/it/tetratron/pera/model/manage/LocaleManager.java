/*
 /////////////////////////
 File name LocaleManager.java;
 Description the LocaleManager class, 
 	implements the GoF's Singleton pattern 
 	and using a identifier returns a string translated for the user;
 Package it.tetratron.pera.model;
 License GLP 3;
 Author ( s ) Francesco Rosso;
 Owner TetraTron Group;
 Date 14/2/2012;
 /////////////////////////
 */

package it.tetratron.pera.model.manage;

/**
 * 
 * The LocaleManager class, implements the GoF's Singleton pattern and using a identifier returns a string translated for the user
 * @author Francesco Rosso
 * 
 */
import java.util.Locale;
import java.util.ResourceBundle;

public class LocaleManager {
	private static final String BUNDLE_NAME = "it.tetratron.pera.model.manage.strings";
	private static ResourceBundle rb;
	static {
		try {
			rb = ResourceBundle.getBundle(BUNDLE_NAME, Locale.ENGLISH);
		} catch (Exception e) {
			rb = ResourceBundle.getBundle(BUNDLE_NAME, Locale.ENGLISH);
		}
	}

	/**
	 * @param key
	 *            the key associated in the resource file that has to be
	 *            translated
	 * @return a translated string associated to the key in the resource file
	 */
	public static String getString(String key) {
		try {
			String keyValue = new String(rb.getString(key).getBytes(
					"ISO-8859-1"), "UTF-8");
			if (keyValue.isEmpty()) {
				return key;
			}
			return keyValue;
		} catch (Exception e) {
			return key;
		}
	}

	/*
	 * Set to private to avoid other classes to create an instance of
	 * LocaleManager
	 */
	private LocaleManager() {
	}
}
