/*
 /////////////////////////
 File name SakeManager.java;
 Description the SakeManager class, it's used manage files in the .sake file format;
 Package it.tetratron.pera.model;
 License GLP 3;
 Author ( s ) Francesco Rosso;
 Owner TetraTron Group;
 Date 23/2/2012;
 /////////////////////////
 */

package it.tetratron.pera.model.manage;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.FileHeader;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

// per gestire aggiunta/rimozione di oggetti generici all'interno dei file .sake
/*
import com.thoughtworks.xstream.*;
import com.thoughtworks.xstream.io.xml.DomDriver;
*/

/**
 * 
 * Description the SakeManager class, it's used manage files in the .sake file format
 * @author Francesco Rosso
 *
 */
public class SakeManager implements FileManager {

	public enum Type {
		Region, Voxel
	};

	/**
	 * Returns the temporary directory for the system, ending with a separator
	 */
	private static String getTempDir() {
		String tmpDir = System.getProperty("java.io.tmpdir");
		if (!(tmpDir.endsWith("/") || tmpDir.endsWith("\\")))
			tmpDir = tmpDir + System.getProperty("file.separator");
		return tmpDir;
	}

	/**
	 * Static method to get the type of a .sake file. Throws an exception if the
	 * type is not defined
	 * 
	 * @param filename
	 * @throws ErrorExc
	 */
	public static Type getType(String filename) throws ErrorExc {
		return new SakeManager(filename).getType();
	}

	private ZipFile _fileZip = null;

	public SakeManager() {

	}

	public SakeManager(String file) {
		setFile(file);
	}

	/**
	 * Add a File to the .sake file. If the .sake file does not exists, this
	 * method creates a new .sake file. Throws an exception if there is an
	 * IOException.
	 * 
	 * @param filename
	 * @throws ErrorExc
	 */
	public void addFile(File filename) throws ErrorExc {
		try {
			// Initiate Zip Parameters
			if (!filename.exists())
				throw new ErrorExc("EModImpossibleToStoreFile", "File "+filename.getName()+" does not exists");
			ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE); // set
																			// compression
																			// method
																			// to
																			// store
																			// compression
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
			_fileZip.addFile(filename, parameters);
		} catch (ZipException e) {
			throw new ErrorExc("EModImpossibleToStoreFile", e.getMessage());
		}
	}

	/**
	 * Add a File to the .sake file. If overwrite is set to false, this method
	 * equals to addFile(filename). If the .sake file does not exists, this
	 * method creates a new .sake file. Throws an exception if there is an
	 * IOException.
	 * 
	 * @param filename
	 * @param overwrite
	 * @throws ErrorExc
	 */
	public void addFile(File filename, boolean overwrite)
			throws ErrorExc {
		if (overwrite)
			try {
				this.removeFile(filename.getName());
			} catch (ErrorExc e) {
			}
		this.addFile(filename);
	}

	/**
	 * Extracts a file in the java.io.tmpdir from the .sake file and returns his
	 * pointer. The file is created in the java.io.tmpdir. The caller will be
	 * responsible to delete that file. Throws an exception if the given
	 * filename could not be found in the .sake file.
	 * 
	 * @param filename
	 * @return the pointer to the extracted file
	 * @throws ErrorExc
	 */
	public File getFile(String filename) throws ErrorExc {
		try {
			_fileZip.extractFile(filename, SakeManager.getTempDir());
			File extracted = new File(SakeManager.getTempDir() + filename);
			// extracted.deleteOnExit();
			return extracted;
		} catch (ZipException e) {
			throw new ErrorExc("EModFileNotPresent", "Cannot find file "+filename+" in the given sake file");
		}
	}
	
	public File getFileCSV(String filename, String where) throws ErrorExc {
		try {
			_fileZip.extractFile(filename, where);
			File extracted = new File(where + filename);
			return extracted;
		} catch (ZipException e) {
			throw new ErrorExc("EModFileNotPresent", "Cannot find file "+filename+" in the given sake file");
		}
	}
	
	/**
	 * Returns the file path. Thrown an exception if the file does not exists.
	 * 
	 * @return String the file path
	 * @throws ErrorExc
	 */
	@Override
	public String getFilename() throws ErrorExc {
		try {
			return _fileZip.getFileHeaders().toString();
		} catch (ZipException e) {
			throw new ErrorExc("EModFileNotPresent", e.getMessage());
		}
	}

	/**
	 * Add an object to the .sake file. If boolean is set to false, this method
	 * equals to addObj(Object, String). If the .sake file does not exists, this
	 * method creates a new .sake file. Throws an exception if the objectname
	 * is null or empty or there is a tempfile with the same name of objectname
	 * or if there is an IOException
	 * 
	 * @param obj
	 *            the object that has to be saved
	 * @param objectname
	 *            the name of the object, so you can recovery it later
	 * @param overwrite
	 *            true if you want to overwrite previous files
	 * @throws ErrorExc
	 */
	/*
	public synchronized void addObj(Object obj, String objectname,
			boolean overwrite) throws ErrorExc {
		if (overwrite)
			try {
				this.removeObj(objectname);
			} catch (ErrorExc e) {
			}
		this.addObj(obj, objectname);
	}
	*/

	/**
	 * Add an object to the .sake file. If the .sake file does not exists, this
	 * method creates a new .sake file. Throws an exception if the objectname
	 * is null or empty or there is a tempfile with the same name of objectname
	 * or if there is an IOException
	 * 
	 * @param obj
	 *            the object that has to be saved
	 * @param objectname
	 *            the name of the object, so you can recovery it later
	 * @throws ErrorExc
	 */
	/*
	public synchronized void addObj(Object obj, String objectname)
			throws ErrorExc {

		if (objectname == null || objectname.isEmpty())
			throw new ErrorExc("EModInvalidArgument");

		String tempDir = SakeManager.getTempDir();
		objectname = objectname + ".xml";
		File temp = new File(tempDir + objectname);

		if (temp.exists())
			throw new ErrorExc("EModCannotSaveFile");

		Writer output;

		try {
			temp.createNewFile();
			XStream xstream = new XStream(new DomDriver());
			// FileWriter always assumes default encoding is OK!
			output = new BufferedWriter(new FileWriter(temp));
			xstream.toXML(obj, output);
			output.close();
			this.addFile(temp);
			temp.delete();
		} catch (IOException e) {
			// generated by createNewFile or BufferedWriter. Probabilmente tale
			// file esiste gi�.
			throw new ErrorExc("EModCannotSaveFile", e.getMessage());
		}
	}
	*/

	/**
	 * This method reads the content of the .sake file to check if it is a Voxel
	 * or Region type. Throws an exception if the .sake file does not exists or
	 * if the type is not already set.
	 * 
	 * @return the type of the current sake file
	 * @throws ErrorExc
	 */
	public SakeManager.Type getType() throws ErrorExc {
		if (_fileZip.getFile().exists())
			if (isFilePresent("META-Region"))
				return SakeManager.Type.Region;
			else if (isFilePresent("META-Voxel"))
				return SakeManager.Type.Voxel;
		throw new ErrorExc("EModTypeNotDefined");
	}

	/**
	 * Throws an exception if the .sake file does not exists.
	 * 
	 * @param filename
	 * @return true if and only if the given filename exists in the .sake file.
	 *         False otherwhise.
	 * @throws ErrorExc
	 *             if the sake file is empty or not correctly initialized
	 */

	@SuppressWarnings("unchecked")
	public boolean isFilePresent(String filename) throws ErrorExc {
		if (_fileZip.getFile().exists()) {
			List fileHeaderList;
			try {
				fileHeaderList = _fileZip.getFileHeaders();
			} catch (ZipException e) {
				throw new ErrorExc("EModFileSakeNotPresent", e.getMessage());
			}
			for (int i = 0; i < fileHeaderList.size(); i++) {
				FileHeader fileHeader = (FileHeader) fileHeaderList.get(i);
				if (fileHeader.getFileName().equals(filename))
					return true;
			}
		}
		return false;
	}

	/**
	 * Removes the file provided in the input parameters in the .sake file. If
	 * the file does not exists, this method throws an exception.
	 * 
	 * @param filename
	 * @throws ErrorExc
	 */
	public void removeFile(String filename) throws ErrorExc {
		if (_fileZip.getFile().exists())
			try {
				_fileZip.removeFile(filename);
			} catch (ZipException e) {
				throw new ErrorExc("EModImpossibleToRemoveFile", e.getMessage());
			}
	}

	/**
	 * Returns the object from the .sake file. The dynamic type of the return
	 * param is the dynamic type of the original file. Myracles of Java! Throws
	 * an exception if the object does not exists in the .sake file.
	 * 
	 * @param objectname
	 * @return the requested Object
	 * @throws ErrorExc
	 */
	/*
	public Object getObj(String objectname) throws ErrorExc {
		// throws exception if file does not exists
		File t = getFile(objectname + ".xml");
		XStream xstream = new XStream(new DomDriver());
		Object obj = xstream.fromXML(t);
		t.delete();
		return obj;
	}
	*/

	/**
	 * Creates a new SakeManager object with the given file path. If it not
	 * exists, it is not created at this point.
	 * 
	 * @param file
	 */
	@Override
	public void setFile(String file) {
		if (!file.endsWith(".sake"))
			file = file + ".sake";
		try {
			this._fileZip = new ZipFile(file);
		} catch (ZipException e) {
			// Note: If this zip file is a split file then this method throws an
			// exception as
			// Zip Format Specification does not allow updating split zip files
			// Because we do not want to handle .sake splitted files, we will o
			// nothing with this exception
			e.printStackTrace();
		}
	}

	/**
	 * Removes the Object provided in the input parameters in the .sake file. If
	 * the Object does not exists, this method throws an exception.
	 * 
	 * @param objectname
	 * @throws ErrorExc
	 */
	/*
	public synchronized void removeObj(String objectname) throws ErrorExc {
		removeFile(objectname + ".xml");
	}
	 */
	
	/**
	 * Set the .sake file to be a Region or a Voxel type. If the .sake file does
	 * not exists, it is created at this point. Throws an Exception if we can't
	 * set the type. Overwrites previous types.
	 * 
	 * @param type
	 * @throws ErrorExc
	 */
	public void setType(SakeManager.Type type) throws ErrorExc {
		String tempDir = SakeManager.getTempDir();
		// temp = File.createTempFile("META-Region", "");
		String filename = null, other = null;
		if (type == Type.Region) {
			filename = new String("META-Region");
			other = new String("META-Voxel");
		} else if (type == Type.Voxel) {
			// type==Voxel
			filename = new String("META-Voxel");
			other = new String("META-Region");
		}
		if (_fileZip.getFile().exists())
			try {
				if (isFilePresent(other))
					this.removeFile(other);
				if (isFilePresent(filename))
					return;
			} catch (ErrorExc e) {
			}
		File temp = new File(tempDir + filename);
		try {
			temp.createNewFile();
			// lololol
			Writer output = new BufferedWriter(new FileWriter(temp));
			try {
				// FileWriter always assumes default encoding is OK!
				output.write("Help! My name is Flynn and Davide Cabbia, Enrico Palleva, Francesco Rosso, Jacopo Zambon, Luca Guizzon, Alessandro Ferlin and Lorenzo Scorzato trapped me in this software!");
			} finally {
				output.close();
			}
			// lololol
			this.addFile(temp);
			temp.delete();
		} catch (IOException e) {
			// generated by createnewfile. Probabilmente tale file esiste gia'.
			throw new ErrorExc("EModImpossibleToCreateFile", e.getMessage());
		}
	}

}
