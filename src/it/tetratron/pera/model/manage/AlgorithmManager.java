/*
 /////////////////////////
 File name AlgorithmManager.java;
 This class provide all method to make an elaboration.
 Package it.tetratron.pera.model.manage.AlgorithmManager;
 License GLP 3;
 Author ( s ) Ferlin Alessandro;
 Owner TetraTron Group;
 Date 2012.03.18;
 /////////////////////////
 */
package it.tetratron.pera.model.manage;

import it.tetratron.pera.model.objects.ObjInterface;
import it.tetratron.pera.model.params.*;
import it.tetratron.pera.model.algorithms.*;

/**
 * This class provide all method to make an elaboration.
 * @author Ferlin Alessandro.
 */
public class AlgorithmManager extends ObjectManager /*implements Runnable*/{
	
	/*
	 * Parameters needed for the execution of the algorithm
	 */
	
	private ParamInterface param;
	
	/*
	 * Algorithm that will be executed
	 */
	
	private AlgorithmInterface algorithm;
	
	private ObjInterface result;
	
	public AlgorithmManager(){
		super();
	}
	
	/**
	 * Sets @param algo as the current algorithm, with runAlgorithm() will be run @param algo.
	 * @param algo the algorithm to be set as current algorithm
	 */
	public void setAlgorithm(AlgorithmInterface algo){
		algorithm = algo;
	}
	
	/**
	 * Gets the parameters for the elaborations
	 * @return parameters used by the elaboration
	 */
	public ParamInterface getParams(){
		return param;
	}
	
	/**
	 * Sets @param par as the current parameters
	 * @param par the parameters to be set as current parameters
	 */
	public void setParams(ParamInterface par){
		param = par;
	}
	
	/**
	 * Gets the output of the algorithm
	 * @return an ObjInterface, as output of the algorithm elaboration
	 */
	public ObjInterface getResult(){
		return result;
	}
	
	/**
	 * Run the algorithm previously select, using object and parameters previously select.
	 * If algorithm run correctly will be call notifyObservers().
	 * @throws ErrorExc if no Algorithm is selected, or if algorithm throw an exception.
	 */
	public void runAlgorithm() throws ErrorExc{
		if(algorithm==null)
			throw new ErrorExc("EModAlgorithmNotSelected");
		if(param!=null && param.check()==false)
			throw new ErrorExc("EModParamNotCorrect");
		
		try {
			result= algorithm.run(param, obj);
		} catch (ErrorExc e) {
			throw e;
		}
		setChanged();
		notifyObservers();
	}
}

