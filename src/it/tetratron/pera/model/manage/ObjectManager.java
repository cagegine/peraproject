/*
 /////////////////////////
 File name ObjectManager.java;
 This class provides method to save, load and set memento of an object.
 Package it.tetratron.pera.model.manage;
 License GLP 3;
 Author ( s ) Ferlin Alessandro;
 Owner TetraTron Group;
 Date 2012.03.18;
 /////////////////////////
 */
package it.tetratron.pera.model.manage;

import it.tetratron.pera.model.objects.*;
import java.util.Observable;
import java.util.Vector;

/**
 * This class provides method to save, load and set memento of an object.
 * @author Ferlin Alessandro.
 *
 */

public class ObjectManager extends Observable{
	/**
	 * Reference to the objects used by the elaborations
	 */
	protected Vector<ObjInterface> obj= new Vector<ObjInterface>(); 
	
	/**
	* Memento objects for restoring the system after an undo operation
	*/
	private Memento artMem;
	private Memento regMem;
	private Memento voxMem;

	public ObjectManager(){
		super();
	}
	
	/**
	 * A public method that returns the actual object
	 */
	public Vector<ObjInterface> getItems() {return obj;}
	
	/**
	 * A public method that set the Arterial object inside the objects vector
	 * @param new_art the new Arterial object to be setted
	 */
	public void setArterial(ArterialObj new_art){
		obj.removeElement(getArterial());
		obj.add(new_art);
	}
	
	/**
	 * A public method that set the Region object inside the objects vector
	 * @param new_reg the new Region object to be setted
	 */
	public void setRegion(RegionObj new_reg){
		obj.removeElement(getRegion());
		obj.add(new_reg);
	}
	
	/**
	 * A public method that set the Voxel object inside the objects vector
	 * @param new_vox the new Voxel object to be setted
	 */	
	public void setVoxel(VoxelObj new_vox){
		obj.removeElement(getVoxel());
		obj.add(new_vox);
	}
	
	/**
	 * A public method that set the Sake Region object inside the objects vector
	 * @param new_sro the new Sake Region object to be setted
	 */
	public void setSakeRegion(SakeRegionObj new_sro){
		obj.removeElement(getSakeRegion());
		obj.add(new_sro);
	}
	
	/**
	 * A public method that set the Sake Voxel object inside the objects vector
	 * @param new_svo the new Sake Voxel object to be setted
	 */
	public void setSakeVoxel(SakeVoxelObj new_svo){
		obj.removeElement(getSakeVoxel());
		obj.add(new_svo);
	}
	
	/**
	 * Get the arterial object stored in the objects vector
	 * @return The arterial object used by the elaboration
	 */
	public ArterialObj getArterial() {
		ArterialObj ref_art=null;
		for(int i = 0; i<obj.size() && ref_art==null; i++){
			if(obj.get(i) instanceof ArterialObj)
				ref_art=(ArterialObj)obj.get(i);
		}
		return ref_art;
	}
	
	/**
	 * Get the region object stored in the objects vector
	 * @return The region object used by the elaboration
	 */
	public RegionObj getRegion() {
		RegionObj ref_reg=null;
		for(int i = 0; i<obj.size() && ref_reg==null; i++){
			if(obj.get(i) instanceof RegionObj)
				ref_reg=(RegionObj)obj.get(i);
		}
		return ref_reg;
	}
	
	/**
	 * Get the voxel object stored in the objects vector
	 * @return The voxel object used by the elaboration
	 */
	public VoxelObj getVoxel() {
		VoxelObj ref_vox=null;
		for(int i = 0; i<obj.size() && ref_vox==null; i++){
			if(obj.get(i) instanceof VoxelObj)
				ref_vox=(VoxelObj)obj.get(i);
		}
		return ref_vox;
	}
	
	/**
	 * Get the Sake Region object stored in the objects vector
	 * @return The Sake Region object used by the elaboration
	 */
	public SakeRegionObj getSakeRegion(){
		SakeRegionObj ref_sakereg=null;
		for(int i = 0; i<obj.size() && ref_sakereg==null; i++){
			if(obj.get(i) instanceof SakeRegionObj)
				ref_sakereg=(SakeRegionObj)obj.get(i);
		}
		return ref_sakereg;
	}
	
	/**
	 * Get the Sake Voxel object stored in the objects vector
	 * @return The Sake Voxel object used by the elaboration
	 */
	public SakeVoxelObj getSakeVoxel(){
		SakeVoxelObj ref_sakevox=null;
		for(int i = 0; i<obj.size() && ref_sakevox==null; i++){
			if(obj.get(i) instanceof SakeVoxelObj)
				ref_sakevox=(SakeVoxelObj)obj.get(i);
		}
		return ref_sakevox;
	}
	
	/**
	 * Load and store an arterial object in the objects vector
	 * @param path The path where the arterial file is stored in the file system
	 */
	public void loadArterial(String path) throws ErrorExc{
		ArterialObj ref=getArterial();
		if(ref==null)
			ref=new ArterialObj();
		else{
			obj.removeElement(ref);
		}
		ref.load(path);
		obj.add(ref);
	}
	
	/**
	 * Load and store a region object in the objects vector
	 * @param path The path where the region file is stored in the file system
	 */
	public void loadRegion(String path) throws ErrorExc{
		RegionObj ref=getRegion();
		if(ref==null)
			ref=new RegionObj();
		else{
			obj.removeElement(ref);
		}
		ref.load(path);
		obj.add(ref);
	}
	
	/**
	 * Load and store a voxel object in the objects vector
	 * @param dynpet_path the path where the image file is stored in the file system
	 * @param mask_path the path where the mask file related to the image is stored in the file system
	 */
	public void loadVoxel(String dynpet_path, String mask_path) throws ErrorExc{
		VoxelObj ref=getVoxel();
		if(ref==null)
			ref=new VoxelObjAdapter();
		else{
			obj.removeElement(ref);
		}
		ref.load(dynpet_path,mask_path);
		obj.add(ref);
	}
	

	/**
	 * Load and store a Sake object at region level in the objects vector
	 * @param path The path where the SakeRegion file is stored in the file system
	 */
	public void loadSakeRegion(String path) throws ErrorExc{
		SakeRegionObj ref= getSakeRegion();
		if(ref==null)
			ref=new SakeRegionObj();
		else{
			obj.removeElement(ref);
		}
		ref.load(path);
		obj.add(ref);
	}
	
	/**
	 * Load and store a Sake object at voxel level in the objects vector
	 * @param path The path where the SakeVoxel file is stored in the file system
	 */
	public void loadSakeVoxel(String path) throws ErrorExc{
		SakeVoxelObj ref= getSakeVoxel();
		if(ref==null)
			ref=new SakeVoxelObj();
		else{
			obj.removeElement(ref);
		}
		ref.load(path);
		obj.add(ref);
	}
	
	
	/**
	 * Save the arterial object in the file system
	 * @param path The path where the arterial file has to be stored in the file system
	 */
	public void saveArterial(String path) throws ErrorExc{
		ArterialObj ref=getArterial();
		if(ref==null)
			throw new ErrorExc("EModArtObjNotFound");
		ref.save(path);
	}
	
	/**
	 * Save the region object in the file system
	 * @param path The path where the region file has to be stored in the file system
	 */
	public void saveRegion(String path) throws ErrorExc{
		RegionObj ref=getRegion();
		if(ref==null)
			throw new ErrorExc("EModRegObjNotFound");
		ref.save(path);
	}
	
	/**
	 * Save the voxel object in the file system
	 * @param path the path where the voxel file has to be stored in the file system
	 */
	public void saveVoxel(String path) throws ErrorExc{
		VoxelObj ref=getVoxel();
		if(ref==null)
			throw new ErrorExc("EModVoxObjNotFound");
		ref.save(path);
	}
	
	/**
	 * Save the Sake Region object in the file system
	 * @param path the path where the Sake Region file has to be stored in the file system
	 */
	public void saveSakeRegion(String path) throws ErrorExc{
		SakeRegionObj ref=getSakeRegion();
		if(ref==null)
			throw new ErrorExc("EModSakeRegionObjNotFound");
		ref.save(path);
	}
	
	/**
	 * Save the Sake Voxel object in the file system
	 * @param path the path where the Sake Voxel file has to be stored in the file system
	 */
	public void saveSakeVoxel(String path) throws ErrorExc{
		SakeVoxelObj ref=getSakeVoxel();
		if(ref==null)
			throw new ErrorExc("EModSakeVoxelObjNotFound");
		ref.save(path);
	}
	
	/**
	 * This method clears the objects vector
	 */
	public void clear(){
		obj.removeAllElements();
		artMem=null;
		regMem=null;
		voxMem=null;
	}

	/**
	* Create the memento for a given input object (inside the objects vector), useful for restoring
	* the system state after an undo operation performed by the user
	*/
	public void createMemento(ObjInterface arv) throws ErrorExc{
		if(arv instanceof ArterialObj)
			artMem= ((ArterialObj) arv).createMemento();
		if(arv instanceof RegionObj)
			regMem= ((RegionObj) arv).createMemento();
		if(arv instanceof VoxelObj)
			voxMem= ((VoxelObj) arv).createMemento();
	}
	
	/**
	 * Restore the state of a given input Object inside the objects vector from the Memento
	 */
	public void restoreFromMemento(ObjInterface arv){
		if(arv instanceof ArterialObj)
			setArterial((ArterialObj)artMem.restoreState());
		if(arv instanceof RegionObj)
			setRegion((RegionObj)regMem.restoreState());
		if(arv instanceof VoxelObj)
			setVoxel((VoxelObj)voxMem.restoreState());
	}
	
}
