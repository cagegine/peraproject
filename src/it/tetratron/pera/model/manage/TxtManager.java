/*
 /////////////////////////
 File name TxtManager.java;
 Description the TxtManager class, it's used to parse an arterial or region tabs (\t) delimited .txt file;
 Package it.tetratron.pera.model;
 License GLP 3;
 Author ( s ) Francesco Rosso;
 Owner TetraTron Group;
 Date 14/2/2012;
 /////////////////////////
 */

package it.tetratron.pera.model.manage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

/**
 * Description the TxtManager class, it's used to parse an arterial or region tabs (\t) delimited .txt file;
 * @author Francesco Rosso
 *
 */
public class TxtManager implements FileManager {

	// we save the file path&filename
	private String filename = null;

	private String[] labels = null;

	/**
	 * Set content and labels to default. The filename will remain the same.
	 */
	public TxtManager() {
		this.setDefaults();
	}

	/**
	 * The constructor. It will set the filename with the given param and the
	 * labels to default.
	 * 
	 * @param filename
	 */
	public TxtManager(String filename) {
		this.setFile(filename);
	}

	/**
	 * This method checks if :
	 * <ul>
	 * <li>the filename is set</li>
	 * <li>the content is set</li>
	 * <li>if labels are set, it checks if they are consistent with the given
	 * content</li>
	 * </ul>
	 * If one of these conditions are not met, it throws an ErrorExc
	 * 
	 * @param content
	 * @throws ErrorExc
	 */
	private void checkConsistencyBeforeSaving(double[][] content)
			throws ErrorExc {
		if (filename == null)
			throw new ErrorExc("EModFilenameNotSet");

		if (content == null)
			throw new ErrorExc("EModContentNotSet");

		// controllo che content sia ben formattato
		if (labels != null && labels.length != content[0].length)
			throw new ErrorExc("EModContentNotConsistentWithLabels");
	}

	/**
	 * This method checks if :
	 * <ul>
	 * <li>the filename is set</li>
	 * <li>the content is set</li>
	 * <li>if labels are set, it checks if they are consistent with the given
	 * content</li>
	 * </ul>
	 * If one of these conditions are not met, it throws an ErrorExc
	 * 
	 * @param content
	 * @throws ErrorExc
	 */
	private void checkConsistencyBeforeSaving(Object[][] content)
			throws ErrorExc {
		if (filename == null)
			throw new ErrorExc("EModFilenameNotSet");

		if (content == null)
			throw new ErrorExc("EModContentNotSet");

		// controllo che content sia ben formattato
		if (labels != null && labels.length != content[0].length)
			throw new ErrorExc("EModContentNotConsistentWithLabels");

	}

	/**
	 * This method will read and return the file content, labels included in the
	 * first row (if set).
	 * 
	 * @throws ErrorExc
	 */
	public String[][] getFileContent() throws ErrorExc {
		String[][] cont = this.readFile();
		String[] labs = null;
		try {
			labs = getLabels();
		} catch (ErrorExc e) {
			// labels are not set, we can return the content without labels
			return cont;
		}

		// if we get here, labels are set and we need to return content + labels
		// at the begin
		String[][] allContent = new String[cont.length + 1][cont[0].length];
		allContent[0] = labs;
		for (int i = 0; i < cont.length; i++) {
			allContent[i + 1] = cont[i];
		}

		return allContent;

	}

	/**
	 * Throws exception if the file is not set
	 * 
	 * @return the path to the file that is associated with this class. Could be
	 *         also null.
	 * @throws ErrorExc
	 */
	@Override
	public String getFilename() throws ErrorExc {
		if (filename == null)
			throw new ErrorExc("EModFilenameNotSet");
		return filename;
	}

	/**
	 * 
	 * @return the file's labels associated with this class.
	 * @throws ErrorExc
	 *             if labels are not set
	 */
	public String[] getLabels() throws ErrorExc {
		if (labels == null)
			throw new ErrorExc("EModLabelsNotSet");
		return labels;
	}

	private boolean isValidLine(String line) {
		return (!line.isEmpty());
	}

	/**
	 * This method will read and return the file content.
	 * 
	 * @return a multi-dimensional String array that represents the file data
	 *         content.
	 * @throws ErrorExc
	 *             if the filename is not set, the file is not found, the file
	 *             cannot be read, the file is not consistent with the number of
	 *             labels, the file cannot be closed
	 */
	public String[][] readFile() throws ErrorExc {

		BufferedReader readbuffer;

		try {
			readbuffer = new BufferedReader(new FileReader(getFilename()));
		} catch (FileNotFoundException e) {
			throw new ErrorExc("EModFileNotFound", e.getMessage());
		} catch (ErrorExc e) {
			throw e;
		}

		String strRead;
		String splitarray[];
		Vector<String[]> contenuto = new Vector<String[]>();
		int nOfColumns = 0, nOfLabels = 0, currentRow = 0;

		// first of all, we need to check if the first line is a label line or
		// not

		boolean validLine = false;
		do {
			try {
				strRead = readbuffer.readLine();
				currentRow += 1;
			} catch (IOException e) {
				throw new ErrorExc("EModImpossibleToReadFile", e.getMessage());
			}
			if (strRead!= null && isValidLine(strRead)) {
				splitarray = strRead.split("\t");
				nOfColumns = splitarray.length;
				if (nOfColumns > 0) {
					try {
						Double.parseDouble(splitarray[0]);
						// if we arrive at this point, the file does not have
						// labels inside and the current line is part of the
						// content
						contenuto.add(splitarray);
					} catch (NumberFormatException e) {
						labels = splitarray;
					}
					nOfLabels = nOfColumns;
					validLine = true;
				}
			}
		} while (strRead!= null && validLine != true);

		// controlliamo il resto del file
		try {
			while ((strRead = readbuffer.readLine()) != null) {
				currentRow += 1;
				if (isValidLine(strRead)) {
					splitarray = strRead.split("\t");
					nOfColumns = splitarray.length;
					if (nOfColumns > 0) {
						if (nOfColumns != nOfLabels)
							throw new ErrorExc("EModFileNotConsistent", "Row "+currentRow);
						contenuto.add(splitarray);
					}
				}
			}
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToReadFile", e.getMessage());
		} catch (Exception e) {
			throw new ErrorExc("EModImpossibleToReadFile", e.getMessage());
		}

		// close the stream to release memory resources
		try {
			readbuffer.close();
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToCloseFile", e.getMessage());
		}

		// convert the content into an array
		return vectorToArray(contenuto);
	}
	
	/**
	 * Converts a Vector of String arrays into a String array or arrays. Used by readFile().
	 * @param vector
	 * @return the vector in an array format
	 */
	private String[][] vectorToArray(Vector<String[]> vector){
		String[][] array = null;
		if (!vector.isEmpty()) {
			array = new String[vector.size()][vector.get(0).length];

			for (int i = 0; i < vector.size(); i++) {
				array[i] = vector.get(i);
			}
		}
		return array;
	}

	/**
	 * This method will read and return the file content.
	 * 
	 * @return a multi-dimensional double array that represents the file data
	 *         content.
	 * @throws ErrorExc
	 *             if the filename is not set, the file is not found, the file
	 *             cannot be read, the file is not consistent with the number of
	 *             labels, the file cannot be closed, the file content is not
	 *             filled with double values
	 */
	public double[][] readFileDouble() throws ErrorExc {

		String[][] contenuto = readFile();

		// convertiamo la matrice di stringhe in un array
		double[][] content = null;
		if (contenuto != null) {
			content = new double[contenuto.length][contenuto[0].length];

			for (int i = 0; i < contenuto.length; i++) {
				for (int j = 0; j < contenuto[i].length; j++)
					try {
						content[i][j] = Double.parseDouble(contenuto[i][j]);
					} catch (NumberFormatException e) {
						throw new ErrorExc(
								"EModParserExcMaybeNotRightConverter",
								e.getMessage());
					}
			}
		}
		return content;
	}

	/**
	 * This method saves the file content in the path associated in this class
	 * with the setFilename method. Throws an exception if:
	 * <ul>
	 * <li>the filename is not set</li>
	 * <li>the content is not set</li>
	 * <li>the content is not consistent with the setted labels</li>
	 * <li>it's impossible to write the file</li>
	 * <li>it's impossible to close the writebuffer</li>
	 * <ul>
	 * 
	 * @throws ErrorExc
	 */
	public void saveFile(double[][] content) throws ErrorExc {

		checkConsistencyBeforeSaving(content);

		BufferedWriter writebuffer;

		try {
			writebuffer = new BufferedWriter(new FileWriter(filename));
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToWriteFile", e.getMessage());
		}

		try {
			// Saving labels
			this.writeLabels(writebuffer);
			// Saving file content
			for (int i = 0; i < content.length; i++) {
				this.writeLine(toLine(content[i]), writebuffer);
			}
		} catch (ErrorExc e) {
			throw e;
		} finally {
			try {
				writebuffer.close();
			} catch (IOException e) {
				throw new ErrorExc("EModImpossibleToCloseFile", e.getMessage());
			}
		}
	}

	/**
	 * This method saves the file content in the path associated in this class
	 * with the setFilename method. Throws an exception if:
	 * <ul>
	 * <li>the filename is not set</li>
	 * <li>the content is not set</li>
	 * <li>the content is not consistent with the setted labels</li>
	 * <li>it's impossible to write the file</li>
	 * <li>it's impossible to close the writebuffer</li>
	 * <ul>
	 * 
	 * @throws ErrorExc
	 */
	public void saveFile(String[][] content) throws ErrorExc {

		this.checkConsistencyBeforeSaving(content);

		BufferedWriter writebuffer;

		try {
			writebuffer = new BufferedWriter(new FileWriter(filename));
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToWriteFile", e.getMessage());
		}

		try {
			// Saving labels
			this.writeLabels(writebuffer);
			// Saving file content
			for (int i = 0; i < content.length; i++) {
				this.writeLine(toLine(content[i]), writebuffer);
			}
		} catch (ErrorExc e) {
			throw e;
		} finally {
			try {
				writebuffer.close();
			} catch (IOException e) {
				throw new ErrorExc("EModImpossibleToCloseFile", e.getMessage());
			}
		}
	}

	/**
	 * set labels to null
	 */
	private void setDefaults() {
		labels = null;
	}

	@Override
	/**
	 * @param filename the path to the file that has to be associated with this class
	 */
	public void setFile(String filename) {
		setFile(filename, true);
	}

	/**
	 * This method will set the file associated with this class with another
	 * file. If the filename does not ends with a .txt estension, it will be
	 * setted automatically
	 * 
	 * @param filename
	 *            the path to the file that has to be associated with this class
	 * @param setDefaults
	 *            if true, will also reset file content and file labels. If
	 *            false, content and labels will remain the same. This can be
	 *            helpful if you want to cosy the same file to another path
	 */
	public void setFile(String filename, boolean setDefaults) {
		if (!filename.endsWith(".txt"))
			filename = filename + ".txt";
		if (setDefaults == true)
			this.setDefaults();
		this.filename = filename;
	}

	/**
	 * This method is handy if you want to rename the file labels.
	 * 
	 * @param newLabels
	 *            a multi dimensional String array representing the labels
	 * @throws Exception
	 *             if the number of labels you want to set is not the same to
	 *             the number of labels associated with this file.
	 */
	public void setLabels(String newLabels[]) throws ErrorExc {
		if (this.labels != null
				&& (newLabels == null || this.labels.length != newLabels.length))
			throw new ErrorExc("EModLabelsNotConsistent");
		this.labels = newLabels;
	}

	/**
	 * This method is helpful when a file saving occurs. It creates a tab
	 * delimited line that can be inserted in the new file.
	 * 
	 * @param line
	 *            a double array containing the line that has to be saved
	 * @return a tab (\t) delimited line ending with a newline (\n)
	 */
	private String toLine(double line[]) {
		String newLine = "";
		newLine = Double.toString(line[0]);
		for (int i = 1; i < line.length; i++) {
			newLine = newLine + "\t" + Double.toString(line[i]);
		}
		newLine = newLine + "\n";
		return newLine;
	}

	/**
	 * This method is helpful when a file saving occurs. It creates a tab
	 * delimited line that can be inserted in the new file.
	 * 
	 * @param line
	 *            a String array containing the line that has to be saved
	 * @return a tab (\t) delimited line ending with a newline (\n)
	 */
	private String toLine(String line[]) {
		String newLine = "";
		newLine = line[0];
		for (int i = 1; i < line.length; i++) {
			newLine = newLine + "\t" + line[i];
		}
		newLine = newLine + "\n";
		return newLine;
	}

	/**
	 * This method writes the labels set in setLabels in the writebuffer object.
	 * If labels are not set, it won't write anything. Throws an exception if we
	 * cannot write into the writebuffer.
	 * 
	 * @param writebuffer
	 * @throws ErrorExc
	 */
	private void writeLabels(BufferedWriter writebuffer) throws ErrorExc {
		if (labels != null)
			this.writeLine(toLine(labels), writebuffer);
	}

	/**
	 * This method writes a line in the writebuffer object. Throws an exception
	 * if we cannot write into the writebuffer.
	 * 
	 * @param line
	 *            the line to write
	 * @param writebuffer
	 * @throws ErrorExc
	 */
	private void writeLine(String line, BufferedWriter writebuffer)
			throws ErrorExc {
		try {
			writebuffer.write(line);
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToWriteFile", e.getMessage());
		}
	}

}
