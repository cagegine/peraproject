/*
 /////////////////////////
 File name ErrorExc.java;
 Description the ErrorExc class, used for managing exceptions in this software;
 Package it.tetratron.pera.model;
 License GLP 3;
 Author ( s ) Francesco Rosso;
 Owner TetraTron Group;
 Date 19/2/2012;
 /////////////////////////
 */
package it.tetratron.pera.model.manage;


/**
 * Class used for managing exceptions in this software
 * @author Francesco Rosso
 *
 */
public class ErrorExc extends Exception {

	private static final long serialVersionUID = 1L;
	private String key;
	private String message = null;
	private String details = "";

	// we don't want other object to initialize an empty class
	private ErrorExc() {
		super();
	}

	/**
	 * Constructor. Each class handle a message identified by a key.
	 * 
	 * @param key
	 */
	public ErrorExc(String key) {
		this();
		this.key = key;
	}

	/**
	 * Constructor. Each class handle a message identified by a key.
	 * 
	 * @param key
	 * @param details
	 */
	public ErrorExc(String key, String details) {
		this();
		this.key = key;
		this.setDetails(details);
	}

	/**
	 * 
	 * @return the key associated with this exception
	 */
	public String getKey() {
		return key;
	}

	/**
	 * 
	 * @return the message associated with this Exception that could be shown in
	 *         the View
	 */
	@Override
	public String getMessage() {
		// lazy initialization
		if (message == null || message.isEmpty())
			message = LocaleManager.getString(key);
		return message;
	}

	/**
	 * @return the details of the exception
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @param details
	 *            the details of the exception to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}
}
