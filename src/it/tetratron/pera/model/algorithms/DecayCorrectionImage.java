/**
//////////////////////////////////
*DecayCorrectionImage.java
*This class run algorithm DecayCorrectionImage
*it.tetratron.pera.model.algorithms
*GPLv3
*@author Ferlin Alessandro
*TetraTron Group
*2012.02.16
//////////////////////////////////
*/
package it.tetratron.pera.model.algorithms;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.*;
import it.tetratron.pera.model.params.*;
import java.util.Vector;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * This class runs DecayCorrectionImage algorithm.
 * @author Ferlin Alessandro
 *
 */
public class DecayCorrectionImage implements AlgorithmInterface {
	
	/**
	 *If there is object with type VoxelObj and RegionObj into Vector objects and 
	 *type of parameters is TypeOfCorrection start algorithm with objects 
	 *and parameters . 
	 *If algorithm run without problem an object VoxelObj contains results
	 *will be return.
	 *
	 *@param parameters must be a TypeOfCorrection object
	 *@param objects vector must contain an VoxelObj and RegionObj object, it will be the object to elaborate
	 *@return a VoxelObj object contains the result of elaboration
	 */
	
	
	@Override
	public ObjInterface run(ParamInterface parameters, Vector<ObjInterface> objects) throws ErrorExc{
		
		java.io.PrintStream ref_printStream=new java.io.PrintStream(MatlabSingleton.getOutputStream());
		
		VoxelObj ref_vox=null;
		RegionObj ref_reg=null;
		TypeOfCorrection ref_par=null;
		
		
		if(!(parameters instanceof TypeOfCorrection)) 
			throw new ErrorExc("EModParamNotCorrect");
		ref_par=(TypeOfCorrection)parameters;
		
		
		for(int i=0; (i < objects.size()) && (ref_vox==null || ref_reg==null); i++){

			if(objects.get(i) instanceof VoxelObj && ref_vox==null)
				ref_vox=(VoxelObj)objects.get(i);
			
			if(objects.get(i) instanceof RegionObj && ref_reg==null)
				ref_reg=(RegionObj)objects.get(i);
		}
		
		if(ref_reg==null){
			throw new ErrorExc("EModRegObjNotFound");
		}
		
		
		if(ref_vox==null){ 							  
			throw new ErrorExc("EModVoxObjNotFound");
		}
		
		if(ref_reg.getTime_pet().size() != ref_vox.getT())
			throw new ErrorExc("EModTimeOfVoxObjRegObjNotSame");

		ref_printStream.println("Start processing...");
		
		Object[] output=null;
		try{
			output=MatlabSingleton.getInstance().getPreprocessing().DecayCorrectionImage(1,((SlashVector)ref_reg.getTime_pet()).rotate(),
												ref_vox.getDynPet(),
												ref_vox.getDynMask(),
												ref_par.getTypeOfCorrection());
		}
		catch(MWException e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}catch(Exception e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}
		
		ref_printStream.println("Processing over...");
		ref_printStream.println("Taking the result...");
		
		VoxelObj aux=new VoxelObjAdapter();
		aux.copyHeader(ref_vox);
		aux.setDynPet((double[][][][])((MWNumericArray)output[0]).toDoubleArray());
		((MWNumericArray)output[0]).dispose();
		
		aux.copyInMaskHeader(ref_vox);
		aux.setMask(ref_vox.getDynMask());
		
		ref_printStream.println("Finish.");
		
		return aux;
	}
}