/**
//////////////////////////////////
*AlgorithmInterface.java
*This is the common interface for all the types of Algorithms handled by the system
*it.tetratron.pera.model.algorithms
*GPLv3
*@author Ferlin Alessandro
*TetraTron Group
*2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.algorithms;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.*;
import java.util.Vector;	
import it.tetratron.pera.model.params.ParamInterface;

/**
 * This is the common interface for all the types of algorithms handled by the system
 * it.tetratron.pera.model.algorithms
 * @author Ferlin Alessandro
 *
 */

public interface AlgorithmInterface {
	/**
	 *if objects into Vector objects are the correct type and parameters is the
	 *correct type start algorithm with objects and parameters . 
	 *if algorithm run without problem an object ObjInterface with results
	 *will be return 
	 *@param parameters the parameter needed by algorithm
	 *@param objects vector contains the object that will be elaborated
	 *@return the result of elaboration.
	 */
	public ObjInterface run(ParamInterface parameters, Vector<ObjInterface> objects) throws ErrorExc;
}
