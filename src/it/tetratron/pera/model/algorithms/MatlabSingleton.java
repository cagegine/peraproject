/**
//////////////////////////////////
MatlabSingleton. java
This class is a singleton that create a single instance of Sake_java.preprocessing and Sake_java.quantification.
Also create an instance of java.io.OutputStream where the matlab massage will be print.
Package it.tetratron.pera.model.algorithms 
GPLv3
Ferlin Alessandro
TetraTron Group
2012.03.01
//////////////////////////////////
*/
package it.tetratron.pera.model.algorithms;

import it.tetratron.pera.model.manage.ErrorExc;

import com.mathworks.toolbox.javabuilder.MWComponentOptions;

import Sake_java.preprocessing;
import Sake_java.quantification;

/**
 * This class is a singleton that create a single instance of Sake_java.preprocessing and Sake_java.quantification.
Also create an instance of java.io.OutputStream where the matlab massage will be print.
 * @author Ferlin Alessandro.
 */
 

public class MatlabSingleton {

	private static MatlabSingleton instance=null;
	private preprocessing ref_pre=null;
	private quantification ref_sa=null;
	private static java.io.OutputStream outputStream;
	
	/**
	 * Private constructor, it create an instance of preprocessing and quantification from Sake_java.jar
	 * Default outputStream is System.out
	 */
	private MatlabSingleton() throws ErrorExc{
		outputStream=System.out;
		try {
			ref_pre=new preprocessing();
			ref_sa=new quantification();
		} catch (Exception e) {
			throw new ErrorExc("EModMCRNotInstalled");
		}
		catch (Error e) {
			throw new ErrorExc("EModMCRNotInstalled");
		}
		instance=this;
	}
	
	/**
	 * Private constructor, it create an instance of preprocessing and quantification from Sake_java.jar
	 * The outputStream is out.
	 * @param out where print will be redirected.
	 */
	private MatlabSingleton(java.io.OutputStream out) throws ErrorExc{
		
		java.io.PrintStream printStream=new java.io.PrintStream(out);
		MWComponentOptions option=new MWComponentOptions();
		option.setErrorStream(printStream);
		option.setPrintStream(printStream);
		
		try {
			ref_pre=new preprocessing(option);
			ref_sa=new quantification(option);
		} catch (Exception e) {
			throw new ErrorExc("EModMCRNotInstalled");
		}
		catch (Error e) {
			throw new ErrorExc("EModMCRNotInstalled");
		}
		instance=this;
	}
	
	/**
	 * returns an instance of MatlabSingleton 
	 */
	
	public static MatlabSingleton getInstance() throws ErrorExc{
		if(instance==null)
			throw new ErrorExc("EModMCRNotInstalled");
		return instance;
	}
	
	/**
	 * Returns an instance of Sake_java.preprocessing.
	 * @return Sake_java.preprocessing
	 */
	public preprocessing getPreprocessing(){
		return ref_pre;
	}
	
	/**
	 * Returns an instance of Sake_java.quantification.
	 * @return Sake_java.quantification
	 */
	public quantification getQuantification(){
		return ref_sa;
	}
	
	/**
	 * Returns the java.io.OutputStream where matlab message will be print.
	 * @return java.io.OutputStream
	 */
	public static java.io.OutputStream getOutputStream() throws ErrorExc{
		if(instance==null)
			new MatlabSingleton();
		return outputStream;
	}
	
	/**
	 * Sets the OutputStream out where matlab output will be print. 
	 * @param out
	 */
	public static void setOutputStream(java.io.OutputStream out) throws ErrorExc{
		outputStream=out;
		new MatlabSingleton(out);
	}
}
