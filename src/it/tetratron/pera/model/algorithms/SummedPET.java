/**
//////////////////////////////////
*SummedPET.java
*This class run algorithm SummedPET
*it.tetratron.pera.model.algorithms
*GPLv3
*@author Ferlin Alessandro
*TetraTron Group
*2012.02.16
//////////////////////////////////
*/
package it.tetratron.pera.model.algorithms;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.*;
import it.tetratron.pera.model.params.*;
import java.util.Vector;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * This class runs SummedPET algorithm.
 * @author Ferlin Alessandro
 *
 */
public class SummedPET implements AlgorithmInterface {
	
	/**
	 *If there is object with type VoxelObj and RegionObj into Vector objects start algorithm with 
	 *object and parameters. 
	 *The parameters will not be considered. 
	 *If algorithm run without problem an object VoxelObj contains results
	 *will be return.
	 *@param parameters
	 *@param objects vector must contain an VoxelObj, it will be the object to elaborate
	 *@return a VoxelObj contains the result of elaboration
	 */
	
	@Override
	public ObjInterface run(ParamInterface parameters, Vector<ObjInterface> objects) throws ErrorExc{

		java.io.PrintStream ref_printStream=new java.io.PrintStream(MatlabSingleton.getOutputStream());
		
		VoxelObj ref_vox=null;
		RegionObj ref_reg=null;
		
		for(int i=0; (i < objects.size()) && (ref_vox==null || ref_reg==null); i++){
			
			if(objects.get(i) instanceof VoxelObj && ref_vox==null)
				ref_vox=(VoxelObj)objects.get(i);
			
			if(objects.get(i) instanceof RegionObj && ref_reg==null)
				ref_reg=(RegionObj)objects.get(i);
		}
		
		if(ref_reg==null)
			throw new ErrorExc("EModRegObjNotFound");
		
		
		if(ref_vox==null) 							  
			throw new ErrorExc("EModVoxObjNotFound");
		
		if(ref_reg.getTime_pet().size() != ref_vox.getT())
			throw new ErrorExc("EModTimeOfVoxObjRegObjNotSame");

		ref_printStream.println("Start processing...");
		
		Object[] output=null;
		try{
			output=MatlabSingleton.getInstance().getPreprocessing().summedPET(1,((SlashVector)ref_reg.getTime_pet()).rotate(),
									((SlashVector)ref_reg.getDelta()).rotate(),
									ref_vox.getDynPet(),
									ref_vox.getDynMask());
		}
		catch(MWException e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}catch(Exception e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}
		
		ref_printStream.println("Processing over...");
		ref_printStream.println("Taking the result...");
		
		VoxelObj aux=new VoxelObjAdapter();
		
		aux.copyInMaskHeader(ref_vox);
		aux.setMask(ref_vox.getDynMask());
		
		aux.copyMaskHeader(ref_vox);
		if(((MWNumericArray)output[0]).numberOfDimensions()==2)
			aux.setDynPet((double[][])((MWNumericArray)output[0]).toDoubleArray());
		else
			aux.setDynPet((double[][][])((MWNumericArray)output[0]).toDoubleArray());
		((MWNumericArray)output[0]).dispose();
		
		ref_printStream.println("Finish.");
		
		return aux;
	}
}