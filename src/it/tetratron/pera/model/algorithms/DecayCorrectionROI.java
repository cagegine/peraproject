/**
//////////////////////////////////
*DecayCorrectionROI. java
*This class run algorithm DecayCorrectionROI
*it.tetratron.pera.model.algorithms
*GPLv3
*@author Ferlin Alessandro
*TetraTron Group
*2012.02.16
//////////////////////////////////
*/
package it.tetratron.pera.model.algorithms;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.*;
import it.tetratron.pera.model.params.*;
import java.util.Vector;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * This class runs DecayCorrectionROI algorithm.
 * @author Ferlin Alessandro
 *
 */
public class DecayCorrectionROI implements AlgorithmInterface {
	
	/**
	 *If there is object with type RegionObj into Vector objects and 
	 *type of parameters is TypeOfCorrection start algorithm with objects 
	 *and parameters . 
	 *If algorithm run without problem an object RegionObj contains results
	 *will be return.
	 *
	 *@param parameters must be a TypeOfCorrection object
	 *@param objects vector must contain an RegionObj object, it will be the object to elaborate
	 *@return a RegionObj object contains the result of elaboration
	 */
	
	@Override
	public ObjInterface run(ParamInterface parameters, Vector<ObjInterface> objects) throws ErrorExc{
		
		java.io.PrintStream ref_printStream=new java.io.PrintStream(MatlabSingleton.getOutputStream());
		
		RegionObj ref_reg=null;
		TypeOfCorrection ref_par;
		
		
		if(!(parameters instanceof TypeOfCorrection)) 
			throw new ErrorExc("EModParamNotCorrect");
		ref_par=(TypeOfCorrection)parameters;
		
		
		for(int i=0; (i < objects.size()) && ref_reg==null; i++){
		
			if(objects.get(i) instanceof RegionObj)
				ref_reg=(RegionObj)objects.get(i);
		}
		

		if(ref_reg==null){ 							  
			throw new ErrorExc("EModRegObjNotFound");
		}
		
		ref_printStream.println("Start processing...");
		
		Object[] output=null;
		try{
			output=MatlabSingleton.getInstance().getPreprocessing().DecayCorrectionROI(1,((SlashVector)ref_reg.getTime_pet()).rotate(),
												((SlashVector)ref_reg.getDelta()).rotate(),
												((SlashVector)ref_reg.getNsd()).rotate(),
												ref_reg.getRoiArray(),
												ref_par.getTypeOfCorrection());
		}
		catch(MWException e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}catch(Exception e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}
		
		ref_printStream.println("Processing over...");
		ref_printStream.println("Taking the result...");
		
		RegionObj aux=new RegionObj();
		aux.setLabels(ref_reg.getLabels());
		aux.setTime_pet(ref_reg.getTime_pet());
		aux.setDelta(ref_reg.getDelta());
		aux.setNsd(ref_reg.getNsd());
		aux.setRoi((double[][])((MWNumericArray)output[0]).toDoubleArray());
		
		ref_printStream.println("Finish.");
		
		return aux;
	}
}