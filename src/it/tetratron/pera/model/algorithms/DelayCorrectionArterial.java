/**
//////////////////////////////////
*DelayCorrectionArterial. java
*This class run algorithm DelayCorrectionArterial
*it.tetratron.pera.model.algorithms
*GPLv3
*@author Ferlin Alessandro
*TetraTron Group
*2012.02.16
//////////////////////////////////
*/
package it.tetratron.pera.model.algorithms;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.*;
import it.tetratron.pera.model.params.*;
import java.util.Vector;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * This class runs DelayCorrection algorithm.
 * @author Ferlin Alessandro
 *
 */
public class DelayCorrectionArterial implements AlgorithmInterface {
	
	/**
	 *If there is object with type ArterialObj into Vector objects and 
	 *type of parameters is Delay start algorithm with objects 
	 *and parameters . 
	 *If algorithm run without problem an object ArterialObj contains results
	 *will be return.
	 *
	 *@param parameters must be a Delay object
	 *@param objects vector must contain an ArterialObj object, it will be the object to elaborate
	 *@return a ArterialObj object contains the result of elaboration
	 */
	
	@Override
	public ObjInterface run(ParamInterface parameters, Vector<ObjInterface> objects) throws ErrorExc{

		java.io.PrintStream ref_printStream=new java.io.PrintStream(MatlabSingleton.getOutputStream());
		
		ArterialObj ref_art=null;
		Delay ref_par=null;
		

		if(!(parameters instanceof Delay)) 
			throw new ErrorExc("EModParamNotCorrect");
		ref_par=(Delay)parameters;
		

		for(int i=0; (i < objects.size()) && ref_art==null; i++){
			if(objects.get(i) instanceof ArterialObj)
				ref_art=(ArterialObj)objects.get(i);
		}
		
		if(ref_art==null){ 							  
			throw new ErrorExc("EModArtObjNotFound");
		}
		
		ref_printStream.println("Start processing...");

		Object[] output=null;
		try{
			output=MatlabSingleton.getInstance().getPreprocessing().DelayCorrection(3,((SlashVector)ref_art.getTime_plasma()).rotate(),
											((SlashVector)ref_art.getPlasma()).rotate(),
											((SlashVector)ref_art.getBlood()).rotate(),
											ref_par.getDelay());
		}
		catch(MWException e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}catch(Exception e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}
		
		ref_printStream.println("Processing over...");
		ref_printStream.println("Taking the result...");
		
		ArterialObj aux=new ArterialObj();
		aux.setLabels(ref_art.getLabels());
		aux.setTime_plasma((double[][])((MWNumericArray)output[0]).toDoubleArray());
		aux.setPlasma((double[][])((MWNumericArray)output[1]).toDoubleArray());
		aux.setBlood((double[][])((MWNumericArray)output[2]).toDoubleArray());
		
		ref_printStream.println("Finish.");
		
		return aux;
	}
}