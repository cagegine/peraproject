/**
//////////////////////////////////
*RegionStandard.java
*This class run algorithm RegionStandard
*it.tetratron.pera.model.algorithms
*GPLv3
*@author Ferlin Alessandro
*TetraTron Group
*2012.02.16
//////////////////////////////////
*/
package it.tetratron.pera.model.algorithms;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.*;
import it.tetratron.pera.model.params.*;
import java.util.Vector;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * This class runs Std_ROI algorithm.
 * @author Ferlin Alessandro
 *
 */
public class RegionStandard implements AlgorithmInterface {
	
	/**
	 *If there is object with type RegionObj and ArterialObj into Vector objects and 
	 *type of parameters is StdSAParam start algorithm with objects 
	 *and parameters . 
	 *If algorithm run without problem an object SakeRegionObj contains results
	 *will be return.
	 *@param parameters must be a StdSAParam
	 *@param objects vector must contain an RegionObj and an ArterialObj object, it will be the object to elaborate
	 *@return SakeRegionObj contains the result of elaboration
	 */
	
	@Override
	public ObjInterface run(ParamInterface parameters, Vector<ObjInterface> objects) throws ErrorExc{

		java.io.PrintStream ref_printStream=new java.io.PrintStream(MatlabSingleton.getOutputStream());
		
		RegionObj ref_reg=null;
		ArterialObj ref_art=null;
		StdSAParam ref_par=null;
		

		if(!(parameters instanceof StdSAParam)) 
			throw new ErrorExc("EModParamNotCorrect");
		ref_par=(StdSAParam)parameters;
		

		for(int i=0; (i < objects.size()) && (ref_reg==null || ref_art==null); i++){

			if(objects.get(i) instanceof RegionObj && ref_reg==null)
				ref_reg=(RegionObj)objects.get(i);
			
			if(objects.get(i) instanceof ArterialObj && ref_art==null)			
				ref_art=(ArterialObj)objects.get(i);
		}

		if(ref_reg==null){
			throw new ErrorExc("EModRegObjNotFound");
		}
		

		if(ref_art==null){ 							  
			throw new ErrorExc("EModArtObjNotFound");
		}

		ref_printStream.println("Start processing...");
				
		Object[] output=null;
		try{
			output=MatlabSingleton.getInstance().getQuantification().stdSA_ROI(4,((SlashVector)ref_art.getTime_plasma()).rotate(),
									((SlashVector)ref_art.getPlasma()).rotate(),
									((SlashVector)ref_art.getBlood()).rotate(),
									((SlashVector)ref_reg.getTime_pet()).rotate(),
									((SlashVector)ref_reg.getNsd()).rotate(),
									ref_reg.getRoiArray(),
									ref_par.getWorkspace_Dir(),
									ref_par.getNComp(),
									ref_par.getBeta1(),
									ref_par.getBeta2(),
									ref_par.getType());
		}
		catch(MWException e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}catch(Exception e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}
		
		ref_printStream.println("Processing over...");
		ref_printStream.println("Taking the result...");

		SakeRegionObj aux=new SakeRegionObj();
		aux.setParamInterface(ref_par);
		aux.setRegionObj(ref_reg);
		aux.setArterialObj(ref_art);
		aux.setAlgorithm("StdSA");
		aux.setFit((double[][])((MWNumericArray)output[0]).toDoubleArray());
		aux.setParameters((double[][])((MWNumericArray)output[1]).toDoubleArray());
		aux.setAlfa((double[][])((MWNumericArray)output[2]).toDoubleArray());
		aux.setBeta((double[][])((MWNumericArray)output[3]).toDoubleArray());
		
		ref_printStream.println("Finish.");
		
		return aux;
	}
}