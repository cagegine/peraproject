/**
//////////////////////////////////
*ImageStandard. java
*This class run algorithm ImageStandard
*it.tetratron.pera.model.algorithms
*GPLv3
*@author Ferlin Alessandro
*TetraTron Group
*2012.02.16
//////////////////////////////////
*/
package it.tetratron.pera.model.algorithms;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.*;
import it.tetratron.pera.model.params.*;
import java.util.Vector;
import com.mathworks.toolbox.javabuilder.MWException;
import com.mathworks.toolbox.javabuilder.MWNumericArray;

/**
 * This class runs Std_Image algorithm.
 * @author Ferlin Alessandro
 *
 */
public class ImageStandard implements AlgorithmInterface {

	/**
	 *If there is object with type VoxelObj, RegionObj and ArterialObj into Vector objects and 
	 *type of parameters is StdSAParam start algorithm with objects 
	 *and parameters . 
	 *If algorithm run without problem an object SakeVoxelObj contains results
	 *will be return.
	 *
	 *@param parameters must be a StdSAParam object
	 *@param objects vector must contain an VoxelObj, RegionObj and ArterialObj object, it will be the object to elaborate
	 *@return a VoxelObj object contains the result of elaboration
	 */
	
	@Override
	public ObjInterface run(ParamInterface parameters, Vector<ObjInterface> objects) throws ErrorExc{

		java.io.PrintStream ref_printStream=new java.io.PrintStream(MatlabSingleton.getOutputStream());
		
		VoxelObj ref_vox=null;
		RegionObj ref_reg=null;
		ArterialObj ref_art=null;
		StdSAParam ref_par=null;
		
	
		if(!(parameters instanceof StdSAParam)) 
			throw new ErrorExc("EModParamNotCorrect");
		ref_par=(StdSAParam)parameters;
		
	
		for(int i=0; (i < objects.size()) && (ref_vox==null || ref_reg==null || ref_art==null); i++){
	
			if(objects.get(i) instanceof VoxelObj && ref_vox==null)
				ref_vox=(VoxelObj)objects.get(i);
			
			if(objects.get(i) instanceof RegionObj && ref_reg==null)
				ref_reg=(RegionObj)objects.get(i);
			
			if(objects.get(i) instanceof ArterialObj && ref_art==null)
				ref_art=(ArterialObj)objects.get(i);
		}
		
		if(ref_art==null){ 							  
			throw new ErrorExc("EModArtObjNotFound");
		}
		
		
		if(ref_reg==null){
			throw new ErrorExc("EModRegObjNotFound");
		}
		
		
		if(ref_vox==null){ 							  
			throw new ErrorExc("EModVoxObjNotFound");
		}

		if(ref_reg.getTime_pet().size() != ref_vox.getT())
			throw new ErrorExc("EModTimeOfVoxObjRegObjNotSame");
		
		
		ref_printStream.println("Start processing...");
		
		Object[] output=null;
		try{
			output=MatlabSingleton.getInstance().getQuantification().stdSA_image(5,((SlashVector)ref_art.getTime_plasma()).rotate(),
										((SlashVector)ref_art.getPlasma()).rotate(),
										((SlashVector)ref_art.getBlood()).rotate(),
										((SlashVector)ref_reg.getTime_pet()).rotate(),
										((SlashVector)ref_reg.getNsd()).rotate(),
										ref_vox.getDynPet(),
										ref_vox.getDynMask(),
										ref_par.getWorkspace_Dir(),
										ref_par.getNComp(),
										ref_par.getBeta1(),
										ref_par.getBeta2(),
										ref_par.getType());
		}
		catch(MWException e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}catch(Exception e){
			ref_printStream.println("Internal Matlab error, impossible to complete elaboration.");
			throw new ErrorExc("EModMatlabError");
		}
		
		ref_printStream.println("Processing over...");
		ref_printStream.println("Taking the result...");
		
		SakeVoxelObj aux=new SakeVoxelObj();
		
		VoxelObj fit=new VoxelObjAdapter();
		fit.copyHeader(ref_vox);
		fit.setDynPet((double[][][][])((MWNumericArray)output[0]).toDoubleArray());
		((MWNumericArray)output[0]).dispose();
		output[0]=null; //free memory
		
		VoxelObj mapVt=new VoxelObjAdapter();
		mapVt.copyMaskHeader(ref_vox);
		if(((MWNumericArray)output[1]).numberOfDimensions()==2)
			mapVt.setDynPet((double[][])((MWNumericArray)output[1]).toDoubleArray());
		else
			mapVt.setDynPet((double[][][])((MWNumericArray)output[1]).toDoubleArray());
		((MWNumericArray)output[1]).dispose();
		output[1]=null; //free memory
		
		VoxelObj mapKi=new VoxelObjAdapter();
		mapKi.copyMaskHeader(ref_vox);
		if(((MWNumericArray)output[2]).numberOfDimensions()==2)
			mapKi.setDynPet((double[][])((MWNumericArray)output[2]).toDoubleArray());
		else
			mapKi.setDynPet((double[][][])((MWNumericArray)output[2]).toDoubleArray());
		((MWNumericArray)output[2]).dispose();
		output[2]=null; //free memory
		
		VoxelObj mapVb=new VoxelObjAdapter();
		mapVb.copyMaskHeader(ref_vox);
		if(((MWNumericArray)output[3]).numberOfDimensions()==2)
			mapVb.setDynPet((double[][])((MWNumericArray)output[3]).toDoubleArray());
		else
			mapVb.setDynPet((double[][][])((MWNumericArray)output[3]).toDoubleArray());
		((MWNumericArray)output[3]).dispose();
		output[3]=null; //free memory
		
		VoxelObj mapOrder=new VoxelObjAdapter();
		mapOrder.copyMaskHeader(ref_vox);
		if(((MWNumericArray)output[4]).numberOfDimensions()==2)
			mapOrder.setDynPet((double[][])((MWNumericArray)output[4]).toDoubleArray());
		else
			mapOrder.setDynPet((double[][][])((MWNumericArray)output[4]).toDoubleArray());
		((MWNumericArray)output[4]).dispose();
		output[4]=null; //free memory
		
		//set SakeVoxelObj
		aux.setRawVoxel(ref_vox);
		aux.setFit(fit);
		aux.setMapVt(mapVt);
		aux.setMapKi(mapKi);
		aux.setMapVb(mapVb);
		aux.setMapOrder(mapOrder);
		aux.setParamInterface(ref_par);
		aux.setAlgorithm("StdSA");
		aux.setArterialObj(ref_art);
		aux.setRegionObj(ref_reg);
		
		ref_printStream.println("Finish.");
		
		return aux;
	}
}
