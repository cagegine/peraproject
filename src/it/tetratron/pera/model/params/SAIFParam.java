/*
//////////////////////////////////
SAIFParam.java
This is the class for handling the right parameters for Iterative Filtering Spectral Analysis
algorithms
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.SlashVector;

import java.util.Vector;

/**
 * This is the class for handling the right parameters for Iterative Filtering Spectral Analysis
 * @author Palleva Enrico
 *
 */
public class SAIFParam extends SAParam{
	private Vector<Double> passband;
	
	public SAIFParam(){	
	}
	
	
	/**
	 * Creates a new SAIFParam
	 * @param w is a string parameter, that indicates the workspace directory
	 * @param b1 is a double parameter, that indicates the beta1 value
	 * @param b2 is a double parameter, that indicates the beta2 value
	 * @throws ErrorExc exception if the paramters aren't set correctly
	 */
	public SAIFParam(String w,Double b1,Double b2, Integer ncomp) throws ErrorExc{
		setWorkspace_Dir(w);
		try{
			setNComp(ncomp);
			setBeta1(b1);
			setBeta2(b2);
		}catch(ErrorExc e){
			throw e;
		}
	}
	
	/**
	 * Checks if Passband values are in a right range and the data consistency between them
	 * @param a First Passband value
	 * 		  b Second Passband value
	 * @return False if Passband1<beta1 or Passband2>beta2 or Passband1<0 or Passband2>0
	 * 		   else True
	 */
	private boolean checkPassband(Double a, Double b){
		if(a<beta1 || b>beta2 || a<0 || b<0)
			return false;
		else
			return true;
	}
	
	/**
	 * Gets the passband vector
	 * @return a vector that contains two double values (Passband)
	 */
	public Vector<Double> getPassband(){
		if(passband!=null && checkPassband(passband.get(0),passband.get(1)))
			return passband;
		else
			return getDefaultPassband();
	}
	
	/**
	 * Sets and returns the default values for the passband vector
	 */
	private Vector<Double> getDefaultPassband(){
		passband=new Vector<Double>();
		passband.add(0,beta1*10);
		passband.add(1,beta2/2);
		return passband;
	}
	
	/**
	 * Sets the values for the passband vector
	 * @param a is a double parameter
	 * @param b is a double parameter
	 * @throws ErrorExc exception if the parameters aren't set correctly
	 */
	public void setPassband(Double a,Double b) throws ErrorExc{
		passband=new SlashVector();
		passband.add(0,a);
		passband.add(1, b);	
		if(checkPassband(a,b)==false)
			throw new ErrorExc("PassbandValuesNotValid");
	}


	@Override
	public boolean check() {
		if(passband!=null)
			if(checkNcomp(ncomp) && checkWorkspace(workspace_dir) && checkBeta1(beta1) && checkBeta2(beta2) && checkPassband(passband.get(0),passband.get(1)))
				return true;
		return false;
	}
}
