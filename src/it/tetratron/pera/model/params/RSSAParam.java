/*
//////////////////////////////////
RSSAParam.java
This is the class for handling the right parameters for Rank Shaping Spectral Analysis
algorithms
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.SlashVector;

import java.util.Vector;

/**
 * This is the class for handling the right parameters for Rank Shaping Spectral Analysis
 * @author Palleva Enrico
 *
 */
public class RSSAParam extends SAParam{
	private Vector<Double> SNR;
	
	public RSSAParam(){	
	}
	
	
	/**
	 * @param w is a string parameter, that indicates the workspace directory
	 * @param b1 is a double parameter, that indicates the beta1 value
	 * @param b2 is a double parameter, that indicates the beta2 value
	 * @param ncomp is an integer parameter, that indicates the ncomp value
	 * @throws ErrorExc exception if the parameters aren't set correctly
	 */
	public RSSAParam(String w,Double b1,Double b2, Integer ncomp) throws ErrorExc{
		setWorkspace_Dir(w);
		try{
			setNComp(ncomp);
			setBeta1(b1);
			setBeta2(b2);	
		}catch(ErrorExc e){
			throw e;
		}
	}
	
	/**
	 * Sets the default values for the SNR vector
	 */
	private Vector<Double> getDefaultSNR(){
		SNR=new SlashVector();
		SNR.add(0,beta1*10);
		SNR.add(1,beta2/2);
		return SNR;
	}
	
	/**
	 * Checks if SNR values are in a right range and the data consistency between them
	 * @param a First SNR value
	 * 		  b Second SNR value
	 * @return False if SNR1<beta1 or SNR2>beta2 or SNR1<0 or SNR2>0
	 * 		   else True
	 */
	private boolean checkSNR(Double a,Double b){
		if(a<beta1 || b>beta2 || a<0 || b<0)
			return false;
		else
			return true;
	}
	
	/**
	 * Sets the values for the SNR vector
	 * @param a is a double parameter
	 * @param b is a double parameter
	 * @throws ErrorExc exception if the parameters aren't set correctly
	 */
	public void setSNR(Double a,Double b) throws ErrorExc{
		SNR=new SlashVector();
		SNR.add(0,a);
		SNR.add(1,b);
		if(checkSNR(a,b)==false)
			throw new ErrorExc("SNRvalueNotValid");
			
	}
	
	/**
	 * Gets the SNR vector
	 * @return a vector of two double values
	 */
	public Vector<Double> getSNR(){
		if(SNR!=null && checkSNR(SNR.get(0),SNR.get(1))==true)
			return SNR;
		else
			return getDefaultSNR(); //RITORNARE UN VECTOR CON VALORI DEFAULT
	}


	@Override
	public boolean check() {
		if(SNR!=null)
			if(checkNcomp(ncomp) && checkWorkspace(workspace_dir) && checkBeta1(beta1) && checkBeta2(beta2) && checkSNR(SNR.get(0),SNR.get(1)))
				return true;
		return false;
	}
}
