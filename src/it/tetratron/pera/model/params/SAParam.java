/*
//////////////////////////////////
SAParam.java
This is the class for handling the right parameters for Spectral Analysis
algorithms
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;

import java.io.File;

import it.tetratron.pera.model.manage.ErrorExc;

/**
 * This is the class for handling the right parameters for Spectral Analysis
 * @author Palleva Enrico
 *
 */
public abstract class SAParam implements ParamInterface {
	protected String workspace_dir;
	protected Integer ncomp;  //set default
	protected Double beta1; //set default
	protected Double beta2; //set default
	
	protected SAParam(){}
	
	/**
	 * Creates a new SAParam object
	 * @param w is a string that indicates the workspace directory
	 * @param b1 is a double parameter that indicates the beta1 value
	 * @param b2 is a double parameter that indicates the beta2 value
	 * @throws ErrorExc exception if the value aren't set correctly
	 */
	protected SAParam(String w,Double b1,Double b2, Integer ncomp) throws ErrorExc{
		setWorkspace_Dir(w);
		try{
			setNComp(ncomp);
			setBeta1(b1);
			setBeta2(b2);
		}catch(ErrorExc e){
			throw e;
		}
	}
	
	
	/**
	 *Checks if Workspace exists 
	 * @param w path path of workspace
	 * @return true if workspace exists else false
	 */
	static public boolean checkWorkspace(String w){
		File dir = new File(w);
		if(!dir.exists())
			return false;
		return true;
	}
	
	/**
	 *Checks if ncomp is correct 
	 * @param nc number of component
	 * @return True if ncomp between 50 and 500, else return False 
	 */
	protected boolean checkNcomp(Integer nc){
		if(nc<50 || nc>500)
			return false;
		else
			return true;
	}
	
	/**
	 * Checks if beta1 is correct 
	 * @param b value of beta1
	 * @return true if b between 0.00001 and 1, else return false 
	 */
	protected boolean checkBeta1(Double b){
		if(b<0.00001 || b>1)
			return false;
		else
			return true;
	}
	
	/**
	 * Checks if beta2 is correct 
	 * @param b value of beta2
	 * @return true if b between 0.001 and 100, else return false 
	 */
	
	protected boolean checkBeta2(Double b){
		if(b<0.001 || b>100)
			return false;
		else
			return true;
	}
	
	
	/*
	protected boolean checkBeta(Double b1, Double b2){
		if(b1==null)
			return checkBeta1(b1);
		if(b2==null)
			return checkBeta2(b2);
		if(b1>(b2/10))
			return false;
		return false;
	}
	*/
	
	/**
	 * Sets the string for the workspace directory
	 * @param w is a string that indicates the value for the workspace directory
	 */
	public void setWorkspace_Dir(String w) throws ErrorExc{
		File dir = new File(w);
		workspace_dir=dir.getPath();
		
		if(!(workspace_dir.endsWith("/") || workspace_dir.endsWith("\\")) )
			workspace_dir = workspace_dir + System.getProperty("file.separator");
		
		if(checkWorkspace(w)==false)
			throw new ErrorExc("EModWorkspaceNotExists");
	}
	
	
	/**
	 * Sets the value for the ncomp parameter
	 * @param n is an integer parameter
	 * @throws ErrorExc exception if the ncomp value isn't set correctly
	 */
	public void setNComp(Integer n) throws ErrorExc{
		ncomp=n;
		if(checkNcomp(n)==false)
			throw new ErrorExc("EModNCompNegativeOrNull");
	}
	
	/**
	 * Sets the value for the beta1 parameter
	 * @param b is a double parameter
	 * @throws ErrorExc exception if the the beta1 value isn't set correctly
	 */
	public void setBeta1(Double b) throws ErrorExc{
		beta1=b;
		if(checkBeta1(b)==false)
			throw new ErrorExc("EModBeta1NegativeOrNull");
	}
	
	/**
	 * Sets the value for the beta2 parameter
	 * @param b is a double parameter
	 * @throws ErrorExc exception if the beta2 value isn't set correctly
	 */
	public void setBeta2(Double b) throws ErrorExc{
		beta2=b;
		if(checkBeta2(b)==false)
			throw new ErrorExc("EModBeta2NegativeOrNull");
	}
	
	/**
	 * Gets the workspace directory 
	 * @return a string value representing the workspace directory path
	 */
	public String getWorkspace_Dir(){
		if(checkWorkspace(workspace_dir)==true)
			return workspace_dir;
		else
			return ""; //TODO: DA SETTARE UNA WORKSPACE CHE SIA PRESENTE SEMPRE
	}
	
	/**
	 * Gets the ncomp value
	 * @return an integer value representing the ncomp parameter
	 */
	public Integer getNComp(){
		if(checkNcomp(ncomp)==true)
			return ncomp;
		else
			return getDefaultNComp();
	}
	
	/**
	 * Gets the beta1 value
	 * @return a double value representing the beta1 parameter
	 */
	public Double getBeta1(){
		if(checkBeta1(beta1))
			return beta1;
		else
			return getDefaultBeta1();
	}
	
	/**
	 * Gets the beta2 value
	 * @return a double value representing the beta2 parameter
	 */
	public Double getBeta2(){
		if(checkBeta2(beta2))
			return beta2;
		else
			return getDefaultBeta2();
	}
	
	/**
	* Returns the Default value of NComp
	 * @return the default value for NComp: 100
	 */
	public static Integer getDefaultNComp(){
		return 100;
	}
	
	/**
	* Returns the Default value of Beta1
	 * @return the default value for Beta1: 0.01
	 */
	public static Double getDefaultBeta1(){
		return 0.01;
	}
	
	/**
	* Returns the Default value of Beta2
	 * @return the default value for Beta2: 1.0
	 */
	public static Double getDefaultBeta2(){
		return 1.0;
	}
}