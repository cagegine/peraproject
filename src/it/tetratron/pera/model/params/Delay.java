/*
//////////////////////////////////
Delay.java
This is the class for handling the right parameters for algorithms
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;

import it.tetratron.pera.model.manage.ErrorExc;

/**
 * This is the class for handling the right parameters for ArterialDelay algorithm
 * @author Palleva Enrico
 *
 */
public class Delay implements ParamInterface{
	private Double delay;
	
	public Delay(){
		
	}
	
	/**
	 * Creates a new Delay object
	 * @param d is the delay for the algorithm
	 * @throws ErrorExc exception if the delay value isn't correct
	 */
	public Delay(Double d) throws ErrorExc{
			setDelay(d);
	}
	
	/**
	 * Checks if delay is in a right range
	 * @param d Delay to be checked
	 * @return True if delay is between -100 and 100, else False
	 */
	private boolean checkDelay(Double d){
		if(d<-100 || d>100)
			return false;
		else
			return true;
	}
	
	/**
	 * Sets the value of delay parameter
	 * @param delay the delay for the algorithm
	 * @throws ErrorExc exception if the delay value isn't correct
	 */
	public void setDelay(Double delay) throws ErrorExc{
		this.delay=delay;
		if(checkDelay(delay)==false)
			throw new ErrorExc("EModDelayValueNotValid");
	}
	
	/**
	 * Gets the value of delay parameter
	 * @return a double parameter, the delay for the algorithm
	 */
	public Double getDelay(){
		if(checkDelay(delay)==true)
			return delay;
		else
			return getDefaultDelay(); //valore di default
	}
	
	/**
	* Return the Default value of delay
	 * @return the default value for delay: 0.0
	 */
	public static Double getDefaultDelay(){
		return 0.0;
	}

	/**
	 * return true if all Attributes are correct 
	 */
	@Override
	public boolean check() { 
		if(checkDelay(delay)==true)
			return true;
		else
			return false;
	}
}