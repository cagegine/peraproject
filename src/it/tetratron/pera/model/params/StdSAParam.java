/*
//////////////////////////////////
StdSAParam.java
This is the class for handling the right parameters for Standard Spectral Analysis
algorithms
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;

import it.tetratron.pera.model.manage.ErrorExc;

/**
 * This is the class for handling the right parameters for Standard Spectral Analysis
 * @author Palleva Enrico
 *
 */
public class StdSAParam extends SAParam{
	private Integer type=1; //default
	
	public StdSAParam(){
	}
	
	/**
	 * Creates a new StdSAParam object
	 * @param w is a string parameter, that indicates the workspace directory
	 * @param b1 is a double parameter, that indicates the beta1 value
	 * @param b2 is a double parameter, that indicates the beta2 value
	 * @param t is an integer parameter, that indicates the type value
	 * @throws ErrorExc exception if the parameters aren't set correctly
	 */
	public StdSAParam(String w,Double b1,Double b2,Integer t, Integer ncomp) throws ErrorExc{
		setWorkspace_Dir(w);
		try{
			setNComp(ncomp);
			setBeta1(b1);
			setBeta2(b2);
			setType(t);
		}catch(ErrorExc e){
			throw e;
		}
	}
	
	/**
	 * Checks if Type is correct 
	 * @param t an Integer representing Type
	 * @return True if t between 1 and 3, else return False 
	 */
	private boolean checkType(Integer t){
		if(t<1 || t>3)
			return false;
		else
			return true;
	}
	
	/**
	 * Sets the value of the type parameter
	 * @param t is an integer parameter, representing the type of analysis
	 * @throws ErrorExc exception if the type value isn't set correctly
	 */
	public void setType(Integer t) throws ErrorExc{
		if(checkType(t)==false)
			throw new ErrorExc("EModTypeValueNotValid");
		else
			type=t;
	}
	
	/**
	 * Gets the value of the type parameter
	 * @return an integer value
	 */
	public Integer getType(){
		return type;
	}

	@Override
	public boolean check() {
		if(checkNcomp(ncomp) && checkWorkspace(workspace_dir) && checkBeta1(beta1) && checkBeta2(beta2) && checkType(type))
			return true;
		return false;
		
	}
}