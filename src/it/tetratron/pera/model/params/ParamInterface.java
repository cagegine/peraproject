/*
//////////////////////////////////
ParamInterface.java
This is the common interface for all the parameters handled by the system
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;

/**
 * This is the common interface for all the parameters handled by the system
 * @author Palleva Enrico
 *
 */
public interface ParamInterface {
	/**
	*	Checks if the parameters have the right parameters
	**/
	public boolean check();
}
