/*
//////////////////////////////////
TypeOfCorrection.java
This is the class for handling the right parameters for ArterialDecay and DecayCorrections algorithms
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;

import it.tetratron.pera.model.manage.ErrorExc;

/**
 * This is the class for handling the right parameters for ArterialDecay and DecayCorrections algorithms
 * @author Palleva Enrico
 *
 */
public class TypeOfCorrection implements ParamInterface {
	private Integer typeOfCorrection; 
	
	public TypeOfCorrection(){
		/*Set default values*/
	}
	
	/**
	 * Creates a new TypeOfCorrection object
	 * @param t indicates the type of analysis for the algorithm
	 * @throws ErrorExc exception if the typeOfAnalysis value isn't set correctly
	 */
	public TypeOfCorrection(Integer t) throws ErrorExc{
		try{
			setTypeOfCorrection(t);
		}catch(ErrorExc e){
			throw e;
		}
	}
	
	/**
	 * Checks if Type is correct 
	 * @param t an Integer representing Type
	 * @return True if t between 1 and 3, else return False 
	 */
	private boolean checkType(Integer t){
		if(t<1 || t>3)
			return false;
		else
			return true;
	}
	
	/**
	 * Sets the typeOfAnalysis value correctly from an input Integer
	 * @param t is an integer parameter, representing the type of analysis
	 * @throws ErrorExc
	 */
	public void setTypeOfCorrection(Integer t) throws ErrorExc{
		typeOfCorrection=t;
		if(checkType(t)==false)
			throw new ErrorExc("EModTypeOfCorrectionValueNotValid");
	}
	
	/**
	 * Gets the typeOfAnalysis value
	 * @return an integer that indicates the type of analysis for the algorithm
	 */
	public Integer getTypeOfCorrection(){
		if(checkType(typeOfCorrection)==true)
			return typeOfCorrection;
		else
			return 1; //default
	}

	@Override
	public boolean check() {
		return checkType(typeOfCorrection);
	}
}