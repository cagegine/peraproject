/*
//////////////////////////////////
ModArtParam.java
This is the class for handling the right parameters for ModArterial algorithm
it.tetratron.pera.model.params
GPLv3
Palleva Enrico
TetraTron Group
2012.02.15
//////////////////////////////////
*/

package it.tetratron.pera.model.params;
import it.tetratron.pera.model.manage.ErrorExc;

import java.util.Vector;

/**
 * This is the class for handling the right parameters for ModArterial algorithm
 * @author Palleva Enrico
 *
 */
public class ModArtParam implements ParamInterface {
	private Vector<Double> par_in;
	private boolean typeOfAnalysis=false; //default false
	
	public ModArtParam(){
	}
	
	/**
	 * Creates a new ModArtParam object
	 * @param v is a vector that contains double parameters
	 * @param b is a boolean parameter, it indicates the type of analysis for the algorithm
	 * @throws ErrorExc if the parameters aren't set correctly
	 */
	public ModArtParam(Vector<Double> v,boolean b) throws ErrorExc{
		setPar_In(v);
		setTypeOfAnalysis(b);
	}
	
	/**
	 * Creates a new ModArtParam object
	 * @param v is a vector that contains double parameters
	 * @param b is an integer parameter, it indicates the type of analysis for the algorithm
	 * @throws ErrorExc exception if the parameters aren't set correctly
	 */
	public ModArtParam(Vector<Double> v,int b) throws ErrorExc{
		setPar_In(v);
		if(b==0)
			setTypeOfAnalysis(false);
		else
			setTypeOfAnalysis(true);
			
	}
	
	/**
	 * Creates a new ModArtParam object
	 * @param d is an array of double parameters
	 * @param b is a boolean parameter, it indicates the type of analysis for the algorithm
	 * @throws ErrorExc exception if the parameters aren't set correctly
	 */
	public ModArtParam(double[][] d,boolean b) throws ErrorExc{
		setPar_In(d);
		setTypeOfAnalysis(b);
	}
	
	/**
	 * Returns the Default values of par_in
	 * @return vector contains default values for par_in
	 */
	public static Vector<Double> getDefaultParIn(){
		Vector<Double> v=new Vector<Double>();
		v.add(100.0);
		v.add(30.0);
		v.add(10.0);
		v.add(0.1);
		v.add(0.001);
		v.add(0.0001);
		return v;
	}
	
	/**
	 * Checks par_in, if is correct return true, else false
	 */
	private boolean checkParIn(Vector<Double> par_in){
		if(par_in==null)
			return false;
		for(int i=0;i<par_in.size();i++){
			if(par_in.get(i)<0.0)
				return false;
		}
		return true;	
	}
	
	/**
	 * Sets the par_in values correctly
	 * @param par_in is a vector of double parameters
	 * @throws ErrorExc exception if par_in value isn't set correctly
	 */
	public void setPar_In(Vector<Double> par_in) throws ErrorExc{
		this.par_in=par_in;   //setta sempre il par_in
		if(checkParIn(par_in)==false)
			throw new ErrorExc("EModPar_inParametersNotValid"); //se non è corretto lancia un'eccezione
	}
	
	/**
	 * Sets the par_in value correctly
	 * @param ds is an array of double parameters
	 * @throws ErrorExc exception if par_in value isn't set correctly 
	 */
	public void setPar_In(double[][] ds) throws ErrorExc{
		Vector<Double> v=new Vector<Double>();
		for(int i=0;i<ds.length;i++)
			v.add(ds[i][0]);
		
		par_in=v;
		if(checkParIn(v)==false)
			throw new ErrorExc("EModPar_inParametersNotValid");
	}
	
	/**
	 * Sets the typeOfAnalysis value correctly
	 * @param b is a boolean parameter that indicates the type of analysis for the algorithm
	 */
	public void setTypeOfAnalysis(int b){
		if(b==0)
			typeOfAnalysis=false;
		else
			typeOfAnalysis=true;
	}
	
	public void setTypeOfAnalysis(boolean b){
			typeOfAnalysis=b;
	}
	
	
	/**
	 * Gets the par_in values
	 * @return a vector of double parameters containing the parameters values
	 */
	public Vector<Double> getPar_In(){
		if(checkParIn(par_in)==true)
			return par_in;
		else
			return getDefaultParIn();
	}
	
	/**
	 * Gets the typeOfAnalysis value
	 * @return False if typeOfAnalysis=Fit, True if typeOfAnalysis=Solve
	 */
	public boolean getTypeOfAnalysis(){
			return typeOfAnalysis;
	}

	/**
	 * Return true if all Attributes are correct 
	 */
	@Override
	public boolean check() {
		return checkParIn(par_in);
	}
}