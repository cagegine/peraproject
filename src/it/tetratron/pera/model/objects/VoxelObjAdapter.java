/**
//////////////////////////////////
VoxelObjAdapter. java
This is the class for handling the Voxel objects 
used by the system in the memory 
it.tetratron.pera.model.objects
GPLv3
Zambon Jacopo
TetraTron Group
2012.03.01
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.nifti.*;

/**
 * This is the class for handling the Voxel objects used by the system in the memory 
 * @author Zambon Jacopo
 *
 */
public class VoxelObjAdapter implements VoxelObj{
	
	private Nifti1Dataset adaptee=null;
	private Nifti1Dataset adaptee_mask=null;
	private static String directory;
	private File cache_file=null;
	private File cache_file_mask=null;
	
	static{
		String temp=System.getProperty("java.io.tmpdir");
		if(!(temp.endsWith("/") || temp.endsWith("\\")))
			temp+=System.getProperty("file.separator");
		directory=temp;
	}
	
	/**
	 * Nested class implementing the memento
	 * @author Zambon Jacopo
	 *
	 */
	class VoxelMem implements Memento{
		VoxelObj state;
		
		/**
		 * creates the Voxel Memento
		 * @throws ErrorExc 
		 */
		public VoxelMem() throws ErrorExc{
			state= new VoxelObjAdapter();
			if(existsMask()){
				  ((VoxelObjAdapter) state).copyInMaskHeader((VoxelObj)(VoxelObjAdapter.this));
				  state.setMask(VoxelObjAdapter.this.getDynMask());
				}
			state.copyHeader((VoxelObj)(VoxelObjAdapter.this));
			//check if the dynPet is a 3D or a 4D volume
			if(state.getAdaptee().TDIM==0)
				state.setDynPet(VoxelObjAdapter.this.getDynPet((short)0));
			else
				state.setDynPet(VoxelObjAdapter.this.getDynPet());
		}

		@Override
		public ObjInterface restoreState() {
			adaptee= state.getAdaptee();
			adaptee_mask= state.getAdapteeMask();
			return VoxelObjAdapter.this;
		}
	}
	
	/**
	 * Default Constructor 
	 */
	public VoxelObjAdapter(){
		adaptee=new Nifti1Dataset();
		adaptee_mask=new Nifti1Dataset();
	}
	
	/**
	 * One parameter Constructor
	 * @param path the path where the image is in the file system
	 * @throws ErrorExc
	 */
	public VoxelObjAdapter(String path) throws ErrorExc{
		this();
		this.load(path);
	}
	
	/**
	 * Two parameters Constructor
	 * @param dynpet_path the path where the image is in the file system
	 * @param mask_path the path where the mask is in the file system
	 * @throws ErrorExc
	 */
	public VoxelObjAdapter(String dynpet_path, String mask_path) throws ErrorExc{	
		this(dynpet_path);
		this.loadMask(mask_path);
	}
	
	/**
	 * Check consistency of image and mask image: X, Y, Z dim of mask must be equal to X, Y, Z dim of image 
	 * @throws ErrorExc
	 */
	private void checkConsistence() throws ErrorExc{
		if(adaptee.existsHdr())
				if(existsMask()){
					int one=0;
					if(adaptee_mask.ZDIM==0){
						one=1;
					}
						//adaptee_mask.ZDIM=1;
					if(adaptee_mask.XDIM!=adaptee.XDIM) throw new ErrorExc("EModMaskDimXNotCorrect");
					if(adaptee_mask.YDIM!=adaptee.YDIM) throw new ErrorExc("EModMaskDimYNotCorrect");
					if(adaptee_mask.ZDIM+one!=adaptee.ZDIM) throw new ErrorExc("EModMaskDimZNotCorrect");
		}
	}
	
	@Override
	public void load(String path) throws ErrorExc{
		adaptee.setHeaderFilename(path);
		adaptee.setDataFilename(path);
		try{
			adaptee.readHeader();
		}
		catch(FileNotFoundException fnf){
			throw new ErrorExc("EModFileDynpetNotFound");
		}
		catch(IOException io){
			throw new ErrorExc("EModGeneralIOError");
		}
	}
	
	@Override
	public void load(String dynpet_path, String mask_path) throws ErrorExc{
		try{
			this.load(dynpet_path);
			this.loadMask(mask_path);
		}
		catch(ErrorExc e){
			throw e;
		}
	}
	
	@Override
	public void loadMask(String path) throws ErrorExc{
		if(adaptee.existsDat()){
			adaptee_mask.setHeaderFilename(path);
			adaptee_mask.setDataFilename(path);
			try{
				adaptee_mask.readHeader();
			}
			catch(FileNotFoundException fnf){
				throw new ErrorExc("EModMaskNotFound");
			}
			catch(IOException io){
				throw new ErrorExc("EModGeneralIOError");
			}
			this.checkConsistence();
		}
		else{
			throw new ErrorExc("EModFirstLoadMask");
		}
	}
	
	@Override
	public void save(String path) throws ErrorExc{
		//if the image is in a .nii file ---> save in a .nii file
		if(adaptee.ds_is_nii)
			if(!path.endsWith(".nii"))
				path+=".nii";
		
		double[][][][] aux=null;
		try{
			aux= adaptee.readDoubleVol();
			adaptee.setHeaderFilename(path);
			adaptee.writeHeader();
			adaptee.setDataFilename(path);
			adaptee.writeVol(aux);
		}catch(FileNotFoundException fnf){
			throw new ErrorExc("EModFileDynpetNotFound");
		}
		catch(IOException io){
			throw new ErrorExc("EModGeneralIOError");
		}
	}
	
	/**
	 * Save the new mask in the file system
	 * @param mask_path the path where the new mask has to be saved
	 * @throws ErrorExc
	 */
	public void saveMask(String mask_path) throws ErrorExc{
		double[][][] aux=null;
		adaptee_mask.setHeaderFilename(mask_path);
		try{
			adaptee_mask.writeHeader();
			aux=adaptee_mask.readDoubleVol((short)0);
			adaptee_mask.setDataFilename(mask_path);
			adaptee_mask.writeVol(aux,(short)0);			
		}
		catch(FileNotFoundException fnf){
			throw new ErrorExc("EModSaveMaskError");
		}
		catch(IOException io){
			throw new ErrorExc("EModGeneralIOError");
		}
	}
	
	@Override
	public void save(String dynpet_path, String mask_path) throws ErrorExc{
		this.save(dynpet_path);
		this.saveMask(mask_path);
	}
	
	@Override
	public String getHeaderFilename(){
		return adaptee.getHeaderFilename();
	}
	
	@Override
	public String getHeaderMaskFilename(){
		return adaptee_mask.getHeaderFilename();
	}
	
	@Override
	public void copyHeader(VoxelObj vox) throws ErrorExc{
		adaptee.setHeaderFilename(vox.getHeaderFilename());
		try{
			adaptee.readHeader();
		}
		catch(IOException io){
			throw new ErrorExc("EModFileDynpetNotFound");
		}
	}
	
	@Override
	public void copyMaskHeader(VoxelObj vox) throws ErrorExc{
		adaptee.setHeaderFilename(vox.getHeaderMaskFilename());
		try{
			adaptee.readHeader();
		}
		catch(IOException io){
			throw new ErrorExc("EModFileDynpetNotFound");
		}
	}
	
	@Override
	public void copyInMaskHeader(VoxelObj vox)throws ErrorExc{
		adaptee_mask.setHeaderFilename(vox.getHeaderMaskFilename());
		try{
			adaptee_mask.readHeader();
		}
		catch(IOException io){
			throw new ErrorExc("EModFileDynmaskNotFound");
		}
	}
	
	@Override
	public double[][][] getDynPet(short time) throws ErrorExc{
		double[][][] aux=null;
		try{
			aux=adaptee.readDoubleVol(time);
		}
		catch(IOException io){throw new ErrorExc("EModErrorReadingDynpet");}
		return aux;
	}
	
	@Override
	public double[][][][] getDynPet() throws ErrorExc{
		double[][][][] aux=null;
		try{
			aux=adaptee.readDoubleVol();
		}
		catch(IOException io){throw new ErrorExc("EModErrorReadingDynpet");}
		catch(Error x){throw new ErrorExc("EModOutOfMemory");}
		return aux;
	}
	
	@Override
	public double[][][] getDynMask() throws ErrorExc{
		double[][][] aux=null;
		try{
			aux=adaptee_mask.readDoubleVol((short)0);
		}
		catch(IOException io){throw new ErrorExc("EModErrorReadingMask");}
		return aux;
	}

	@Override
	public void setDynPet(double[][][][] dynpet) throws ErrorExc{
		try {
			cache_file=File.createTempFile(Integer.toString(this.hashCode()),".img", new File(directory));
		} catch (IOException e) {
			throw new ErrorExc("EModDynpetError");
		}
		cache_file.deleteOnExit();
		adaptee.XDIM=(short)dynpet.length;
		adaptee.YDIM=(short)dynpet[0].length;
		adaptee.ZDIM=(short)dynpet[0][0].length;
		adaptee.TDIM=(short)dynpet[0][0][0].length;
		adaptee.setDataFilename(cache_file.getPath());
		try{
			adaptee.writeVol(dynpet);
		}
		catch(IOException io){throw new ErrorExc("EModErrorWritingDynpet");}
	}

	@Override
	public void setDynPet(double[][][] dynpet) throws ErrorExc{
		try {
			cache_file=File.createTempFile(Integer.toString(this.hashCode()),".img", new File(directory));
		} catch (IOException e) {
			throw new ErrorExc("EModDynpetError");
		}
		cache_file.deleteOnExit();
		adaptee.XDIM=(short)dynpet.length;
		adaptee.YDIM=(short)dynpet[0].length;
		adaptee.ZDIM=(short)dynpet[0][0].length;
		adaptee.TDIM=(short)0;
		adaptee.setDataFilename(cache_file.getPath());
		try{
			adaptee.writeVol(dynpet,(short)0);
		}
		catch(IOException io){throw new ErrorExc("EModErrorWritingDynpet");}
	}
	
	@Override
	public void setDynPet(double[][] dynpet) throws ErrorExc{
		try {
			cache_file=File.createTempFile(Integer.toString(this.hashCode()),".img", new File(directory));
		} catch (IOException e) {
			throw new ErrorExc("EModDynpetError");
		}
		cache_file.deleteOnExit();
		adaptee.XDIM=(short)dynpet.length;
		adaptee.YDIM=(short)dynpet[0].length;
		//adaptee.ZDIM=(short)1;
		//adaptee.TDIM=(short)1;
		adaptee.setDataFilename(cache_file.getPath());
		double[][][] dynpetthree= new double[dynpet.length][dynpet[0].length][1];
		for(int i=0;i<dynpet.length;i++)
			for(int j=0; j<dynpet[0].length;j++)
				dynpetthree[i][j][0]= dynpet[i][j];
		try{
			adaptee.writeVol(dynpetthree,(short)0);
		}
		catch(IOException io){throw new ErrorExc(/*"EModErrorWritingDynpet_2D"*/io.getMessage());}
	}
	
	@Override
	public void setMask(double[][][] mask) throws ErrorExc{
		try {
			cache_file_mask=File.createTempFile("mask_" + this.hashCode(),".img", new File(directory));
		} catch (IOException e) {
			throw new ErrorExc("EModDynpetError");
		}
		cache_file_mask.deleteOnExit();
		adaptee_mask.XDIM=(short)mask.length;
		adaptee_mask.YDIM=(short)mask[0].length;
		adaptee_mask.ZDIM=(short)mask[0][0].length;
		adaptee_mask.TDIM=(short)0;
		adaptee_mask.setDataFilename(cache_file_mask.getPath());
		try{
			adaptee_mask.writeVol(mask,(short)0);
		}
		catch(IOException io){throw new ErrorExc("EModErrorWritingDynmask");}
	}
	
	@Override
	public boolean existsMask(){
		if(adaptee_mask.existsHdr())
		  return true;
		else return false;
	}
	
	@Override
	public double getValue(int x, int y, int z, short t) throws ErrorExc{
		double[][][] d=null;
		try{
			d=adaptee.readDoubleVol(t);
		}
		catch(IOException io){throw new ErrorExc("EModErrorReadingDynpet");}
		return d[x][y][z];
	}

	@Override
	public double[] getTmcrs(int x, int y, int z) throws ErrorExc{
		double[] aux=null;
		try {
			aux=adaptee.readDoubleTmcrs((short)x, (short)y, (short)z);
		} catch (IOException e) {
			throw new ErrorExc("EModErrorReadingDynpet");
		}
		return aux;
		
	}
	
	/**
	 * Override of the finalized() method used by the garbage collector
	 */
	public void finalized(){
		if(cache_file!=null)
			cache_file.delete();
		if(cache_file_mask!=null)
			cache_file_mask.delete();
	}
	
	@Override
	public Nifti1Dataset getAdaptee() {
		return adaptee;
	}

	@Override
	public Nifti1Dataset getAdapteeMask() {
		return adaptee_mask;
	}
	
	@Override
	public int getX(){
		//adaptee.readHeader();
		return (int)adaptee.XDIM;
	}
	
	@Override
	public int getY(){
		//adaptee.readHeader();
		return (int)adaptee.YDIM;
	}
	
	@Override
	public int getZ(){
		//adaptee.readHeader();
		return (int)adaptee.ZDIM;
	}
	
	@Override
	public int getT(){
		//adaptee.readHeader();
		return (int)adaptee.TDIM;
	}
	
	/**
	 * Gets the dimensions of the voxel width along the three dimensions
	 * @return A 3-element array with the voxel dimensions along x,y,z
	 */
	public double[] getVoxDims(){
		double[] voxDims= new double[3];
		voxDims[0]= adaptee.pixdim[1];	//voxel width along dimension x
		voxDims[1]= adaptee.pixdim[2];	//voxel width along dimension y
		voxDims[2]= adaptee.pixdim[3];	//voxel width along dimension z
		return voxDims;
	}
	
	/**
	 * Calls the creation of the memento 
	 * @return The memento, TS=Memento, TD= VoxelMem
	 * @throws ErrorExc 
	 */
    public Memento createMemento() throws ErrorExc{
    	return new VoxelMem();
    }
}
