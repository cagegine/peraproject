/**
//////////////////////////////////
ArterialObj. java
This is the class for handling the objects at Arterial Level
it.tetratron.pera.model.objects
GPLv3
Zambon Jacopo
TetraTron Group
2012.03.01
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;
import java.util.*;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.TxtManager;
import it.tetratron.pera.model.objects.SlashVector;

/**
 * This is the class for handling the objects at Arterial Level
 * @author Zambon Jacopo
 *
 */
public class ArterialObj implements ObjInterface, Cloneable {
	private List<Double> time_plasma;
	private List<Double> plasma;
	private List<Double> blood;
	private String[] labels;
	private String path;
	
	/**
	 * Nested class implementing the memento
	 * @author Zambon Jacopo
	 *
	 */
	class ArterialMem implements Memento{
		private List<Double> time_plasma_mem;
		private List<Double> plasma_mem;
		private List<Double> blood_mem;
		private String[] labels_mem;
		private String path_mem;
		
		/**
		 * Create the Region Memento
		 */
		public ArterialMem() {
			time_plasma_mem= time_plasma;
			plasma_mem= plasma;
			blood_mem=blood;
			labels_mem= labels;
			path_mem=path;
		}

		@Override
		public ObjInterface restoreState() {
			time_plasma= time_plasma_mem;
	        plasma= plasma_mem;
	        blood= blood_mem;
	        labels= labels_mem;
	        path= path_mem;
	        return ArterialObj.this;
		}
	}

	public ArterialObj(){
		setDefaultLabels();
	}

	/**
	 * Load the txt file and split it in the memory Object
	 */
	public void load(String _path) throws ErrorExc {
		path= _path;
		TxtManager art_file= new TxtManager(path);
		String[][] splitted= art_file.readFile();
		//create the vectors for the files input
		time_plasma= new SlashVector();
		plasma= new SlashVector();
		blood= new SlashVector();
		//fill the vectors
		if(splitted==null) throw new ErrorExc("EModArterialFileError");
		int length= splitted.length;
		//the time_plasma vector must have increasing values
		//tp_min is the previous value for each iteration
		String[] data= splitted[0];
		//tot number of columns
		int tot_cols= data.length;
		//input file at arterial level must have 
		//3 columns (time_plasma+plasma+blood)
		if(tot_cols!=3) throw new ErrorExc("EModArterialFileError");
		//seek for labels
		try{
			labels= art_file.getLabels();
		}
		catch(ErrorExc e){
			setDefaultLabels();
		}
		Double tp_min= Double.parseDouble(data[0]);
		time_plasma.add(tp_min);
		plasma.add(Double.parseDouble(data[1]));
		blood.add(Double.parseDouble(data[2]));
		for(int i=1; i<length; i++){
			data= splitted[i];
			//a line can be empty
			//usually: the last line can be empty
			if(data[0]!=null){
				Double tp= Double.parseDouble(data[0]);
				if(tp<=tp_min)
					throw new ErrorExc("EModTimePlasmaError");
				else{
					time_plasma.add(tp);
					tp_min=tp;
				}
				plasma.add(Double.parseDouble(data[1]));
				blood.add(Double.parseDouble(data[2]));
			}
		}	
	}
	
	/**
	 * Sets the new labels as changed by the user through the GUI
	 * @param new_labels the new array of labels
	 */
	public void setLabels(String[] new_labels){
		labels= new_labels;
	}
	
	/**
	 * Sets the default labels for the Arterial file
	 */
	private void setDefaultLabels() {
		labels= new String[3];
		labels[0]= "time_plasma";
		labels[1]= "plasma";
		labels[2]= "blood";
	}
	
	/**
	 * Gets the String array of all the labels
	 * @return the array of labels for the Arterial object
	 */
	public String[] getLabels(){
		return labels;
	}
	
	/**
	 * Gets the String label for the Time vector
	 * @return the String label of Time vector
	 */
	public String getTimeLabel(){
		return labels[0];
	}
	
	/**
	 * Gets the String label for the Plasma vector
	 * @return the String label of Plasma vector
	 */
	public String getPlasmaLabel(){
		return labels[1];
	}
	
	/**
	 * Gets the String label for the Blood vector
	 * @return the String label of Blood vector
	 */
	public String getBloodLabel(){
		return labels[2];
	}
	
	/**
	 * Gets the name of the arterial file on the file system
	 * @return a string with the name of the file
	 * @throws ErrorExc 
	 */
	public String getFilename() throws ErrorExc{
		if(path==null)
			throw new ErrorExc("EModFilenameNotSet");
		return path;
	}
	
	@Override
	public void save(String path) throws ErrorExc {
		String[][] content= pack();
		TxtManager art_file= new TxtManager(path);
		art_file.setLabels(labels);
		art_file.saveFile(content);
	}
	
	/**
	 * Packs the vectors in a double array of String to save them in an output file
	 * @return The new double array of String
	 */
	public String[][] pack(){
		//first dimension= a vector size (vectors size are the same)
		int lines= time_plasma.size();
		String[][] toSend= new String[lines][3];
		//in the line "lin" put in the first place the time_plasma "lin" value
		//						in the second place the plasma "lin" value
		//						in the third place the blood "lin" value
		for(int lin=0; lin<lines; lin++){
			toSend[lin][0]= time_plasma.get(lin).toString();
			toSend[lin][1]= plasma.get(lin).toString();
			toSend[lin][2]= blood.get(lin).toString();
		}
		return toSend;
	}
	
	/**
	 * @return The time_plasma vector
	 * @throws ErrorExc 
	 */
	public List<Double> getTime_plasma() throws ErrorExc{
		if(time_plasma==null)
			throw new ErrorExc("EModNoTimePlasmaData");
		return time_plasma;
	}
	
	/**
	 * @return The plasma vector
	 * @throws ErrorExc 
	 */
	public List<Double> getPlasma() throws ErrorExc{
		if(plasma==null)
			throw new ErrorExc("EModNoPlasmaData");
		return plasma;
	}
	
	/**
	 * @return The blood vector
	 * @throws ErrorExc 
	 */
	public List<Double> getBlood() throws ErrorExc{
		if(blood==null)
			throw new ErrorExc("EModNoBloodData");
		return blood;
	}
	
	/**
	 * Sets the new vector of time_plasma from a vector
	 * @param newtp the new time_plasma vector
	 */
	public void setTime_plasma(Vector<Double> newtp){
		time_plasma= new SlashVector(newtp);
	}
	
	/**
	 * Sets the new vector of plasma from a vector
	 * @param newp the new plasma vector
	 */
	public void setPlasma(Vector<Double> newp){
		plasma= new SlashVector(newp);
	}
	
	/**
	 * Sets the new vector of blood from a vector
	 * @param newb the new blood vector
	 */
	public void setBlood(Vector<Double> newb){
    	blood= new SlashVector(newb);
    }
	
	/**
	 * Sets the new vector of time plasma from an array
	 * @param doubles the new blood array
	 */
	public void setTime_plasma(double[][] doubles) {
		time_plasma= new SlashVector();
		for(int i=0; i<doubles.length;i++)
			time_plasma.add(doubles[i][0]);
	}

	/**
	 * Sets the new vector of plasma from an array
	 * @param doubles the new plasma array
	 */
	public void setPlasma(double[][] doubles) {
		plasma= new SlashVector();
		for(int i=0; i<doubles.length;i++)
			plasma.add(doubles[i][0]);
	}

	/**
	 * Sets the new vector of blood from an array
	 * @param doubles the new blood array
	 */
	public void setBlood(double[][] doubles) {
		blood= new SlashVector();
		for(int i=0; i<doubles.length;i++)
			blood.add(doubles[i][0]);
	}
    
	/**
	 * Calls the creation of the memento 
	 * @return The memento, TS=Memento, TD= ArterialMem
	 */
    public Memento createMemento(){
    	return new ArterialMem();
    }

}
