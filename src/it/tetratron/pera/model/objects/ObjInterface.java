/**
//////////////////////////////////
ObjInterface. java
This is the common interface for all the types of Object handled by the system
it.tetratron.pera.model.objects
GPLv3
Zambon Jacopo
TetraTron Group
2012.02.13
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;

import it.tetratron.pera.model.manage.ErrorExc;

public interface ObjInterface {
	/**
	 * Loads the file in the memory
	 * @param path represents the path where the file is in the file system
	 * @throws Exception 
	 */
	public void load(String path) throws ErrorExc;
	
	/**
	 * Saves the object data into a file in the file system
	 * @param path represents the path where the file has to be saved in the file system
	 * @throws Exception 
	 */
	public void save(String path) throws ErrorExc;
	
}
