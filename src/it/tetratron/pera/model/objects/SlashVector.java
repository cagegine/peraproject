/**
//////////////////////////////////
SlashVector. java
Modified Vector to handle input/output for Matlab functions
it.tetratron.pera.model.objects
GPLv3
Ferlin Alessandro, Zambon Jacopo
TetraTron Group
2012.03.01
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;

import java.util.Collection;
import java.util.Vector;

/**
 * Modified Vector to handle input/output for Matlab functions
 * @authors Ferlin Alessandro, Zambon Jacopo
 *
 */
public class SlashVector extends Vector<Double> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//******************************************************************
	/**
	 * Constructors derived by Vector
	 */
	
	
	public SlashVector() {
		super();
	}

	public SlashVector(int arg0) {
		super(arg0);
	}
	
	public SlashVector(Vector<Double> v){
		super(v);
	}

	
	@SuppressWarnings("unchecked")
	public SlashVector(Collection arg0) {
		super(arg0);
	}

	public SlashVector(int arg0, int arg1) {
		super(arg0, arg1);
	}
	//********************************************************************
	
	/**
	 * Matlab functions require a "vertical" array as an input
	 * @return a nx1 Double array 
	 */
	public Double[][] rotate(){
		int rows= this.size();
		Double[][] vertical= new Double[rows][1];
		for(int i=0; i<rows; i++)
			vertical[i][0]= (Double) this.get(i);
		return vertical;
	}
	
	/**
	 * Get a SlashVector from a bidimensional array (nx1 or 1xn), as the result of a Matlab function
	 * @param v the bidimensional double array returned by the Matlab function
	 * @return A SlashVector with the elements of the bidimensional array
	 */
	public static SlashVector toHorizontal(double[][] v){
		SlashVector toReturn= new SlashVector();
		//v is a "1xn" matrix
		if(v.length==1)
			for(int i=0; i<v[0].length;i++)
				toReturn.add(v[0][i]);
		//v is a "nx1" matrix
		else
			for(int i=0; i<v.length;i++)
				toReturn.add(v[i][0]);
		return toReturn;
	}
	
	
	/*
	 * Get a double array from a SlashVector object
	 * @return The new double array
	 */
	/*
	public double[] todoubleArray(){
		int length= size();
		double[] toReturn= new double[length];
		for(int i=0;i<length;i++)
			toReturn[i]=get(i);
		return toReturn;
	}*/
}
