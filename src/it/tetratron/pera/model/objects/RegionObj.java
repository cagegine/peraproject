/**
//////////////////////////////////
RegionObj. java
This is the class for handling the objects at Region Level
it.tetratron.pera.model.objects
GPLv3
Zambon Jacopo
TetraTron Group
2012.03.01
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;
import java.util.*;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.TxtManager;

/**
 * This is the class for handling the objects at Region Level
 * @author Zambon Jacopo
 *
 */
public class RegionObj implements ObjInterface, Cloneable{
	private List<Double> time_pet;
	private List<Double> delta;
	private List<Double> nsd;
	private Double[][] roi;
	private String[] labels;
	//columns in the roi matrix = first dimension
	private int roi_cols;
	private String path;
	
	/**
	 * Nested class implementing the memento
	 * @author Zambon Jacopo
	 *
	 */
	class RegionMem implements Memento{
		private List<Double> time_pet_mem;
		private List<Double> delta_mem;
		private List<Double> nsd_mem;
		private Double[][] roi_mem;
		private String[] labels_mem;
		//columns in the roi matrix = first dimension
		private int roi_cols_mem;
		private String path_mem;
		
		/**
		 * Create the Region Memento
		 */
		public RegionMem() {
			time_pet_mem= time_pet;
			delta_mem= delta;
			nsd_mem=nsd;
			roi_mem=roi;
			labels_mem= labels;
			roi_cols_mem=roi_cols;
			path_mem=path;
		}

		@Override
		public ObjInterface restoreState() {
			time_pet= time_pet_mem;
	        delta= delta_mem;
	        nsd= nsd_mem;
	        roi= roi_mem;
	        labels= labels_mem;
	        roi_cols=roi_cols_mem;
	        path= path_mem;
	        return RegionObj.this;
		}
	}
	
	public RegionObj(){
		labels= new String[3];
		labels[0]= "time_pet";
		labels[1]= "delta";
		labels[2]= "nsd";
	}
	
	/**
	 * Loads the txt file and split it in the memory Object
	 */
	public void load(String _path) throws ErrorExc {
		path= _path;
		TxtManager reg_file= new TxtManager(path);
		String[][] splitted= reg_file.readFile();
		if(splitted==null)
			throw new ErrorExc("EModRegionFileError");
		//create the vectors for the files input
		time_pet= new SlashVector();
		delta= new SlashVector();
		nsd= new SlashVector();
		//fill the vectors
		int length= splitted.length;
		//the time_plasma vector must have increasing values
		//tp_min is the previous value for each iteration
		String[] data= splitted[0];
		//tot number of columns
		int tot_cols= data.length;
		//input file at region level must have 
		//at least 4 columns (time_pet+delta+nsd+(at least)1 region)
		if(tot_cols<4) throw new ErrorExc("EModRegionFileError");
		//number of columns in ROI
		roi_cols= tot_cols-3;
		//seek for labels
		try{
			labels= reg_file.getLabels();
		}
		catch(ErrorExc e){
			labels= new String[tot_cols];
			labels[0]= "time_pet";
			labels[1]= "delta";
			labels[2]= "nsd";
			for(int i=0; i<roi_cols;i++)
				labels[3+i]= "ROI_"+i;
		}
		roi= new Double[length][roi_cols];
		Double tp_min= Double.parseDouble(data[0]);
		time_pet.add(tp_min);
		delta.add(Double.parseDouble(data[1]));
		nsd.add(Double.parseDouble(data[2]));
		//roi[0][row] is the "row" element of the first line
		for(int row=0;row<roi_cols;row++)
			roi[0][row]= Double.parseDouble(data[3+row]);
		//split the other lines
		for(int i=1; i<length; i++){
			data= splitted[i];
			//a line can be empty
			//usually: the last line can be empty
			if(data[0]!=null){
				Double tp= Double.parseDouble(data[0]);
				if(tp<=tp_min)
					throw new ErrorExc("EModTimePetError");
				else{
					time_pet.add(tp);
					tp_min=tp;
				}
				delta.add(Double.parseDouble(data[1]));
				nsd.add(Double.parseDouble(data[2]));
				for(int row=0;row<roi_cols;row++)
					//roi[i][row] is the "row" element of the "i" line
					roi[i][row]= Double.parseDouble(data[3+row]);
			}
		}
	}
	
	/**
	 * Gets the String array of all the labels
	 * @return the array of labels for the Region object
	 */
	public String[] getLabels(){
		return labels;
	}
	
	/**
	 * Sets the new labels as changed by the user through the GUI
	 * @param new_labels the new array of labels
	 */
	public void setLabels(String[] new_labels){
		labels= new_labels;
	}
	
	/**
	 * Gets the String label for the Time vector
	 * @return the String label of Time vector
	 */
	public String getTimeLabel(){
		return labels[0];
	}
	
	/**
	 * Gets the String label for the Delta vector
	 * @return the String label of Delta vector
	 */
	public String getDeltaLabel(){
		return labels[1];
	}
	
	/**
	 * Gets the String label for the Nsd vector
	 * @return the String label of Nsd vector
	 */
	public String getNSDLabel(){
		return labels[2];
	}
	
	/**
	 * Gets the String array of labels for the ROI matrix
	 * @return the String array labels of ROI matrix
	 * @throws ErrorExc 
	 */
	public String[] getRoiLabel() throws ErrorExc{
		if(labels.length==3)
			//ROI labels not setted
			throw new ErrorExc("EModNoRoiLabels");
		String[] aux=new String[labels.length-3];
		for(int i=0; i<labels.length-3; i++)
			aux[i]=labels[i+3];
		return aux;
	}
	
	
	@Override
	public void save(String path) throws ErrorExc {
		String[][] content= pack();
		TxtManager reg_file= new TxtManager(path);
		reg_file.setLabels(labels);
		reg_file.saveFile(content);
	}
	
	/**
	 * Gets the name of the arterial file on the file system
	 * @return a string with the name of the file
	 * @throws ErrorExc 
	 */
	public String getFilename() throws ErrorExc{
		if(path==null)
			throw new ErrorExc("EModFilenameNotSet");
		return path;
	}
	
	/**
	 * Packs the vectors and the roi matrix in a double array of String to save them in an output file
	 * @return the new double array of String
	 */
	public String[][] pack(){
		//first dimension= a vector size (vectors size are the same)
		int lines= time_pet.size();
		String[][] toSend= new String[lines][3+roi_cols];
		//in the line "lin" put in the first place the time_plasma "lin" value
		//						in the second place the plasma "lin" value
		//						in the third place the blood "lin" value
		for(int lin=0; lin<lines; lin++){
			toSend[lin][0]= time_pet.get(lin).toString();
			toSend[lin][1]= delta.get(lin).toString();
			toSend[lin][2]= nsd.get(lin).toString();
			//for each line of the roi (first matrix dimension) add to the new double array
			//the "col" element
			for(int col=0; col<roi_cols; col++)
				toSend[lin][3+col]= roi[lin][col].toString();
		}
		return toSend;
	}
	
	/**
	 * @return The time_pet vector
	 * @throws ErrorExc 
	 */
	public List<Double> getTime_pet() throws ErrorExc{
		if(time_pet==null)
			throw new ErrorExc("EModNoTimePetData");
		return time_pet;
	}
	
	/**
	 * @return The delta vector
	 * @throws ErrorExc 
	 */
	public List<Double> getDelta() throws ErrorExc{
		if(delta==null)
			throw new ErrorExc("EModNoDeltaPetData");
		return delta;
	}
	
	/**
	 * @return The nsd vector
	 * @throws ErrorExc 
	 */
	public List<Double> getNsd() throws ErrorExc{
		if(nsd==null)
			throw new ErrorExc("EModNoNsdData");
		return nsd;
	}
	
	/**
	 * @return The Roi matrix as a vector of vectors
	 * @throws ErrorExc 
	 */
	public List<List<Double>> getRoi() throws ErrorExc{
		List<List<Double>> roi_vector= new Vector<List<Double>>();
		for(int j=0; j<roi[0].length;j++){
			List<Double> vector= new Vector<Double>(); 
			for(int i=0;i<roi.length;i++)
				vector.add(roi[i][j]);
			roi_vector.add(vector);
		}
		if(roi_vector.size()==0)
			throw new ErrorExc("EModNoRoiData");
		return roi_vector;
	}
	
	/**
	 * @return The Roi matrix as a bidimensional array
	 * @throws ErrorExc 
	 */
	public Double[][] getRoiArray() throws ErrorExc{
		if(roi==null)
			throw new ErrorExc("EModNoRoiData");
		return roi;
	}
	
	/**
	 * Sets the new vector of time_pet
	 * @param list the new time_pet vector
	 */
	public void setTime_pet(List<Double> list){
		time_pet= new SlashVector(list);
	}
	
	/**
	 * Sets the new vector of delta
	 * @param list the new delta vector
	 */
	public void setDelta(List<Double> list){
		delta= new SlashVector(list);
	}
	
	/**
	 * Sets the new vector of nsd
	 * @param list the new nsd vector
	 */
	public void setNsd(List<Double> list){
		nsd= new SlashVector(list);
	}
	
	/**
	 * Sets the new double array of roi
	 * @param newr the new roi double array
	 */
	public void setRoi(double[][] newr){
		Double[][] roinew= new Double[newr.length][newr[0].length];
		for(int i=0;i<newr.length;i++)
			for(int j=0; j<newr[0].length;j++)
				roinew[i][j]= newr[i][j];
		roi= roinew;
		roi_cols= newr[0].length;
	}
	
	/**
	 * Calls the creation of the memento 
	 * @return The memento, TS=Memento, TD= RegionMem
	 */
    public Memento createMemento(){
    	return new RegionMem();
    }

}
