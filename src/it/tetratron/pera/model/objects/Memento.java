/**
//////////////////////////////////
Memento. java
Interface for handling the memento
it.tetratron.pera.model.objects
GPLv3
Zambon Jacopo
TetraTron Group
2012.02.13
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;

public interface Memento {
	
	/**
	 * Restore the state from a previous elaboration
	 */
	public ObjInterface restoreState();
}
