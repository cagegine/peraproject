/**
//////////////////////////////////
VoxelObj. java
This is the interface for handling the Voxel objects 
used by the system from the input file
it.tetratron.pera.model.objects
GPLv3
Zambon Jacopo
TetraTron Group
2012.03.01
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;
import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.objects.nifti.Nifti1Dataset;

/**
 * This is the interface for handling the Voxel objects used by the system from the input file
 * @author Zambon Jacopo
 *
 */
public interface VoxelObj extends ObjInterface{
	
	/**
	 * Loads the image file and the mask in the memory
	 * @param dynpet_path represents the path where the image file is in the file system
	 * @param mask_path represents the path where the mask is in the file syste,
	 * @throws ErrorExc 
	 */
	public void load(String dynpet_path, String mask_path) throws ErrorExc;
	
	/**
	 * Loads the mask in the memory
	 * @param mask_path represents the path where the file is in the file system
	 * @throws ErrorExc 
	 */
	public void loadMask(String mask_path) throws ErrorExc;
	
	/**
	 * Saves the image file and the mask in the memory
	 * @param dynpet_path represents the path where the image file has to be saved in the file system
	 * @param mask_path represents the path where the mask has to be saved in the file system
	 * @throws ErrorExc 
	 */
	public void save(String dynpet_path, String mask_path) throws ErrorExc;
	
	/**
	 * Reads one 3D volume from disk and return it as 3D double array
	 * 
	 * @param time
	 *            T dimension of volume to read
	 * @return 3D double array, scale and offset have been applied. Array
	 *         indices are [X][Y][Z].
	 * @throws ErrorExc 
	 */
	public double[][][] getDynPet(short time) throws ErrorExc;
	
	/**
	 * Reads all image from disk and return it as 4D double array, it requires a lot of ram if the image is big
	 * 
	 * @return 4D double array, scale and offset have been applied. Array
	 *         indices are [X][Y][Z][T].
	 * @throws ErrorExc 
	 */
	public double[][][][] getDynPet() throws ErrorExc;
	
	/**
	 * Reads 3D volume of mask from disk and return it as 3D double array
	 * 
	 * @return 3D double array, scale and offset have been applied. Array
	 *         indices are [X][Y][Z].
	 * @throws ErrorExc 
	 */
	public double[][][] getDynMask() throws ErrorExc;
	
	/**
	 * Reads and returns the image value at x, y, z, t position. 
	 * @throws ErrorExc 
	 */
	public double getValue(int x, int y, int z, short t) throws ErrorExc;
	
	/**
	 * Gets the filename for the dataset header file
	 * @return String with the disk filename for the dataset header file
	 */
	public String getHeaderFilename();
	
	/**
	 * Reads one 1D timecourse from a 4D dataset, ie all T values for a given XYZ location. Scaling is applied.
	 * Parameters:
	 * x - X dimension of vol to read (0 based index)
	 * y - Y dimension of vol to read (0 based index)
	 * z - Z dimension of vol to read (0 based index)
	 * @return
	 * 1D double array
	 * @throws
	 * ErrorExc
	 */
	public double[] getTmcrs(int x, int y, int z) throws ErrorExc;
	
	/**
	 * Gets the filename for the dataset mask header file
	 * @return String with the disk filename for the dataset mask header file
	 */
	public String getHeaderMaskFilename();
	
	/**
	 * Reads and Copies the information of dataset header from another VoxelObj
	 * @param vox represents the VoxelObj to copy
	 * @throws ErrorExc 
	 */
	public void copyHeader(VoxelObj vox) throws ErrorExc;
	
	/**
	 * Reads and Copies the information of dataset header of the Mask in the image header
	 * @param vox represents the VoxelObj containing the mask to copy
	 * @throws ErrorExc 
	 */
	public void copyMaskHeader(VoxelObj vox) throws ErrorExc;
	
	/**
	 * Reads and Copies the information of dataset header of the Mask to the Mask header
	 * @param vox represents the VoxelObj contain mask to copy
	 * @throws ErrorExc 
	 */
	public void copyInMaskHeader(VoxelObj vox) throws ErrorExc;
	
	/**
	 * Sets the Dataset DynPet from a 2D image.
	 * @param dynpet double[][][], the 2D image to set.
	 * @throws ErrorExc 
	 */
	public void setDynPet(double[][] dynpet) throws ErrorExc;
	
	/**
	 * Sets the Dataset DynPet from a 3D image.
	 * @param dynpet double[][][], the 3D image to set.
	 * @throws ErrorExc 
	 */
	public void setDynPet(double[][][] dynpet) throws ErrorExc;
	
	/**
	 * Sets the Dataset DynPet from a 4D image.
	 * @param dynpet double[][][][], the 4D image to set.
	 * @throws ErrorExc 
	 */
	public void setDynPet(double[][][][] dynpet) throws ErrorExc;
	
	/**
	 * Sets the Dataset Mask.
	 * @param mask the 3D image mask to set.
	 * @throws ErrorExc 
	 */
	public void setMask(double[][][] mask) throws ErrorExc;
	
	/**
	* Checks if the mask is present
	* @return True if the mask is setted, else False
	*/
	public boolean existsMask();

	/**
	 * @return the adaptee image file
	 */
	Nifti1Dataset getAdaptee();

	/**
	 * @return the adaptee mask file
	 */
	Nifti1Dataset getAdapteeMask();
	
	/**
	 * Gets the X dimension of the image
	 */
	public int getX();
	
	/**
	 * Gets the Y dimension of the image
	 */
	public int getY();
	
	/**
	 * Gets the Z dimension of the image
	 */
	public int getZ();
	
	/**
	 * Gets the T dimension of the image
	 */
	public int getT();
	
	/**
	 * Calls the creation of the memento 
	 * @return The memento, TS=Memento, TD= VoxelMem
	 */
	public Memento createMemento() throws ErrorExc;
}
