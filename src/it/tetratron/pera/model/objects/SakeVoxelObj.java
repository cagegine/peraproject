/**
//////////////////////////////////
SakeVoxelObj. java
This is the class for handling the objects for the Sake files at Voxel Level
it.tetratron.pera.model.objects
GPLv3
Francesco Rosso
TetraTron Group
2012.03.01
//////////////////////////////////
 */

package it.tetratron.pera.model.objects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.SakeManager;
import it.tetratron.pera.model.params.RSSAParam;
import it.tetratron.pera.model.params.SAIFParam;
import it.tetratron.pera.model.params.SAParam;
import it.tetratron.pera.model.params.StdSAParam;

/**
 * This is the class for handling the objects for the Sake files at Voxel Level
 * @author Rosso Francesco
 *
 */
public class SakeVoxelObj extends Sake implements ObjInterface {

	public enum VoxelObjs {
		mapVt, mapKi, mapVb
	}
	private String filename = null;
	private SAParam params = null;
	private ArterialObj ref_art = null;

	private RegionObj ref_reg = null;
	private VoxelObj mapVt = null;
	private VoxelObj mapKi = null;
	private VoxelObj mapVb = null;
	private VoxelObj mapOrder = null;
	private VoxelObj fit = null;

	private VoxelObj rawVoxel = null;;

	private VoxelObjs current = VoxelObjs.mapVt;
	private String algorithm = null;

	/**
	 * Method to check data consistency before save and before load. it checks
	 * if:
	 * <ul>
	 * <li>fit, mapOrder, mapVt, mapKi, mapVb are set</li>
	 * <li>mapVt, mapKi, mapVb must have the same dimensions</li>
	 * </ul>
	 * If one or more than these conditions are not met, it throws an ErrorExc
	 * 
	 * @throws ErrorExc
	 */
	private void checkConsistency() throws ErrorExc {

		getFit();
		getMapOrder();
		getRawVoxel();

		// Dimensions must be exactly the same
		if (getMapVt().getX() != getMapKi().getX())
			throw new ErrorExc("EModDataMatrixNotConsistent");
		if (getMapVt().getX() != getMapVb().getX())
			throw new ErrorExc("EModDataMatrixNotConsistent");
		if (getMapKi().getX() != getMapVb().getX())
			throw new ErrorExc("EModDataMatrixNotConsistent");

		if (getMapVt().getY() != getMapKi().getY())
			throw new ErrorExc("EModDataMatrixNotConsistent");
		if (getMapVt().getY() != getMapVb().getY())
			throw new ErrorExc("EModDataMatrixNotConsistent");
		if (getMapKi().getY() != getMapVb().getY())
			throw new ErrorExc("EModDataMatrixNotConsistent");

		if (getMapVt().getZ() != getMapKi().getZ())
			throw new ErrorExc("EModDataMatrixNotConsistent");
		if (getMapVt().getZ() != getMapVb().getZ())
			throw new ErrorExc("EModDataMatrixNotConsistent");
		if (getMapKi().getZ() != getMapVb().getZ())
			throw new ErrorExc("EModDataMatrixNotConsistent");

		/*
		 * //inutile da checkare if(mapVt.getT() != mapKi.getT()) throw new
		 * ErrorExc("EModDataMatrixNotConsistent"); if(mapVt.getT() !=
		 * mapVb.getT()) throw new ErrorExc("EModDataMatrixNotConsistent");
		 * if(mapKi.getT() != mapVb.getT()) throw new
		 * ErrorExc("EModDataMatrixNotConsistent");
		 */
	}

	/**
	 * Method to save the arterial file in the given file
	 * 
	 * @param artFile The name of the txt file where to save the arterial file 
	 * @throws ErrorExc
	 */
	private void createArterialFile(File artFile) throws ErrorExc {
		ref_art.save(artFile.getPath());
	}

	/**
	 * This method will create a fit file based on the parameters utilized in
	 * the execution
	 * 
	 * @param filename The name of the txt file which will contain the fit matrix
	 * @throws ErrorExc
	 */
	private void createFitFile(File fitFile) throws ErrorExc {
		fit.save(fitFile.getPath());
	}

	/**
	 * This method will create a log file based on the parameters utilized in
	 * the algorithm execution
	 * 
	 * @param filename The name of the txt file which will contain the log data
	 * @throws ErrorExc
	 */
	private void createLogFile(File filename) throws ErrorExc {
		Properties propFile = new Properties();
		try {
			propFile.setProperty("RegionObj", getRegionObj().getFilename());
			propFile.setProperty("ArterialObj", getArterialObj().getFilename());
			propFile.setProperty("DynPet", rawVoxel.getHeaderFilename());
			propFile.setProperty("DynPetMask", rawVoxel.getHeaderMaskFilename());
			propFile.setProperty("ParWorkspace_Dir", getParamInterface()
					.getWorkspace_Dir());
			
			if (params instanceof SAIFParam) {
				propFile.setProperty("algorithm", "SAIF");
				algorithm = "SAIF";
				propFile.setProperty("ParPassband[0]", Double
						.toString((((SAIFParam) getParamInterface())
								.getPassband().get(0))));
				propFile.setProperty("ParPassband[1]", Double
						.toString((((SAIFParam) getParamInterface())
								.getPassband().get(1))));
			} else if (params instanceof RSSAParam) {
				propFile.setProperty("algorithm", "RSSA");
				algorithm = "RSSA";
				propFile.setProperty("ParSNR[0]", Double
						.toString((((RSSAParam) getParamInterface()).getSNR()
								.get(0))));
				propFile.setProperty("ParSNR[1]", Double
						.toString((((RSSAParam) getParamInterface()).getSNR()
								.get(1))));
			} else if (params instanceof StdSAParam) {
				propFile.setProperty("algorithm", "StdSA");
				algorithm = "StdSA";
				propFile.setProperty("ParTypeOfCorrection", Integer
						.toString(((StdSAParam) getParamInterface()).getType()));
			}
			propFile.setProperty("ParBeta1",
					Double.toString(getParamInterface().getBeta1()));
			propFile.setProperty("ParBeta2",
					Double.toString(getParamInterface().getBeta2()));
			propFile.setProperty("ParNComp",
					Integer.toString(getParamInterface().getNComp()));
			propFile.store(new FileOutputStream(filename.getPath()),
					null);
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToStoreTempFile", e.getMessage());
		}
	}
	
	public String getAlgorithm() throws ErrorExc {
		if (algorithm == null)
			throw new ErrorExc("EModAlgorithmNotSet");
		return algorithm;
	}


	/**
	 * Method to store the mapKi in the given file
	 * 
	 * @param mapKiFile the file where to save the mapKi image 
	 * @throws ErrorExc
	 */
	private void createMapKiFile(File mapKiFile) throws ErrorExc {
		mapKi.save(mapKiFile.getPath());
	}
	
	/**
	 * Method to store the mapOrder in the given file
	 * 
	 * @param mapOrderFile the file where to save the mapOrder image 
	 * @throws ErrorExc
	 */
	private void createMapOrderFile(File mapOrderFile) throws ErrorExc {
		mapOrder.save(mapOrderFile.getPath());
	}
	
	/**
	 * Method to store the mapVb in the given file
	 * 
	 * @param mapVbFile the file where to save the mapVb image 
	 * @throws ErrorExc
	 */
	private void createMapVbFile(File mapVbFile) throws ErrorExc {
		mapVb.save(mapVbFile.getPath());
	}

	/**
	 * Method to store the mapVt in the given file
	 * 
	 * @param mapVtFile the file where to save the mapVt image 
	 * @throws ErrorExc
	 */
	private void createMapVtFile(File mapVtFile) throws ErrorExc {
		mapVt.save(mapVtFile.getPath());
	}
	
	/**
	 * Method to store the rawVoxel in the given file
	 * @param rawVoxelFile the file where to save the rawVoxel image 
	 * @throws ErrorExc
	 */
	private void createRawVoxel(File rawVoxelFile) throws ErrorExc {
		rawVoxel.save(rawVoxelFile.getPath());
	}

	/**
	 * Method to save the regional file in the given file
	 * 
	 * @param regFile The name of the txt file where the regional file has to be saved
	 * @throws ErrorExc
	 */
	private void createRegionalFile(File regFile) throws ErrorExc {
		ref_reg.save(regFile.getPath());
	}

	/**
	 * @return the Arterial object. Throws an exception if it's not set
	 * @throws ErrorExc
	 */
	public ArterialObj getArterialObj() {
		return ref_art;
	}

	/**
	 * Method to return the ArterialObj path used in the algorithm's execution.
	 * @return the path to the file
	 * @throws ErrorExc if the sake filename is not set, if there is no logfile or if there is a general IOError
	 */
	public String getArterialPathFromLog() throws ErrorExc{
		Properties propFile = new Properties();
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		try {
			propFile.load(new FileInputStream(file));
		} catch (IOException e) {
			throw new ErrorExc("EModGeneralIOError", e.getMessage());
		}
		String path = propFile.getProperty("ArterialObj");
		file.delete();
		return path;
	}

	/**
	 * Method to return the algorithm and inputs used in the algorithm's execution.
	 * @return the array of string representing the algorithm and its input data
	 * @throws ErrorExc if the sake filename is not set, if there is no logfile or if there is a general IOError
	 */
	public String[] getInputs() throws ErrorExc{
		Properties propFile = new Properties();
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		try {
			propFile.load(new FileInputStream(file));
		} catch (IOException e) {
			throw new ErrorExc("EModGeneralIOError", e.getMessage());
		}
		String[] inputs= new String[5];
		String algo=propFile.getProperty("algorithm");
		if(algo.startsWith("SAIF"))
			algo="Spectral Analysis Iterative Filtering";
		if(algo.startsWith("RSSA"))
			algo="Rank Shaping Spectral Analysis";
		if(algo.startsWith("StdSA"))
			algo="Standard Spectral Analysis";
		inputs[0]= "Algorithm: "+algo;
		inputs[1]= "Number of components: "+propFile.getProperty("ParNComp");
		inputs[2]= "Beta1: "+propFile.getProperty("ParBeta1");
		inputs[3]= "Beta2: "+propFile.getProperty("ParBeta2");
		if(algo.startsWith("Spectral")){
			inputs[4]= "Passband: "+propFile.getProperty("ParPassband[0]")+", "+propFile.getProperty("ParPassband[1]");
		}
		if(algo.startsWith("Rank")){
			inputs[4]= "SNR: "+propFile.getProperty("ParSNR[0]")+", "+propFile.getProperty("ParSNR[1]");
		}
		if(algo.startsWith("Standard")){
			String tof= propFile.getProperty("ParTypeOfCorrection");
			if(tof.startsWith("1"))
				inputs[4]= "Type of Correction: Reversible";
			if(tof.startsWith("2"))
				inputs[4]= "Type of Correction: Irreversible";
			if(tof.startsWith("3"))
				inputs[4]= "Type of Correction: Unknown";
		}
		file.delete();
		return inputs;
	}

	/**
	 * Method to return the data matrix of the object set with setCurrentObj().
	 * 
	 * @return the pointer to the data matrix of the object set with
	 *         setCurrentObj().
	 * @throws ErrorExc
	 */
	public double[][][] getCurrentDynPet() throws ErrorExc {
		if (getCurrentObj() == VoxelObjs.mapKi)
			return mapKi.getDynPet((short) 0);
		if (getCurrentObj() == VoxelObjs.mapVb)
			return mapVb.getDynPet((short) 0);
		return mapVt.getDynPet((short) 0);
	}

	/**
	 * @return the image object as it is choosen by the user (mapVb, mapKi, mapVt)
	 */
	public VoxelObjs getCurrentObj() {
		return current;
	}

	/**
	 * @return the filename and path of the .sake file
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @return the fit image object
	 * @throws ErrorExc
	 *             if the object is not set
	 */
	public VoxelObj getFit() throws ErrorExc {
		if (fit == null)
			throw new ErrorExc("EModFitNotSet");
		return fit;
	}

	/**
	 * @return the mapKi image object
	 * @throws ErrorExc
	 *             if the object is not set
	 */
	public VoxelObj getMapKi() throws ErrorExc {
		if (mapKi == null)
			throw new ErrorExc("EModMapKiNotSet");
		return mapKi;
	}

	/**
	 * @return the mapOrder image object
	 * @throws ErrorExc
	 *             if the object is not set
	 */
	public VoxelObj getMapOrder() throws ErrorExc {
		if (mapOrder == null)
			throw new ErrorExc("EModMapOrderNotSet");

		return mapOrder;
	}

	/**
	 * @return the mapVb image object
	 * @throws ErrorExc
	 *             if the object is not set
	 */
	public VoxelObj getMapVb() throws ErrorExc {
		if (mapVb == null)
			throw new ErrorExc("EModMapVbNotSet");
		return mapVb;
	}

	/**
	 * 
	 * @return the mapVt image object
	 * @throws ErrorExc
	 *             if the object is not set
	 */
	public VoxelObj getMapVt() throws ErrorExc {
		if (mapVt == null)
			throw new ErrorExc("EModMapVtNotSet");
		return mapVt;
	}

	/**
	 * Gets the T dimension of the fit image
	 * 
	 * @return The T dimension
	 */
	public int getMaxT() {
		return fit.getT();
	}

	/**
	 * Gets the X dimension of the fit image
	 * 
	 * @return The X dimension
	 */
	public int getMaxX() {
		return fit.getX();
	}

	/**
	 * Gets the Y dimension of the fit image
	 * 
	 * @return The Y dimension
	 */
	public int getMaxY() {
		return fit.getY();
	}

	/**
	 * Gets the Z dimension of the fit image
	 * 
	 * @return The Z dimension
	 */
	public int getMaxZ() {
		return fit.getZ();
	}

	/**
	 * @return the algoritm's execution parameters
	 */
	public SAParam getParamInterface() {
		return params;
	}

	/**
	 * @return the rawVoxel image, if it's present
	 */
	public VoxelObj getRawVoxel() throws ErrorExc {
		if (rawVoxel == null)
			throw new ErrorExc("EModRawVoxelNotSet");
		return rawVoxel;
	}

	/**
	 * Method to return the DynMask path used in the algorithm's execution.
	 * @return the path to the file
	 * @throws ErrorExc if the sake filename is not set, if there is no logfile or if there is a general IOError
	 */
	public String getRawVoxelMaskPathFromLog() throws ErrorExc{
		Properties propFile = new Properties();
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		try {
			propFile.load(new FileInputStream(file));
		} catch (IOException e) {
			throw new ErrorExc("EModGeneralIOError", e.getMessage());
		}
		String path = propFile.getProperty("DynPetMask");
		file.delete();
		return path;
	}

	/**
	 * Method to return the DynPet path used in the algorithm's execution.
	 * @return the path to the file
	 * @throws ErrorExc if the sake filename is not set, if there is no logfile or if there is a general IOError
	 */
	public String getRawVoxelPathFromLog() throws ErrorExc{
		Properties propFile = new Properties();
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		try {
			propFile.load(new FileInputStream(file));
		} catch (IOException e) {
			throw new ErrorExc("EModGeneralIOError", e.getMessage());
		}
		String path = propFile.getProperty("DynPet");
		file.delete();
		return path;
	}

	/**
	 * @return the Regional object. Throws an exception if it's not set
	 * @throws ErrorExc
	 */
	public RegionObj getRegionObj() {
		return ref_reg;
	}

	/**
	 * Method to return the RegionObj file path used in the algorithm's execution.
	 * @return the path to the file
	 * @throws ErrorExc if the sake filename is not set, if there is no logfile or if there is a general IOError
	 */
	public String getRegionPathFromLog() throws ErrorExc{
		Properties propFile = new Properties();
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		try {
			propFile.load(new FileInputStream(file));
		} catch (IOException e) {
			throw new ErrorExc("EModGeneralIOError", e.getMessage());
		}
		String path = propFile.getProperty("RegionObj");
		file.delete();
		return path;
	}

	/**
	 * Return true if and only if the sakefile has the meta type sets to voxel.
	 * Throws an exception if the meta file is not contained in the sake file or
	 * the sake file is not valid
	 * 
	 * @param sakefile
	 * @throws ErrorExc
	 */
	private boolean isValid(SakeManager sakefile) throws ErrorExc {
		return sakefile.getType() == SakeManager.Type.Voxel;
	}

	/**
	 * Method to load the data inside the sake file. It checks if the .sake file is a valid Voxel file.
	 * Loads mapVt, mapKi, mapVb, fit, mapOrder into memory. Throws an exception
	 * if the file is not valid.
	 * 
	 * @throws ErrorExc
	 */
	public void load() throws ErrorExc {
		SakeManager sakefile = new SakeManager(filename);
		if (!this.isValid(sakefile))
			throw new ErrorExc("EModSakeNotValid");
		this.loadFit();
		this.loadMapVt();
		this.loadMapKi();
		this.loadMapVb();
		this.loadMapOrder();
		this.loadRawVoxel();
		this.loadRegional();
		this.loadArterial();

		this.checkConsistency();
	}

	/**
	 * Method to load a file from a file system path. It calls setFilename(path) and load(). It checks
	 * if the .sake file is a valid Voxel file. Loads mapVt, mapKi, mapVb, fit,
	 * mapOrder into memory. Throws an exception if the file is not valid. The
	 * path to the filename to be set
	 * 
	 * @param path The name of the sake file that has to be load in memory
	 * @throws ErrorExc
	 */
	@Override
	public void load(String path) throws ErrorExc {
		setFilename(path);
		load();
	}

	/**
	 * Loads the arterial object present in the .sake file into memory. Throws
	 * an exception if the file cannot be found
	 * 
	 * @throws ErrorExc
	 */
	private void loadArterial() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		ArterialObj art = new ArterialObj();
		File artFile = sakefile.getFile("arterial.txt");
		art.load(artFile.getPath());
		artFile.delete();
		this.setArterialObj(art);
	}

	/**
	 * Method to load the fit object stored into the .sake file into the memory .
	 * Throws an exception if the file is not present
	 * 
	 * @throws ErrorExc
	 */
	private void loadFit() throws ErrorExc {
		SakeManager sakefile = new SakeManager(filename);
		File file = sakefile.getFile("fit.nii");
		VoxelObj obj = new VoxelObjAdapter();
		obj.load(file.getPath());
		file.deleteOnExit();

		this.setFit(obj);
	}

	/**
	 * Method to load the mapKi object stored into the .sake file into the memory.
	 * Throws an exception if the file is not present
	 * 
	 * @throws ErrorExc
	 */
	private void loadMapKi() throws ErrorExc {
		SakeManager sakefile = new SakeManager(filename);
		File file = sakefile.getFile("mapKi.nii");
		VoxelObj obj = new VoxelObjAdapter();
		obj.load(file.getPath());
		file.deleteOnExit();

		this.setMapKi(obj);
	}

	/**
	 * Method to load the mapOrder object stored into the .sake
	 * file into the memory. Throws an exception if the file is not present
	 * 
	 * @throws ErrorExc
	 */
	private void loadMapOrder() throws ErrorExc {
		SakeManager sakefile = new SakeManager(filename);
		File file = sakefile.getFile("mapOrder.nii");
		VoxelObj obj = new VoxelObjAdapter();
		obj.load(file.getPath());
		file.deleteOnExit();

		this.setMapOrder(obj);
	}

	/**
	 * Method to load the mapVb object stored into the .sake file into the memory.
	 * Throws an exception if the file is not present
	 * 
	 * @throws ErrorExc
	 */
	private void loadMapVb() throws ErrorExc {
		SakeManager sakefile = new SakeManager(filename);
		File file = sakefile.getFile("mapVb.nii");
		VoxelObj obj = new VoxelObjAdapter();
		obj.load(file.getPath());
		file.deleteOnExit();

		this.setMapVb(obj);
	}

	/**
	 * Method to load the mapVt object stored into the .sake file into the memory.
	 * Throws an exception if the file is not present
	 * 
	 * @throws ErrorExc
	 */
	private void loadMapVt() throws ErrorExc {
		SakeManager sakefile = new SakeManager(filename);
		File file = sakefile.getFile("mapVt.nii");
		VoxelObj obj = new VoxelObjAdapter();
		obj.load(file.getPath());
		file.deleteOnExit();

		this.setMapVt(obj);
	}

	/**
	 * Method to load the original voxel object stored into the .sake file into the memory.
	 * Throws an exception if the file is not present
	 * 
	 * @throws ErrorExc
	 */
	private void loadRawVoxel() throws ErrorExc {
		SakeManager sakefile = new SakeManager(filename);
		File file = sakefile.getFile("rawVoxel.nii");
		VoxelObj obj = new VoxelObjAdapter();
		obj.load(file.getPath());
		file.deleteOnExit();

		this.setRawVoxel(obj);
	}

	/**
	 * Loads the regional object present in the .sake file into memory. Throws
	 * an exception if the file cannot be found
	 * 
	 * @throws ErrorExc
	 */
	private void loadRegional() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		RegionObj reg = new RegionObj();
		File regFile = sakefile.getFile("regional.txt");
		reg.load(regFile.getPath());
		regFile.delete();
		this.setRegionObj(reg);
	}

	/**
	 * Save elaboration results in a .sake file. Existing files inside will be
	 * overwritten. Throws an exception if:
	 * <ul>
	 * <li>Parameters are not set</li>
	 * <li>Impossibility to save temporary files to store inside the sake</li>
	 * </ul>
	 * 
	 * @throws ErrorExc
	 */
	public void save() throws ErrorExc {
		// let's check if all values are set
		this.checkConsistency();
		if (filename == null)
			throw new ErrorExc("EModFilenameNotSet");
		if (params == null || ref_reg == null || ref_art == null)
			throw new ErrorExc("EModExecutionParametersNotSet");
		
		SakeManager sakeFile = new SakeManager(getFilename());

		// if we arrive at this point, we can save everything

		// TODO gestire eccezioni
		// TODO finally per eliminare sakeFile in caso la creazione degli altri
		// file torni errore
		File mapVtFile = new File(getTempDir() + "mapVt.nii");
		File mapKiFile = new File(getTempDir() + "mapKi.nii");
		File mapVbFile = new File(getTempDir() + "mapVb.nii");
		File mapOrderFile = new File(getTempDir() + "mapOrder.nii");
		File rawVoxelFile = new File(getTempDir() + "rawVoxel.nii");
		File artFile = new File(getTempDir() + "arterial.txt");
		File regFile = new File(getTempDir() + "regional.txt");
		File fitFile = new File(getTempDir() + "fit.nii");
		File logFile = new File(getTempDir() + "logfile.txt");

		this.createLogFile(logFile);
		this.createFitFile(fitFile);
		this.createMapKiFile(mapKiFile);
		this.createMapOrderFile(mapOrderFile);
		this.createMapVbFile(mapVbFile);
		this.createRawVoxel(rawVoxelFile);
		this.createMapVtFile(mapVtFile);
		this.createArterialFile(artFile);
		this.createRegionalFile(regFile);

		sakeFile.addFile(fitFile, true);
		sakeFile.addFile(mapKiFile, true);
		sakeFile.addFile(mapOrderFile, true);
		sakeFile.addFile(mapVbFile, true);
		sakeFile.addFile(mapVtFile, true);
		sakeFile.addFile(rawVoxelFile, true);
		sakeFile.addFile(logFile, true);
		sakeFile.addFile(artFile, true);
		sakeFile.addFile(regFile, true);

		fitFile.delete();
		mapKiFile.delete();
		mapOrderFile.delete();
		mapVbFile.delete();
		mapVtFile.delete();
		regFile.delete();
		artFile.delete();
		logFile.delete();
		
		sakeFile.setType(SakeManager.Type.Voxel);
		
	}

	/**
	 * Save elaboration results in a .sake file. It calls setFilename() and
	 * save() methods. Existing files inside will be overwritten. Throws an
	 * exception if:
	 * <ul>
	 * <li>Parameters are not set</li>
	 * <li>Impossibility to save temporary files to store inside the sake</li>
	 * </ul>
	 * 
	 * @throws ErrorExc
	 */
	@Override
	public void save(String path) throws ErrorExc {
		this.setFilename(path);
		this.save();
	}

	/**
	 * Sets the ArterialObj file from an ArterialObj input
	 * @param ref_art
	 *            the Arterial object to set
	 */
	public void setArterialObj(ArterialObj ref_art) {
		this.ref_art = ref_art;
	}

	/**
	 * Sets the current VoxelObj  file
	 * @param current
	 *            the Voxel object to set as current
	 */
	public void setCurrentObj(VoxelObjs current) {
		this.current = current;
	}

	/**
	 * Sets the file name from a path String 
	 * @param filename
	 *            the sake filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Sets the fit matrix returned by the quantification algorithm's execution
	 * @param fit
	 *            the fit matrix to set
	 */
	public void setFit(VoxelObj fit) {
		this.fit = fit;
	}

	/**
	 * Sets the mapKi image returned by the quantification algorithm's execution
	 * @param fit
	 *            the mapKi image to set
	 */
	public void setMapKi(VoxelObj mapKi) {
		this.mapKi = mapKi;
	}

	/**
	 * Sets the mapOrder image returned by the quantification algorithm's execution
	 * @param fit
	 *            the mapOrder image to set
	 */
	public void setMapOrder(VoxelObj mapOrder) {
		this.mapOrder = mapOrder;
	}

	/**
	 * Sets the mapVb image returned by the quantification algorithm's execution
	 * @param fit
	 *            the mapVb image to set
	 */
	public void setMapVb(VoxelObj mapVb) {
		this.mapVb = mapVb;
	}

	/**
	 * Sets the mapVt image returned by the quantification algorithm's execution
	 * @param fit
	 *            the mapVt image to set
	 */
	public void setMapVt(VoxelObj mapVt) {
		this.mapVt = mapVt;
	}

	/**
	 * Sets the ParamInterface Objects from a ParamInterface input
	 * @param params
	 *            the ParamInterface used in the algorithm execution to set
	 */
	public void setParamInterface(SAParam params) {
		this.params = params;
	}
	
	/**
	 * Sets the rawVoxel image used for running the quantification algorithm's execution
	 * @param fit
	 *            the mapVt image to set
	 */
	public void setRawVoxel(VoxelObj rawVoxel) {
		this.rawVoxel = rawVoxel;
	}
	
	/**
	 * Sets the RegionObj file from a RegionObj input
	 * @param ref_reg
	 *            the Region object to set
	 */
	public void setRegionObj(RegionObj ref_reg) {
		this.ref_reg = ref_reg;
	}
	
	public void setAlgorithm(String alg){
		algorithm=alg;
	}


}
