/**
//////////////////////////////////
Sake. java
This is the common class for the Sake files handled by the system
it.tetratron.pera.model.objects
GPLv3
Zambon Jacopo
TetraTron Group
2012.02.13
//////////////////////////////////
*/

package it.tetratron.pera.model.objects;
import it.tetratron.pera.model.manage.ErrorExc;

/**
 * This is the common class for the Sake files handled by the system
 * @author Zambon Jacopo
 *
 */
public abstract class Sake implements ObjInterface {

	/**
	 * Returns the temporary directory for the system, ending with a separator
	 * @return temporary directory for the system
	 */
	public static String getTempDir(){
		String tmpDir = System.getProperty("java.io.tmpdir");
		if( !(tmpDir.endsWith("/") || tmpDir.endsWith("\\")) )
			tmpDir = tmpDir + System.getProperty("file.separator");
		return tmpDir;
	}

	@Override
	public void load(String path) throws ErrorExc {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void save(String path) throws ErrorExc {
		// TODO Auto-generated method stub
		
	}

}
