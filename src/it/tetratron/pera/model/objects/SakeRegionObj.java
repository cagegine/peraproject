/**
//////////////////////////////////
SakeRegionObj. java
This is the class for handling the objects for the Sake files at Region Level
it.tetratron.pera.model.objects
GPLv3
Francesco Rosso
TetraTron Group
2012.02.29
//////////////////////////////////
 */

package it.tetratron.pera.model.objects;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import it.tetratron.pera.model.manage.ErrorExc;
import it.tetratron.pera.model.manage.SakeManager;
import it.tetratron.pera.model.manage.TxtManager;
import it.tetratron.pera.model.params.RSSAParam;
import it.tetratron.pera.model.params.SAIFParam;
import it.tetratron.pera.model.params.SAParam;
import it.tetratron.pera.model.params.StdSAParam;

/**
 * This is the class for handling the objects for the Sake files at Region Level
 * @author Rosso Francesco
 *
 */
public class SakeRegionObj extends Sake implements ObjInterface {

	private double[][] alfa = null;
	private double[][] beta = null;
	private double[][] fit = null;
	private double[][] parameters = null;
	// the .sake file
	private String filename = null;
	private RegionObj ref_reg = null;
	private ArterialObj ref_art = null;
	private SAParam params = null;
	private String algorithm = null;
	/**
	 * Method to check data consistency before save and before load. It checks
	 * if:
	 * <ul>
	 * <li>number of columns in parameters matrix and fit matrix must be the
	 * same</li>
	 * <li>number of columns in alfa matrix and fit matrix must be the same</li>
	 * <li>alfa matrix and beta matrix must have the same dimensions</li>
	 * <li>in each alfa and beta column, negative values must occur after
	 * positive ones</li>
	 * <li>parameters matrix must have 10 lines</li>
	 * </ul>
	 * Throws an ErrorExc if at least one of the previous conditions are not met
	 * 
	 * @throws ErrorExc
	 */
	private void checkConsistency() throws ErrorExc {
		// we check data consistency
		// number of columns in parameters and fit must be the same
		if (getFitArray()[0].length != getParametersArray()[0].length)
			throw new ErrorExc("EModDataNotConsistent");
		// number of columns in alfa and fit must be the same
		if (getFitArray()[0].length != getAlfaArray()[0].length)
			throw new ErrorExc("EModDataNotConsistent");
		// alfa and beta must have the same dimensions
		if (getAlfaArray().length != getBetaArray().length
				|| getAlfaArray()[0].length != getBetaArray()[0].length)
			throw new ErrorExc("EModDataNotConsistent");
		if (getParametersArray().length != 10)
			throw new ErrorExc("EModDataNotConsistent");

		// in each alfa and beta column, negative values must occur after
		// positive ones
		try {
			this.checkPositiveNegativeValues(getAlfaArray());
		} catch (ErrorExc e) {
			throw new ErrorExc("EModFoundNegativeValuesAlfa", e.getMessage());
		}
		try {
			this.checkPositiveNegativeValues(getBetaArray());
		} catch (ErrorExc e) {
			throw new ErrorExc("EModFoundNegativeValuesBeta", e.getMessage());
		}
	}

	/**
	 * Checks if in the given matrix there are positive values after negative
	 * values in every column. Matrix must be != null. Throws an exception if
	 * negative values are found.
	 * 
	 * @param matrix
	 * @throws ErrorExc
	 */
	private void checkPositiveNegativeValues(double[][] matrix) throws ErrorExc {
		boolean negativeFound = false;
		for (int column = 0; column < matrix[0].length; column++) {
			for (int row = 0; row < matrix.length; row++) {
				if (matrix[row][column] < 0)
					negativeFound = true;
				else if (matrix[row][column] > 0 && negativeFound == true)
					throw new ErrorExc("EModFoundNegativeValues");
			}
			negativeFound = false;
		}
	}

	/**
	 * This method will create an alfa file using the alfa matrix
	 * 
	 * @param filename The name of the txt file which will contain the alfa matrix
	 * @throws ErrorExc
	 */
	private void createAlfaFile(File filename) throws ErrorExc {
		TxtManager txt = new TxtManager();
		txt.setFile(filename.getPath());
		txt.saveFile(alfa);
	}

	/**
	 * Method to save the arterial file in the given file
	 * 
	 * @param artFile The name of the txt file where the arterial file has to be saved 
	 * @throws ErrorExc
	 */
	private void createArterialFile(File artFile) throws ErrorExc {
		ref_art.save(artFile.getPath());
	}

	/**
	 * this method will create a beta file using the beta matrix
	 * 
	 * @param filename The name of the txt file which will contain the beta matrix
	 * @throws ErrorExc
	 */
	private void createBetaFile(File filename) throws ErrorExc {
		TxtManager txt = new TxtManager();
		txt.setFile(filename.getPath());
		txt.saveFile(beta);
	}

	/**
	 * This method will create a fit file based on the parameters utilized in
	 * the execution
	 * 
	 * @param filename The name of the txt file which will contain the fit matrix
	 * @throws ErrorExc
	 */
	private void createFitFile(File filename) throws ErrorExc {
		TxtManager txt = new TxtManager();
		txt.setFile(filename.getPath());

		String[] newLabels = this.getOldLabelsFromRegional();
		newLabels[0] = "time_PET";
		txt.setLabels(newLabels);

		List<Double> time_PET = getRegionObj().getTime_pet();

		String[][] toSave = new String[fit.length][fit[0].length + 1];

		if (getFitArray().length != time_PET.size())
			throw new ErrorExc("EModWTF");

		for (int i = 0; i < getFitArray().length; i++) {
			toSave[i][0] = time_PET.get(i).toString();
		}

		for (int i = 0; i < getFitArray().length; i++)
			for (int j = 0; j < getFitArray()[0].length; j++) {
				toSave[i][j + 1] = Double.toString(fit[i][j]);
			}
		txt.saveFile(toSave);
	}

	/**
	 * This method will create a log file based on the parameters utilized in
	 * the algorithm execution
	 * 
	 * @param filename The name of the txt file which will contain the log data
	 * @throws ErrorExc
	 */
	private void createLogFile(File filename) throws ErrorExc {
		Properties propFile = new Properties();
		try {
			propFile.setProperty("RegionObj", getRegionObj().getFilename());
			propFile.setProperty("ArterialObj", getArterialObj().getFilename());
			propFile.setProperty("ParWorkspace_Dir", params.getWorkspace_Dir());
			
			if (params instanceof SAIFParam) {
				propFile.setProperty("algorithm", "SAIF");
				algorithm = "SAIF";
				propFile.setProperty("ParPassband[0]", Double
						.toString((((SAIFParam) params).getPassband().get(0))));
				propFile.setProperty("ParPassband[1]", Double
						.toString((((SAIFParam) params).getPassband().get(1))));
			} else if (params instanceof RSSAParam) {
				propFile.setProperty("algorithm", "RSSA");
				algorithm = "RSSA";
				propFile.setProperty("ParSNR[0]",
						Double.toString((((RSSAParam) params).getSNR().get(0))));
				propFile.setProperty("ParSNR[1]",
						Double.toString((((RSSAParam) params).getSNR().get(1))));
			} else if (params instanceof StdSAParam) {
				propFile.setProperty("algorithm", "StdSA");
				algorithm = "StdSA";
				propFile.setProperty("ParTypeOfCorrection",
						Integer.toString(((StdSAParam) params).getType()));
			}
			propFile.setProperty("ParBeta1", Double.toString(params.getBeta1()));
			propFile.setProperty("ParBeta2", Double.toString(params.getBeta2()));
			propFile.setProperty("ParNComp",
					Integer.toString(params.getNComp()));
			propFile.store(new FileOutputStream(filename.getPath()),
					null);
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToStoreTempFile", e.getMessage());
		}
	}
	
	public String getAlgorithm() throws ErrorExc {
		if (algorithm == null)
			throw new ErrorExc("EModAlgorithmNotSet");
		return algorithm;
	}

	public void createLogFileCSV(String pathToSaveFile) throws ErrorExc {
		Properties propFile = new Properties();
		try {
			propFile.setProperty("Region file", getRegionObj().getFilename());
			propFile.setProperty("Arterial file", getArterialObj().getFilename());
			//propFile.setProperty("Workspace directory", params.getWorkspace_Dir());
			
			if (params instanceof SAIFParam) {
				propFile.setProperty("Algorithm", "Iterative Filtering Spectral Analysis");
				algorithm = "SAIF";
				propFile.setProperty("Passband min", Double
						.toString((((SAIFParam) params).getPassband().get(0))));
				propFile.setProperty("Passband max", Double
						.toString((((SAIFParam) params).getPassband().get(1))));
			} else if (params instanceof RSSAParam) {
				propFile.setProperty("Algorithm", "Rank Shaping Spectral Analysis");
				algorithm = "RSSA";
				propFile.setProperty("SNR min",
						Double.toString((((RSSAParam) params).getSNR().get(0))));
				propFile.setProperty("SNR max",
						Double.toString((((RSSAParam) params).getSNR().get(1))));
			} else if (params instanceof StdSAParam) {
				propFile.setProperty("Algorithm", "Standard Spectral Analysis");
				algorithm = "StdSA";
				propFile.setProperty("Type of correction",
						Integer.toString(((StdSAParam) params).getType()));
			}
			//propFile.setProperty("Beta 1", Double.toString(params.getBeta1()));
			//propFile.setProperty("Beta 2", Double.toString(params.getBeta2()));
			//propFile.setProperty("Number of components",
			//		Integer.toString(params.getNComp()));
			propFile.store(new FileOutputStream(pathToSaveFile),
					null);
		} catch (IOException e) {
			throw new ErrorExc("EModImpossibleToStoreTempFile", e.getMessage());
		}
	}

	/**
	 * This method will create a parameters file based on the parameters used in
	 * the execution
	 * 
	 * @param filename The name of the txt file which will contain the parameters matrix
	 * @throws ErrorExc
	 */
	private void createParametersFile(File filename) throws ErrorExc {
		TxtManager txt = new TxtManager();
		txt.setFile(filename.getPath());

		String[] newLabels = this.getOldLabelsFromRegional();
		newLabels[0] = "parameters";
		txt.setLabels(newLabels);

		String[][] toSave = new String[10][parameters[0].length + 1];
		toSave[0][0] = "WRSS";
		toSave[1][0] = "RSS";
		toSave[2][0] = "Vt";
		toSave[3][0] = "CV_Vt";
		toSave[4][0] = "Ki";
		toSave[5][0] = "CV_Ki";
		toSave[6][0] = "Vb";
		toSave[7][0] = "CV_Vb";
		toSave[8][0] = "Order";
		toSave[9][0] = "Trapping";

		for (int i = 0; i < 10; i++)
			for (int j = 0; j < parameters[0].length; j++)
				toSave[i][j + 1] = Double.toString(parameters[i][j]);
		txt.saveFile(toSave);
	}
	
	/**
	 * Method to save the regional file in the given file
	 * 
	 * @param regFile The name of the txt file where the regional file has to be saved
	 * @throws ErrorExc
	 */
	private void createRegionalFile(File regFile) throws ErrorExc {
		ref_reg.save(regFile.getPath());
	}

	/**
	 * @return The alfa vector of vectors. Throws an exception if the matrix is
	 *         not set.
	 * @throws ErrorExc
	 */
	public List<List<Double>> getAlfa() throws ErrorExc {
		List<List<Double>> roi_vector = new Vector<List<Double>>();
		for (int j = 0; j < (getAlfaArray())[0].length; j++) {
			List<Double> vector = new Vector<Double>();
			for (int i = 0; i < (getAlfaArray()).length; i++)
				vector.add((getAlfaArray())[i][j]);
			roi_vector.add(vector);
		}
		if (roi_vector.size() == 0)
			throw new ErrorExc("EModNoRoiData");
		return roi_vector;

	}

	/**
	 * @return The alfa matrix. Throws an exception if the matrix is not set.
	 * @throws ErrorExc
	 */
	public double[][] getAlfaArray() throws ErrorExc {
		if (alfa == null)
			throw new ErrorExc("EModAlfaNotSet");
		return alfa;
	}

	/**
	 * @return the Arterial object. Throws an exception if it's not set
	 * @throws ErrorExc
	 */
	public ArterialObj getArterialObj() throws ErrorExc {
		if (ref_art == null)
			throw new ErrorExc("EModArterialObjNotSet");
		return ref_art;
	}

	/**
	 * Method to return the ArterialObj path used in the algorithm's execution.
	 * @return the path to the file
	 * @throws ErrorExc if the sake filename is not set, if there is no logfile or if there is a general IOError
	 */
	public String getArterialPathFromLog() throws ErrorExc{
		Properties propFile = new Properties();
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		try {
			propFile.load(new FileInputStream(file));
		} catch (IOException e) {
			throw new ErrorExc("EModGeneralIOError", e.getMessage());
		}
		String path = propFile.getProperty("ArterialObj");
		file.delete();
		return path;
	}

	public File getLog() throws ErrorExc{
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		return file;
	}

	/**
	 * @return The beta vector of vectors. Throws an exception if the matrix is
	 *         not set.
	 * @throws ErrorExc
	 */
	public List<List<Double>> getBeta() throws ErrorExc {
		List<List<Double>> roi_vector = new Vector<List<Double>>();
		for (int j = 0; j < (getBetaArray())[0].length; j++) {
			List<Double> vector = new Vector<Double>();
			for (int i = 0; i < (getBetaArray()).length; i++)
				vector.add((getBetaArray())[i][j]);
			roi_vector.add(vector);
		}
		if (roi_vector.size() == 0)
			throw new ErrorExc("EModNoRoiData");
		return roi_vector;
	}

	/**
	 * @return The beta matrix returned by the quantification algorithm's
	 *         execution. Throws an exception if the matrix is not set.
	 * @throws ErrorExc
	 */
	public double[][] getBetaArray() throws ErrorExc {
		if (beta == null)
			throw new ErrorExc("EModBetaNotSet");
		return beta;
	}

	/**
	 * @return the sake filename. throw an exception if it is not set
	 * @throws ErrorExc
	 */
	public String getFilename() throws ErrorExc {
		if (filename == null)
			throw new ErrorExc("EModFilenameNotSet");
		return filename;
	}

	/**
	 * @return The fit vector of vectors. Throws an exception if the matrix is
	 *         not set.
	 * @throws ErrorExc
	 */
	public List<List<Double>> getFit() throws ErrorExc {
		List<List<Double>> roi_vector = new Vector<List<Double>>();
		for (int j = 0; j < (getFitArray())[0].length; j++) {
			List<Double> vector = new Vector<Double>();
			for (int i = 0; i < (getFitArray()).length; i++)
				vector.add((getFitArray())[i][j]);
			roi_vector.add(vector);
		}
		if (roi_vector.size() == 0)
			throw new ErrorExc("EModNoRoiData");
		return roi_vector;

	}

	/**
	 * @return The fit matrix returned by the quantification algorithm's
	 *         execution. Throws an exception if the matrix is not set or
	 *         present in the .sake file.
	 * @throws ErrorExc
	 */
	public double[][] getFitArray() throws ErrorExc {
		if (fit == null)
			throw new ErrorExc("EModFitNotSet");
		return fit;
	}

	/**
	 * This method will return the fit file content, labels included.
	 * 
	 * @return the file content data matrix
	 * @throws ErrorExc
	 */
	public String[][] getFitContent() throws ErrorExc {

		try {
			SakeManager sakefile = new SakeManager(getFilename());
			File file = sakefile.getFile("fit.txt");
			TxtManager txt = new TxtManager(file.getPath());
			String[][] contTemp = txt.getFileContent();
			file.delete();
			return contTemp;
		} catch (ErrorExc e) {

		}

		File fitFile = new File(getTempDir() + "tempFit.txt");
		this.createParametersFile(fitFile);
		TxtManager txt = new TxtManager(fitFile.getAbsolutePath());
		txt.readFile();
		String[][] content = txt.getFileContent();
		fitFile.delete();
		return content;

	}

	/**
	 * This method will return an array of labels with the first element set to
	 * null (utilized in createFitFile and createParametersFile)
	 * 
	 * @throws ErrorExc
	 */
	private String[] getOldLabelsFromRegional() {
		TxtManager oldRegion = new TxtManager();
		String[][] cont = null;
		String[] newLabels = null;
		try {
			oldRegion.setFile(getRegionObj().getFilename());
			// let's read the original labels
			cont = oldRegion.readFile();
			newLabels = new String[cont[0].length - 3 + 1];
			// may launch exception
			String[] origLabels = oldRegion.getLabels();
			for (int i = 0; i < cont[0].length - 3; i++) {
				newLabels[i + 1] = origLabels[i + 3];
			}
		} catch (ErrorExc e) {
			// no labels read, try to assign some
			newLabels = new String[fit[0].length + 1];
			for (int i = 0; i < fit[0].length; i++) {
				newLabels[i + 1] = "ROI_" + (i );
			}
		}
		return newLabels;
	}

	/**
	 * @return The parameters vector of vectors. Throws an exception if the
	 *         matrix is not set.
	 * @throws ErrorExc
	 */
	public List<List<Double>> getParameters() throws ErrorExc {
		List<List<Double>> roi_vector = new Vector<List<Double>>();
		for (int j = 0; j < (getParametersArray())[0].length; j++) {
			List<Double> vector = new Vector<Double>();
			for (int i = 0; i < (getParametersArray()).length; i++)
				vector.add((getParametersArray())[i][j]);
			roi_vector.add(vector);
		}
		if (roi_vector.size() == 0)
			throw new ErrorExc("EModNoRoiData");
		return roi_vector;

	}

	/**
	 * @return The parameters matrix returned by the quantification algorithm's
	 *         execution
	 * @throws ErrorExc
	 */
	public double[][] getParametersArray() throws ErrorExc {
		if (parameters == null)
			throw new ErrorExc("EModParametersNotSet");
		return parameters;
	}

	/**
	 * This method will return the parameters file content, labels included. If
	 * the file is present in the sake file, this function will return its
	 * content, if it's not, this function will attempt to create one.
	 * 
	 * @return the file content data matrix
	 * @throws ErrorExc
	 */
	public String[][] getParametersContent() throws ErrorExc {
		try {
			SakeManager sakefile = new SakeManager(getFilename());
			File file = sakefile.getFile("parameters.txt");
			TxtManager txt = new TxtManager(file.getPath());
			String[][] contTemp = txt.getFileContent();
			file.delete();
			return contTemp;
		} catch (ErrorExc e) {

		}
		File parametersFile = new File(getTempDir() + "tempParameters.txt");
		this.createParametersFile(parametersFile);
		TxtManager txt = new TxtManager(parametersFile.getAbsolutePath());
		txt.readFile();
		String[][] content = txt.getFileContent();
		parametersFile.delete();
		return content;
	}

	/**
	 * @return the ParamInterface Object used in the algorithm execution
	 */
	public SAParam getParamInterface() {
		return params;
	}

	/**
	 * @return the Regional object. Throws an exception if it's not set
	 * @throws ErrorExc
	 */
	public RegionObj getRegionObj() throws ErrorExc {
		if (ref_reg == null)
			throw new ErrorExc("EModRegionObjNotSet");
		return ref_reg;
	}

	/**
	 * Method to return the RegionObj file path used in the algorithm's execution.
	 * @return the path to the file
	 * @throws ErrorExc if the sake filename is not set, if there is no logfile or if there is a general IOError
	 */
	public String getRegionPathFromLog() throws ErrorExc{
		Properties propFile = new Properties();
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("logfile.txt");
		try {
			propFile.load(new FileInputStream(file));
		} catch (IOException e) {
			throw new ErrorExc("EModGeneralIOError", e.getMessage());
		}
		String path = propFile.getProperty("RegionObj");
		file.delete();
		return path;
	}

	/**
	 * Method to check if the given SakeFile is a valid region file. Throws an
	 * exception if the type is not set.
	 * 
	 * @param sakefile The name and path of the .sake file whose validity has to be checked
	 * @return True if the sake file is valid, else False
	 * @throws ErrorExc
	 */
	private boolean isValid(SakeManager sakefile) throws ErrorExc {
		return sakefile.getType() == SakeManager.Type.Region;
	}

	/**
	 * Method to load file. It checks if the .sake file is a valid Region file.
	 * It loads alfa, beta, fit, parameters, regional and arterial objects into
	 * memory. Throws an exception if the file is not valid or if alfa, beta,
	 * parameters, regional and arterial objects or fit are not set or
	 * consistent.
	 * 
	 * @throws ErrorExc
	 */
	public void load() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		if (!this.isValid(sakefile))
			throw new ErrorExc("EModSakeNotValid");
		this.loadAlfa();
		this.loadBeta();
		this.loadFit();
		this.loadParameters();
		this.loadRegional();
		this.loadArterial();

		// let's check if everything is consistent
		this.checkConsistency();
	}

	/**
	 * Method to load a file. It calls setFilename(path) and load(). It checks
	 * if the .sake file is a valid Region file. It loads alfa, beta, fit,
	 * parameters into memory. Throws an exception if the file is not valid or
	 * if alfa, beta, parameters or fit are not set or consistent. The path to
	 * the filename to be set
	 * 
	 * @param path The name of the sake file that has to be load in memory
	 * @throws ErrorExc
	 */
	@Override
	public void load(String path) throws ErrorExc {
		setFilename(path);
		load();
	}

	/**
	 * Loads the alfa matrix in the .sake file into memory. Throws an exception
	 * if the matrix data cannot be found or is corrupted
	 * 
	 * @throws ErrorExc
	 */
	private void loadAlfa() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("alfa.txt");
		TxtManager txt = new TxtManager(file.getPath());
		double[][] cont = txt.readFileDouble();
		file.delete();
		this.setAlfa(cont);
	}

	/**
	 * Loads the arterial object present in the .sake file into memory. Throws
	 * an exception if the file cannot be found
	 * 
	 * @throws ErrorExc
	 */
	private void loadArterial() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		ArterialObj art = new ArterialObj();
		File artFile = sakefile.getFile("arterial.txt");
		art.load(artFile.getPath());
		artFile.delete();
		this.setArterialObj(art);
	}

	/**
	 * Loads the beta matrix in the .sake file into memory. Throws an exception
	 * if the matrix data cannot be found or is corrupted
	 * 
	 * @throws ErrorExc
	 */
	private void loadBeta() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("beta.txt");
		TxtManager txt = new TxtManager(file.getPath());
		double[][] cont = txt.readFileDouble();
		file.delete();
		this.setBeta(cont);
	}

	/**
	 * Loads the fit matrix in the .sake file into memory. Throws an exception
	 * if the matrix data cannot be found or is corrupted
	 * 
	 * @throws ErrorExc
	 */
	private void loadFit() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("fit.txt");
		TxtManager txt = new TxtManager(file.getPath());
		String[][] contTemp = txt.readFile();
		file.delete();

		double[][] cont = new double[contTemp.length][contTemp[0].length - 1];
		for (int i = 0; i < contTemp.length; i++)
			// la prima colonna non ci interessa, si tratta del time_PET
			for (int j = 1; j < contTemp[0].length; j++) {
				cont[i][j - 1] = Double.parseDouble(contTemp[i][j]);
			}
		this.setFit(cont);
	}

	/**
	 * Loads the parameters matrix in the .sake file into memory. Throws an
	 * exception if the matrix data cannot be found or is corrupted
	 * 
	 * @throws ErrorExc
	 */
	private void loadParameters() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		File file = sakefile.getFile("parameters.txt");
		TxtManager txt = new TxtManager(file.getPath());
		String[][] contTemp = txt.readFile();
		file.delete();

		double[][] cont = new double[10][contTemp[0].length - 1];
		for (int i = 0; i < 10; i++) {
			// la prima colonna non ci interessa, sono le labels
			for (int j = 1; j < contTemp[0].length; j++) {
				cont[i][j - 1] = Double.parseDouble(contTemp[i][j]);
			}
		}

		this.setParameters(cont);
	}

	/**
	 * Loads the regional object present in the .sake file into memory. Throws
	 * an exception if the file cannot be found
	 * 
	 * @throws ErrorExc
	 */
	private void loadRegional() throws ErrorExc {
		SakeManager sakefile = new SakeManager(getFilename());
		RegionObj reg = new RegionObj();
		File regFile = sakefile.getFile("regional.txt");
		reg.load(regFile.getPath());
		regFile.delete();
		this.setRegionObj(reg);
	}

	/**
	 * Save elaboration results in a .sake file. Existing files inside will be
	 * overwritten. Throws an exception if:
	 * <ul>
	 * <li>Parameters (alfa, beta, parameters, fit, filename, arterial and
	 * regional objects) are not set</li>
	 * <li>Impossibility to save temporary files to store inside the sake</li>
	 * <ul>
	 * 
	 * @throws ErrorExc
	 */
	public void save() throws ErrorExc {
		// let's check if all values are set
		if (getParamInterface() == null || getRegionObj() == null
				|| getArterialObj() == null)
			throw new ErrorExc("EModExecutionParametersNotSet");

		this.checkConsistency();

		// if we arrive at this point, we can save everything

		SakeManager sakeFile = new SakeManager(getFilename());

		// TODO gestire eccezioni
		// TODO finally per eliminare sakeFile in caso la creazione degli altri
		// file torni errore
		File parametersFile = new File(getTempDir() + "parameters.txt");
		File alfaFile = new File(getTempDir() + "alfa.txt");
		File betaFile = new File(getTempDir() + "beta.txt");
		File fitFile = new File(getTempDir() + "fit.txt");
		File artFile = new File(getTempDir() + "arterial.txt");
		File regFile = new File(getTempDir() + "regional.txt");
		File logFile = new File(getTempDir() + "logfile.txt");

		this.createLogFile(logFile);
		this.createParametersFile(parametersFile);
		this.createAlfaFile(alfaFile);
		this.createBetaFile(betaFile);
		this.createFitFile(fitFile);
		this.createArterialFile(artFile);
		this.createRegionalFile(regFile);
		
		sakeFile.addFile(parametersFile, true);
		sakeFile.addFile(alfaFile, true);
		sakeFile.addFile(betaFile, true);
		sakeFile.addFile(fitFile, true);
		sakeFile.addFile(logFile, true);
		sakeFile.addFile(artFile, true);
		sakeFile.addFile(regFile, true);

		parametersFile.delete();
		alfaFile.delete();
		betaFile.delete();
		fitFile.delete();
		regFile.delete();
		artFile.delete();
		logFile.delete();
		
		sakeFile.setType(SakeManager.Type.Region);
		
	}

	/**
	 * Save elaboration results in the given .sake file. Existing files inside
	 * will be overwritten. Throws an exception if:
	 * <ul>
	 * <li>Parameters (alfa, beta, parameters, fit, filename) are not set</li>
	 * <li>Impossibility to save temporary files to store inside the sake</li>
	 * <ul>
	 * 
	 * @param path
	 *            the path and filename of the .sake file
	 * @throws ErrorExc
	 */
	@Override
	public void save(String path) throws ErrorExc {
		this.setFilename(path);
		this.save();
	}

	/**
	 * Sets the alfa matrix returned by the quantification algorithm's execution
	 * 
	 * @param alfa The new alfa matrix to set
	 */
	public void setAlfa(double[][] alfa) {
		this.alfa = alfa;
	}

	/**
	 * Sets the ArterialObj file from an ArterialObj input
	 * @param ref_art
	 *            the Arterial object to set
	 */
	public void setArterialObj(ArterialObj ref_art) {
		this.ref_art = ref_art;
	}

	/**
	 * Sets the beta matrix returned by the quantification algorithm's execution
	 * @param beta
	 *            the new beta matrix to set
	 */
	public void setBeta(double[][] beta) {
		this.beta = beta;
	}

	/**
	 * Sets the file name from a path String 
	 * @param filename
	 *            the sake filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * Sets the fit matrix returned by the quantification algorithm's execution
	 * @param fit
	 *            the fit matrix to set
	 */
	public void setFit(double[][] fit) {
		this.fit = fit;
	}

	/**
	 * Sets the parameters matrix returned by the quantification algorithm's execution
	 * @param parameters
	 *            the parameters to set
	 */
	public void setParameters(double[][] parameters) {
		this.parameters = parameters;
	}

	/**
	 * Sets the ParamInterface Objects from a ParamInterface input
	 * @param params
	 *            the ParamInterface used in the algorithm execution to set
	 */
	public void setParamInterface(SAParam params) {
		this.params = params;
	}

	/**
	 * Sets the RegionObj file from a RegionObj input
	 * @param ref_reg
	 *            the Region object to set
	 */
	public void setRegionObj(RegionObj ref_reg) {
		this.ref_reg = ref_reg;
	}

	public void setAlgorithm(String alg){
		algorithm=alg;
	}

	public SakeManager getSakeFile()throws ErrorExc {
		SakeManager lol=null;
		System.out.println(getFilename());
		try {
			 lol = new SakeManager(getFilename());
		} catch (ErrorExc e) {
			throw new ErrorExc("EModGeneralIOError",e.getMessage());
		}
		return lol;
	}
}
