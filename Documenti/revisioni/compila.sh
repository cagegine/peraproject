#!/bin/bash/

VER=1.0

cd Glossario
pdflatex --jobname=glossario-$(cat glossario.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) glossario.tex
pdflatex --jobname=glossario-$(cat glossario.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) glossario.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd NormeDiProgetto
pdflatex --jobname=norme_di_progetto-$(cat norme_di_progetto.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) norme_di_progetto.tex
pdflatex --jobname=norme_di_progetto-$(cat norme_di_progetto.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) norme_di_progetto.tex
mv *.pdf ../../consegne/RQ/interni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd StudioDiFattibilita
pdflatex --jobname=studio_di_fattibilita-$(cat studio_di_fattibilita.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) studio_di_fattibilita.tex
pdflatex --jobname=studio_di_fattibilita-$(cat studio_di_fattibilita.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) studio_di_fattibilita.tex
mv *.pdf ../../consegne/RQ/interni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd AnalisiDeiRequisiti
pdflatex --jobname=analisi_dei_requisiti-$(cat analisi_dei_requisiti.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) analisi_dei_requisiti.tex
pdflatex --jobname=analisi_dei_requisiti-$(cat analisi_dei_requisiti.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) analisi_dei_requisiti.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd PianoDiQualifica
pdflatex --jobname=piano_di_qualifica-$(cat piano_di_qualifica.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) piano_di_qualifica.tex
pdflatex --jobname=piano_di_qualifica-$(cat piano_di_qualifica.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) piano_di_qualifica.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd PianoDiProgetto
pdflatex --jobname=piano_di_progetto-$(cat piano_di_progetto.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) piano_di_progetto.tex
pdflatex --jobname=piano_di_progetto-$(cat piano_di_progetto.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) piano_di_progetto.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd LetteraDiPresentazione
pdflatex --jobname=lettera_di_presentazione lettera_presentazione.tex
pdflatex --jobname=lettera_di_presentazione lettera_presentazione.tex
mv *.pdf ../../consegne/RQ/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd SpecificaTecnica
pdflatex --jobname=specifica_tecnica-$(cat specifica_tecnica.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) specifica_tecnica.tex
pdflatex --jobname=specifica_tecnica-$(cat specifica_tecnica.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) specifica_tecnica.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd DefinizioneDiProdotto
pdflatex --jobname=definizione_di_prodotto-$(cat definizione_di_prodotto.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) definizione_di_prodotto.tex
pdflatex --jobname=definizione_di_prodotto-$(cat definizione_di_prodotto.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) definizione_di_prodotto.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd ManualeUtente
pdflatex --jobname=manuale_utente-$(cat manuale_utente.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) manuale_utente.tex
pdflatex --jobname=manuale_utente-$(cat manuale_utente.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) manuale_utente.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

cd LetteraDiPresentazione
pdflatex --jobname=lettera_presentazione-$(cat lettera_presentazione.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) lettera_presentazione.tex
pdflatex --jobname=lettera_presentazione-$(cat lettera_presentazione.tex | grep "\def" | grep \ultimaVersione | cut -d"{" -f2 | cut -d"}" -f1) lettera_presentazione.tex
mv *.pdf ../../consegne/RQ/esterni/
rm *.toc
rm *.lof
rm *.lot
rm *.out
rm *.aux
rm *.log
cd ..

#find . -name *.tex | while read fn; do
#	filename=$(basename $fn)
#	extension=${filename##*.}
#	filename=${filename%.*}
#	pdflatex --jobname=$(echo $filename)-$(echo $VER) $(echo $fn)
#	pdflatex --jobname=$(echo $filename)-$(echo $VER) $(echo $fn)
#done


